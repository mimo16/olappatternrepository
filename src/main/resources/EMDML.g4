grammar EMDML;

emdm_stmt
:
	(
		(
			c_stmt
			| d_stmt
			| g_stmt
			| i_stmt
			| x_stmt
			| s_stmt
			| f_stmt
		) ';'
	)+
;

/**** CREATE STATEMENTS ****/
c_stmt
:
	CREATE
	(
		OR REPLACE
	)?
	(
		cp_stmt
		| ct_stmt
		| ct_stmt
		| cm_stmt
		| cr_stmt
	)
;

cp_stmt
:
	PATTERN
	(
		p_def
		| p_descr
		| p_temp
	)
;

ct_stmt
:
	t_def
	|
	(
		TERM
		(
			t_descr
			| t_temp
		)
	)
;

cm_stmt
:
	cube_def
	| dim_def
;

cr_stmt
:
	r_exp
;

/****** DELETE STATEMENTS *******/
d_stmt
:
	DELETE
	(
		p_del
		| t_del
		| m_del
		| r_exp
	)
;

/* CHANGELOG - 2020-08-18:
 * - removed v_stmt
 */

/* CHANGELOG - 2020-08-18:
 * - removed t_exec
 */
/****** EXECUTE STATEMENTS *******/
x_stmt
:
	EXECUTE
	(
		p_exec
	)
;

/****** INSTANTIATE STATEMENTS *******/
/* CHANGELOG - 2020-08-18
 * - removed t_inst 
 */
i_stmt
:
	INSTANTIATE
	(
		p_inst
	)
;

/****** EVALUATE STATEMENTS  *******/
g_stmt
:
	GROUND
	(
		p_grnd
	)
;

/********** SHOW STATEMENTS *********/
s_stmt
:
	SHOW path_exp
;

/********** SEARCH STATEMENTS *********/
f_stmt
:
	SEARCH s_trgt
	(
		IN path_exp
	)? s_exp+
;

s_trgt
:
	REPOSITORY
	| CATALOGUE
	| GLOSSARY
	| MULTIDIMENSIONAL_MODEL
	| PATTERN
	| TERM
	| CUBE
	| DIMENSION
;

/* CHANGELOG - 20200522: 
 *  - changed to s_txt to s_txt+
 */
s_exp
:
	CONTAIN s_str IN
    (
        s_sct
        (
            ',' s_sct
        )*
    )+
;

s_sct
:
	NAME
	| LANGUAGE
	| ALIAS
	| PROBLEM
	| SOLUTION
	| EXAMPLE
	| RELATED
;

/********** REPOSITORY RELATED STATEMENTS *********/
/* CHANGELOG - 202020522:
 *  - changed s_name to path_exp (no context needed in command line)
 */
r_exp
:
	(
		REPOSITORY
		| CATALOGUE
		| GLOSSARY
		| MULTIDIMENSIONAL_MODEL
	) s_name
;

/********** PATTERN RELATED STATEMENTS **********/

/* CHANGELOG - 2020-09-15: 
 *  - changed FRAGMENTS to LOCAL CUBES
 *  - changed frag_decl to lc_decl
 */
p_def
:
	p_name WITH
	(
		PARAMETERS param_decl+ END PARAMETERS COL
	)?
	(
		DERIVED ELEMENTS derv_decl+ END DERIVED ELEMENTS COL
	)?
	(
		LOCAL CUBES lc_decl+ END LOCAL CUBES COL
	)?
	(
		CONSTRAINTS cstr_decl+ END CONSTRAINTS COL
	)? END PATTERN
;

/* CHANGELOG - 2020-08-17: 
 *  - removed d_name
 */
p_descr
:
	DESCRIPTION FOR p_name WITH
	(
		(
			LANGUAGE '=' lang_name
			| ALIAS '=' alias_name
			(
				',' alias_name
			)*
			| PROBLEM '=' prob_txt
			| SOLUTION '=' sol_txt
			| EXAMPLE '=' ex_txt
			| RELATED '=' p_loc_name
			(
				',' p_loc_name
			)*
		) COL
	)+ END PATTERN DESCRIPTION
;

/* CHANGELOG - 2020-08-17: 
 *  - removed temp_name
 */
p_temp
:
	TEMPLATE FOR p_name WITH temp_elem+ END PATTERN TEMPLATE
;

/* CHANGELOG - 2020-09-15: 
 *  - BINDING block as optional
 */
p_inst
:
	PATTERN p_name AS p_name WITH BINDINGS? binding_exp
	(
		',' binding_exp
	)* (END BINDINGS)?
	(
		FOR path_exp
	)?
;

/* CHANGELOG - 2020-08-20
 * - updated p_del rule
 */
p_del
:
	PATTERN
	(
		p_del_descr
		| p_del_temp
		| p_name
	)
;

p_del_descr
:
	DESCRIPTION FOR p_name
	(
		WITH LANGUAGE '=' lang_name
	)?
;
	
p_del_temp
:
	TEMPLATE FOR p_name
	(
		WITH
		(
			temp_elem_meta
		)+
		(
			',' temp_elem_meta
		)*
	)?
;	

p_grnd
:
	PATTERN p_name AS p_name FOR mdm_name
;

/* CHANGELOG - 20200522: 
 *  - added optional TEMPLATE keyword
 *  - added p_exec_all, p_exec_single, and p_ctx_expr
 */
p_exec
:
	PATTERN p_name FOR mdm_name USING voc_name
	(
		WITH TEMPLATE
		(
			temp_elem_meta
		)+
		(
			',' temp_elem_meta
		)*
	)?
;

/* CHANGELOG - 2020-08-18
 * - p_exec_all and p_exec_single removed 
 * - p_ctx_expr removed
 */

/********** TERM RELATED STATEMENTS **********/
/* CHANGELOG - 2020-08-17: 
 *  - added applies to 
 */
t_def
:
	t_type t_name WITH
	(
		PARAMETERS param_decl+ END PARAMETERS COL
	)?
	(
		CONSTRAINTS cstr_decl+ END CONSTRAINTS COL
	)?
	(
		RETURNS value_set_name COL
	)? END t_type
;

/* CHANGELOG - 2020-08-17:
 * removed d_name
 */
t_descr
:
	DESCRIPTION FOR t_name WITH
	(
		(
			LANGUAGE '=' lang_name
			| ALIAS '=' t_name
			(
				',' t_name
			)*
			| DESCRIPTION '=' descr_txt
		) COL
	)+ END TERM DESCRIPTION
;

/* CHANGELOG - 2020-08-17:
 *	- removed temp_name 
 */
t_temp
:
	TEMPLATE FOR t_name WITH temp_elem+ END TERM TEMPLATE
;

/* CHANGELOG - 2020-08-19
 * - updated t_del
 */
t_del
:
	TERM
	(
		t_del_descr 
		| t_del_temp
		| t_name
	)
;

t_del_descr
:
	DESCRIPTION FOR t_name
	(
		WITH LANGUAGE '=' lang_name
	)?
;

t_del_temp
:
	TEMPLATE FOR t_name
	(
		WITH
		(
			temp_elem_meta
		)+
		(
			',' temp_elem_meta
		)*
	)?
;

/* CHANGELOG - 2020-08-18: 
 *  - removed t_grnd
 */

/* CHANGELOG - 2020-08-18: 
 *  - removed t_grnd
 */

/* CHANGELOG - 20200522: 
 *  - added t_name and mdm_name as path exps
 */

/********** PATTERN AND TERM RELATED STATEMENTS **********/
/* CHANGELOG - 2020-08-17: 
 *  - removed return_decl 
 */
temp_elem
:
	(
		temp_elem_meta
		| EXPRESSION '=' temp_txt
	) COL
;

/* CHANGELOG - 2020-08-18
 * - added temp_elem_meta
 */
temp_elem_meta
:
	DATA_MODEL '=' model_name
	| VARIANT '=' variant_name
	| LANGUAGE '=' lang_name
	| DIALECT '=' dialect_name
;

param_decl
:
	var_decl COL
;

derv_decl
:
	var_decl
	(
		for_exp var_exp
	)? '<=' elem_exp '.'
	(
		elem_exp | RETURNS
	) COL
;

for_exp
:
	FOR
	(
		var_exp
		| '(' var_exp
		(
			',' var_exp
		)+ ')'
	) IN var_exp WITH
;

var_decl
:
	var_exp ':' type op_exp?
;

/* CONSTRAINT DECLARATIONS */
cstr_decl
:
	(
		sv_cstr_decl
		| mv_cstr_decl
	) COL
;

mv_cstr_decl
:
	for_exp sv_cstr_decl
	(
		',' sv_cstr_decl
	)*
;

/* CHANGELOG 2020-08-17:
 * - added return_cstr_decl
 */
sv_cstr_decl
:
	type_cstr_decl
	| dom_cstr_decl
	| prop_cstr_decl
	| return_cstr_decl
	| app_cstr_decl
	| scope_cstr_decl
	| rollup_cstr_decl
	| descr_cstr_decl
	| term_cstr_decl
;

type_cstr_decl
:
	elem_exp ':' type
;

dom_cstr_decl
:
	elem_exp '.' elem_exp ':' elem_exp
;

prop_cstr_decl
:
	elem_exp HAS m_prop_type elem_exp
;

return_cstr_decl
:
	elem_exp RETURNS elem_exp
;

app_cstr_decl
:
	elem_exp IS_APPLICABLE_TO
	(
		elem_exp
		| '(' elem_exp ',' elem_exp ')'
	)
;

scope_cstr_decl
:
	(
		var_exp WITH
	)? scope_exp
	(
		',' scope_exp
	)*
;

scope_exp
:
	var_exp IN var_exp
;

rollup_cstr_decl
:
	elem_exp '.' elem_exp ROLLS_UP_TO elem_exp '.' elem_exp
;

descr_cstr_decl
:
	elem_exp '.' elem_exp DESCRIBED_BY elem_exp '.' elem_exp
;

term_cstr_decl
:
	TERM elem_exp WITH
	(
		EXPECTED PARAMETERS param_decl+ END EXPECTED PARAMETERS COL
	)?
	(
		EXPECTED CONSTRAINTS
		(
			sv_cstr_decl COL
		)+ END EXPECTED CONSTRAINTS COL
	)? END TERM
;

/* FRAGMENT DECLARATIONS */
lc_decl
:
	(
		sv_lc_decl
		| mv_lc_decl
	) COL
;

mv_lc_decl
:
	for_exp sv_lc_decl
	(
		',' sv_lc_decl
	)*
;

sv_lc_decl
:
	type_lc_decl
	| dom_lc_decl
	| prop_lc_decl
;

/* CHANGELOG - 2020-08-17:
 * changed from var_exp to const_exp
 */
type_lc_decl
:
	const_exp ':' type
;

/* CHANGELOG - 2020-08-17:
 * dom_assrt_dyn_dom removed
 */
dom_lc_decl
:
	elem_exp '.' elem_exp ':'
	(
		elem_exp
	)
;

prop_lc_decl
:
	elem_exp HAS m_prop_type elem_exp
;

rollup_frag_decl
:
	elem_exp '.' elem_exp ROLLS_UP_TO elem_exp '.' elem_exp
;

descr_frag_decl
:
	elem_exp '.' elem_exp DESCRIBED_BY elem_exp '.' elem_exp
;

binding_exp
:
	var_exp '+'? '=' val_exp
;

val_exp
:
	sv_exp
	| mv_exp
;

sv_exp
:
	const_exp
	|
	(
		'(' elem_acc_exp
		(
			',' elem_acc_exp
		)+ ')'
	)
;

mv_exp
:
	'{' sv_exp
	(
		',' sv_exp
	)* '}'
;

/* CHANGELOG: 
 *  - added elem_acc_exp representing accesses or constants
 */
elem_acc_exp
:
	const_exp
	| var_acc_exp
;

/* MULTI-VALUED EXPRESSIONS */

/* CHANGELOG: 
 *  - added var_acc_exp representing access of collection variables
 *  - added array_acc_exp representing access of array variable
 *  - added map_acc_exp representing access of map variable
 *  - added tuple_acc representing column idx
 */

/* CHANGELOG: 2020-04-07
 * - changed '()' tuple_acc_exp to ( '()' tuple_acc_exp? )?
 * - changed (array_acc_exp	| map_simple_acc_exp) to optional ?
 */
var_acc_exp
:
	'<' var_label '>'
	(
		'()' tuple_acc_exp?
	)?
	(
		array_acc_exp
		| map_simple_acc_exp
	)?
;

array_acc_exp
:
	'[' idx_no ']'
;

/* CHANGELOG 2020-04-01
 * Added Tuple-Index-Value
 */

/* CHANGELOG 2020-04-03
 * changed from map_acc_exp to map_simple_acc_exp
 */
map_simple_acc_exp
:
	'*' '['
	(
		idx_name
		|
		(
			'(' idx_name
			(
				',' idx_name
			)+ ')'
		)
	) ']'
;

/* CHANGELOG 2020-04-07
 * prefixed dot for column access of a tuple
 */
tuple_acc_exp
:
	'.[' idx_no ']'
;

elem_exp
:
	const_exp
	| var_exp
;

/* CHANGELOG: 
 *  - changed array_exp to map_exp; <dim>[] --> <dim>*
 *  - changed set_exp to array_exp; <dimRole>* --> <dimRole>[]
 */
var_exp
:
	'<' var_label '>' tuple_exp?
	(
		map_exp
		| array_exp
	)?
;

/* CHANGELOG 2020-04-01
 * Added tuple_acc_exp? 
 */
tuple_exp
:
	(
		'()' tuple_acc_exp?
		| '('
		(
			idx_exp
			(
				',' idx_exp
			)+
		)? ')'
	)
;

array_exp
:
	(
		'[]'
		| array_acc_exp
	)
;

/* CHANGELOG 2020-04-03
 * Added map_acc_exp
 */
map_exp
:
	'*' map_acc_exp?
;

map_acc_exp
:
	'[' idx_exp ']'
;

idx_exp
:
	idx_name
	| idx_no
	| sv_exp
	| var_exp
;

const_exp
:
	const_name
;

op_exp
:
	IS_OPTIONAL
;

path_exp
:
	elem_name
	(
		'/' elem_name
	)*
;

/**********  MULTIDIMENSIONAL MODEL RELATED STATEMENTS **********/
cube_def
:
	CUBE cube_name WITH MEASURE PROPERTIES meas_decl+ END MEASURE PROPERTIES COL
	DIMENSION_ROLE PROPERTIES dim_role_decl+ END DIMENSION_ROLE PROPERTIES COL
	END CUBE
;

dim_def
:
	DIMENSION dim_name WITH
	(
		(
			LEVEL PROPERTIES lvl_decl+ END LEVEL PROPERTIES COL
		)
		|
		(
			ATTRIBUTE PROPERTIES attr_decl+ END ATTRIBUTE PROPERTIES COL
		)
		|
		(
			CONSTRAINTS
			(
				roll_up_rel_decl
				| descr_by_rel_decl
			)+ END CONSTRAINTS COL
		)
	)+ END DIMENSION
;

m_del
:
	(
		CUBE cube_name
		| DIMENSION dim_name
	)
;

meas_decl
:
	meas_name ':' val_set_name COL
;

dim_role_decl
:
	dim_role_name ':' dim_loc_name COL
;

lvl_decl
:
	lvl_name ':' val_set_name COL
;

attr_decl
:
	attr_name ':' val_set_name COL
;

roll_up_rel_decl
:
	lvl_name ROLLS_UP_TO lvl_name COL
;

descr_by_rel_decl
:
	lvl_name DESCRIBED_BY attr_name COL
;

/**********  AVAILABLE TYPES **********/
type
:
	m_entity_type
	| m_prop_type
	| t_type
	| v_type
	| a_type
;

/* CHANGELOG - 2020-08-17: 
 *  DERIVED_MEASURE to UNARY_DERIVED_MEASURE, BINARY_DERIVED_MEASURE
 *  CUBE_PREDICATE to UNARY_CUBE_PREDICATE, BINARY_CUBE_PREDICATE
 *  DIMENSION_PREDICATE to UNARY_DIMENSION_PREDICATE, BINARY_DIMENSION_PREDICATE
 *  CUBE_GROUPING removed
 */
t_type
:
	UNARY_CUBE_PREDICATE
	| BINARY_CUBE_PREDICATE
	| UNARY_CALCULATED_MEASURE
	| BINARY_CALCULATED_MEASURE
	| CUBE_ORDERING
	| UNARY_DIMENSION_PREDICATE
	| BINARY_DIMENSION_PREDICATE
	| DIMENSION_GROUPING
	| DIMENSION_ORDERING
;

m_entity_type
:
	CUBE
	| DIMENSION
;

m_prop_type
:
	MEASURE
	| DIMENSION_ROLE
	| CUBE_PROPERTY
	| LEVEL
	| ATTRIBUTE
	| DIMENSION_PROPERTY
;

/* CHANGELOG - 2020-08-17: 
 *  VALUE_SET to NUMBER_VALUE_SET, STRING_VALUE_SET
 */
v_type
:
	NUMBER_VALUE_SET
	| STRING_VALUE_SET
	| val_set_name
;

a_type
:
	BINARY_TUPLE
	| TERNARY_TUPLE
	| QUARTERNARY_TUPLE
;

descr_txt
:
	STRING
;

temp_txt
:
	STRING
;

prob_txt
:
	STRING
;

sol_txt
:
	STRING
;

ex_txt
:
	STRING
;

var_label
:
	NAME
;

alias_name
:
	STRING
;

model_name
:
	STRING
;

variant_name
:
	STRING
;

lang_name
:
	STRING
;

dialect_name
:
	STRING
;

const_name
:
	STRING
;

meas_name
:
	STRING
;

dim_role_name
:
	STRING
;

lvl_name
:
	STRING
;

attr_name
:
	STRING
;

val_set_name
:
	STRING
;

idx_name
:
	STRING
;

elem_name
:
	STRING
;

value_set_name
:
	STRING
;

idx_no
:
	NUMBER
;

s_str
:
	STRING
;

p_loc_name
:
	STRING
;

dim_loc_name
:
	STRING
;

/* CHANGELOG - 202020522:
 *  - changed p_name to path_exp (no context needed in command line)
 *  - changed d_name to path_exp (no context needed in command line)
 *  - changed temp_name to path_exp (no context needed in command line)
 *  - changed t_name to path_exp (no context needed in command line)
 *  - changed cube_name to path_exp (no context needed in command line)
 *  - changed dim_name to path_exp (no context needed in command line) 
 *  - changed m_name to mdm_name with path_exp (no context needed in command line) 
 *  - changed voc_name to path_exp (no context needed in command line) 
 */
p_name
:
	path_exp
;

t_name
:
	path_exp
;

cube_name
:
	path_exp
;

dim_name
:
	path_exp
;

mdm_name
:
	path_exp
;

voc_name
:
	path_exp
;

s_name
:
	path_exp
;

/********** FRAGMENTS AND TOKENS **********/

/* FRAGMENTS */
fragment
US
:
	'_'
;

fragment
A
:
	[aA]
;

fragment
B
:
	[bB]
;

fragment
C
:
	[cC]
;

fragment
D
:
	[dD]
;

fragment
E
:
	[eE]
;

fragment
F
:
	[fF]
;

fragment
G
:
	[gG]
;

fragment
H
:
	[hH]
;

fragment
I
:
	[iI]
;

fragment
J
:
	[jJ]
;

fragment
K
:
	[kK]
;

fragment
L
:
	[lL]
;

fragment
M
:
	[mM]
;

fragment
N
:
	[nN]
;

fragment
O
:
	[oO]
;

fragment
P
:
	[pP]
;

fragment
Q
:
	[qQ]
;

fragment
R
:
	[rR]
;

fragment
S
:
	[sS]
;

fragment
T
:
	[tT]
;

fragment
U
:
	[uU]
;

fragment
V
:
	[vV]
;

fragment
W
:
	[wW]
;

fragment
X
:
	[xX]
;

fragment
Y
:
	[yY]
;

fragment
Z
:
	[zZ]
;

/* TOKENS (KEY WORDS) */

/* CHANGELOG - 2020-09-15: 
 *  - changed UNARY_DERIVED_MEASURE to UNARY_CALCULATED_MEASURE
 *  - changed BINARY_DERIVED_MEASURE to BINARY_CALCULATED_MEASURE
 */
ALIAS
:
	A L I A S
;

AS
:
	A S
;

APPLIES
:
	A P P L I E S
;

ATTRIBUTE
:
	A T T R I B U T E
;

BINARY_TUPLE
:
	B I N A R Y US T U P L E
;

BINARY_CUBE_PREDICATE
:
	B I N A R Y US C U B E US P R E D I C A T E
;

BINARY_CALCULATED_MEASURE
:
	B I N A R Y US C A L C U L A T E D US M E A S U R E
;

BINARY_DIMENSION_PREDICATE
:
	B I N A R Y US D I M E N S I O N US P R E D I C A T E
;

BINDINGS
:
	B I N D I N G S
;

CATALOGUE
:
	C A T A L O G U E
;

CONSTRAINTS
:
	C O N S T R A I N T S
;

CONTAIN
:
	C O N T A I N
;

CONTEXT
:
	C O N T E X T
;

CREATE
:
	C R E A T E
;

CUBE
:
	C U B E
;

CUBES
:
	C U B E S
;

CUBE_ORDERING
:
	C U B E US O R D E R I N G
;

CUBE_PROPERTY
:
	C U B E US P R O P E R T Y
;

DATA_MODEL
:
	D A T A US M O D E L
;

DELETE
:
	D E L E T E
;

DERIVED
:
	D E R I V E D
;

DESCRIBED_BY
:
	D E S C R I B E D US B Y
;

DESCRIPTION
:
	D E S C R I P T I O N
;

DIALECT
:
	D I A L E C T
;

DIMENSION
:
	D I M E N S I O N
;

DIMENSION_GROUPING
:
	D I M E N S I O N US G R O U P I N G
;

DIMENSION_ORDERING
:
	D I M E N S I O N US O R D E R I N G
;

DIMENSION_PROPERTY
:
	D I M E N S I O N US P R O P E R T Y
;

DIMENSION_ROLE
:
	D I M E N S I O N US R O L E
;

DOMAIN
:
	D O M A I N
;

ELEMENTS
:
	E L E M E N T S
;

END
:
	E N D
;

EXAMPLE
:
	E X A M P L E
;

EXECUTE
:
	E X E C U T E
;

EXPECTED
:
	E X P E C T E D
;

EXPRESSION
:
	E X P R E S S I O N
;

FOR
:
	F O R
;

FROM
:
	F R O M
;

GROUND
:
	G R O U N D
;

HAS
:
	H A S
;

IN
:
	I N
;

INSTANCE
:
	I N S T A N C E
;

INSTANTIATE
:
	I N S T A N T I A T E
;

IS_APPLICABLE_TO
:
	I S US A P P L I C A B L E US T O
;

IS_APPLIED_TO
:
	I S US A P P L I E D US T O
;

IS_OPTIONAL
:
	I S US O P T I O N A L
;

LANGUAGE
:
	L A N G U A G E
;

LEVEL
:
	L E V E L
;

LOCAL
:
	L O C A L
;

MEASURE
:
	M E A S U R E
;

MULTIDIMENSIONAL_MODEL
:
	M U L T I D I M E N S I O N A L US M O D E L
;

NUMBER_VALUE_SET
:
	N U M B E R US V A L U E US S E T
;

OR
:
	O R
;

OWNER
:
	O W N E R
;

PARAMETERS
:
	P A R A M E T E R S
;

PATTERN
:
	P A T T E R N
;

PATTERN_ALIAS
:
	P A T T E R N US A L I A S
;

PROBLEM
:
	P R O B L E M
;

PROPERTIES
:
	P R O P E R T I E S
;

RELATED
:
	R E L A T E D
;

REPOSITORY
:
	R E P O S I T O R Y
;

REPLACE
:
	R E P L A C E
;

RETURNS
:
	R E T U R N S
;

ROLLS_UP_TO
:
	R O L L S US U P US T O
;

SEARCH
:
	S E A R C H
;

SHOW
:
	S H O W
;

SOLUTION
:
	S O L U T I O N
;

STRING_VALUE_SET
:
	S T R I N G US V A L U E US S E T
;

TEMPLATE
:
	T E M P L A T E
;

TERM
:
	T E R M
;

TERNARY_TUPLE
:
	T E R N A R Y US T U P L E
;

TO
:
	T O
;

UNARY_CUBE_PREDICATE
:
	U N A R Y US C U B E US P R E D I C A T E
;

UNARY_CALCULATED_MEASURE
:
	U N A R Y US C A L C U L A T E D US M E A S U R E
;

UNARY_DIMENSION_PREDICATE
:
	U N A R Y US D I M E N S I O N US P R E D I C A T E
;

USING
:
	U S I N G
;

VALIDATE
:
	V A L I D A T E
;

VARIABLES
:
	V A R I A B L E S
;

VARIANT
:
	V A R I A N T
;

GLOSSARY
:
	G L O S S A R Y
;

WITH
:
	W I T H
;

QUARTERNARY_TUPLE
:
	Q U A R T E R N A R Y US T U P L E
;

// LEXER RULES START WITH UPPERCASE

STRING
:
	'"'
	(
		~["]
		| '""'
	)* '"'
;

NAME
:
	[_a-zA-Z] [_a-zA-Z0-9]*
;

NUMBER
:
	[1-9] [0-9]*
;

SPACE
:
	[ ] -> channel (HIDDEN)
;

WS
:
	[\r\n\t]+ -> skip
;

COMMENT
:
	'/*' .*? '*/' -> skip
;

LINE_COMMENT
:
	'--' .*?
	(
		'\r'
		| '\n'
	) -> skip
;

COL
:
	';'
;
