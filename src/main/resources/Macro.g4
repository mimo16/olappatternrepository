grammar Macro;

// PROBLEM: https://stackoverflow.com/questions/29777778/antlr-4-5-mismatched-input-x-expecting-x
// https://medium.com/@fwouts/a-quick-intro-to-antlr4-5f4f35719823
/* 
*{ SELECT * FROM }* $dimKey("Animal")

*{ SELECT * FROM }* $dimKey(Animal)

*{ SELECT col FROM }* $dimKey(Animal, cc)

*{ SELECT "col" FROM }* $dimKey("Animal", icc) { 

	*{ SELECT * FROM }* $dimKey("Animal")
	
	*{ SELECT * FROM }* $dimKey(Animal)
}
 */
macrol
:
	(
		macro_call
		| txt
	)*
;

txt
:
	QUERY_TEXT
;

macro_call
:
	macro_name '(' macro_call_arguments ')'
	(
		'{' macro_body '}'
	)?
;

macro_name
:
	DIMKEY
	| EXPR
;

macro_body
:
	macrol
;

macro_call_arguments
:
	| macro_call_argument
	(
		',' macro_call_argument
	)*
;

macro_call_argument
:
	QUOTED_NAME
	| UNQUOTED_NAME
;

/********** FRAGMENTS AND KEYWORDS **********/
localCube
DOL
:
	'$'
;

localCube
A
:
	[aA]
;

localCube
B
:
	[bB]
;

localCube
C
:
	[cC]
;

localCube
D
:
	[dD]
;

localCube
E
:
	[eE]
;

localCube
F
:
	[fF]
;

localCube
G
:
	[gG]
;

localCube
H
:
	[hH]
;

localCube
I
:
	[iI]
;

localCube
J
:
	[jJ]
;

localCube
K
:
	[kK]
;

localCube
L
:
	[lL]
;

localCube
M
:
	[mM]
;

localCube
N
:
	[nN]
;

localCube
O
:
	[oO]
;

localCube
P
:
	[pP]
;

localCube
Q
:
	[qQ]
;

localCube
R
:
	[rR]
;

localCube
S
:
	[sS]
;

localCube
T
:
	[tT]
;

localCube
U
:
	[uU]
;

localCube
V
:
	[vV]
;

localCube
W
:
	[wW]
;

localCube
X
:
	[xX]
;

localCube
Y
:
	[yY]
;

localCube
Z
:
	[zZ]
;

// LEXER RULES START WITH UPPERCASE

EXPR
:
	DOL E X P R
;

DIMKEY
:
	DOL D I M K E Y
;

QUOTED_NAME
:
	'"' NAME '"'
;

UNQUOTED_NAME
:
	NAME
;

QS
:
	'*{'
;

QE
:
	'}*'
;

NAME
:
	[_a-zA-Z] [_a-zA-Z0-9 ]*
;

QUERY_TEXT
:
	QS ~[$]* QE
;

SPACE
:
	[ ] -> channel (HIDDEN)
;

WS
:
	[\r\n\t]+ -> skip
;
