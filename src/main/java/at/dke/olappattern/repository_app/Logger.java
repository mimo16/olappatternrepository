package at.dke.olappattern.repository_app;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Writer;

public class Logger {
    public static void log(String s){
        try {
            Writer output;
            output = new BufferedWriter(new FileWriter("C://Temp/log.txt", true));  //clears file every time
            output.append(s);
            output.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
