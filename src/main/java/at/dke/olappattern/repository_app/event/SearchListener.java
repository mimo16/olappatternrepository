package at.dke.olappattern.repository_app.event;

public interface SearchListener {
    /**
     * Method to process OLAP pattern language statements regarding search operations
     * @param event representation of the language statement
     */
    void processSearchStatement(SearchEvent event);
}
