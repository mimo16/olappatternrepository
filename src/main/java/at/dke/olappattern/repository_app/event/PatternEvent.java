package at.dke.olappattern.repository_app.event;

import at.dke.olappattern.data.pattern.Pattern;

import java.util.EventObject;

public class PatternEvent extends EventObject {
    private Pattern pattern = null;
    private String path = null;
    public enum Action { CREATE, INSTANTIATE, DELETE, EXECUTE }
    private Action action;

    public PatternEvent(Object source){
        super(source);
    }

    public PatternEvent(Object source, Pattern pattern, Action action, String path) {
        super(source);
        this.pattern = pattern;
        this.action = action;
        this.path = path;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
