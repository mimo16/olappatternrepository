package at.dke.olappattern.repository_app.event;

import at.dke.olappattern.data.repository.OrganizationElement;

import java.util.EventObject;

public class OrganizationElementEvent extends EventObject {
    private OrganizationElement organizationElement = null;
    String path = null;
    public enum Action { CREATE, DELETE }
    private Action action;

    public OrganizationElementEvent(Object source) {
        super(source);
    }

    public OrganizationElementEvent(Object source, OrganizationElement organizationElement, Action action,
                                    String path){
        super(source);
        this.organizationElement = organizationElement;
        this.action = action;
        this.path = path;
    }

    public OrganizationElement getRepositoryHierarchyElement() {
        return organizationElement;
    }

    public void setRepository(OrganizationElement organizationElement) {
        this.organizationElement = organizationElement;
    }

    public void setRepositoryHierarchyElement(OrganizationElement organizationElement) {
        this.organizationElement = organizationElement;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
