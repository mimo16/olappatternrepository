package at.dke.olappattern.repository_app.event;

import java.util.*;
import java.util.stream.Collectors;

public class SearchEvent extends EventObject {
    //private SearchObject searchObject = null;
    private String searchTarget;
    private String searchPath;
    //Contains all the search strings as keys and the parameter-list to search in, as values
    private Map<String, List<String>> searchStrings = new HashMap();

    public SearchEvent(Object source) {
        super(source);
    }

    /*public SearchEvent(Object source, SearchObject searchObject){
        super(source);
        this.searchObject = searchObject;
    }

    public SearchObject getSearchObject() {
        return searchObject;
    }

    public void setSearchObject(SearchObject searchObject) {
        this.searchObject = searchObject;
    }*/

    public void addSearchString(String searchString){
        this.searchStrings.put(searchString, new LinkedList<>());
    }

    public void addParameterForSearchString(String searchString, String parameter){
        List<String> temp = this.searchStrings.get(searchString);
        temp.add(parameter);
        this.searchStrings.replace(searchString, temp);
    }

    public List<String> getSearchStrings(){
        return this.searchStrings.keySet().stream().collect(Collectors.toList());
    }

    public List<String> getParametersForSearchString(String searchString){
        return this.searchStrings.get(searchString);
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchPath() {
        return searchPath;
    }

    public void setSearchPath(String searchPath) {
        this.searchPath = searchPath;
    }

    public void setSearchStrings(Map<String, List<String>> searchStrings) {
        this.searchStrings = searchStrings;
    }
}
