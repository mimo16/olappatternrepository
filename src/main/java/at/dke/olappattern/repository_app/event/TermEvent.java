package at.dke.olappattern.repository_app.event;

import at.dke.olappattern.data.term.Term;

import java.util.EventObject;

public class TermEvent extends EventObject {
    private Term term = null;
    private String path = null;
    public enum Action { CREATE, DELETE }
    private Action action;

    public TermEvent(Object source) {
        super(source);
    }

    public TermEvent(Object source, Term term, Action action, String path){
        super(source);
        this.term = term;
        this.action = action;
        this.path = path;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
