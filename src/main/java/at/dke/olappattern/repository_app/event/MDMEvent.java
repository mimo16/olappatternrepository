package at.dke.olappattern.repository_app.event;

import at.dke.olappattern.data.MDM.MDMEntity;

import java.util.EventObject;

public class MDMEvent extends EventObject {
    private MDMEntity entity;
    private String path;
    public enum Action {CREATE, DELETE}
    private Action action;

    public MDMEvent(Object source) {
        super(source);
    }

    public MDMEvent(Object source, MDMEntity entity, Action action, String path){
        super(source);
        this.entity = entity;
        this.action = action;
        this.path = path;
    }

    public MDMEntity getEntity() {
        return entity;
    }

    public void setEntity(MDMEntity entity) {
        this.entity = entity;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
