package at.dke.olappattern.repository_app.event;

public interface PatternListener {
    /**
     * Method to process OLAP pattern language (create and delete) statements regarding patterns
     * @param event representation of the language statement
     */
    void processPattern(PatternEvent event);

    /**
     * Method to process OLAP pattern language statements regarding pattern instantiations
     * @param event representation of the pattern (containing variables and their values)
     * @param patternToInstantiate the path of the pattern to be instantiated
     */
    void processPatternInstantiation(String patternToInstantiate, PatternEvent event);

    /**
     * Method to process OLAP pattern language statements regarding pattern executions
     * @param event representation of the pattern to be executed
     * @param mdm MDM defining the context
     * @param glossary Glossary defining the context
     */
    void processPatternExecution(String mdm, String glossary, PatternEvent event);
}
