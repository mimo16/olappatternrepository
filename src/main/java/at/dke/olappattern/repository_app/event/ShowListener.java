package at.dke.olappattern.repository_app.event;

public interface ShowListener {
    /**
     * Method to process OLAP pattern language statements regarding show operations
     * @param event representation of the language statement
     */
    public void processShowStatement(ShowEvent event);
}
