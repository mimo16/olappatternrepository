package at.dke.olappattern.repository_app.event;

public interface MDMListener {
    /**
     * Method to process OLAP pattern language statements regarding MDMElements
     * @param event representation of the language statement
     */
    public void processMDMElement(MDMEvent event);
}
