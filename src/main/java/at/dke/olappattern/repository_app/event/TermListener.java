package at.dke.olappattern.repository_app.event;

public interface TermListener {
    /**
     * Method to process OLAP pattern language statements regarding terms
     * @param event representation of the language statement
     */
    void processTerm(TermEvent event);
}
