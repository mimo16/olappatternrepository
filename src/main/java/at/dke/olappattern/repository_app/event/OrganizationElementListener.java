package at.dke.olappattern.repository_app.event;

public interface OrganizationElementListener {
    /**
     * Method to process OLAP pattern language statements regarding OrganizationElements
     * @param event representation of the language statement
     */
    void processOrganizationElement(OrganizationElementEvent event);
}
