package at.dke.olappattern.repository_app.event;

import java.util.EventObject;

public class ShowEvent extends EventObject {
    private String path;

    public ShowEvent(Object source) {
        super(source);
    }

    public ShowEvent(Object source, String path){
        super(source);
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
