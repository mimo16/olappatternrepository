package at.dke.olappattern.repository_app;

public class Initializer {
    public static LanguageProcessingUnit languageProcessingUnit = new LanguageProcessor();
    public static StorageUnit storageUnit = new StorageComponent();
    public static ValidatorUnit validatorUnit = new Validator();
    public static PreprocessorUnit preprocessorUnit = new TemplatePreprocessor(storageUnit, validatorUnit);
    public static Controller controller = new Controller(storageUnit, preprocessorUnit);

    public Initializer(){
        languageProcessingUnit.addMDMListener(controller);
        languageProcessingUnit.addPatternListener(controller);
        languageProcessingUnit.addTermListener(controller);
        languageProcessingUnit.addSearchListener(controller);
        languageProcessingUnit.addOrganizationElementListener(controller);
        languageProcessingUnit.addShowListener(controller);
    }

    /*public void processCommand(String command, AsyncResponse response){
        controller.setResponse(response);
        try{
            languageProcessingUnit.process(command);
            Logger.log(command + "\r\n");
        }
        catch (Exception e){
            controller.respond(e.getMessage());
        }
    }*/
}
