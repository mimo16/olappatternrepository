package at.dke.olappattern.repository_app;

/**
 * An interface describing a Component that is able to resolve $exec and $dimKey macros for pattern executions
 */
public interface MacroResolver {
    /**
     * A method to resolve the $exec macro, i.e. to execute a specific term template with a given array of arguments
     * @param termName the name of the term to be executed
     * @param language the language the template must have
     * @param dialect the language dialect the template must have
     * @param arguments the array of arguments
     * @return The query snippet representing the executed term
     */
    public String expr(String termName, String language, String dialect, String[] arguments);

    /**
     * A method to resolve the dimKey parameter returning the base level of a given Dimension
     * @param dimName the name of the Dimension
     * @return the name of the dimension's base level
     */
    public String dimKey(String dimName);
}
