package at.dke.olappattern.repository_app;

import at.dke.olappattern.repository_app.event.*;

public interface LanguageProcessingUnit {
    void process(String s);
    void addMDMListener(MDMListener l);
    void addPatternListener(PatternListener l);
    void addTermListener(TermListener l);
    void addSearchListener(SearchListener l);
    void addOrganizationElementListener(OrganizationElementListener l);
    void addShowListener(ShowListener l);
}
