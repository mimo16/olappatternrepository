package at.dke.olappattern.repository_app;

import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

import static at.dke.olappattern.repository_app.Initializer.controller;

// The Java class will be hosted at the URI path "/repository"
@Path("/repository")
public class IntefaceProvider {
    public static final String INPUT_STATEMENT = "src/main/resources/test.txt";
    static Initializer initializer = new Initializer();

    public static void main(String[] args) throws IOException {
        try {
            String command = new String(Files.readAllBytes(Paths.get(INPUT_STATEMENT)));
            Initializer.languageProcessingUnit.process(command);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void executeCommand(final JsonObject operation, @Suspended final AsyncResponse response) throws SQLException, ClassNotFoundException {
        String command = operation.getString("command");

            controller.setResponse(response);
            try {
                Initializer.languageProcessingUnit.process(command);
                Logger.log(command + "\r\n");
            } catch (Exception e) {
                Initializer.controller.respond(e.getMessage());
            }
    }

    @OPTIONS
    public Response options() {
        return Response.ok("")
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "POST")
                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
}
