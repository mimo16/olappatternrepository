package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.MDM.MDM;
import at.dke.olappattern.data.pattern.Pattern;
import at.dke.olappattern.data.repository.Glossary;

public interface ValidatorUnit {
    /**
     * Method to evaluate whether a pattern can be executed in the context of a specific eMDM
     * @param pattern   pattern to be executed
     * @param mdm       multidimensional data model
     * @param glossary  business term by which the mdm is enriched
     * @return          true if executable, false if not
     */
    boolean validate(Pattern pattern, MDM mdm, Glossary glossary);

    Pattern ground(Pattern pattern, MDM mdm, Glossary glossary);
}
