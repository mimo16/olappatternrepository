package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.MDM.*;
import at.dke.olappattern.data.constraint.*;
import at.dke.olappattern.data.localcube.*;
import at.dke.olappattern.data.variable.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import at.dke.olappattern.data.Context;
import at.dke.olappattern.data.Target;
import at.dke.olappattern.data.Template;
import at.dke.olappattern.data.pattern.Pattern;
import at.dke.olappattern.data.pattern.PatternDescription;
import at.dke.olappattern.data.repository.Catalogue;
import at.dke.olappattern.data.repository.Glossary;
import at.dke.olappattern.data.repository.Repository;
import at.dke.olappattern.data.repository.OrganizationElement;
import at.dke.olappattern.data.term.Term;
import at.dke.olappattern.data.term.TermDescription;
import at.dke.olappattern.data.term.Type;
import at.dke.olappattern.data.value.TupleEntry;
import at.dke.olappattern.data.value.TupleValue;
import at.dke.olappattern.repository_app.event.*;
import org.hibernate.HibernateException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;
import java.util.List;

public class Controller implements TermListener, PatternListener, MDMListener, SearchListener, OrganizationElementListener,
        ShowListener {
    private StorageUnit storageUnit = null;
    private PreprocessorUnit preprocessorUnit = null;
    private AsyncResponse response = null;
    //private at.dke.olap_pattern_repo.ValidatorUnit validatorUnit = null;

    public Controller(StorageUnit storageUnit, PreprocessorUnit preprocessorUnit){
        this.storageUnit = storageUnit;
        this.preprocessorUnit = preprocessorUnit;
        //this.validatorUnit = validatorUnit;
        response = null;
    }

    public void setResponse(AsyncResponse response) {
        this.response = response;
    }

    @Override
    public void processMDMElement(MDMEvent event) {
        if(event.getPath() == null){
            respond("Missing Path!");
            return;
        }
        String[] pathSplits = event.getPath().split("/");
        MDMEntity entity = event.getEntity();
        try {
            storageUnit.openSession();
            MDM mdm = storageUnit.getMDM(pathSplits[0], pathSplits[1]);
            if(mdm == null){
                respond("Could not find path");
            }
            switch (event.getAction()) {
                case CREATE:
                    if (entity instanceof Cube){
                        Cube c = (Cube)entity;

                        for(DimensionRole dr : c.getDimensionRoles()){
                            Dimension d = mdm.getDimensionByName(dr.getDimension().getName());
                            if(d != null) {
                                dr.setDimension(d);
                            }
                            else{
                                throw new IllegalStateException("Dimension " + dr.getDimension().getName() +
                                        " is not defined for " + mdm.getName());
                            }
                        }
                        Cube tempCube = mdm.getCubeByName(c.getName());
                        if(tempCube != null){
                            mdm.getCubes().remove(tempCube);
                            storageUnit.deleteCube(tempCube);
                        }
                        for(Measure m : c.getMeasures()){
                            m.setDomain(mdm.addValueSetIfNotExists(m.getDomain()));
                        }
                        mdm.addCube(c);
                        storageUnit.persistMDM(mdm);
                        respond("Cube created!");
                    }
                    else if(entity instanceof Dimension){
                        Dimension d = (Dimension) entity;

                        Dimension tempDimension = mdm.getDimensionByName(d.getName());
                        if(tempDimension != null){
                            mdm.getCubes().remove(tempDimension);
                            storageUnit.deleteDimension(tempDimension);
                        }
                        else{
                            for(Level level : d.getLevels()){
                                level.setDomain(mdm.addValueSetIfNotExists(level.getDomain()));
                            }
                            for(Attribute attribute : d.getAttributes()){
                                attribute.setDomain(mdm.addValueSetIfNotExists(attribute.getDomain()));
                            }
                            mdm.addDimension(d);
                        }
                        storageUnit.persistMDM(mdm);
                        respond("Dimension created!");
                    }
                    break;
                case DELETE:
                    if(entity instanceof Cube){
                        storageUnit.deleteCube(pathSplits[0], pathSplits[1], entity.getName());
                        respond("Cube deleted!");
                    }
                    else if(entity instanceof Dimension){
                        storageUnit.deleteDimension(pathSplits[0], pathSplits[1], entity.getName());
                        respond("Dimension deleted!");
                    }
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            respond("Error in path");
        }
        catch(Exception e){
            respond(e.getMessage());
        }
        finally {
            storageUnit.closeSession();
        }
    }

    @Override
    public void processPattern(PatternEvent event) {
        Logger.log("Pattern parsed called \r\n");
        Pattern pattern = event.getPattern();
        if(event.getPath() == null){
            respond("Missing Path!");
            return;
        }
        if(pattern.getName() == null){
            respond("Error in Path!");
            return;
        }
        String[] pathSplits = event.getPath().split("/");
        Pattern temp = null;
        try {
            storageUnit.openSession();
            Catalogue catalogue = null;

                    switch (event.getAction()) {
                case CREATE:
                    if(pattern.getDescriptions().isEmpty() && pattern.getTemplates().isEmpty()) {
                        catalogue = storageUnit.getCatalogue(pathSplits[0], pathSplits[1]);
                        if (catalogue == null) {
                            respond("Could not find the given path");
                        }

                        Type t = storageUnit.getType("Pattern");
                        if (t != null) pattern.setType(t);
                        else pattern.setType(new Type("Pattern"));

                        temp = catalogue.getPatternByName(pattern.getName());

                        if (temp != null) {
                            catalogue.removePattern(temp);
                            storageUnit.deletePattern(temp);
                            catalogue.addPattern(pattern);
                            storageUnit.persistCatalogue(catalogue);
                        } else {
                            catalogue.addPattern(pattern);
                        }
                        Logger.log("Catalogue " + catalogue.getName() + " (Patterns: " + catalogue.getPatterns().size() + ")\r\n");
                        storageUnit.persistCatalogue(catalogue);

                        respond("Pattern created!");
                    }
                    else if (!pattern.getDescriptions().isEmpty()){
                        temp = storageUnit.getPattern(pathSplits[0], pathSplits[1], pattern.getName());
                        if(temp != null) {
                            for (PatternDescription pd : pattern.getDescriptions()) {
                                storageUnit.deletePatternDescriptions(pathSplits[0], pathSplits[1], pattern.getName(), pd);
                                temp.addPatternDescription(pd);
                            }
                            Logger.log("Pattern " + temp.getName() + "(Descriptions: "+ temp.getDescriptions().size() + ")\r\n");
                            storageUnit.persistPattern(temp);
                            respond("Pattern description created!");
                        }
                        else{
                            respond("Could not find " + pattern.getName() + " in " + pathSplits[0] + "/" +
                                    pathSplits[1]);
                        }
                    }
                    else if (!pattern.getTemplates().isEmpty()){
                        temp = storageUnit.getPattern(pathSplits[0], pathSplits[1], pattern.getName());
                        if(temp != null) {
                            for (Template pt : pattern.getTemplates()) {
                                storageUnit.deletePatternTemplates(pathSplits[0], pathSplits[1], pattern.getName(), pt);
                                temp.addTemplate(pt);
                            }
                            storageUnit.persistPattern(temp);
                            respond("Pattern template created!");
                        }
                        else{
                            respond("Could not find " + pattern.getName() + " in " + pathSplits[0] + "/" +
                                    pathSplits[1]);
                        }
                    }
                    break;
                case DELETE:
                    if (pattern.getDescriptions().isEmpty() && pattern.getTemplates().isEmpty()) {
                        catalogue = storageUnit.getCatalogue(pathSplits[0], pathSplits[1]);
                        if(catalogue == null){
                            respond("Could not find the given path");
                        }
                        temp = catalogue.getPatternByName(pattern.getName());
                        catalogue.getPatterns().remove(temp);
                        //this.repositoryComponent.deletePattern(pathSplits[0], pathSplits[1], pattern.getName());
                        this.storageUnit.deletePattern(temp);
                        respond("Pattern deleted!");
                    }
                    else if(!pattern.getDescriptions().isEmpty()){
                        for (PatternDescription pd : pattern.getDescriptions()) {
                            storageUnit.deletePatternDescriptions(pathSplits[0], pathSplits[1],
                                    pattern.getName(), pd);
                            respond("Pattern description deleted!");
                        }
                    }
                    else if(!pattern.getTemplates().isEmpty()){
                        for(Template t : pattern.getTemplates()){
                            storageUnit.deletePatternTemplates(pathSplits[0], pathSplits[1],
                                    pattern.getName(), t);
                            respond("Pattern template deleted!");
                        }
                    }
                    break;
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            respond("Error in path");
        }
        catch (Exception e){
            respond(e.getMessage());
        }
        finally {
            storageUnit.closeSession();
        }
    }

    @Override
    public void processPatternInstantiation(String patternToInstantiate, PatternEvent event) {
        if(event.getPath() == null){
            respond("Missing Path!");
            return;
        }
        String[] pathSplits = event.getPath().split("/");

        Pattern instantiation = event.getPattern();
        Pattern toInstantiate = null;

        try {
            storageUnit.openSession();
            Catalogue c = storageUnit.getCatalogue(pathSplits[0], pathSplits[1]);
            if (c != null) {
                String[] toInstantiateSplits = patternToInstantiate.split("/");
                toInstantiate = storageUnit.getPattern(toInstantiateSplits[0], toInstantiateSplits[1], toInstantiateSplits[2]);
                if (toInstantiate != null) {
                    Type type = toInstantiate.getType();
                    instantiation.setType(type);
                    this.instantiateContext(toInstantiate, instantiation);

                    for(Template t : toInstantiate.getTemplates()){
                        instantiation.addTemplate(t.copy());
                    }
                    c.addPattern(instantiation);
                    storageUnit.persistCatalogue(c);
                    storageUnit.closeSession();
                    respond("Pattern instantiated!");
                } else {
                    respond("No such Pattern found in the database");
                }
            } else {
                respond("Path not found");
            }
        }
        catch (Exception e){
            respond(e.getMessage());
        }
        finally {
            storageUnit.closeSession();
        }
    }

    @Override
    public void processPatternExecution(String mdm, String glossary, PatternEvent event) {
        try{
            respond(preprocessorUnit.executePattern(mdm, glossary, event.getPattern(), event.getPath()));
        }
        catch (Exception e){
            respond(e.getMessage());
        }
    }

    @Override
    public void processTerm(TermEvent event) {
        if(event.getPath() == null){
            respond("Missing path!");
            return;
        }
        String[] pathSplits = event.getPath().split("/");

        Term term = event.getTerm();
        Term temp = null;
        try {
            this.storageUnit.openSession();
            Glossary glossary = this.storageUnit.getGlossary(pathSplits[0], pathSplits[1]);
            if(glossary == null){
                respond("Could not find the given path");
            }
            else {
                switch (event.getAction()) {
                    case CREATE:
                        //System.out.println(term);
                        if(term.getDescriptions().isEmpty() && term.getTemplates().isEmpty()) {
                            temp = glossary.getTermByName(term.getName());
                            Type type = storageUnit.getType(term.getType().getName());
                            if (type != null) term.setType(type);

                            if (temp != null) {
                                glossary.removeTerm(temp);
                                storageUnit.deleteTerm(temp);
                            }
                            glossary.addTerm(term);
                            storageUnit.persistGlossary(glossary);
                            respond("Business term " + term.getName() + " created in vocabulary " + glossary.getName() + "!");
                        }
                        else if(!term.getDescriptions().isEmpty()){
                            temp = storageUnit.getTerm(pathSplits[0], pathSplits[1], term.getName());
                            if (temp != null) {
                                for (TermDescription td : term.getDescriptions()) {
                                    storageUnit.deleteTermDescriptions(pathSplits[0], pathSplits[1], term.getName(), td);
                                    temp.addDescription(td);
                                    respond("Term description formulated in " + td.getLanguage() + " added to business term " + term.getName() + "!");
                                }
                                storageUnit.persistTerm(temp);
                                //respond("Term description created!");
                            } else {
                                respond("Could not find " + term.getName() + " in " + pathSplits[0] + "/" +
                                        pathSplits[1] + "!");
                            }
                        }
                        else if(!term.getTemplates().isEmpty()){
                            temp = storageUnit.getTerm(pathSplits[0], pathSplits[1], term.getName());
                            if (temp != null) {
                                for (Template t : term.getTemplates()) {
                                    storageUnit.deleteTermTemplates(pathSplits[0], pathSplits[1], term.getName(), t);
                                    temp.addTemplate(t);
                                    respond("Term template formulated in " + t.getLanguage() + " language with " + t.getDialect()
                                            + ((t.getDataModel() == null) ? "" : (" for data model " + t.getDataModel() + " realized in " + t.getVariant()) )
                                                    + " added to business term " + term.getName() + "!");
                                }
                                storageUnit.persistTerm(temp);
                                //respond("Term template created!");
                            } else {
                                respond("Could not find " + term.getName() + " in " + pathSplits[0] + "/" +
                                        pathSplits[1] + "!");
                            }
                        }
                        break;
//                    case DESCRIPTION:
//
//                        break;
//                    case TEMPLATE:
//
//                        break;
                    case DELETE:
                        if (term.getTemplates().isEmpty() && term.getDescriptions().isEmpty()) {
                            Term t = glossary.getTermByName(term.getName());
                            if(t != null){
                                glossary.removeTerm(t);
                                storageUnit.deleteTerm(t);
                                respond("Term deleted!");
                            }
                            else{
                                respond("Term does not exist");
                            }
                        }
                        else if(!term.getDescriptions().isEmpty()){
                            for (TermDescription td : term.getDescriptions()) {
                                storageUnit.deleteTermDescriptions(pathSplits[0], pathSplits[1],
                                        term.getName(), td);
                                respond("Term description deleted!");
                            }
                        }
                        else if(!term.getTemplates().isEmpty()){
                            for(Template t : term.getTemplates()){
                                storageUnit.deleteTermTemplates(pathSplits[0], pathSplits[1],
                                        term.getName(), t);
                                respond("Term template deleted!");
                            }
                        }
                        break;
                }
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            respond("Error in path");
        }
        catch(HibernateException e){
            respond("Hibernate Error: " + e.getMessage());
        }
        catch(Exception e){
            respond(e.getMessage());
        }
        finally {
            this.storageUnit.closeSession();
        }
    }

    /**
     * Method to instantiate terms or patterns.
     * @param toInstantiate The basic term or pattern that should be instantiated and already exists in the database
     * @param instantiation The new more specific pattern, so far only containing variables and their values
     */
    private void instantiateContext(Context toInstantiate, Context instantiation){
        if(toInstantiate == null || instantiation == null) throw new IllegalArgumentException("Null Parameter");

        // Copy the characteristics of a variable from the basic term or pattern + copy not instantiated variables
        for (Target t : toInstantiate.getTargets()) {
            if (t instanceof Variable) {
                Variable definition = (Variable) t.copy();
                Variable v = instantiation.getVariableByName(definition.getName());
                if (v != null) {
                    v.setOptional(definition.getOptional());
                    v.setPosition(definition.getPosition());
                    v.setValueKind(definition.getValueKind());
                    v.setVariableRole(definition.getVariableRole());
                    v.setKind(definition.getKind());
                } else {
                    instantiation.addTarget(definition);
                }
            }
        }

        //Copy all other targets (e.g. single values for type constraints)
        for (Target t : toInstantiate.getTargets()) {
            if (!(t instanceof Variable)) {
                this.addTargetToInstantiationIfNotExists(instantiation, t);
            }
        }
        //copy the constraints
        for (Constraint constraint : toInstantiate.getConstraints()) {
            this.addConstraintToInstantiation(instantiation, constraint);
        }
        //copy the derivation rules
        for (DerivationRule dr : toInstantiate.getDerivationRules()) {
            this.addDerivationRuleToInstantiation(instantiation, dr);
        }
        for (LocalCube a : toInstantiate.getLocalCubes()){
            this.addLocalCubeToInstantiation(instantiation, a);
        }
    }

    /**
     * Method that adds a target to an instantiation of a term or pattern if it does not already exist
     * @param context ther term or pattern to be instantiated
     * @param target the target to be added
     * @return the added target or the existing one
     */
    private Target addTargetToInstantiationIfNotExists(Context context, Target target){
        if(context == null || target == null) return null;

        if(target instanceof MapVariableAccess){
            MapVariableAccess mva = (MapVariableAccess) target;
            MapVariableAccess m = mva.copy();
            m.setVariable(context.getMapVariableByName(m.getVariable().getName()));
            m.setRowIndex(this.addTargetToInstantiationIfNotExists(context, m.getRowIndex()));
            return context.addMapVariableAccessIfNotExists(m);
        }
        else if(target instanceof ArrayVariableAccess){
            ArrayVariableAccess ava = (ArrayVariableAccess) target;
            ArrayVariableAccess a = ava.copy();
            a.setVariable(context.getArrayVariableByName(a.getVariable().getName()));
            return context.addArrayVariableAccessIfNotExists(a);
        }
        else if(target instanceof TupleValue){
            TupleValue tv = (TupleValue)target;
            TupleValue t = tv.copy();
            for(TupleEntry te : t.getTupleEntries()){
                te.setValue(this.addTargetToInstantiationIfNotExists(context, te.getValue()));
            }
            return context.addTupleValueIfNotExists(t);
        }
        else{
            Target t = target.copy();
            return context.addTargetIfNotExists(t);
        }
    }

    /**
     * Adds a constraint of an existing basic term or pattern to a new instantiation
     * @param context the instantiation term or pattern
     * @param constraint the constraint to be added
     */
    private void addConstraintToInstantiation(Context context, Constraint constraint){
        if(constraint instanceof BinaryAppConstraint){
            BinaryAppConstraint temp = (BinaryAppConstraint)constraint;
            BinaryAppConstraint c = temp.copy();
            c.setTerm(this.addTargetToInstantiationIfNotExists(context,c.getTerm()));
            c.setEntity1(this.addTargetToInstantiationIfNotExists(context, c.getEntity1()));
            c.setEntity2(this.addTargetToInstantiationIfNotExists(context, c.getEntity2()));
            context.addConstraint(c);
        }
        else if(constraint instanceof DescribedByConstraint){
            DescribedByConstraint temp = (DescribedByConstraint) constraint;
            DescribedByConstraint c = temp.copy();
            c.setDimension(this.addTargetToInstantiationIfNotExists(context, c.getDimension()));
            c.setLevel(this.addTargetToInstantiationIfNotExists(context, c.getLevel()));
            c.setAttribute(this.addTargetToInstantiationIfNotExists(context, c.getAttribute()));
            context.addConstraint(c);
        }
        else if(constraint instanceof DomainConstraint){
            DomainConstraint temp = (DomainConstraint) constraint;
            DomainConstraint c = temp.copy();
            c.setProperty(this.addTargetToInstantiationIfNotExists(context, c.getProperty()));
            c.setEntity(this.addTargetToInstantiationIfNotExists(context, c.getEntity()));
            c.setDomain(this.addTargetToInstantiationIfNotExists(context, c.getDomain()));
            context.addConstraint(c);
        }
        else if(constraint instanceof PropertyConstraint){
            PropertyConstraint temp = (PropertyConstraint) constraint;
            PropertyConstraint c = temp.copy();
            c.setProperty(this.addTargetToInstantiationIfNotExists(context, c.getProperty()));
            c.setEntity(this.addTargetToInstantiationIfNotExists(context, c.getEntity()));
            c.setType(this.addTargetToInstantiationIfNotExists(context, c.getType()));
            context.addConstraint(c);
        }
        else if(constraint instanceof RollUpConstraint){
            RollUpConstraint temp = (RollUpConstraint) constraint;
            RollUpConstraint c = temp.copy();
            c.setDimension(this.addTargetToInstantiationIfNotExists(context, c.getDimension()));
            c.setParentLevel(this.addTargetToInstantiationIfNotExists(context, c.getParentLevel()));
            c.setChildLevel(this.addTargetToInstantiationIfNotExists(context, c.getChildLevel()));
            context.addConstraint(c);
        }
        else if(constraint instanceof ScopeConstraint){
            ScopeConstraint temp = (ScopeConstraint) constraint;
            ScopeConstraint c = temp.copy();
            c.setTuple(this.addTargetToInstantiationIfNotExists(context, c.getTuple()));
            c.setScope(this.addTargetToInstantiationIfNotExists(context, c.getScope()));
            context.addConstraint(c);
        }
        else if(constraint instanceof TypeConstraint){
            TypeConstraint temp = (TypeConstraint) constraint;
            TypeConstraint c = temp.copy();
            c.setElement(this.addTargetToInstantiationIfNotExists(context, c.getElement()));
            c.setType(this.addTargetToInstantiationIfNotExists(context, c.getType()));
            context.addConstraint(c);
        }
        else if(constraint instanceof UnaryAppConstraint){
            UnaryAppConstraint temp = (UnaryAppConstraint) constraint;
            UnaryAppConstraint c = temp.copy();
            c.setEntity(this.addTargetToInstantiationIfNotExists(context, c.getEntity()));
            c.setTerm(this.addTargetToInstantiationIfNotExists(context, c.getTerm()));
            context.addConstraint(c);
        }
        else if(constraint instanceof ReturnConstraint){
            ReturnConstraint temp = (ReturnConstraint) constraint;
            ReturnConstraint c = temp.copy();
            c.setEntity(this.addTargetToInstantiationIfNotExists(context, c.getEntity()));
            c.setReturnType(this.addTargetToInstantiationIfNotExists(context, c.getReturnType()));
            context.addConstraint(c);
        }
    }

    /**
     * Adds a derivation rule of an existing basic term or pattern to a new instantiation
     * @param context the instantiation term or pattern
     * @param dr the derivation rule to be added
     */
    private void addDerivationRuleToInstantiation(Context context, DerivationRule dr){
        if(dr instanceof DomainDerivationRule) {
            DomainDerivationRule domainDerivationRule = ((DomainDerivationRule) dr).copy();
            domainDerivationRule.setVariable(context.getVariableByName(domainDerivationRule.getVariable().getName()));
            domainDerivationRule.setProperty(this.addTargetToInstantiationIfNotExists(context, domainDerivationRule.getProperty()));
            domainDerivationRule.setEntity(this.addTargetToInstantiationIfNotExists(context, domainDerivationRule.getEntity()));
            domainDerivationRule.setDomain(this.addTargetToInstantiationIfNotExists(context, domainDerivationRule.getDomain()));
            context.addDerivationRule(domainDerivationRule);
        }
        else if(dr instanceof ReturnTypeDerivationRule){
            ReturnTypeDerivationRule returnTypeDerivationRule = (ReturnTypeDerivationRule) dr.copy();
            returnTypeDerivationRule.setVariable(context.getVariableByName(returnTypeDerivationRule.getVariable().getName()));
            returnTypeDerivationRule.setBusinessTerm(this.addTargetToInstantiationIfNotExists(context, returnTypeDerivationRule.getBusinessTerm()));
            context.addDerivationRule(returnTypeDerivationRule);
        }
    }

    private void addLocalCubeToInstantiation(Context context, LocalCube localCube){
        if(localCube instanceof DescribedByLocalCube){
            DescribedByLocalCube temp = (DescribedByLocalCube) localCube;
            DescribedByLocalCube c = temp.copy();
            c.setDimension(this.addTargetToInstantiationIfNotExists(context, c.getDimension()));
            c.setLevel(this.addTargetToInstantiationIfNotExists(context, c.getLevel()));
            c.setAttribute(this.addTargetToInstantiationIfNotExists(context, c.getAttribute()));
            context.addLocalCube(c);
        }
        else if(localCube instanceof DomainLocalCube){
            DomainLocalCube temp = (DomainLocalCube) localCube;
            DomainLocalCube c = temp.copy();
            c.setProperty(this.addTargetToInstantiationIfNotExists(context, c.getProperty()));
            c.setEntity(this.addTargetToInstantiationIfNotExists(context, c.getEntity()));
            c.setDomain(this.addTargetToInstantiationIfNotExists(context, c.getDomain()));
            context.addLocalCube(c);
        }
        else if(localCube instanceof PropertyLocalCube){
            PropertyLocalCube temp = (PropertyLocalCube) localCube;
            PropertyLocalCube c = temp.copy();
            c.setProperty(this.addTargetToInstantiationIfNotExists(context, c.getProperty()));
            c.setEntity(this.addTargetToInstantiationIfNotExists(context, c.getEntity()));
            c.setType(this.addTargetToInstantiationIfNotExists(context, c.getType()));
            context.addLocalCube(c);
        }
        else if(localCube instanceof RollUpLocalCube){
            RollUpLocalCube temp = (RollUpLocalCube) localCube;
            RollUpLocalCube c = temp.copy();
            c.setDimension(this.addTargetToInstantiationIfNotExists(context, c.getDimension()));
            c.setParentLevel(this.addTargetToInstantiationIfNotExists(context, c.getParentLevel()));
            c.setChildLevel(this.addTargetToInstantiationIfNotExists(context, c.getChildLevel()));
            context.addLocalCube(c);
        }
        else if(localCube instanceof TypeLocalCube){
            TypeLocalCube temp = (TypeLocalCube) localCube;
            TypeLocalCube c = temp.copy();
            c.setElement(this.addTargetToInstantiationIfNotExists(context, c.getElement()));
            c.setType(this.addTargetToInstantiationIfNotExists(context, c.getType()));
            context.addLocalCube(c);
        }
    }

    @Override
    public void processSearchStatement(SearchEvent event) {
        //SearchObject searchObject = at.dke.olappattern.repository.event.getSearchObject();
        try {
            boolean noEntry = false;
            if (event.getSearchStrings() != null && !event.getSearchStrings().isEmpty()) {
                storageUnit.openSession();
                if (event.getSearchTarget().equals("PATTERN")) {
                    List<Pattern> patterns = null;
                    patterns = storageUnit.searchPatterns(event);
                    if (patterns != null && !patterns.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(patterns));
                            //mapper.writeValue(new File("C://temp/test.json"), patterns);
                        } else {
                            for (Pattern p : patterns) {
                                for (PatternDescription pd : p.getDescriptions()) {
                                    //todo: System.out.println(p.descriptionStatement(pd.getName()));
                                    System.out.println("\r\n");
                                }
                            }
                        }
                    } else noEntry = true;
                } else if (event.getSearchTarget().equals("TERM")) {
                    List<Term> terms = null;
                    terms = storageUnit.searchTerms(event);
                    Logger.log(terms.get(0).toString());

                    if (terms != null && !terms.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(terms));
                        } else {
                            for (Term t : terms) {
                                System.out.println(t);
                            }
                        }
                    } else noEntry = true;
                } else if (event.getSearchTarget().equals("REPOSITORY")) {
                    List<Repository> repositories = storageUnit.searchRepositories(event);
                    if (repositories != null && !repositories.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(repositories));
                        } else {
                            System.out.println("Repository names:");
                            for (Repository r : repositories) {
                                System.out.println(r.getName());
                            }
                        }
                    } else {
                        noEntry = true;
                    }
                } else if (event.getSearchTarget().equals("GLOSSARY")) {
                    List<Glossary> vocabularies = storageUnit.searchGlossaries(event);
                    if (vocabularies != null && !vocabularies.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(vocabularies));
                        } else {
                            System.out.println("Glossary names:");
                            for (Glossary v : vocabularies) {
                                System.out.println(v.getName());
                            }
                        }
                    } else {
                        noEntry = true;
                    }
                } else if (event.getSearchTarget().equals("CATALOGUE")) {
                    List<Catalogue> catalogues = storageUnit.searchCatalogues(event);
                    if (catalogues != null && !catalogues.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(catalogues));
                        } else {
                            System.out.println("Catalogue names:");
                            for (Catalogue c : catalogues) {
                                System.out.println(c.getName());
                            }
                        }
                    } else {
                        noEntry = true;
                    }
                } else if (event.getSearchTarget().equals("MULTIDIMENSIONAL_MODEL")) {
                    List<MDM> mdms = storageUnit.searchMDMs(event);
                    if (mdms != null && !mdms.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(mdms));
                        } else {
                            System.out.println("MDM names:");
                            for (MDM m : mdms) {
                                System.out.println(m.getName());
                            }
                        }
                    } else {
                        noEntry = true;
                    }
                } else if (event.getSearchTarget().equals("CUBE")) {
                    List<Cube> cubes = storageUnit.searchCubes(event);
                    if (cubes != null && !cubes.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(cubes));
                        } else {
                            System.out.println("Cube names:");
                            for (Cube c : cubes) {
                                System.out.println(c.getName());
                            }
                        }
                    } else {
                        noEntry = true;
                    }
                } else if (event.getSearchTarget().equals("DIMENSION")) {
                    List<Dimension> dimensions = storageUnit.searchDimensions(event);
                    if (dimensions != null && !dimensions.isEmpty()) {
                        if (response != null) {
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                            sendJsonResponse(mapper.writeValueAsString(dimensions));
                        } else {
                            System.out.println("Dimension names:");
                            for (Dimension d : dimensions) {
                                System.out.println(d.getName());
                            }
                        }
                    } else {
                        noEntry = true;
                    }
                }
                storageUnit.closeSession();
            }
            if(noEntry) respond("No entries found for the given parameters!");
        }
        catch (Exception e) {
            respond(e.getMessage());
        }
    }

    @Override
    public void processOrganizationElement(OrganizationElementEvent event) {
        OrganizationElement element = event.getRepositoryHierarchyElement();

        try {
            storageUnit.openSession();
            Repository r = null;
            switch(event.getAction()) {
                case CREATE:
                    if (element instanceof Repository) {
                        r = (Repository) element;
                        if(storageUnit.getRepository(r.getName()) != null){
                            respond("The repository already exists!");
                        }
                        else {
                            storageUnit.persistRepository(r);
                            storageUnit.closeSession();
                            respond("Repository created!");
                        }
                    } else {
                        if (event.getPath() != null) {
                            String[] pathSplits = event.getPath().split("/");
                            r = storageUnit.getRepository(pathSplits[0]);
                            if (r != null) {
                                if (element instanceof Catalogue) {
                                    Catalogue c = (Catalogue) element;
                                    if(storageUnit.getCatalogue(r.getName(), c.getName()) != null){
                                        respond("The catalogue already exists!");
                                    }
                                    else {
                                        r.addCatalogue(c);
                                        storageUnit.persistRepository(r);
                                        respond("Catalogue created!");
                                    }
                                }
                                if (element instanceof Glossary) {
                                    Glossary g = (Glossary) element;
                                    if(storageUnit.getGlossary(r.getName(), g.getName()) != null){
                                        respond("The glossary already exists!");
                                    }
                                    else {
                                        r.addGlossary(g);
                                        storageUnit.persistRepository(r);
                                        respond("Glossary created!");
                                    }
                                }
                                if (element instanceof MDM) {
                                    MDM m = (MDM) element;
                                    if(storageUnit.getMDM(r.getName(), m.getName()) != null){
                                        respond("The multidimensional model already exists!");
                                    }
                                    else {
                                        r.addMDM(m);
                                        storageUnit.persistRepository(r);
                                        respond("Multidimensional model created!");
                                    }
                                }
                            } else {
                                respond("No existing repository with this name");
                            }
                        } else {
                            respond("Missing path!");
                        }
                    }
                    break;
                case DELETE:
                    if(element instanceof Repository){
                        storageUnit.deleteRepository(element.getName());
                        respond("Repository deleted!");
                    }
                    else{
                        if(event.getPath() != null) {
                            String[] pathSplits = event.getPath().split("/");
                            if (element instanceof Catalogue) {
                                storageUnit.deleteCatalogue(pathSplits[0], element.getName());
                                respond("Catalogue deleted!");
                            } else if (element instanceof Glossary) {
                                storageUnit.deleteGlossary(pathSplits[0], element.getName());
                                respond("Vocabulary deleted!");
                            } else if (element instanceof MDM) {
                                storageUnit.deleteMDM(pathSplits[0], element.getName());
                                respond("MDM deleted!");
                            }
                        }
                        else{
                            respond("Missing path!");
                        }
                    }
                    break;
            }
        }
        catch (Exception e){
            respond(e.getMessage());
        }
        finally {
            storageUnit.closeSession();
        }
    }

    @Override
    public void processShowStatement(ShowEvent event) {
        String path = event.getPath();
        Logger.log("Path: " + path + "\r\n");
        path = path.replace("\"", "");
        String[] pathSplits = path.split("/");
        int pathDepth = pathSplits.length;

        storageUnit.openSession();
        if(pathDepth >= 1){
            Repository r = storageUnit.getRepository(pathSplits[0]);
            if(r == null) respond("Path not found");
            if(pathDepth == 1) respond(r.generateCreateStatement());

            Catalogue c = null;
            Glossary g = null;
            MDM m = null;

            if(pathDepth >= 2){
                c = r.getCatalogueByName(pathSplits[1]);
                if(c != null && pathDepth == 2) {
                    respond(c.generateCreateStatement(r.getName()));
                    return;
                }

                g = r.getGlossaryByName(pathSplits[1]);
                if(g != null && pathDepth == 2) {
                    respond(g.generateCreateStatement(r.getName()));
                    return;
                }

                m = r.getMDMByName(pathSplits[1]);
                if(m != null && pathDepth == 2){
                    respond(m.generateCreateStatement(r.getName()));
                    return;
                }

                if(c == null && g == null && m == null && pathDepth == 2) {
                    respond("Path not found");
                    return;
                }
            }
            if(pathDepth == 3){
                Pattern p = null;
                Term t = null;
                Cube cu = null;
                Dimension d = null;

                if(c != null){
                    p = c.getPatternByName(pathSplits[2]);
                    if(p != null) {
                        respond(p.generateCreateStatement(r.getName(), c.getName()) + "\r\n" +
                                "\r\n \r\n \r\n" +
                                p.templateStatements(r.getName(), c.getName()) +
                                "\r\n \r\n \r\n" +
                                p.descriptionStatements(r.getName(), c.getName()));
                        return;
                    }
                }
                if(g != null){
                    t = g.getTermByName(pathSplits[2]);
                    if(t != null) {
                        respond(t.generateCreateStatement(r.getName(), g.getName()) + "\r\n" +
                                "\r\n \r\n \r\n" +
                                t.templateStatements(r.getName(), g.getName()) +
                                "\r\n \r\n \r\n" +
                                t.descriptionStatements(r.getName(), g.getName()));
                        return;
                    }
                }
                if(m != null){
                    cu = m.getCubeByName(pathSplits[2]);
                    if(cu != null) {
                        respond(cu.generateCreateStatement(r.getName(), m.getName()));
                        return;
                    }

                    d = m.getDimensionByName(pathSplits[2]);
                    if(d != null) {
                        respond(d.generateCreateStatement(r.getName(), m.getName()));
                        return;
                    }
                }

                if(c == null && g == null && cu == null && d == null){
                    respond ("Path not found");
                }
            }
        }

        storageUnit.closeSession();
    }

    /**
     * Method that sends a result message as a json object
     * @param str The message to be sent
     */
    private void sendStringResponse(String str){
        JsonObjectBuilder job = Json.createObjectBuilder();
        job.add("result", str);
        JsonObject j = job.build();

        response.resume(Response.ok().entity(j).
                header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Headers",
                        "origin, content-type, accept")
                .header("Access-Control-Allow-Methods","POST").build());
    }

    /**
     * Method that allows to respond to the user independet whether the application runs as console or web application
     * @param s The response message
     */
    protected void respond(String s){
        if(response != null) {
            sendStringResponse(s);
            this.response = null;
        }
        else System.out.println(s);
    }

    /**
     * Method that takes the string representation of json object and sends it to the user
     * @param s the string representation of the json object
     */
    private void sendJsonResponse(String s){
        String result = "{ \"result\" : " + s + " }";

        response.resume(Response.ok().entity(result).
                header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Headers",
                        "origin, content-type, accept")
                .header("Access-Control-Allow-Methods","POST").build());
    }
}
