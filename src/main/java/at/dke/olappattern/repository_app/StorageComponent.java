package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.Context;
import at.dke.olappattern.data.MDM.*;
import at.dke.olappattern.data.Target;
import at.dke.olappattern.data.Template;
import at.dke.olappattern.data.pattern.Pattern;
import at.dke.olappattern.data.pattern.PatternDescription;
import at.dke.olappattern.data.repository.Catalogue;
import at.dke.olappattern.data.repository.Glossary;
import at.dke.olappattern.data.repository.Repository;
import at.dke.olappattern.data.term.Term;
import at.dke.olappattern.data.term.TermDescription;
import at.dke.olappattern.data.term.Type;
import at.dke.olappattern.data.value.ArrayVariableEntry;
import at.dke.olappattern.data.value.MapVariableEntry;
import at.dke.olappattern.data.value.TupleEntry;
import at.dke.olappattern.data.value.TupleValue;
import at.dke.olappattern.data.variable.*;
import at.dke.olappattern.repository_app.event.SearchEvent;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * A specific RepositoryUnit built on Hibernate with a PostgreSQL database
 */
public class StorageComponent implements StorageUnit {
    private SessionFactory factory = null;
    private Session session = null;

    public StorageComponent(){
        this.factory = new Configuration().configure().buildSessionFactory();
    }

    @Override
    public void openSession(){
        if(this.session == null || !this.session.isOpen()) {
            this.session = this.factory.openSession();
        }
    }

    @Override
    public void closeSession(){
        if(session != null) {
            session.clear();
            session.close();
            session = null;
        }
    }

    @Override
    public void persistPattern(Pattern p) {
        this.persist(p);
    }

    @Override
    public void persistTerm(Term t) {
        this.persist(t);
    }

    @Override
    public void persistCatalogue(Catalogue c) {
        this.persist(c);
    }

    @Override
    public void persistMDM(MDM m) {
        this.persist(m);
    }

    @Override
    public void persistGlossary(Glossary glossary) {
        this.persist(glossary);
    }

    @Override
    public void persistRepository(Repository r) {
        this.persist(r);
    }

    //method to persist any type of object
    private void persist(Object o){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(o == null) throw new IllegalArgumentException("Null parameter");
        Transaction tx = null;
        try {
            boolean subCall = false;
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                subCall = true;
                tx = session.getTransaction();
            }
            else {
                session.clear();
                tx = session.beginTransaction();
            }

            session.saveOrUpdate(o);
            session.flush();
            if(!subCall) {
                tx.commit();
            }

        }catch (Throwable ex) {
            if(tx != null) tx.rollback();
            throw ex;
        }
    }

    //Delete method for context objects, i.e. patterns & terms
    private void deleteContext(Context c){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(c == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            boolean subCall = false;
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                subCall = true;
                tx = session.getTransaction();
            }
            else {
                tx = session.beginTransaction();
            }

//            for(DerivationRule dr : c.getDerivationRules()){
//                dr.setVariable(null);
//                if(dr instanceof DomainDerivationRule){
//                    ((DomainDerivationRule) dr).setDomain(null);
//                    ((DomainDerivationRule) dr).setEntity(null);
//                    ((DomainDerivationRule) dr).setProperty(null);
//                }
//                else if(dr instanceof ReturnTypeDerivationRule){
//                    ((ReturnTypeDerivationRule) dr).setBusinessTerm(null);
//                }
//            }
            Set<DerivationRule> derivationRules = c.getDerivationRules();
            for(Iterator<DerivationRule> i = derivationRules.iterator(); i.hasNext();){
                DerivationRule e = i.next();
                i.remove();
                session.remove(e);
            }

            c.getConstraints().clear();
            c.getDerivationRules().clear();

            for(Target t : c.getTargets()){
                if(t instanceof MapVariable){
                    Set<MapVariableEntry> entries = ((MapVariable) t).getMapVariableEntries();
                    for(Iterator<MapVariableEntry> i = entries.iterator(); i.hasNext();){
                        MapVariableEntry e = i.next();
                        i.remove();
                        session.remove(e);
                    }
                    ((MapVariable) t).getMapVariableEntries().clear();
                }
                else if(t instanceof ArrayVariable){
                    Set<ArrayVariableEntry> entries = ((ArrayVariable) t).getArrayVariableEntries();
                    for(Iterator<ArrayVariableEntry> i = entries.iterator(); i.hasNext();){
                        ArrayVariableEntry e = i.next();
                        i.remove();
                        session.remove(e);
                    }
                    ((ArrayVariable) t).getArrayVariableEntries().clear();
                }
                else if(t instanceof SingleVariable){
                    ((SingleVariable) t).setValue(null);
                }
            }
            for(Target t : c.getTargets()){
                if(t instanceof TupleValue){
                    Set<TupleEntry> entries = ((TupleValue) t).getTupleEntries();
                    for(Iterator<TupleEntry> i = entries.iterator(); i.hasNext();){
                        TupleEntry te = i.next();
                        i.remove();
                        session.remove(te);
                    }
                    ((TupleValue) t).getTupleEntries().clear();
                }
                else if(t instanceof MapVariableAccess){
                    ((MapVariableAccess) t).setRowIndex(null);
                }
            }

            c.getTargets().clear();
            session.delete(c);
            if(!subCall) {
                session.flush();
                tx.commit();
                session.clear();
            }

        }catch (Exception ex) {
            throw ex;
        }
    }

    //method removes a pattern which is already removed from the catalogue it was in.
    @Override
    public void deletePattern(Pattern p){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(p == null) throw new IllegalArgumentException("Null parameter");
        Transaction tx = null;
        boolean subCall = false;
        if(session.getTransaction() != null && session.getTransaction().isActive()){
            subCall = true;
            tx = session.getTransaction();
        }
        else {
            tx = session.beginTransaction();
        }
        try {
            deleteFromRelatedPatterns(p);
            p.getTemplates().clear();
            p.getDescriptions().clear();
            this.deleteContext(p);

            if(!subCall) {
                session.flush();
                tx.commit();
                session.clear();
            }
        }catch (Exception e){
            tx.rollback();
            throw e;
        }
    }

    /*@Override
    public void deletePattern(String repository, String catalogue, String pattern){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null && catalogue == null && pattern == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        boolean subCall = false;
        if(session.getTransaction() != null && session.getTransaction().isActive()){
            subCall = true;
            tx = session.getTransaction();
        }
        else {
            session.clear();
            tx = session.beginTransaction();
        }
        try {
            Pattern p = this.getPattern(repository, catalogue, pattern);
            if(p != null){
                deleteFromRelatedPatterns(p);
                Catalogue c = this.getCatalogue(repository, catalogue);
                c.removePattern(p);
                this.deletePattern(p);
            }
            else throw new IllegalStateException("Pattern does not exist");

            if(!subCall) {
                session.flush();
                tx.commit();
                session.clear();
            }
        }catch (Exception e){
            tx.rollback();
            throw e;
        }
    }*/

    //method to remove the name of a deleted pattern from all related pattern lists
    private void deleteFromRelatedPatterns(Pattern pattern){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(pattern == null) throw new IllegalArgumentException("Null parameter");
        Transaction tx = null;
        boolean subCall = false;
        if(session.getTransaction() != null && session.getTransaction().isActive()){
            subCall = true;
            tx = session.getTransaction();
        }
        else {
            tx = session.beginTransaction();
        }
        try {
            Query query = session.createQuery("SELECT DISTINCT d FROM Catalogue c JOIN c.patterns p JOIN p.descriptions d JOIN d.relatedPatterns r" +
                    " WHERE :pattern in elements(c.patterns) AND :name in elements(r) ");
            query.setParameter("pattern", pattern);
            query.setParameter("name", pattern.getName());
            List result = query.list();
            //tx.commit();

            for (Object temp : result){
                PatternDescription d = (PatternDescription) temp;
                for (Iterator<String> i = d.getRelatedPatterns().iterator(); i.hasNext(); ) {
                    String s = i.next();
                    if(s.equals(pattern.getName())){
                        i.remove();
                    }
                }
                this.persist(d);
            }
            if(!subCall) {
                session.flush();
                tx.commit();
                session.clear();
            }
        }catch (Throwable ex) {
            if(tx != null){
                tx.rollback();
            }
            throw ex;
        }
    }

    @Override
    public void deletePatternDescriptions(String repository, String catalogue, String pattern, PatternDescription description){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || pattern == null || catalogue == null || description == null) throw new IllegalArgumentException("Null parameter");
        Transaction tx = null;
        try {
            String queryString = "DELETE FROM PatternDescription WHERE id in (SELECT d.id FROM Repository r " +
                    "JOIN r.catalogues c JOIN c.patterns p JOIN p.descriptions d " +
                    "WHERE lower(r.name) = lower(:r) AND lower(c.name) = lower(:c) AND lower(p.name) = lower(:p))";

            if(description.getLanguage() != null){
                queryString += " AND lower(language) = lower(:l)";
            }

            Query query = session.createQuery(queryString)
                    .setParameter("r", repository)
                    .setParameter("c", catalogue)
                    .setParameter("p", pattern);

            if(description.getLanguage() != null){
                query.setParameter("l", description.getLanguage());
            }

            tx = session.beginTransaction();
            query.executeUpdate();
            session.flush();
            tx.commit();
            session.clear();
        }catch (Throwable ex) {}
    }

    @Override
    public void deletePatternTemplates(String repository, String catalogue, String pattern, Template template){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || catalogue == null || pattern == null
            || template == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            String queryString = "DELETE FROM PatternTemplate WHERE id in (SELECT tt.id FROM Repository r " +
                    "JOIN r.catalogues c JOIN c.patterns p JOIN p.templates tt " +
                    "WHERE lower(r.name) = lower(:r) AND lower(c.name) = lower(:c) AND lower(p.name) = lower(:p))";

            if(template.getLanguage() != null){
                queryString += " AND lower(language) = lower(:l)";
            }
            if(template.getDialect() != null){
                queryString += " AND lower(dialect) = lower(:d)";
            }
            if(template.getDataModel() != null){
                queryString += " AND lower(data_model) = lower(:dm)";
            }
            if(template.getVariant() != null){
                queryString += " AND lower(variant) = lower(:v)";
            }

            Query query = session.createQuery(queryString)
                    .setParameter("r", repository)
                    .setParameter("c", catalogue)
                    .setParameter("p", pattern);

            if(template.getLanguage() != null){
                query.setParameter("l", template.getLanguage());
            }
            if(template.getDialect() != null){
                query.setParameter("d", template.getDialect());
            }
            if(template.getDataModel() != null){
                query.setParameter("dm", template.getDataModel());
            }
            if(template.getVariant() != null){
                query.setParameter("v", template.getVariant());
            }

            tx = session.beginTransaction();
            query.executeUpdate();
            session.flush();
            tx.commit();
            session.clear();
        }catch (Throwable ex) { }
    }

    @Override
    public void deletePatternTemplates(Template template){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(template == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.remove(template);
            session.flush();
            tx.commit();
        }catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deletePatternDescriptions(PatternDescription description){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(description == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.remove(description);
            session.flush();
            tx.commit();
        }catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteTerm(Term t){
        t.getTemplates().clear();
        t.getDescriptions().clear();
        this.deleteContext(t);
    }

    @Override
    public void deleteTermDescriptions(String repository, String glossary, String term, TermDescription description){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || glossary == null || term == null || description == null) throw new IllegalArgumentException("Null parameter");
        Transaction tx = null;
        try {
            String queryString = "DELETE FROM TermDescription WHERE id in (SELECT d.id FROM Repository r " +
                    "JOIN r.glossaries g JOIN g.terms t JOIN t.descriptions d " +
                    "WHERE lower(r.name) = lower(:r) AND lower(g.name) = lower(:g) AND lower(t.name) = lower(:t))";

            if(description.getLanguage() != null){
                queryString += " AND lower(language) = lower(:l)";
            }

            Query query = session.createQuery(queryString)
                    .setParameter("r", repository)
                    .setParameter("g", glossary)
                    .setParameter("t", term);

            if(description.getLanguage() != null){
                query.setParameter("l", description.getLanguage());
            }

            tx = session.beginTransaction();
            query.executeUpdate();
            session.flush();
            tx.commit();
            session.clear();
        }catch (Throwable ex) {  }
    }

    @Override
    public void deleteTermTemplates(String repository, String glossary, String term, Template template){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || glossary == null || term == null || template == null) throw new IllegalArgumentException("Null parameter");
        Transaction tx = null;
        try {
            String queryString = "DELETE FROM TermTemplate WHERE id in (SELECT tt.id FROM Repository r " +
                    "JOIN r.glossaries g JOIN g.terms t JOIN t.templates tt " +
                    "WHERE lower(r.name) = lower(:r) AND lower(g.name) = lower(:g) AND lower(t.name) = lower(:t))";

            if(template.getLanguage() != null){
                queryString += " AND lower(language) = lower(:l)";
            }
            if(template.getDialect() != null){
                queryString += " AND lower(dialect) = lower(:d)";
            }

            Query query = session.createQuery(queryString)
                    .setParameter("r", repository)
                    .setParameter("g", glossary)
                    .setParameter("t", term);

            if(template.getLanguage() != null){
                query.setParameter("l", template.getLanguage());
            }
            if(template.getDialect() != null){
                query.setParameter("d", template.getDialect());
            }

            tx = session.beginTransaction();
            query.executeUpdate();
            session.flush();
            tx.commit();
            session.clear();
        }catch (Throwable ex) {
        }
    }

    @Override
    public void deleteCatalogue(String repository, String catalogue){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || catalogue == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            Repository r = this.getRepository(repository);
            if(r != null) {
                for (Iterator<Catalogue> i = r.getCatalogues().iterator(); i.hasNext(); ) {
                    Catalogue cat = i.next();
                    if (cat.getName().equals(catalogue)) {
                        tx = session.beginTransaction();
                        for (Iterator<Pattern> j = cat.getPatterns().iterator(); j.hasNext(); ) {
                            Pattern p = j.next();
                            j.remove();
                            this.deletePattern(p);
                        }
                        i.remove();
                        session.remove(cat);
                        session.flush();
                        tx.commit();
                        break;
                    }
                }
            }
            else{
                throw new IllegalArgumentException("Not a valid repository name");
            }
        }catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteCatalogue(Catalogue c){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(c == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            boolean subCall = false;
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                subCall = true;
            }
            else{
                tx = session.beginTransaction();
            }
            for (Iterator<Pattern> i = c.getPatterns().iterator(); i.hasNext(); ) {
                Pattern p = i.next();
                i.remove();
                this.deletePattern(p);
            }
            session.remove(c);
            if(!subCall){
                session.flush();
                tx.commit();
            }
        }
        catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteGlossary(String repository, String glossary){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || glossary == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            Repository r = this.getRepository(repository);
            if(r != null) {
                for (Iterator<Glossary> i = r.getGlossaries().iterator(); i.hasNext(); ) {
                    Glossary voc = i.next();
                    if (voc.getName().equals(glossary)) {
                        tx = session.beginTransaction();
                        for (Iterator<Term> j = voc.getTerms().iterator(); j.hasNext(); ) {
                            Term t = j.next();
                            j.remove();
                            this.deleteTerm(t);
                        }
                        i.remove();
                        session.remove(voc);
                        session.flush();
                        tx.commit();
                        break;
                    }
                }
            }
            else{
                throw new IllegalArgumentException("Not a valid repository name");
            }
        }catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteGlossary(Glossary glossary){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(glossary == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            boolean subCall = false;
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                subCall = true;
            }
            else{
                tx = session.beginTransaction();
            }
            for (Iterator<Term> i = glossary.getTerms().iterator(); i.hasNext(); ) {
                Term t = i.next();
                i.remove();
                this.deleteTerm(t);
            }
            session.remove(glossary);
            if(!subCall){
                session.flush();
                tx.commit();
            }
        }
        catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteCube(String repository, String mdm, String cube){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || mdm == null || cube == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            MDM m = this.getMDM(repository, mdm);
            if(mdm != null) {
                Cube c = m.getCubeByName(cube);
                if (c != null) {
                    boolean subCall = false;
                    if(session.getTransaction() != null && session.getTransaction().isActive()){
                        subCall = true;
                    }
                    else{
                        tx = session.beginTransaction();
                    }
                    m.removeCube(c);
                    session.remove(c);
                    if (!subCall) {
                        session.flush();
                        tx.commit();
                    }
                }
                else{
                    throw new IllegalStateException("No such cube in " + mdm);
                }
            }
            else{
                throw new IllegalStateException("Cannot find path");
            }
        }
        catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteCube(Cube c) {
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(c == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.remove(c);
            session.flush();
            tx.commit();
        }catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteDimension(String repository, String mdm, String dimension){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || mdm == null || dimension == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            MDM m = this.getMDM(repository, mdm);
            if(m != null) {
                Dimension d = m.getDimensionByName(dimension);
                if (d != null) {
                    boolean subCall = false;
                    if(session.getTransaction() != null && session.getTransaction().isActive()){
                        subCall = true;
                    }
                    else{
                        tx = session.beginTransaction();
                    }
                    m.removeDimension(d);
                    session.remove(d);
                    if (!subCall) {
                        session.flush();
                        tx.commit();
                    }
                }
                else{
                    throw new IllegalStateException("No such dimension in " + mdm);
                }
            }
            else{
                throw new IllegalStateException("Cannot find path");
            }
        }
        catch (Throwable ex) {
            if(tx != null){
                tx.rollback();
            }
            throw ex;
        }
    }

    @Override
    public void deleteDimension(Dimension d) {
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(d == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.remove(d);
            session.flush();
            tx.commit();
        }catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public void deleteMDM(String repository, String mdm){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null || mdm == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            Repository r = this.getRepository(repository);
            if(r != null) {
                MDM m = r.getMDMByName(mdm);
                if (m != null) {
                    boolean subCall = false;
                    if(session.getTransaction() != null && session.getTransaction().isActive()){
                        subCall = true;
                    }
                    else{
                        tx = session.beginTransaction();
                    }
                    for (Iterator<Cube> i = m.getCubes().iterator(); i.hasNext(); ) {
                        Cube c = i.next();
                        for (Iterator<Measure> j = c.getMeasures().iterator(); j.hasNext(); ) {
                            Measure measure = j.next();
                            j.remove();
                            session.remove(measure);
                        }
                        for (Iterator<DimensionRole> j = c.getDimensionRoles().iterator(); j.hasNext(); ) {
                            DimensionRole d = j.next();
                            j.remove();
                            session.remove(d);
                        }
                        i.remove();
                        session.remove(c);
                    }
                    session.flush();
                    for (Iterator<Dimension> i = m.getDimensions().iterator(); i.hasNext(); ) {
                        Dimension d = i.next();
                        for (Iterator<Attribute> j = d.getAttributes().iterator(); j.hasNext(); ) {
                            Attribute a = j.next();
                            j.remove();
                            session.remove(a);
                        }
                        for (Iterator<RollUp> j = d.getRollUps().iterator(); j.hasNext(); ) {
                            RollUp ru = j.next();
                            j.remove();
                            session.remove(ru);
                        }
                        for (Iterator<Level> j = d.getLevels().iterator(); j.hasNext(); ) {
                            Level l = j.next();
                            j.remove();
                            session.remove(l);
                        }
                        i.remove();
                        session.remove(d);
                    }
                    session.flush();
                    for (Iterator<ValueSet> i = m.getValueSets().iterator(); i.hasNext(); ) {
                        ValueSet vs = i.next();
                        i.remove();
                        session.remove(vs);
                    }
                    session.flush();
                    r.removeMDM(m);
                    session.remove(m);
                    if (!subCall) {
                        session.flush();
                        tx.commit();
                    }
                }
                else{
                    throw new IllegalStateException("No such dimension in " + mdm);
                }
            }
            else{
                throw new IllegalStateException("Cannot find path");
            }
        }
        catch (Throwable ex) {
            if(tx != null){
                tx.rollback();
            }
            throw ex;
        }
    }

    @Override
    public void deleteMDM(MDM mdm){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(mdm == null) throw new IllegalArgumentException("Null parameter");

        Transaction tx = null;
        try {
            boolean subCall = false;
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                subCall = true;
            }
            else{
                tx = session.beginTransaction();
            }
            for (Iterator<Cube> i = mdm.getCubes().iterator(); i.hasNext(); ) {
                Cube c = i.next();
                for (Iterator<Measure> j = c.getMeasures().iterator(); j.hasNext(); ) {
                    Measure m = j.next();
                    j.remove();
                    session.remove(m);
                }
                for (Iterator<DimensionRole> j = c.getDimensionRoles().iterator(); j.hasNext(); ) {
                    DimensionRole d = j.next();
                    j.remove();
                    session.remove(d);
                }
                i.remove();
                session.remove(c);
            }
            for (Iterator<Dimension> i = mdm.getDimensions().iterator(); i.hasNext(); ) {
                Dimension d = i.next();
                for (Iterator<Attribute> j = d.getAttributes().iterator(); j.hasNext(); ) {
                    Attribute a = j.next();
                    j.remove();
                    session.remove(a);
                }
                for (Iterator<RollUp> j = d.getRollUps().iterator(); j.hasNext(); ) {
                    RollUp r = j.next();
                    j.remove();
                    session.remove(r);
                }
                for (Iterator<Level> j = d.getLevels().iterator(); j.hasNext(); ) {
                    Level l = j.next();
                    j.remove();
                    session.remove(l);
                }
                i.remove();
                session.remove(d);
            }
            for (Iterator<ValueSet> i = mdm.getValueSets().iterator(); i.hasNext(); ) {
                ValueSet vs = i.next();
                i.remove();
                session.remove(vs);
            }
            session.remove(mdm);
            if (!subCall) {
                session.flush();
                tx.commit();
            }
        }
        catch (Throwable ex) {
            if(tx != null){
                tx.rollback();
            }
            throw ex;
        }
    }

    @Override
    public void deleteRepository(String repository){
        if(session == null || !session.isOpen()) throw new IllegalStateException("No open session");
        if(repository == null) throw new IllegalArgumentException("Null parameter");
        Transaction tx = null;
        try {
            Repository r = this.getRepository(repository);
            if(r != null){
                tx = session.beginTransaction();
                for (Iterator<Catalogue> i = r.getCatalogues().iterator(); i.hasNext(); ) {
                    Catalogue c = i.next();
                    i.remove();
                    this.deleteCatalogue(c);
                }
                for (Iterator<Glossary> i = r.getGlossaries().iterator(); i.hasNext(); ) {
                    Glossary v = i.next();
                    i.remove();
                    this.deleteGlossary(v);
                }
                for (Iterator<MDM> i = r.getMdms().iterator(); i.hasNext(); ) {
                    MDM m = i.next();
                    i.remove();
                    this.deleteMDM(m);
                }

                session.remove(r);
                session.flush();
                tx.commit();
            }
            else{
                throw new IllegalArgumentException("Not a valid repository name");
            }
        }
        catch (Throwable ex) {
            throw ex;
        }
    }

    @Override
    public Type getType(String name){
        if(session == null || !session.isOpen()) return null;
        if(name == null) throw new IllegalArgumentException("Null parameter");

        Type type = null;
        try {
            //session.clear();
            Query query = session.createQuery("FROM Type WHERE name = : type_name");
            query.setParameter("type_name", name);
            Object o = query.getSingleResult();
            //tx.commit();

            type = (Type) o;
            return type;
        } catch (HibernateException e) {
            //System.out.println(e.getMessage());
        } catch (Throwable ex) {
            //System.out.println(ex.getMessage());
        }

        return null;
    }

    @Override
    public Pattern getPattern(String repository, String catalogue, String pattern){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || catalogue == null || pattern == null) throw new IllegalArgumentException("Null parameter");

        Pattern result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!catalogue.startsWith("\"") && !catalogue.endsWith("\"")) catalogue = "\"" + catalogue + "\"";
        if(!pattern.startsWith("\"") && !pattern.endsWith("\"")) pattern = "\"" + pattern + "\"";
        try {
            Query query = session.createQuery("SELECT p FROM Repository r JOIN r.catalogues c JOIN c.patterns p " +
                    "WHERE lower(r.name) = lower(:repo) AND lower(c.name) = lower(:cat) AND lower(p.name) = lower(:pat)");
            query.setParameter("repo", repository);
            query.setParameter("cat", catalogue);
            query.setParameter("pat", pattern);
            Object o = query.getSingleResult();

            result = (Pattern)o;

        }catch (Throwable ex) {
            return null;
        }

        return result;
    }

    @Override
    public List<Pattern> searchPatterns(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<Pattern> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;
            if(searchEvent.getSearchPath() != null){
                //String s = searchObject.getSearchPath().replace("\"", "");
                splits = searchEvent.getSearchPath().split("/");

                queryString += "SELECT DISTINCT p FROM Repository r " +
                        "JOIN r.catalogues c JOIN c.patterns p LEFT OUTER JOIN p.descriptions d LEFT OUTER JOIN " +
                        " d.aliases a LEFT OUTER JOIN d.relatedPatterns rp WHERE r.name = :rep " +
                        "AND c.name = :cat ";
                //JOIN p.descriptions d
                if(!searchEvent.getSearchStrings().isEmpty()){
                    queryString += "AND ";
                }
            }
            else{
                queryString += "SELECT DISTINCT p FROM Pattern p WHERE ";
            }

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    if(param.equals("NAME")){
                        queryString += "lower(p." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                    }
                    else if(param.equals("ALIAS")){
                        queryString += "lower(a) like lower(:" + param.toLowerCase() + ") AND ";
                    }
                    else if(param.equals("RELATED")){
                        queryString += "lower(rp) like lower(:" + param.toLowerCase() + ") AND ";
                    }
                    else {
                        queryString += "lower(d." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                    }
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    System.out.println(value);
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }
            if(splits != null){
                if(!splits[0].startsWith("\"") && !splits[0].endsWith("\"")) splits[0] = "\"" + splits[0] + "\"";
                if(!splits[1].startsWith("\"") && !splits[1].endsWith("\"")) splits[1] = "\"" + splits[1] + "\"";
                query.setParameter("rep", splits[0]);
                query.setParameter("cat", splits[1]);
            }
            /*queryString = "SELECT p from Repository r JOIN r.catalogues c JOIN c.patterns p WHERE r.name = :rep AND p.name like :pat";
            query = session.createQuery(queryString);
            query.setParameter("rep", splits[0]);
            query.setParameter("pat", "%set%");*/
            List results = query.list();

            for (Object o : results){
                found.add((Pattern) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }

    @Override
    public List<Term> searchTerms(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<Term> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;
            if(searchEvent.getSearchPath() != null){
                String s = searchEvent.getSearchPath().replace("\"", "");
                splits = s.split("/");
                queryString += "SELECT DISTINCT t FROM Repository r " +
                        "JOIN r.glossaries v JOIN v.terms t LEFT OUTER JOIN t.descriptions d LEFT OUTER JOIN d.aliases a " +
                        "WHERE r.name = :rep AND v.name = :voc ";
                if(!searchEvent.getSearchStrings().isEmpty()){
                    queryString += "AND ";
                }
            }
            else{
                queryString += "FROM Term t WHERE ";
            }

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    if(param.equals("NAME")){
                        queryString += "lower(t." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                    }
                    else if(param.equals("ALIAS")){
                        queryString += "lower(a) like lower(:" + param.toLowerCase() + ") AND ";
                    }
                    else {
                        queryString += "lower(d." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                    }
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }
            if(splits != null){
                if(!splits[0].startsWith("\"") && !splits[0].endsWith("\"")) splits[0] = "\"" + splits[0] + "\"";
                if(!splits[1].startsWith("\"") && !splits[1].endsWith("\"")) splits[1] = "\"" + splits[1] + "\"";
                query.setParameter("rep", splits[0]);
                query.setParameter("voc", splits[1]);
            }
            List results = query.list();
            Logger.log(results.size() + "");

            int id = -1;
            for (Object o : results){
                found.add((Term) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }

    @Override
    public Term getTerm(String repository, String catalogue, String term){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || catalogue == null || term == null) throw new IllegalArgumentException("Null parameter");

        Term result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!catalogue.startsWith("\"") && !catalogue.endsWith("\"")) catalogue = "\"" + catalogue + "\"";
        if(!term.startsWith("\"") && !term.endsWith("\"")) term = "\"" + term + "\"";
        try {
            Query query = session.createQuery("SELECT t FROM Repository r JOIN r.glossaries v JOIN v.terms t " +
                    "WHERE lower(r.name) = lower(:repo) AND lower(v.name) = lower(:voc) AND lower(t.name) = lower(:term)");
            query.setParameter("repo", repository);
            query.setParameter("voc", catalogue);
            query.setParameter("term", term);
            Object o = query.getSingleResult();

            result = (Term)o;

        } catch (HibernateException e) {
            e.printStackTrace();
        } catch (Throwable ex) {
            return null;
        }

        return result;
    }

    @Override
    public Repository getRepository(String repository){
        if(session == null || !session.isOpen()) return null;
        if(repository == null) throw new IllegalArgumentException("Null parameter");

        Repository result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        try {
            session.clear();
            Query query = session.createQuery("FROM Repository WHERE lower(name) = lower(:name)");
            query.setParameter("name", repository);
            Object o = query.getSingleResult();
            //tx.commit();

            result = (Repository) o;

        } catch (Throwable ex) {
            return null;
        }
        return result;
    }

    @Override
    public MDM getMDM(String repository, String mdm){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || mdm == null) throw new IllegalArgumentException("Null parameter");

        MDM result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!mdm.startsWith("\"") && !mdm.endsWith("\"")) mdm = "\"" + mdm + "\"";
        try {
            //session.clear();
            Query query = session.createQuery("SELECT m FROM Repository r JOIN r.mdms m WHERE lower(r.name) = lower(:repo) " +
                    "AND lower(m.name) = lower(:mdm)");
            query.setParameter("repo", repository);
            query.setParameter("mdm", mdm);
            Object o = query.getSingleResult();
            //tx.commit();

            result = (MDM) o;

        } catch (Throwable ex) {
            return null;
        }
        return result;
    }

    @Override
    public Dimension getDimension(String repository, String mdm, String dimension){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || mdm == null || dimension == null) throw new IllegalArgumentException("Null parameter");

        Dimension result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!mdm.startsWith("\"") && !mdm.endsWith("\"")) mdm = "\"" + mdm + "\"";
        if(!dimension.startsWith("\"") && !dimension.endsWith("\"")) dimension = "\"" + dimension + "\"";
        try {
            session.clear();
            Query query = session.createQuery("SELECT d FROM Repository r JOIN r.mdms m JOIN m.dimensions d " +
                    "WHERE lower(r.name) = lower(:repo) AND lower(m.name) = lower(:mdm) AND lower(d.name) = lower(:dim)");
            query.setParameter("repo", repository);
            query.setParameter("mdm", mdm);
            query.setParameter("dim", dimension);
            Object o = query.getSingleResult();
            //tx.commit();

            result = (Dimension) o;

        } catch (Throwable ex) {
            return null;
        }
        return result;
    }

    @Override
    public Catalogue getCatalogue(String repository, String catalogue){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || catalogue == null) throw new IllegalArgumentException("Null parameter");

        Catalogue result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!catalogue.startsWith("\"") && !catalogue.endsWith("\"")) catalogue = "\"" + catalogue + "\"";
        try {
            //session.clear();
            Query query = session.createQuery("SELECT c FROM Repository r JOIN r.catalogues c WHERE lower(r.name) = lower(:repo) " +
                    "AND lower(c.name) = lower(:cat)");
            query.setParameter("repo", repository);
            query.setParameter("cat", catalogue);
            Object o = query.getSingleResult();

            result = (Catalogue) o;

        } catch (Throwable ex) {
            return null;
        }
        return result;
    }

    @Override
    public Glossary getGlossary(String repository, String glossary){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || glossary == null) throw new IllegalArgumentException("Null parameter");

        Glossary result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!glossary.startsWith("\"") && !glossary.endsWith("\"")) glossary = "\"" + glossary + "\"";
        try {
            //session.clear();
            Query query = session.createQuery("SELECT v FROM Repository r JOIN r.glossaries v WHERE lower(r.name) = lower(:repo) " +
                    "AND lower(v.name) = lower(:voc)");
            query.setParameter("repo", repository);
            query.setParameter("voc", glossary);
            Object o = query.getSingleResult();

            result = (Glossary) o;

        } catch (Throwable ex) {
            return null;
        }
        return result;
    }

    @Override
    public PatternDescription getPatternDescription(String repository, String catalogue, String pattern, String description){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || catalogue == null || pattern == null || description == null) throw new IllegalArgumentException("Null parameter");

        PatternDescription result = null;
        /*if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!catalogue.startsWith("\"") && !catalogue.endsWith("\"")) catalogue = "\"" + catalogue + "\"";
        if(!pattern.startsWith("\"") && !pattern.endsWith("\"")) pattern = "\"" + pattern + "\"";
        if(!description.startsWith("\"") && !description.endsWith("\"")) description = "\"" + description + "\"";
        try {
            session.clear();
            Query query = session.createQuery("SELECT d FROM Repository r JOIN r.catalogues c JOIN c.patterns p " +
                    "JOIN p.descriptions d WHERE r.name = :repo AND c.name = :cat AND p.name = :pat AND d.name = :descr");
            query.setParameter("repo", repository);
            query.setParameter("cat", catalogue);
            query.setParameter("pat", pattern);
            query.setParameter("descr", description);
            Object o = query.getSingleResult();
            //tx.commit();

            result = (PatternDescription) o;

        } catch (Throwable ex) {
            return null;
        }*/
        return result;
    }

    @Override
    public List<Template> getPatternTemplates(String repository, String catalogue, String pattern, Template template){
        if(session == null || !session.isOpen()) return null;
        if(repository == null || catalogue == null || pattern == null || template == null) throw new IllegalArgumentException("Null parameter");

        List<Template> result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!catalogue.startsWith("\"") && !catalogue.endsWith("\"")) catalogue = "\"" + catalogue + "\"";
        if(!pattern.startsWith("\"") && !pattern.endsWith("\"")) pattern = "\"" + pattern + "\"";

        try {
            session.clear();
            String queryString = "SELECT t FROM Repository r JOIN r.catalogues c JOIN c.patterns p " +
                    "JOIN p.templates t WHERE lower(r.name) = lower(:repo) AND lower(c.name) = lower(:cat) AND lower(p.name) = lower(:pat)";

            if(template.getLanguage() != null){
                queryString += " AND lower(language) = lower(:l)";
            }
            if(template.getDialect() != null){
                queryString += " AND lower(dialect) = lower(:d)";
            }
            if(template.getDataModel() != null){
                queryString += " AND lower(data_model) = lower(:dm)";
            }
            if(template.getVariant() != null){
                queryString += " AND lower(variant) = lower(:v)";
            }

            Query query = session.createQuery(queryString);
            query.setParameter("repo", repository);
            query.setParameter("cat", catalogue);
            query.setParameter("pat", pattern);

            if(template.getLanguage() != null){
                query.setParameter("l", template.getLanguage());
            }
            if(template.getDialect() != null){
                query.setParameter("d", template.getDialect());
            }
            if(template.getDataModel() != null){
                query.setParameter("dm", template.getDataModel());
            }
            if(template.getVariant() != null){
                query.setParameter("v", template.getVariant());
            }

            result = query.list();

        } catch (Throwable ex) {
            return null;
        }
        return result;
    }

    @Override
    public List<Template> getTermTemplates(String repository, String glossary, String term, Template template) {
        if(session == null || !session.isOpen()) return null;
        if(repository == null || glossary == null || term == null) throw new IllegalArgumentException("Null parameter");

        List<Template> templates = new LinkedList<>();
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!glossary.startsWith("\"") && !glossary.endsWith("\"")) glossary = "\"" + glossary + "\"";
        if(!term.startsWith("\"") && !term.endsWith("\"")) term = "\"" + term + "\"";

        try {
            session.clear();

            String queryString = "SELECT tt FROM Repository r JOIN r.glossaries g JOIN g.terms t " +
                    "JOIN t.templates tt WHERE lower(r.name) = lower(:repo) AND lower(g.name) = lower(:glos) AND lower(t.name) = lower(:term)";

            if(template != null) {
                if (template.getLanguage() != null) {
                    queryString += " AND lower(language) = lower(:l)";
                }
                if (template.getDialect() != null) {
                    queryString += " AND lower(dialect) = lower(:d)";
                }
            }

            Query query = session.createQuery(queryString);
            query.setParameter("repo", repository);
            query.setParameter("glos", glossary);
            query.setParameter("term", term);

            if(template != null) {
                if (template.getLanguage() != null) {
                    query.setParameter("l", template.getLanguage());
                }
                if (template.getDialect() != null) {
                    query.setParameter("d", template.getDialect());
                }
            }

            templates = query.list();
            //tx.commit();

        } catch (Throwable ex) {
            return null;
        }
        return templates;
    }

    @Override
    public Cube getCube(String repository, String mdm, String cube) {
        if(session == null || !session.isOpen()) return null;
        if(repository == null || mdm == null || cube == null) throw new IllegalArgumentException("Null parameter");

        Cube result = null;
        if(!repository.startsWith("\"") && !repository.endsWith("\"")) repository = "\"" + repository + "\"";
        if(!mdm.startsWith("\"") && !mdm.endsWith("\"")) mdm = "\"" + mdm + "\"";
        if(!cube.startsWith("\"") && !cube.endsWith("\"")) cube = "\"" + cube + "\"";
        try {
            session.clear();
            Query query = session.createQuery("SELECT c FROM Repository r JOIN r.mdms m JOIN m.cubes c " +
                    "WHERE lower(r.name) = lower(:repo) AND lower(m.name) = lower(:mdm) AND lower(c.name) = lower(:cube)");
            query.setParameter("repo", repository);
            query.setParameter("mdm", mdm);
            query.setParameter("cube", cube);
            Object o = query.getSingleResult();
            //tx.commit();

            result = (Cube) o;

        } catch (Throwable ex) {
            return null;
        }
        return result;
    }

    @Override
    public List<Repository> searchRepositories(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<Repository> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;

            queryString += "SELECT r FROM Repository r " +
                    "WHERE ";

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    queryString += "lower(r." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }

            List results = query.list();

            int id = -1;
            for (Object o : results){
                found.add((Repository) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }

    @Override
    public List<Glossary> searchGlossaries(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<Glossary> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;
            if(searchEvent.getSearchPath() != null){
                String s = searchEvent.getSearchPath().replace("\"", "");
                splits = s.split("/");
                queryString += "SELECT v FROM Repository r " +
                        "JOIN r.vocabularies v WHERE r.name = :rep ";
                if(!searchEvent.getSearchStrings().isEmpty()){
                    queryString += "AND ";
                }
            }
            else{
                queryString += "FROM Vocabulary v WHERE ";
            }

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    queryString += "lower(v." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }
            if(splits != null){
                if(!splits[0].startsWith("\"") && !splits[0].endsWith("\"")) splits[0] = "\"" + splits[0] + "\"";
                query.setParameter("rep", splits[0]);
            }
            List results = query.list();

            int id = -1;
            for (Object o : results){
                found.add((Glossary) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }

    @Override
    public List<Catalogue> searchCatalogues(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<Catalogue> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;
            if(searchEvent.getSearchPath() != null){
                String s = searchEvent.getSearchPath().replace("\"", "");
                splits = s.split("/");
                queryString += "SELECT c FROM Repository r " +
                        "JOIN r.catalogues c WHERE r.name = :rep ";
                if(!searchEvent.getSearchStrings().isEmpty()){
                    queryString += "AND ";
                }
            }
            else{
                queryString += "FROM Catalogue c WHERE ";
            }

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    queryString += "lower(c." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }
            if(splits != null){
                if(!splits[0].startsWith("\"") && !splits[0].endsWith("\"")) splits[0] = "\"" + splits[0] + "\"";
                query.setParameter("rep", splits[0]);
            }
            List results = query.list();

            int id = -1;
            for (Object o : results){
                found.add((Catalogue) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }

    @Override
    public List<MDM> searchMDMs(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<MDM> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;
            if(searchEvent.getSearchPath() != null){
                String s = searchEvent.getSearchPath().replace("\"", "");
                splits = s.split("/");
                queryString += "SELECT m FROM Repository r " +
                        "JOIN r.mdms m WHERE r.name = :rep ";
                if(!searchEvent.getSearchStrings().isEmpty()){
                    queryString += "AND ";
                }
            }
            else{
                queryString += "FROM MDM m WHERE ";
            }

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    queryString += "lower(m." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }
            if(splits != null){
                if(!splits[0].startsWith("\"") && !splits[0].endsWith("\"")) splits[0] = "\"" + splits[0] + "\"";
                query.setParameter("rep", splits[0]);
            }
            List results = query.list();

            int id = -1;
            for (Object o : results){
                found.add((MDM) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }

    @Override
    public List<Cube> searchCubes(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<Cube> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;
            if(searchEvent.getSearchPath() != null){
                String s = searchEvent.getSearchPath().replace("\"", "");
                splits = s.split("/");
                queryString += "SELECT c FROM Repository r " +
                        "JOIN r.mdms m JOIN m.cubes c WHERE r.name = :rep AND m.name = :mdm ";
                if(!searchEvent.getSearchStrings().isEmpty()){
                    queryString += "AND ";
                }
            }
            else{
                queryString += "FROM Cube c WHERE ";
            }

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    queryString += "lower(c." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }
            if(splits != null){
                if(!splits[0].startsWith("\"") && !splits[0].endsWith("\"")) splits[0] = "\"" + splits[0] + "\"";
                if(!splits[1].startsWith("\"") && !splits[1].endsWith("\"")) splits[1] = "\"" + splits[1] + "\"";
                query.setParameter("rep", splits[0]);
                query.setParameter("mdm", splits[1]);
            }
            List results = query.list();

            for (Object o : results){
                found.add((Cube) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }

    @Override
    public List<Dimension> searchDimensions(SearchEvent searchEvent){
        if(session == null || !session.isOpen()) return null;
        if(searchEvent == null) throw new IllegalArgumentException("Null parameter");

        List<Dimension> found = new LinkedList<>();
        try {
            session.clear();
            Query query = null;
            String queryString = "";
            String[] splits = null;
            if(searchEvent.getSearchPath() != null){
                String s = searchEvent.getSearchPath().replace("\"", "");
                splits = s.split("/");
                queryString += "SELECT d FROM Repository r " +
                        "JOIN r.mdms m JOIN m.dimensions d WHERE r.name = :rep AND m.name = :mdm ";
                if(!searchEvent.getSearchStrings().isEmpty()){
                    queryString += "AND ";
                }
            }
            else{
                queryString += "FROM Dimension d WHERE ";
            }

            //todo add path
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    queryString += "lower(d." + param.toLowerCase() + ") like lower(:" + param.toLowerCase() + ") AND ";
                }
            }
            queryString = queryString.substring(0, queryString.length() - 5);

            query = session.createQuery(queryString);
            for(String s : searchEvent.getSearchStrings()){
                for(String param : searchEvent.getParametersForSearchString(s)){
                    String value = s.replace("\"", "");
                    query.setParameter(param.toLowerCase(), "%" + value + "%");
                }
            }
            if(splits != null){
                if(!splits[0].startsWith("\"") && !splits[0].endsWith("\"")) splits[0] = "\"" + splits[0] + "\"";
                if(!splits[1].startsWith("\"") && !splits[1].endsWith("\"")) splits[1] = "\"" + splits[1] + "\"";
                query.setParameter("rep", splits[0]);
                query.setParameter("mdm", splits[1]);
            }
            List results = query.list();

            for (Object o : results){
                found.add((Dimension) o);
            }

        }catch (Throwable ex) {
            return null;
        }
        return found;
    }
}
