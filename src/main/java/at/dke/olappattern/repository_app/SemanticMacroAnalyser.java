package at.dke.olappattern.repository_app;

import at.dke.olappattern.macro.MacroBaseListener;
import at.dke.olappattern.macro.MacroParser;

import java.util.LinkedList;
import java.util.List;

/**
 * A class that generates an executable query from an instantiated pattern or term template
 */
public class SemanticMacroAnalyser extends MacroBaseListener {
    private MacroResolver macroResolver = null;
    private String language = null;
    private String dialect = null;
    public String query = "";

    private enum Macro { NONE, EXECUTE, DIM_KEY }
    private Macro macro = Macro.NONE;

    private List<String> macroArguments = null;

    @Override
    public void enterMacrol(MacroParser.MacrolContext ctx) {
        super.enterMacrol(ctx);
        query = "";
        if(macroResolver == null) throw new IllegalStateException("No MacroResolver set!");
    }

    @Override
    public void enterTxt(MacroParser.TxtContext ctx) {
        super.enterTxt(ctx);
        String s = ctx.getText().replace("}*", "");
        s = s.replace("*{", "");
        query += s;
    }

    @Override
    public void enterMacro_call(MacroParser.Macro_callContext ctx) {
        super.enterMacro_call(ctx);
        macro = Macro.NONE;
        macroArguments = new LinkedList<>();
    }

    @Override
    public void enterMacro_name(MacroParser.Macro_nameContext ctx) {
        super.enterMacro_name(ctx);
        if(ctx.getText().equals("$expr")){
            macro = Macro.EXECUTE;
        }
        else if(ctx.getText().equals("$dimKey")){
            macro = Macro.DIM_KEY;
        }
    }

    @Override
    public void enterMacro_call_argument(MacroParser.Macro_call_argumentContext ctx) {
        super.enterMacro_call_argument(ctx);
        macroArguments.add(ctx.getText());
    }

    @Override
    public void exitMacro_call(MacroParser.Macro_callContext ctx) {
        super.exitMacro_call(ctx);
        switch(macro){
            case EXECUTE:
                String termName = macroArguments.remove(0);
                String s = this.macroResolver.expr(termName, language, dialect, macroArguments.toArray(new String[macroArguments.size()]));
                if(s != null) query += s;
                else throw new IllegalStateException("Could not resolve Macro " + ctx.getText());
                break;
            case DIM_KEY:
                String dimName = macroArguments.remove(0);
                String baseLevel = this.macroResolver.dimKey(dimName);
                if(baseLevel != null) query += baseLevel;
                else throw new IllegalStateException("Could not resolve Macro " + ctx.getText());
                break;
        }
    }

    //**********************************************************************
    //Local methods
    public void setMacroResolver(MacroResolver macroResolver) {
        this.macroResolver = macroResolver;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }
}
