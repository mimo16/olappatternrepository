package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.MDM.*;
import at.dke.olappattern.data.constraint.*;
import at.dke.olappattern.data.localcube.*;
import at.dke.olappattern.data.value.*;
import at.dke.olappattern.data.variable.*;
import at.dke.olappattern.language.EMDMLBaseListener;
import at.dke.olappattern.language.EMDMLParser;
import at.dke.olappattern.data.BasicDescription;
import at.dke.olappattern.data.Context;
import at.dke.olappattern.data.Target;
import at.dke.olappattern.data.Template;
import at.dke.olappattern.data.pattern.Pattern;
import at.dke.olappattern.data.pattern.PatternDescription;
import at.dke.olappattern.data.repository.Catalogue;
import at.dke.olappattern.data.repository.Glossary;
import at.dke.olappattern.data.repository.OrganizationElement;
import at.dke.olappattern.data.repository.Repository;
import at.dke.olappattern.data.term.Term;
import at.dke.olappattern.data.term.TermDescription;
import at.dke.olappattern.data.term.Type;
import at.dke.olappattern.repository_app.event.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class SemanticAnalyzer extends EMDMLBaseListener {
    //****************************** EVENT HANDLING ******************************//

    private List<TermListener> termListeners = new ArrayList<>();

    public synchronized void addTermListener( TermListener l ) {
        termListeners.add( l );
    }

    public synchronized void removeTermListener( TermListener l ) {
        termListeners.remove( l );
    }

    private synchronized void fireTermEvent(TermEvent.Action action) {
        TermEvent termEvent = new TermEvent(this, term, action, path);
        for(TermListener l : termListeners){
            l.processTerm(termEvent);
        }
    }

    private List<PatternListener> patternListeners = new ArrayList<>();

    public synchronized void addPatternListener( PatternListener l ) {
        patternListeners.add( l );
    }

    public synchronized void removePatternListener( PatternListener l ) {
        patternListeners.remove( l );
    }

    private synchronized void firePatternEvent(PatternEvent.Action action) {
        PatternEvent patternEvent = new PatternEvent(this, pattern, action, path);
        for(PatternListener l : patternListeners){
            l.processPattern(patternEvent);
        }
    }

    private synchronized void firePatternInstantiationEvent(){
        PatternEvent patternEvent = new PatternEvent(this, pattern, PatternEvent.Action.INSTANTIATE, path);
        for(PatternListener l : patternListeners){
            l.processPatternInstantiation(toInstantiate, patternEvent);
        }
    }

    private List<MDMListener> mdmListeners = new ArrayList<>();

    public synchronized void addMDMListener( MDMListener l ) {
        mdmListeners.add( l );
    }

    public synchronized void removeMDMListener( MDMListener l ) {
        mdmListeners.remove( l );
    }

    private synchronized void fireMDMEvent(MDMEntity entity, MDMEvent.Action action) {
        MDMEvent mdmEvent = new MDMEvent(this, entity, action, path);
        for(MDMListener l : mdmListeners){
            l.processMDMElement(mdmEvent);
        }
    }

    //****************************** PARSING ******************************//

    private enum Statement { NONE, CUBE_DEF, DIMENSION_DEF, TERM, PATTERN}
    private Statement statement = Statement.NONE;

    private Cube cube = null;
    private Measure measure = null;
    private DimensionRole dimensionRole = null;
    private Dimension dimension;
    private Level level = null;
    private Attribute attribute = null;
    private RollUp rollup = null;
    private Term term = null;
    private Context context = null;
    private TypeConstraint typeConstraint = null;
    private PropertyConstraint propertyConstraint = null;
    private TermDescription termDescription = null;
    private BasicDescription basicDescription = null;
    private Template template = null;
    private Pattern pattern = null;
    private DomainDerivationRule domainDerivationRule = null;
    private ReturnTypeDerivationRule returnTypeDerivationRule = null;
    private PatternDescription patternDescription = null;
    private VariableBuilder vb = null;

    //Stacks allow nested definitions
    private Stack<VariableBuilder> variableCallStack = null;
    private Stack<VariableAccess> variableAccessStack = null;

    //stores variables for scope constraints and bindings
    private List<VariableBuilder> tempVariables = null;

    //stores element_exp
    private List<Target> tempTargets = null;

    //stores variables of a for_exp
    private List<VariableBuilder> forVariables = null;

    private TupleValue tuple = null;

    //Stack to allow nested tuple definitions
    private Stack<TupleValue> tupleCallStack = null;
    private VariableAccess variableAccess = null;

    private enum ValueSetPurpose { NONE, MEASURE_DOMAIN, LEVEL_DOMAIN, ATTRIBUTE_DOMAIN }
    private ValueSetPurpose valueSetPurpose = ValueSetPurpose.NONE;

    private enum LevelNamePurpose { NONE, DECLARATION, ROLLUP_RELATION_CHILD, ROLLUP_RELATION_PARENT, DESCRIBED_BY_RELATION}
    private LevelNamePurpose levelNamePurpose = LevelNamePurpose.NONE;

    private enum AttributeNamePurpose { NONE, DECLARATION, DESCRIBED_BY_RELATION }
    private AttributeNamePurpose attributeNamePurpose = AttributeNamePurpose.NONE;

    private enum TermNamePurpose { NONE, TERM_DEFINITION, TERM_DESCRIPTION, TERM_ALIAS, TERM_TEMPLATE, DELETE }
    private TermNamePurpose termNamePurpose = TermNamePurpose.NONE;

    private enum ElementExpressionPurpose { NONE, PARAMETER, SCOPE_CONSTRAINT, DERIVATION_RULE, BINDING }
    private ElementExpressionPurpose elementExpressionPurpose = ElementExpressionPurpose.NONE;

    private enum TextPurpose { NONE, DESCRIPTION, TEMPLATE}
    private TextPurpose textPurpose = TextPurpose.NONE;

    private enum PatternNamePurpose { NONE, PATTERN_DEFINITION, PATTERN_DESCRIPTION, PATTERN_TEMPLATE,
        PATTERN_TO_INSTANTIATE, PATTERN_INSTANTIATED, DELETE, PATTERN_EXECUTION }
    private PatternNamePurpose patternNamePurpose = PatternNamePurpose.NONE;

    private enum TypePurpose { NONE, TYPE_CONSTRAINT, PROPERTY_CONSTRAINT, TYPE_LOCAL_CUBE, PROPERTY_LOCAL_CUBE}
    private TypePurpose typePurpose = TypePurpose.NONE;

    private enum IndexPurpose { NONE, VARIABLE_ACCESS_COL_INDEX, VARIABLE_ACCESS_ROW_INDEX}
    private IndexPurpose indexPurpose = IndexPurpose.NONE;

    private boolean isDeclaration = false;
    private boolean isElement = false;
    private boolean isMVConstraint = false;
    private int position = 1;
    private SingleValue constant = null;
    private boolean isIndexVariable = false;
    private boolean isForVariable = false;
    private boolean isVariableAccess = false;
    private String toInstantiate = null;
    private String bindingVarName = null;
    private List<String> bindingAccessStrings = null;
    private String path = null;

    //region local methods **************************************************************************//

    private void addVariableToContext(VariableBuilder vb, String variableKind, TypeConstraint typeConstraint){
        vb.setVariableRole(variableKind);
        if(context != null){
            if(vb.getKind().equals("SINGLE_VARIABLE")){
                SingleVariable v = vb.buildSingleVariable();
                context.addTarget(v);
                typeConstraint.setElement(v);
                context.addConstraint(typeConstraint);
            }
            else if(vb.getKind().equals("ARRAY_VARIABLE")){
                ArrayVariable v = vb.buildArrayVariable();
                context.addTarget(v);
                typeConstraint.setElement(v);
                context.addConstraint(typeConstraint);
            }
            else if(vb.getKind().equals("MAP_VARIABLE")){
                MapVariable v = vb.buildMapVariable();
                context.addTarget(v);
                typeConstraint.setElement(v);
                context.addConstraint(typeConstraint);
            }
        }
    }

    /* converts Targets if necessary, in order to be applied in constraints (or derivation rules), i.e. all Targets
    *  except VariableBuilders are returned without changes. For every VariableBuilder the corresponding Variable
    *  from the context is returned. If no such Variable is defined, null is returned.
    */
    private Target convertToConstraintTarget(Target t){
        if(t instanceof VariableBuilder){
            VariableBuilder temp = (VariableBuilder)t;
            Variable v = context.getVariableByName(temp.getName());
            if(v == null){
                if(forVariables != null && !forVariables.isEmpty()){
                    for(VariableBuilder var : forVariables){
                        if(temp.getName().equals(var.getName())){
                            return context.getVariableByName((forVariables.get(forVariables.size()-1)).getName());
                        }
                    }
                }
                return null;
            }
            return v;
        }
        return t;
    }

    public VariableAccess createVariableAccess(String varName, Integer colIndex) {
        if (varName == null) return null;
        Variable var = context.getVariableByName(varName);

        if(var instanceof ArrayVariable){
            ArrayVariableAccess ava = new ArrayVariableAccess();
            ArrayVariable v = (ArrayVariable) var;
            ava.setVariable(v);
            ava.setColumnIndex(colIndex);
            return ava;
        }else if(var instanceof MapVariable){
            MapVariableAccess mva = new MapVariableAccess();
            MapVariable v = (MapVariable)var;
            mva.setVariable(v);
            mva.setColumnIndex(colIndex);
            return mva;
        }else if(var instanceof SingleVariable){
            SingleVariableAccess sva = new SingleVariableAccess();
            SingleVariable v = (SingleVariable) var;
            sva.setVariable(v);
            sva.setColumnIndex(colIndex);
            return sva;
        }

        return null;
    }

    public void createVarAccFromVarCallstack (){
        if(variableAccessStack != null && !variableAccessStack.isEmpty() && variableCallStack.isEmpty()){
            //A variable access is already created
            return;
        }
        if(forVariables != null && !forVariables.isEmpty()){
            VariableBuilder vb = variableCallStack.pop();
            variableAccess = createVariableAccess(vb.getName(), null);
            if(variableAccess != null){
                variableAccessStack.push(variableAccess);
                return;
            }

            for(VariableBuilder temp : forVariables){
                if(temp.getName().equals(vb.getName())) {
                    variableAccess = createVariableAccess((forVariables.get(forVariables.size()-1)).getName(), null);
                    if(variableAccess == null ) throw new IllegalStateException("Cannot parse variable access");
                    variableAccessStack.push(variableAccess);
                    break;
                }
            }
        }
        else{
            VariableBuilder vb = variableCallStack.pop();
            variableAccess = createVariableAccess(vb.getName(), null);
            if(variableAccess != null){
                variableAccessStack.push(variableAccess);
                return;
            }
        }
    }

    //endregion *************************************************************************************//

    @Override
    public void enterEmdm_stmt(EMDMLParser.Emdm_stmtContext ctx) {
        super.enterEmdm_stmt(ctx);
        this.statement = Statement.NONE;
        variableCallStack = new Stack<>();
        tupleCallStack = new Stack<>();
        variableAccessStack = new Stack<>();
        path = null;
    }

    @Override
    public void enterC_stmt(EMDMLParser.C_stmtContext ctx) {
        super.enterC_stmt(ctx);
        path = null;
    }



    @Override
    public void enterCube_def(EMDMLParser.Cube_defContext ctx) {
        this.statement = Statement.CUBE_DEF;
        super.enterCube_def(ctx);
        cube = new Cube();
        measure = null;
    }

    @Override
    public void enterCube_name(EMDMLParser.Cube_nameContext ctx) {
        super.enterCube_name(ctx);
        String[] pathSplits = ctx.getText().split("/");
        if(pathSplits.length < 3) throw new IllegalArgumentException("Not a valid cube path");

        String cubeName = pathSplits[pathSplits.length-1];
        path = ctx.getText().replace("/" + cubeName, "");

        if(cube != null) this.cube.setName(cubeName);
        else if (isDelete) fireMDMEvent(new Cube(cubeName), MDMEvent.Action.DELETE);
    }

    @Override
    public void enterMeas_decl(EMDMLParser.Meas_declContext ctx) {
        super.enterMeas_decl(ctx);
        measure = new Measure();
        if(cube != null){ cube.addMeasure(measure); }
        valueSetPurpose = ValueSetPurpose.MEASURE_DOMAIN;
    }

    @Override
    public void enterMeas_name(EMDMLParser.Meas_nameContext ctx) {
        if(measure != null) measure.setName(ctx.getText());
        super.enterMeas_name(ctx);
    }

    @Override
    public void enterVal_set_name(EMDMLParser.Val_set_nameContext ctx) {
        super.enterVal_set_name(ctx);

        switch (valueSetPurpose){
            case MEASURE_DOMAIN:
                if(measure != null) measure.setDomain(new NumberValueSet(ctx.getText()));
                valueSetPurpose = ValueSetPurpose.NONE;
                break;
            case LEVEL_DOMAIN:
                if(level != null) level.setDomain(new StringValueSet(ctx.getText()));
                valueSetPurpose = ValueSetPurpose.NONE;
                break;
            case ATTRIBUTE_DOMAIN:
                if(attribute != null) attribute.setDomain(new StringValueSet(ctx.getText()));
                valueSetPurpose = ValueSetPurpose.NONE;
                break;
        }
    }

    @Override
    public void enterDim_role_decl(EMDMLParser.Dim_role_declContext ctx) {
        super.enterDim_role_decl(ctx);
        dimensionRole = new DimensionRole();
        if(cube != null) { cube.addDimensionRole(dimensionRole); }
    }

    @Override
    public void enterDim_role_name(EMDMLParser.Dim_role_nameContext ctx) {
        super.enterDim_role_name(ctx);
        if(dimensionRole != null) dimensionRole.setName(ctx.getText());
    }

    @Override
    public void enterDim_name(EMDMLParser.Dim_nameContext ctx) {
        super.enterDim_name(ctx);
        if(isDelete){
            String[] pathSplits = ctx.getText().split("/");
            if(pathSplits.length < 3) throw new IllegalArgumentException("Not a valid dimension path");
            String dimName = pathSplits[pathSplits.length-1];
            path = ctx.getText().replace("/" + dimName, "");

            fireMDMEvent(new Dimension(dimName), MDMEvent.Action.DELETE);
        }
        else {
            switch (statement) {
                case DIMENSION_DEF:
                    String[] pathSplits = ctx.getText().split("/");
                    if(pathSplits.length < 3) throw new IllegalArgumentException("Not a valid dimension path");
                    String dimName = pathSplits[pathSplits.length-1];
                    path = ctx.getText().replace("/" + dimName, "");

                    if (dimension != null) {
                        dimension.setName(dimName);
                    }
                    break;
            }
        }
    }

    @Override
    public void enterDim_loc_name(EMDMLParser.Dim_loc_nameContext ctx) {
        super.enterDim_loc_name(ctx);
        if (dimensionRole != null) dimensionRole.setDimension(new Dimension(ctx.getText()));
    }

    @Override
    public void exitCube_def(EMDMLParser.Cube_defContext ctx) {
        super.exitCube_def(ctx);
        fireMDMEvent(cube, MDMEvent.Action.CREATE);
        statement = Statement.NONE;
    }

    @Override
    public void enterDim_def(EMDMLParser.Dim_defContext ctx) {
        super.enterDim_def(ctx);
        statement = Statement.DIMENSION_DEF;
        dimension = new Dimension();
    }

    @Override
    public void enterLvl_decl(EMDMLParser.Lvl_declContext ctx) {
        super.enterLvl_decl(ctx);
        level = new Level();
        if(dimension != null){ dimension.addLevel(level); }
        levelNamePurpose = LevelNamePurpose.DECLARATION;
        valueSetPurpose = ValueSetPurpose.LEVEL_DOMAIN;
    }

    @Override
    public void enterLvl_name(EMDMLParser.Lvl_nameContext ctx) {
        super.enterLvl_name(ctx);
        switch (levelNamePurpose){
            case DECLARATION:
                if(level != null){ level.setName(ctx.getText()); }
                levelNamePurpose = LevelNamePurpose.NONE;
                break;
            case ROLLUP_RELATION_CHILD:
                if(dimension != null && rollup != null) rollup.setChildLevel(dimension.getLevelByName(ctx.getText()));
                levelNamePurpose = LevelNamePurpose.ROLLUP_RELATION_PARENT;
                break;
            case ROLLUP_RELATION_PARENT:
                if(dimension != null && rollup != null) rollup.setParentLevel(dimension.getLevelByName(ctx.getText()));
                levelNamePurpose = LevelNamePurpose.NONE;
                break;
            case DESCRIBED_BY_RELATION:
                if(dimension != null) {
                    level = dimension.getLevelByName(ctx.getText());
                }
                levelNamePurpose = LevelNamePurpose.NONE;
                break;
        }
    }

    @Override
    public void enterAttr_decl(EMDMLParser.Attr_declContext ctx) {
        super.enterAttr_decl(ctx);
        attribute = new Attribute();
        if(dimension != null) dimension.addAttribute(attribute);
        attributeNamePurpose = AttributeNamePurpose.DECLARATION;
        valueSetPurpose = ValueSetPurpose.ATTRIBUTE_DOMAIN;
    }

    @Override
    public void enterAttr_name(EMDMLParser.Attr_nameContext ctx) {
        super.enterAttr_name(ctx);
        switch(attributeNamePurpose){
            case DECLARATION:
                if(attribute != null) attribute.setName(ctx.getText());
                attributeNamePurpose = AttributeNamePurpose.NONE;
                break;
            case DESCRIBED_BY_RELATION:
                if(dimension != null) {
                    attribute = dimension.getAttributeByName(ctx.getText());
                    if(level != null && attribute != null) {
                        attribute.setLevel(level);
                        dimension.addDescribedByRelation(attribute, level);
                    }
                }
                break;
        }
    }

    @Override
    public void enterRoll_up_rel_decl(EMDMLParser.Roll_up_rel_declContext ctx) {
        super.enterRoll_up_rel_decl(ctx);
        rollup = new RollUp();
        if(dimension != null) dimension.addRollUp(rollup);
        levelNamePurpose = LevelNamePurpose.ROLLUP_RELATION_CHILD;
    }

    @Override
    public void enterDescr_by_rel_decl(EMDMLParser.Descr_by_rel_declContext ctx) {
        super.enterDescr_by_rel_decl(ctx);
        levelNamePurpose = LevelNamePurpose.DESCRIBED_BY_RELATION;
        attributeNamePurpose = AttributeNamePurpose.DESCRIBED_BY_RELATION;
        attribute = null;
        level = null;
    }

    @Override
    public void exitDim_def(EMDMLParser.Dim_defContext ctx) {
        super.exitDim_def(ctx);
        fireMDMEvent(dimension, MDMEvent.Action.CREATE);
        statement = Statement.NONE;
    }

    @Override
    public void enterT_def(EMDMLParser.T_defContext ctx) {
        super.enterT_def(ctx);
        context = term = new Term();
        termNamePurpose = TermNamePurpose.TERM_DEFINITION;
        position = 1;
    }

    @Override
    public void exitT_def(EMDMLParser.T_defContext ctx) {
        super.exitT_def(ctx);
        fireTermEvent(TermEvent.Action.CREATE);
    }

    @Override
    public void enterT_type(EMDMLParser.T_typeContext ctx) {
        super.enterT_type(ctx);
        if(term != null && term.getType() == null){
            term.setType(new Type(ctx.getText()));

            Variable variable = null;
            String type = null;

            if(term.getType().getName().equals("UNARY_CUBE_PREDICATE")){
                SingleVariable v = new SingleVariable();
                variable = v;
                type = "CUBE";
            }
            else if(term.getType().getName().equals("BINARY_CUBE_PREDICATE")){
                ArrayVariable v = new ArrayVariable();
                variable = v;
                type = "CUBE";
            }
            else if(term.getType().getName().equals("UNARY_CALCULATED_MEASURE")){
                SingleVariable v = new SingleVariable();
                variable = v;
                type = "CUBE";
            }
            else if(term.getType().getName().equals("BINARY_CALCULATED_MEASURE")){
                ArrayVariable v = new ArrayVariable();
                variable = v;
                type = "CUBE";
            }
            else if(term.getType().getName().equals("CUBE_ORDERING")){
                SingleVariable v = new SingleVariable();
                variable = v;
                type = "CUBE";
            }
            else if(term.getType().getName().equals("UNARY_DIMENSION_PREDICATE")){
                SingleVariable v = new SingleVariable();
                variable = v;
                type = "DIMENSION";
            }
            else if(term.getType().getName().equals("BINARY_DIMENSION_PREDICATE")){
                ArrayVariable v = new ArrayVariable();
                variable = v;
                type = "DIMENSION";
            }
            else if(term.getType().getName().equals("DIMENSION_GROUPING")){
                SingleVariable v = new SingleVariable();
                variable = v;
                type = "DIMENSION";
            }
            else if(term.getType().getName().equals("DIMENSION_ORDERING")){
                SingleVariable v = new SingleVariable();
                variable = v;
                type = "DIMENSION";
            }

            if(variable != null){
                variable.setName("ctx");
                variable.setValueKind("SINGLE");
                variable.setOptional('N');
                variable.setVariableRole("CONTEXT_PARAMETER");
                variable.setPosition(position++);
                context.addTarget(variable);

                SingleValue sv = new SingleValue(type);
                sv = context.addSingleValueIfNotExist(sv);
                TypeConstraint tc = new TypeConstraint();
                tc.setType(sv);
                tc.setElement(variable);
                context.addConstraint(tc);
            }
        }
    }

    @Override
    public void enterT_name(EMDMLParser.T_nameContext ctx) {
        super.enterT_name(ctx);
        String[] pathSplits = ctx.getText().split("/");
        String termName;

        if(path == null && pathSplits.length == 3) {
            //term name as contained in the first line of a create, delete etc. statements
            termName = pathSplits[2];
            path = pathSplits[0] + "/" + pathSplits[1];
        }
        else if(path != null && pathSplits.length == 1){
            //term name as in a term alias
            termName = ctx.getText();
        }
        else{
            throw new IllegalArgumentException("Not a valid term path");
        }

        switch (termNamePurpose){
            case TERM_DEFINITION:
                if(term != null) term.setName(termName);
                termNamePurpose = TermNamePurpose.NONE;
                break;
            case TERM_DESCRIPTION:
                if(termDescription != null) {
                    term = new Term(termName);
                    term.addDescription(termDescription);
                }
                termNamePurpose = TermNamePurpose.TERM_ALIAS;
                break;
            case TERM_ALIAS:
                if(termDescription != null) termDescription.addAlias(ctx.getText());
                break;
            case TERM_TEMPLATE:
                if(template != null){
                    term = new Term(termName);
                    term.addTemplate(template);
                }
                break;
            case DELETE:
                if(term == null) {
                    term = new Term(termName);
                }
                else{
                    term.setName(termName);
                }
                break;
        }

    }

    @Override
    public void enterValue_set_name(EMDMLParser.Value_set_nameContext ctx) {
        super.enterValue_set_name(ctx);
        term.setReturnType(ctx.getText());
    }

    @Override
    public void enterParam_decl(EMDMLParser.Param_declContext ctx) {
        super.enterParam_decl(ctx);
        elementExpressionPurpose = ElementExpressionPurpose.PARAMETER;
    }

    @Override
    public void exitParam_decl(EMDMLParser.Param_declContext ctx) {
        super.exitParam_decl(ctx);
        typePurpose = TypePurpose.NONE;
    }

    @Override
    public void enterVar_decl(EMDMLParser.Var_declContext ctx) {
        super.enterVar_decl(ctx);
        typeConstraint = new TypeConstraint();
        typePurpose = TypePurpose.TYPE_CONSTRAINT;
        isDeclaration = true;
    }

    @Override
    public void enterVar_exp(EMDMLParser.Var_expContext ctx) {
        super.enterVar_exp(ctx);

        vb = new VariableBuilder();
        vb.isElement = isElement;
        if(elementExpressionPurpose == ElementExpressionPurpose.PARAMETER) vb.setPosition(position++);
        variableCallStack.push(vb);
    }

    @Override
    public void enterVar_label(EMDMLParser.Var_labelContext ctx) {
        super.enterVar_label(ctx);
        if(isVariableAccess) {
            if(elementExpressionPurpose == ElementExpressionPurpose.BINDING){
                bindingVarName = ctx.getText();
            }
            else {
                //Variable Access within pattern or term context
                String varName;
                if (forVariables != null && forVariables.size() == 2) {
                    varName = forVariables.get(forVariables.size() - 1).getName();
                } else {
                    varName = ctx.getText();
                }

                variableAccess = createVariableAccess(varName, null);
                if (variableAccess == null)
                    throw new IllegalStateException("Cannot create variable access for " + ctx.getText());
                variableAccessStack.push(variableAccess);
            }
        }
        else {
            vb = variableCallStack.pop();
            vb.setName(ctx.getText());
            variableCallStack.push(vb);
        }
    }

    @Override
    public void enterTuple_exp(EMDMLParser.Tuple_expContext ctx) {
        super.enterTuple_exp(ctx);
        vb = variableCallStack.pop();
        vb.setValueKind("TUPLE");
        variableCallStack.push(vb);
    }

    @Override
    public void enterArray_exp(EMDMLParser.Array_expContext ctx) {
        super.enterArray_exp(ctx);
        if(variableCallStack != null && !variableCallStack.isEmpty()) {
            vb = variableCallStack.pop();
            vb.setKind("ARRAY_VARIABLE");
            variableCallStack.push(vb);
        }
    }

    @Override
    public void enterMap_exp(EMDMLParser.Map_expContext ctx) {
        super.enterMap_exp(ctx);
        vb = variableCallStack.pop();
        vb.setKind("MAP_VARIABLE");
        variableCallStack.push(vb);
    }

    @Override
    public void enterOp_exp(EMDMLParser.Op_expContext ctx) {
        super.enterOp_exp(ctx);
        vb = variableCallStack.pop();
        vb.setOptional('Y');
        variableCallStack.push(vb);
    }

    @Override
    public void enterType(EMDMLParser.TypeContext ctx) {
        super.enterType(ctx);
        switch(typePurpose) {
            case TYPE_CONSTRAINT:
                if (typeConstraint != null && context != null) {
                    SingleValue sv = new SingleValue(ctx.getText());
                    sv = context.addSingleValueIfNotExist(sv);
                    typeConstraint.setType(sv);
                }
                break;
            case TYPE_LOCAL_CUBE:
                if (typeLocalCube != null && context != null) {
                    SingleValue sv = new SingleValue(ctx.getText());
                    sv = context.addSingleValueIfNotExist(sv);
                    typeLocalCube.setType(sv);
                }
                break;
        }
    }

    @Override
    public void exitVar_decl(EMDMLParser.Var_declContext ctx) {
        super.exitVar_decl(ctx);
        isDeclaration = false;
        vb = variableCallStack.pop();
        if(elementExpressionPurpose == ElementExpressionPurpose.PARAMETER) addVariableToContext(vb, "PARAMETER", typeConstraint);
        else {
            addVariableToContext(vb, "DERIVED", typeConstraint);
            //todo: derivationRule.setVariable(context.getVariableByName(vb.getName()));
            tempTargets.add(context.getVariableByName(vb.getName()));
        }
    }

    @Override
    public void enterSv_cstr_decl(EMDMLParser.Sv_cstr_declContext ctx) {
        super.enterSv_cstr_decl(ctx);
        tempTargets = new LinkedList<>();
    }

    @Override
    public void enterMv_cstr_decl(EMDMLParser.Mv_cstr_declContext ctx) {
        super.enterMv_cstr_decl(ctx);
        isMVConstraint = true;
        forVariables = new LinkedList<>();
    }

    @Override
    public void enterFor_exp(EMDMLParser.For_expContext ctx) {
        super.enterFor_exp(ctx);
        isForVariable = true;
    }

    @Override
    public void exitMv_cstr_decl(EMDMLParser.Mv_cstr_declContext ctx) {
        super.exitMv_cstr_decl(ctx);
        isMVConstraint = false;
    }

    @Override
    public void enterType_cstr_decl(EMDMLParser.Type_cstr_declContext ctx) {
        super.enterType_cstr_decl(ctx);
        typePurpose = TypePurpose.TYPE_CONSTRAINT;
        typeConstraint = new TypeConstraint();
        if(!isMVConstraint && context != null) {
            this.context.addConstraint(typeConstraint);
        }
    }

    @Override
    public void enterElem_exp(EMDMLParser.Elem_expContext ctx) {
        super.enterElem_exp(ctx);
        constant = null;
        isElement = true;
    }

    @Override
    public void enterConst_name(EMDMLParser.Const_nameContext ctx) {
        super.enterConst_name(ctx);
        constant = new SingleValue(ctx.getText());
        constant = context.addSingleValueIfNotExist(constant);
    }

    @Override
    public void exitElem_exp(EMDMLParser.Elem_expContext ctx) {
        super.exitElem_exp(ctx);
        isElement = false;

        if(tupleCallStack.isEmpty()) {
            Target element = null;
            if (constant != null) {
                element = constant;
            } else if(!variableCallStack.isEmpty()) {
                element = variableCallStack.pop();
            }

            if (element != null) {
                tempTargets.add(element);
            }
        }
        constant = null;
    }

    @Override
    public void exitDom_cstr_decl(EMDMLParser.Dom_cstr_declContext ctx) {
        super.exitDom_cstr_decl(ctx);
        if (context != null && tempTargets.size() == 3) {
            DomainConstraint dc = new DomainConstraint();
            context.addConstraint(dc);
            Target domain = convertToConstraintTarget(tempTargets.remove(2));
            Target property = convertToConstraintTarget(tempTargets.remove(1));
            Target entity = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
            if(domain != null && property != null && entity != null) {
                dc.setDomain(domain);
                dc.setProperty(property);
                dc.setEntity(entity);
            }
            else{
                throw new IllegalStateException("Cannot parse Domain-Constraint " + ctx.getText());
            }
        }
        else {
            throw new IllegalStateException("Cannot parse Domain-Constraint " + ctx.getText());
        }
    }

    @Override
    public void enterProp_cstr_decl(EMDMLParser.Prop_cstr_declContext ctx) {
        super.enterProp_cstr_decl(ctx);
        typePurpose = TypePurpose.PROPERTY_CONSTRAINT;
    }

    @Override
    public void exitProp_cstr_decl(EMDMLParser.Prop_cstr_declContext ctx) {
        super.exitProp_cstr_decl(ctx);
        typePurpose = TypePurpose.NONE;
        if (propertyConstraint != null && tempTargets.size() == 2) {
            Target property = convertToConstraintTarget(tempTargets.remove(1));
            Target entity = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
            if(property == null || entity == null) throw new IllegalStateException("Cannot parse PropertyConstraint " + ctx.getText());
            propertyConstraint.setProperty(property);
            propertyConstraint.setEntity(entity);
        }
        else {
            throw new IllegalStateException("Cannot parse PropertyConstraint " + ctx.getText() + "!");
        }
    }

    @Override
    public void enterM_prop_type(EMDMLParser.M_prop_typeContext ctx) {
        super.enterM_prop_type(ctx);
        SingleValue type = new SingleValue(ctx.getText());
        if(typePurpose == TypePurpose.PROPERTY_CONSTRAINT && context != null){
            propertyConstraint = new PropertyConstraint();
            type = context.addSingleValueIfNotExist(type);
            propertyConstraint.setType(type);
            context.addConstraint(propertyConstraint);
        }
        else if(typePurpose == TypePurpose.PROPERTY_LOCAL_CUBE && context != null){
            propertyLocalCube = new PropertyLocalCube();
            type = context.addSingleValueIfNotExist(type);
            propertyLocalCube.setType(type);
            context.addLocalCube(propertyLocalCube);
        }
    }

    @Override
    public void exitType_cstr_decl(EMDMLParser.Type_cstr_declContext ctx) {
        super.exitType_cstr_decl(ctx);
        typePurpose = TypePurpose.NONE;
        if(typeConstraint != null && tempTargets.size() == 1){
            Target element = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
            if(element != null) {
                typeConstraint.setElement(element);
            }
            else{
                throw new IllegalStateException("Cannot parse TypeConstraint " + ctx.getText());
            }
        }
        else{
            throw new IllegalStateException("Cannot parse TypeConstraint " + ctx.getText());
        }
    }

    @Override
    public void enterScope_cstr_decl(EMDMLParser.Scope_cstr_declContext ctx) {
        super.enterScope_cstr_decl(ctx);
        elementExpressionPurpose = ElementExpressionPurpose.SCOPE_CONSTRAINT;
        tempVariables = new LinkedList<>();
    }

    @Override
    public void exitVar_exp(EMDMLParser.Var_expContext ctx) {
        super.exitVar_exp(ctx);

        if(variableCallStack.isEmpty() && !variableAccessStack.isEmpty()){
            variableAccess = variableAccessStack.pop();
            variableAccess = context.addVariableAccessIfNotExists(variableAccess);
            if (variableAccess == null)
                throw new IllegalStateException("Cannot parse variable access " + ctx.getText());

            if(variableAccessStack.isEmpty()) {
                tempTargets.add(variableAccess);
            }
            else{
                VariableAccess parent = variableAccessStack.pop();
                if(parent instanceof MapVariableAccess){
                    MapVariableAccess mva = (MapVariableAccess) parent;
                    Target t = mva.getRowIndex();
                    if(t instanceof TupleValue){
                        TupleValue tv = (TupleValue) t;
                        tv.addTupleEntry(new TupleEntry(variableAccess));
                        mva.setRowIndex(tv);

                    }
                    else{
                        mva.setRowIndex(variableAccess);
                    }
                    variableAccessStack.push(mva);
                }
                else{
                    throw new IllegalStateException("Cannot parse variable access " + ctx.getText());
                }
            }
        }
        else if(!variableCallStack.isEmpty()) {
            vb = variableCallStack.pop();
            if (isDeclaration || vb.isElement || !tupleCallStack.isEmpty()) {
                //elements are treated in the exitElem_exp
                //This is the same for definitions and instantiations
                variableCallStack.push(vb);
            } else if (elementExpressionPurpose == ElementExpressionPurpose.BINDING) {
                //It is a variable for which a binding is formulated
                //EXAMPLE <c> = ...
                tempVariables.add(vb);
                if (context.getVariableByName(vb.getName()) == null) {
                    context.addVariableIfNotExists(vb.buildVariable());
                }
            } else if (isForVariable) {
                forVariables.add(vb);
            }
            else{
                tempTargets.add(vb);
            }
        }
        else throw new IllegalStateException("Cannot parse variable expression " + ctx.getText());
    }

    @Override
    public void enterIdx_no(EMDMLParser.Idx_noContext ctx) {
        super.enterIdx_no(ctx);
        Integer index = Integer.parseInt(ctx.getText());
        switch (indexPurpose){
            case VARIABLE_ACCESS_COL_INDEX:
                if(variableAccessStack != null && !variableAccessStack.isEmpty()) {
                    variableAccess = variableAccessStack.pop();
                    variableAccess.setColumnIndex(index);
                    variableAccessStack.push(variableAccess);
                }
                break;
            case VARIABLE_ACCESS_ROW_INDEX:
                if(variableAccessStack != null && !variableAccessStack.isEmpty()) {
                    variableAccessStack.pop();
                    if (variableAccess instanceof ArrayVariableAccess) {
                        ((ArrayVariableAccess) variableAccess).setRowIndex(Integer.parseInt(ctx.getText()));
                    }
                    variableAccessStack.push(variableAccess);
                }
                break;
        }

    }

    @Override
    public void enterIdx_name(EMDMLParser.Idx_nameContext ctx) {
        super.enterIdx_name(ctx);

        switch (indexPurpose){
            case VARIABLE_ACCESS_ROW_INDEX:
                if(elementExpressionPurpose == ElementExpressionPurpose.BINDING){
                    bindingAccessStrings.add(ctx.getText());
                }
                else {
                    if (variableAccessStack != null && !variableAccessStack.isEmpty()) {
                        variableAccessStack.pop();
                        if (variableAccess instanceof MapVariableAccess) {
                            SingleValue sv = new SingleValue(ctx.getText());
                            sv = context.addSingleValueIfNotExist(sv);
                            ((MapVariableAccess) variableAccess).setRowIndex(sv);
                        }
                        variableAccessStack.push(variableAccess);
                    }
                }
                break;
            //todo binding index?
        }
    }

    @Override
    public void exitScope_exp(EMDMLParser.Scope_expContext ctx) {
        super.exitScope_exp(ctx);
        if(tempTargets.size() == 2) {
            ScopeConstraint sc = new ScopeConstraint();
            Target scope = convertToConstraintTarget(tempTargets.remove(1));
            Target tuple = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
            if(scope != null && tuple != null){
                sc.setScope(scope);
                sc.setTuple(tuple);
                context.addConstraint(sc);
            }
            else{
                throw new IllegalStateException("Cannot parse scope expression: " + ctx.getText());
            }
        }
        else{
            throw new IllegalStateException("Cannot parse scope expression: " + ctx.getText());
        }
    }

    @Override
    public void exitScope_cstr_decl(EMDMLParser.Scope_cstr_declContext ctx) {
        super.exitScope_cstr_decl(ctx);
        elementExpressionPurpose = ElementExpressionPurpose.NONE;
    }

    @Override
    public void exitRollup_cstr_decl(EMDMLParser.Rollup_cstr_declContext ctx) {
        super.exitRollup_cstr_decl(ctx);

        if (context != null && tempTargets.size() == 4) {
            RollUpConstraint rc = new RollUpConstraint();
            Target parentLevel = convertToConstraintTarget(tempTargets.remove(3));
            tempTargets.remove(2); //index 2 and 0 both contain the dimension -> discard one
            Target childLevel = convertToConstraintTarget(tempTargets.remove(1));
            Target dimension = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
            if(dimension != null && childLevel != null && parentLevel != null) {
                rc.setDimension(dimension);
                rc.setChildLevel(childLevel);
                rc.setParentLevel(parentLevel);
                context.addConstraint(rc);
            }
            else {
                throw new IllegalStateException("Cannot parse RollUp-Constraint " + ctx.getText());
            }
        }
        else {
            throw new IllegalStateException("Cannot parse RollUp-Constraint " + ctx.getText());
        }
    }

    @Override
    public void exitDescr_cstr_decl(EMDMLParser.Descr_cstr_declContext ctx) {
        super.exitDescr_cstr_decl(ctx);

        if (context != null && tempTargets.size() == 4) {
            DescribedByConstraint dbc = new DescribedByConstraint();
            Target attribute = convertToConstraintTarget(tempTargets.remove(3));
            tempTargets.remove(2); //index 2 and 0 both contain the dimension -> discard one
            Target level = convertToConstraintTarget(tempTargets.remove(1));
            Target dimension = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
            if(attribute != null && level != null && dimension != null) {
                dbc.setDimension(tempTargets.get(0));
                dbc.setLevel(tempTargets.get(1));
                dbc.setAttribute(tempTargets.get(3));
                context.addConstraint(dbc);
            }
            else {
                throw new IllegalStateException("Cannot parse DescribedBy-Constraint " + ctx.getText());
            }
        }
        else {
            throw new IllegalStateException("Cannot parse DescribedBy-Constraint " + ctx.getText());
        }
    }

    @Override
    public void exitApp_cstr_decl(EMDMLParser.App_cstr_declContext ctx) {
        super.exitApp_cstr_decl(ctx);

        if(context != null){
            if(tempTargets.size() == 2){
                UnaryAppConstraint uac = new UnaryAppConstraint();

                Target entity = convertToConstraintTarget(tempTargets.remove(1));
                Target term = convertToConstraintTarget(tempTargets.remove(0));

                //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
                if(term != null && entity != null) {
                    uac.setTerm(term);
                    uac.setEntity(entity);
                    context.addConstraint(uac);
                }
                else{
                    throw new IllegalStateException("Cannot parse App-Constraint " + ctx.getText());
                }
            }
            else if(tempTargets.size() == 3){
                BinaryAppConstraint bac = new BinaryAppConstraint();

                Target entity2 = convertToConstraintTarget(tempTargets.remove(2));
                Target entity1 = convertToConstraintTarget(tempTargets.remove(1));
                Target term = convertToConstraintTarget(tempTargets.remove(0));

                //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
                if(term != null && entity1 != null && entity2 != null) {
                    bac.setTerm(term);
                    bac.setEntity1(entity1);
                    bac.setEntity2(entity2);
                    context.addConstraint(bac);
                }
                else{
                    throw new IllegalStateException("Cannot parse App-Constraint " + ctx.getText());
                }
            }
            else{
                throw new IllegalStateException("Cannot parse App-Constraint " + ctx.getText());
            }
        }
    }

    @Override
    public void exitReturn_cstr_decl(EMDMLParser.Return_cstr_declContext ctx) {
        super.exitReturn_cstr_decl(ctx);
        if (context != null && tempTargets.size() == 2) {
            ReturnConstraint rc = new ReturnConstraint();
            context.addConstraint(rc);

            Target returnType = convertToConstraintTarget(tempTargets.remove(1));
            Target entity = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the constraint
            if(entity != null && returnType != null) {
                rc.setEntity(entity);
                rc.setReturnType(returnType);
            }
            else{
                throw new IllegalStateException("Cannot parse Return-Constraint " + ctx.getText());
            }
        }
        else {
            throw new IllegalStateException("Cannot parse Return-Constraint " + ctx.getText());
        }
    }

    @Override
    public void exitFor_exp(EMDMLParser.For_expContext ctx) {
        super.exitFor_exp(ctx);
        isForVariable = false;
    }

    @Override
    public void enterT_descr(EMDMLParser.T_descrContext ctx) {
        super.enterT_descr(ctx);
        basicDescription = termDescription = new TermDescription();
        termNamePurpose = TermNamePurpose.TERM_DESCRIPTION;
        textPurpose = TextPurpose.DESCRIPTION;
    }

    @Override
    public void exitT_descr(EMDMLParser.T_descrContext ctx) {
        super.exitT_descr(ctx);
        textPurpose = TextPurpose.NONE;
        termNamePurpose = TermNamePurpose.NONE;
        fireTermEvent(TermEvent.Action.CREATE);
    }

    @Override
    public void enterLang_name(EMDMLParser.Lang_nameContext ctx) {
        super.enterLang_name(ctx);
        switch(textPurpose){
            case DESCRIPTION:
                if(basicDescription != null && basicDescription.getLanguage() == null) basicDescription.setLanguage(ctx.getText());
                break;
            case TEMPLATE:
                if(template != null) template.setLanguage(ctx.getText());
                break;
        }

    }

    @Override
    public void enterDescr_txt(EMDMLParser.Descr_txtContext ctx) {
        super.enterDescr_txt(ctx);
        if(termDescription != null && termDescription.getDescription() == null) termDescription.setDescription(ctx.getText());
    }

    @Override
    public void enterT_temp(EMDMLParser.T_tempContext ctx) {
        super.enterT_temp(ctx);
        template = new Template();
        termNamePurpose = TermNamePurpose.TERM_TEMPLATE;
        statement = Statement.TERM;
        statement = Statement.NONE;
        textPurpose = TextPurpose.TEMPLATE;
    }

    @Override
    public void exitT_temp(EMDMLParser.T_tempContext ctx) {
        super.exitT_temp(ctx);
        textPurpose = TextPurpose.NONE;
        statement = Statement.NONE;
        fireTermEvent(TermEvent.Action.CREATE);
    }

    @Override
    public void enterTemp_elem_meta(EMDMLParser.Temp_elem_metaContext ctx) {
        super.enterTemp_elem_meta(ctx);
        if(patternNamePurpose == PatternNamePurpose.PATTERN_EXECUTION && template == null){
            template = new Template();
            context.addTemplate(template);
        }
    }

    @Override
    public void enterModel_name(EMDMLParser.Model_nameContext ctx) {
        super.enterModel_name(ctx);
        if(template != null) template.setDataModel(ctx.getText());
    }

    @Override
    public void enterVariant_name(EMDMLParser.Variant_nameContext ctx) {
        super.enterVariant_name(ctx);
        if(template != null) template.setVariant(ctx.getText());
    }

    @Override
    public void enterDialect_name(EMDMLParser.Dialect_nameContext ctx) {
        super.enterDialect_name(ctx);
        if(template != null) template.setDialect(ctx.getText());
    }

    @Override
    public void enterTemp_txt(EMDMLParser.Temp_txtContext ctx) {
        super.enterTemp_txt(ctx);
        if(template != null) template.setExpression(ctx.getText());
    }

    //*********************************** PATTERNS **********************************//

    @Override
    public void enterP_def(EMDMLParser.P_defContext ctx) {
        super.enterP_def(ctx);
        position = 1;
        context = pattern = new Pattern();
        patternNamePurpose = PatternNamePurpose.PATTERN_DEFINITION;
    }

    @Override
    public void exitP_def(EMDMLParser.P_defContext ctx) {
        super.exitP_def(ctx);
        firePatternEvent(PatternEvent.Action.CREATE);
    }

    @Override
    public void enterP_name(EMDMLParser.P_nameContext ctx) {
        super.enterP_name(ctx);
        String[] pathSplits = ctx.getText().split("/");
        String patternName;

        if(path == null && pathSplits.length == 3) {
            //term name as contained in the first line of a create, delete etc. statements
            patternName = pathSplits[2];
            path = pathSplits[0] + "/" + pathSplits[1];
        }
        else if(path != null && pathSplits.length == 1){
            //term name as in a term alias
            patternName = ctx.getText();
        }
        else{
            throw new IllegalArgumentException("Not a valid pattern path");
        }

        switch(patternNamePurpose){
            case PATTERN_DEFINITION:
                if(pattern != null) pattern.setName(patternName);
                patternNamePurpose = PatternNamePurpose.NONE;
                break;
            case PATTERN_TO_INSTANTIATE:
                toInstantiate = ctx.getText();
                path = null;
                patternNamePurpose = PatternNamePurpose.PATTERN_INSTANTIATED;
                break;
            case PATTERN_INSTANTIATED:
                pattern.setName(patternName);
                patternNamePurpose = PatternNamePurpose.NONE;
                break;
            case PATTERN_DESCRIPTION:
                pattern = new Pattern(patternName);
                pattern.addPatternDescription(patternDescription);
                patternNamePurpose = PatternNamePurpose.NONE;
                break;
            case PATTERN_TEMPLATE:
                context = pattern = new Pattern(patternName);
                pattern.addTemplate(template);
                patternNamePurpose = PatternNamePurpose.NONE;
                break;
            case DELETE:
                if(pattern == null) {
                    pattern = new Pattern(patternName);
                }
                else{
                    pattern.setName(patternName);
                }
                break;
            case PATTERN_EXECUTION:
                context = pattern = new Pattern(patternName);
                break;
        }
    }

    @Override
    public void enterP_loc_name(EMDMLParser.P_loc_nameContext ctx) {
        super.enterP_loc_name(ctx);
        patternDescription.addRelatedPattern(ctx.getText());
    }



    @Override
    public void enterMdm_name(EMDMLParser.Mdm_nameContext ctx) {
        super.enterMdm_name(ctx);
        mdmName = ctx.getText();
    }

    @Override
    public void enterVoc_name(EMDMLParser.Voc_nameContext ctx) {
        super.enterVoc_name(ctx);
        vocabularyName = ctx.getText();
    }

    @Override
    public void enterDerv_decl(EMDMLParser.Derv_declContext ctx) {
        super.enterDerv_decl(ctx);
        elementExpressionPurpose = ElementExpressionPurpose.DERIVATION_RULE;
        forVariables = new LinkedList<>();
        tempVariables = new LinkedList<>();
        tempTargets = new LinkedList<>();
    }

    @Override
    public void exitDerv_decl(EMDMLParser.Derv_declContext ctx) {
        super.exitDerv_decl(ctx);
        elementExpressionPurpose = ElementExpressionPurpose.NONE;

        if(forVariables.size() == 2 && tempTargets.size() == 4){
            //domain derivation rule with for loop
            //EXAMPLE: tempTargets[0] : **SOME_TYPE** FOR forVariables[0] IN forVariables[1] WITH tempTargets[1] <= tempTargets[2].tempTargets[3]
            DomainDerivationRule dr = new DomainDerivationRule();

            Target property = convertToConstraintTarget(tempTargets.remove(3));
            Target entity = convertToConstraintTarget(tempTargets.remove(2));
            Target domain = convertToConstraintTarget(tempTargets.remove(1));
            dr.setVariable((Variable)tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the derivation rule
            if(domain != null && entity != null && property != null){
                dr.setDomain(domain);
                dr.setEntity(entity);
                dr.setProperty(property);
                context.addDerivationRule(dr);
            }
            else{
                throw new IllegalStateException("Cannot parse derived element: " + ctx.getText() + "\r\n" +
                        "Hint: all variables must be previously defined");
            }
        }
        else if(forVariables.isEmpty() && tempVariables.isEmpty() && tempTargets.size() == 3){
            //Domain derivation rule without for loop
            DomainDerivationRule dr = new DomainDerivationRule();

            Target property = convertToConstraintTarget(tempTargets.remove(2));
            Target entity = convertToConstraintTarget(tempTargets.remove(1));
            dr.setVariable((Variable)tempTargets.remove(0));
            dr.setDomain(dr.getVariable());

            //Check whether one of the targets is null, i.e. an undefined variable is used in the derivation rule
            if(entity != null && property != null){
                dr.setEntity(entity);
                dr.setProperty(property);
                context.addDerivationRule(dr);
            }
            else{
                throw new IllegalStateException("Cannot parse derived element: " + ctx.getText() + "\r\n" +
                        "Hint: all variables (except loop variables) must be previously defined");
            }
        }
        else if(forVariables.isEmpty() && tempTargets.size() == 2){
            ReturnTypeDerivationRule dr = new ReturnTypeDerivationRule();

            Target businessTerm = convertToConstraintTarget(tempTargets.remove(1));
            dr.setVariable((Variable)tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the derivation rule
            if(businessTerm != null){
                dr.setBusinessTerm(businessTerm);
                context.addDerivationRule(dr);
            }
            else{
                throw new IllegalStateException("Cannot parse derived element: " + ctx.getText() + "\r\n" +
                        "Hint: all variables must be previously defined");
            }
        }
        else{
            //May only apply if code is not adapted to grammar changes
            throw new IllegalStateException("Cannot parse derived element: " + ctx.getText());
        }
    }


    @Override
    public void enterSv_exp(EMDMLParser.Sv_expContext ctx) {
        super.enterSv_exp(ctx);
        //Following element(s) is/are a part of a TupleValue
        if(elementExpressionPurpose == ElementExpressionPurpose.BINDING && !tempVariables.isEmpty()){
            vb = tempVariables.get(0);
            if(!vb.getValueKind().equals("TUPLE")){
                return;
            }
        }
        tuple = new TupleValue();

        if(variableAccessStack != null && !variableAccessStack.isEmpty()) {
            variableAccess = variableAccessStack.pop();
            if(variableAccess instanceof MapVariableAccess){
                MapVariableAccess mva = (MapVariableAccess) variableAccess;
                mva.setRowIndex(tuple);
                context.addTarget(tuple);
                variableAccessStack.push(mva);
            }
            else{
                throw new IllegalStateException("Cannot apply target index on variables other than type map");
            }
        }
        else{
            tupleCallStack.push(tuple);
        }
    }

    @Override
    public void exitSv_exp(EMDMLParser.Sv_expContext ctx) {
        super.exitSv_exp(ctx);

        if(elementExpressionPurpose == ElementExpressionPurpose.BINDING && !tempVariables.isEmpty()){
            Target value = null;
            vb = null;

            if(!tupleCallStack.isEmpty()){
                value = tupleCallStack.pop();
                context.addTargetIfNotExists(value);
            }
            else if(!variableAccessStack.isEmpty()){
                value = variableAccessStack.pop();
                context.addTargetIfNotExists(value);
            }
            else if(constant != null) value = constant;

            if(value != null){
                if(!variableCallStack.isEmpty()){
                    vb = variableCallStack.pop();
                }
                else{
                    vb = tempVariables.get(0);
                }

                Variable v = context.getVariableByName(vb.getName());

                if(v != null){
                    if(v instanceof SingleVariable){
                        SingleVariable singleVariable = (SingleVariable) v;
                        singleVariable.setValue(value);
                    }
                    else if(v instanceof ArrayVariable){
                        ArrayVariable arrayVariable = (ArrayVariable) v;
                        arrayVariable.addArrayVariableEntry(new ArrayVariableEntry(value));
                    }
                    else if(v instanceof MapVariable){
                        MapVariable mapVariable = (MapVariable) v;
                        mapVariable.addMapVariableEntry(new MapVariableEntry(value));
                    }
                }
            }
            else{
                throw new IllegalStateException("Cannot parse value expression: " + ctx.getText());
            }
        }
    }

    @Override
    public void enterElem_acc_exp(EMDMLParser.Elem_acc_expContext ctx) {
        super.enterElem_acc_exp(ctx);
        isVariableAccess = false;
        constant = null;
        variableAccess = null;
    }

    @Override
    public void enterVar_acc_exp(EMDMLParser.Var_acc_expContext ctx) {
        super.enterVar_acc_exp(ctx);
        isVariableAccess = true;
    }

    @Override
    public void enterMap_simple_acc_exp(EMDMLParser.Map_simple_acc_expContext ctx) {
        super.enterMap_simple_acc_exp(ctx);
        if(bindingVarName != null){
            MapVariable mv = new MapVariable(bindingVarName);
            bindingVarName = null;
            Variable v = context.addVariableIfNotExists(mv);
            if(v instanceof MapVariable){
                mv = (MapVariable) v;
                bindingAccessStrings = new LinkedList<>();
                MapVariableAccess mva = new MapVariableAccess();
                mva.setVariable(mv);
                variableAccessStack.push(mva);
                indexPurpose = IndexPurpose.VARIABLE_ACCESS_ROW_INDEX;
            }
            else{
                throw new IllegalStateException("Variable " + mv.getName() + " must be of Type MapVariable");
            }
        }
    }

    @Override
    public void exitMap_simple_acc_exp(EMDMLParser.Map_simple_acc_expContext ctx) {
        super.exitMap_simple_acc_exp(ctx);
        if(variableAccessStack != null && !variableAccessStack.isEmpty()) {
            variableAccess = variableAccessStack.pop();

            if (variableAccess instanceof MapVariableAccess) {
                MapVariableAccess mva = (MapVariableAccess) variableAccess;
                if (bindingAccessStrings.size() == 1) {
                    SingleValue sv = new SingleValue(bindingAccessStrings.get(0));
                    sv = context.addSingleValueIfNotExist(sv);
                    mva.setRowIndex(sv);
                } else if (bindingAccessStrings.size() >= 1 && bindingAccessStrings.size() <= 4) {
                    TupleValue tv = new TupleValue();
                    for (String s : bindingAccessStrings) {
                        SingleValue sv = new SingleValue(s);
                        sv = context.addSingleValueIfNotExist(sv);
                        tv.addTupleEntry(new TupleEntry(sv));
                    }
                    tv = context.addTupleValueIfNotExists(tv);
                    mva.setRowIndex(tv);
                }

                else{
                    throw new IllegalStateException("A tuple value can only have between one and four values");
                }
            }
            variableAccessStack.push(variableAccess);
        }
    }

    @Override
    public void enterTuple_acc_exp(EMDMLParser.Tuple_acc_expContext ctx) {
        super.enterTuple_acc_exp(ctx);
        indexPurpose = IndexPurpose.VARIABLE_ACCESS_COL_INDEX;
        createVarAccFromVarCallstack();
    }

    @Override
    public void enterArray_acc_exp(EMDMLParser.Array_acc_expContext ctx) {
        super.enterArray_acc_exp(ctx);
        indexPurpose = IndexPurpose.VARIABLE_ACCESS_ROW_INDEX;
        createVarAccFromVarCallstack();
    }

    @Override
    public void enterMap_acc_exp(EMDMLParser.Map_acc_expContext ctx) {
        super.enterMap_acc_exp(ctx);
        createVarAccFromVarCallstack();
    }

    @Override
    public void exitVar_acc_exp(EMDMLParser.Var_acc_expContext ctx) {
        super.exitVar_acc_exp(ctx);
        variableAccess = variableAccessStack.pop();
        if(variableAccessStack.isEmpty()){
            if(elementExpressionPurpose == ElementExpressionPurpose.BINDING){
                variableAccess = context.addVariableAccessIfNotExists(variableAccess);
                variableAccessStack.push(variableAccess);
            }
            else {
                variableAccess = context.addVariableAccessIfNotExists(variableAccess);
                if (variableAccess == null)
                    throw new IllegalStateException("Cannot parse variable access " + ctx.getText());
                tempTargets.add(variableAccess);
                variableAccess = null;
            }
        }
        else{
            VariableAccess parent = variableAccessStack.pop();
            if(parent instanceof MapVariableAccess){
                MapVariableAccess mva = (MapVariableAccess) parent;
                Target t = mva.getRowIndex();
                if(t == null){
                    mva.setRowIndex(variableAccess);
                }
                else if(t instanceof TupleValue){
                    TupleValue tv = (TupleValue) t;
                    variableAccess = context.addVariableAccessIfNotExists(variableAccess);
                    if(variableAccess == null) throw new IllegalStateException("Cannot parse variable access " + ctx.getText());
                    tv.addTupleEntry(new TupleEntry(variableAccess));
                    mva.setRowIndex(tv);
                }
                variableAccessStack.push(mva);
            }
        }
        isVariableAccess = false;
    }

    @Override
    public void exitElem_acc_exp(EMDMLParser.Elem_acc_expContext ctx) {
        super.exitElem_acc_exp(ctx);
        if(!tupleCallStack.isEmpty()){
            //Only happens for bindings...
            Target element = null;
            if(constant != null) {
                element = constant;
            }
            //else if(variableAccess != null) element = variableAccess;
            else if(variableAccessStack != null && !variableAccessStack.isEmpty()) element = variableAccessStack.pop();

            TupleValue tv = tupleCallStack.pop();
            tv.addTupleEntry(new TupleEntry(element));
            tupleCallStack.push(tv);
        }
    }

    @Override
    public void enterP_descr(EMDMLParser.P_descrContext ctx) {
        super.enterP_descr(ctx);
        basicDescription = patternDescription = new PatternDescription();
        patternNamePurpose = PatternNamePurpose.PATTERN_DESCRIPTION;
        textPurpose = TextPurpose.DESCRIPTION;
    }

    @Override
    public void enterAlias_name(EMDMLParser.Alias_nameContext ctx) {
        super.enterAlias_name(ctx);
        patternDescription.addAlias(ctx.getText());
    }

    @Override
    public void enterProb_txt(EMDMLParser.Prob_txtContext ctx) {
        super.enterProb_txt(ctx);
        if(patternDescription.getProblem() == null) patternDescription.setProblem(ctx.getText());
    }

    @Override
    public void enterSol_txt(EMDMLParser.Sol_txtContext ctx) {
        super.enterSol_txt(ctx);
        if(patternDescription.getSolution() == null) patternDescription.setSolution(ctx.getText());
    }

    @Override
    public void enterEx_txt(EMDMLParser.Ex_txtContext ctx) {
        super.enterEx_txt(ctx);
        if(patternDescription.getExample() == null) patternDescription.setExample(ctx.getText());
    }

    @Override
    public void exitP_descr(EMDMLParser.P_descrContext ctx) {
        super.exitP_descr(ctx);
        patternNamePurpose = PatternNamePurpose.NONE;
        textPurpose = TextPurpose.NONE;
        firePatternEvent(PatternEvent.Action.CREATE);
    }

    @Override
    public void enterP_temp(EMDMLParser.P_tempContext ctx) {
        super.enterP_temp(ctx);
        template = new Template();
        patternNamePurpose = PatternNamePurpose.PATTERN_TEMPLATE;
        statement = Statement.PATTERN;
        textPurpose = TextPurpose.TEMPLATE;
    }



    @Override
    public void exitP_temp(EMDMLParser.P_tempContext ctx) {
        super.exitP_temp(ctx);
        patternNamePurpose = PatternNamePurpose.NONE;
        textPurpose = TextPurpose.NONE;
        firePatternEvent(PatternEvent.Action.CREATE);
    }

    @Override
    public void enterBinding_exp(EMDMLParser.Binding_expContext ctx) {
        super.enterBinding_exp(ctx);
        tempVariables = new LinkedList<>();
        elementExpressionPurpose = ElementExpressionPurpose.BINDING;
    }

    @Override
    public void exitBinding_exp(EMDMLParser.Binding_expContext ctx) {
        super.exitBinding_exp(ctx);
        elementExpressionPurpose = ElementExpressionPurpose.NONE;
    }

    @Override
    public void enterP_inst(EMDMLParser.P_instContext ctx) {
        super.enterP_inst(ctx);
        patternNamePurpose = PatternNamePurpose.PATTERN_TO_INSTANTIATE;
        context = pattern = new Pattern();
    }

    @Override
    public void exitP_inst(EMDMLParser.P_instContext ctx) {
        super.exitP_inst(ctx);
        firePatternInstantiationEvent();
    }

    @Override
    public void enterI_stmt(EMDMLParser.I_stmtContext ctx) {
        super.enterI_stmt(ctx);
        this.bindingVarName = null;
        this.bindingAccessStrings = null;
        path = null;
    }

    /****************************** SEARCH STATEMENT PARSING ******************************/

    private SearchEvent searchEvent = null;
    private String searchString = null;

    private enum PathExpressionPurpose { NONE, SEARCH_PATH, SHOW }
    private PathExpressionPurpose pathExpressionPurpose = PathExpressionPurpose.NONE;

    private List<SearchListener> searchListeners = new ArrayList<>();

    public synchronized void addSearchListener( SearchListener l ) {
        searchListeners.add( l );
    }

    public synchronized void removeSearchListener( SearchListener l ) {
        searchListeners.remove( l );
    }

    private synchronized void fireSearchEvent() {
        //SearchEvent searchEvent = new SearchEvent(this, s);
        for(SearchListener l : searchListeners){
            l.processSearchStatement(searchEvent);
        }
    }

    @Override
    public void enterF_stmt(EMDMLParser.F_stmtContext ctx) {
        super.enterF_stmt(ctx);
        this.searchEvent = new SearchEvent(this);
        pathExpressionPurpose = PathExpressionPurpose.SEARCH_PATH;
        searchString = null;
        path = null;
    }

    @Override
    public void enterS_trgt(EMDMLParser.S_trgtContext ctx) {
        super.enterS_trgt(ctx);
        searchEvent.setSearchTarget(ctx.getText());
    }

    @Override
    public void enterS_str(EMDMLParser.S_strContext ctx) {
        super.enterS_str(ctx);
        searchString = ctx.getText();
        searchEvent.addSearchString(searchString);
    }

    @Override
    public void enterS_sct(EMDMLParser.S_sctContext ctx) {
        super.enterS_sct(ctx);
        if(searchString != null) {
            searchEvent.addParameterForSearchString(searchString, ctx.getText());
        }
        else{
            throw new IllegalStateException("Cannot parse search parameter " + ctx.getText());
        }
    }

    @Override
    public void enterPath_exp(EMDMLParser.Path_expContext ctx) {
        super.enterPath_exp(ctx);
        switch(pathExpressionPurpose){
            case SEARCH_PATH:
                searchEvent.setSearchPath(ctx.getText());
                break;
            case SHOW:
                showPath = ctx.getText();
                pathExpressionPurpose = PathExpressionPurpose.NONE;
                break;
        }
    }

    /*@Override
    public void enterS_app(EMDMLParser.S_appContext ctx) {
        super.enterS_app(ctx);
        pathExpressionPurpose = PathExpressionPurpose.APPLICABLE_TO;
    }*/

    @Override
    public void exitF_stmt(EMDMLParser.F_stmtContext ctx) {
        super.exitF_stmt(ctx);
        pathExpressionPurpose = PathExpressionPurpose.NONE;
        fireSearchEvent();
    }

    //************************ Repository Statement Parsing **********************************/

    OrganizationElement repoElement = null;
    String repoElementName = null;

    private List<OrganizationElementListener> organisationElementListeners = new ArrayList<>();

    public synchronized void addOrganisationElementListener(OrganizationElementListener l ) {
        organisationElementListeners.add( l );
    }

    public synchronized void removeOrganisationElementListener(OrganizationElementListener l ) {
        organisationElementListeners.remove( l );
    }

    private synchronized void fireOrganizationElementEvent(OrganizationElement r, OrganizationElementEvent.Action action) {
        OrganizationElementEvent organizationElementEvent = new OrganizationElementEvent(this, r, action, path);
        for(OrganizationElementListener l : organisationElementListeners){
            l.processOrganizationElement(organizationElementEvent);
        }
    }

    @Override
    public void enterCr_stmt(EMDMLParser.Cr_stmtContext ctx) {
        super.enterCr_stmt(ctx);
        repoElementName = null;
    }

    @Override
    public void enterS_name(EMDMLParser.S_nameContext ctx) {
        super.enterS_name(ctx);
        String[] pathSplits = ctx.getText().split("/");
        repoElementName = pathSplits[pathSplits.length-1];
        path = ctx.getText().replace("/" + repoElementName, "");
        if(path.equals(repoElementName)) path = null;
    }

    @Override
    public void exitR_exp(EMDMLParser.R_expContext ctx) {
        super.exitR_exp(ctx);
        try {
            String type;
            if(path != null){
                type = ctx.getText().replace(path + "/" + repoElementName, "");
            }
            else {
                type = ctx.getText().replace(repoElementName, "");
            }

            if(type.equals("REPOSITORY")){
                repoElement = new Repository();
            }
            else if(type.equals("GLOSSARY")){
                repoElement = new Glossary();
            }
            else if(type.equals("MULTIDIMENSIONAL_MODEL")){
                repoElement = new MDM();
            }
            else if(type.equals("CATALOGUE")){
                repoElement = new Catalogue();
            }

            if(repoElement != null){
                repoElement.setName(repoElementName);
            }
        }
        catch (Exception e){
            throw new IllegalStateException("Cannot parse Statement " + ctx.getText());
        }
        if(isDelete){
            isDelete = false;
            fireOrganizationElementEvent(repoElement, OrganizationElementEvent.Action.DELETE);
        }
    }

    @Override
    public void exitCr_stmt(EMDMLParser.Cr_stmtContext ctx) {
        super.exitCr_stmt(ctx);
        fireOrganizationElementEvent(repoElement, OrganizationElementEvent.Action.CREATE);
    }

    /******************** DELETE STATEMENT *************************/
    boolean isDelete = false;

    @Override
    public void enterD_stmt(EMDMLParser.D_stmtContext ctx) {
        super.enterD_stmt(ctx);
        isDelete = true;
        path = null;
    }

    @Override
    public void exitD_stmt(EMDMLParser.D_stmtContext ctx) {
        super.exitD_stmt(ctx);
        //isDelete is responsible for m_del and r_exp
        isDelete = false;
    }

    @Override
    public void enterP_del(EMDMLParser.P_delContext ctx) {
        super.enterP_del(ctx);
        statement = Statement.PATTERN;
        patternNamePurpose = PatternNamePurpose.DELETE;
        pattern = null;
        patternDescription = null;
        template = null;
    }

    @Override
    public void enterP_del_descr(EMDMLParser.P_del_descrContext ctx) {
        super.enterP_del_descr(ctx);
        textPurpose = TextPurpose.DESCRIPTION;
        context = pattern = new Pattern();
        basicDescription = patternDescription = new PatternDescription();
        pattern.addPatternDescription(patternDescription);
    }

    @Override
    public void enterP_del_temp(EMDMLParser.P_del_tempContext ctx) {
        super.enterP_del_temp(ctx);
        textPurpose = TextPurpose.TEMPLATE;
        context = pattern = new Pattern();
        template = new Template();
        pattern.addTemplate(template);
    }

    @Override
    public void exitP_del(EMDMLParser.P_delContext ctx) {
        super.exitP_del(ctx);
        statement = Statement.NONE;
        patternNamePurpose = PatternNamePurpose.NONE;
        firePatternEvent(PatternEvent.Action.DELETE);
    }

    @Override
    public void enterT_del(EMDMLParser.T_delContext ctx) {
        super.enterT_del(ctx);
        statement = Statement.TERM;
        termNamePurpose = TermNamePurpose.DELETE;
        term = null;
        termDescription = null;
        template = null;
    }

    @Override
    public void enterT_del_descr(EMDMLParser.T_del_descrContext ctx) {
        super.enterT_del_descr(ctx);
        textPurpose = TextPurpose.DESCRIPTION;
        context = term = new Term();
        basicDescription = termDescription = new TermDescription();
        term.addDescription(termDescription);
    }

    @Override
    public void enterT_del_temp(EMDMLParser.T_del_tempContext ctx) {
        super.enterT_del_temp(ctx);
        textPurpose = TextPurpose.TEMPLATE;
        context = term = new Term();
        template = new Template();
        term.addTemplate(template);
    }

    @Override
    public void exitT_del(EMDMLParser.T_delContext ctx) {
        super.exitT_del(ctx);
        statement = Statement.NONE;
        termNamePurpose = TermNamePurpose.NONE;
        fireTermEvent(TermEvent.Action.DELETE);
    }

    /********************************** SHOW STATEMENT *************************************/
    private String showPath = null;

    private List<ShowListener> showListeners = new ArrayList<>();

    public synchronized void addShowListener( ShowListener l ) {
        showListeners.add( l );
    }

    public synchronized void removeShowListener( ShowListener l ) {
        showListeners.remove( l );
    }

    private synchronized void fireShowEvent() {
        ShowEvent showEvent = new ShowEvent(this, showPath);
        for(ShowListener l : showListeners){
            l.processShowStatement(showEvent);
        }
    }

    @Override
    public void enterS_stmt(EMDMLParser.S_stmtContext ctx) {
        super.enterS_stmt(ctx);
        path = null;
        showPath = null;
        pathExpressionPurpose = PathExpressionPurpose.SHOW;
    }

    @Override
    public void exitS_stmt(EMDMLParser.S_stmtContext ctx) {
        super.exitS_stmt(ctx);
        fireShowEvent();
        pathExpressionPurpose = PathExpressionPurpose.NONE;
    }

    /********************************** EXECUTION STATEMENT *************************************/

    //private ExecutionObject ex = null;
    private String mdmName;
    private String vocabularyName;

    /*private synchronized void fireTermExecutionEvent() {
        TermEvent termEvent = new TermEvent(this, term, TermEvent.Action.EXECUTE, path);
        for(TermListener l : termListeners){
            l.processTermExecution(mdmName, termEvent);
        }
    }*/

    private synchronized void firePatternExecutionEvent() {
        PatternEvent patternEvent = new PatternEvent(this, pattern, PatternEvent.Action.EXECUTE, path);
        for(PatternListener l : patternListeners){
            l.processPatternExecution(mdmName, vocabularyName, patternEvent);
        }
    }

    @Override
    public void enterX_stmt(EMDMLParser.X_stmtContext ctx) {
        super.enterX_stmt(ctx);
        //ex = new ExecutionObject();
        //pathExpressionPurpose = PathExpressionPurpose.EXECUTION;
        mdmName = null;
        vocabularyName = null;
        path = null;
    }

    /*@Override
    public void enterT_exec(EMDMLParser.T_execContext ctx) {
        super.enterT_exec(ctx);
        termNamePurpose = TermNamePurpose.TERM_EXECUTION;
        //ex.setTarget(ExecutionObject.Target.TERM);
    }*/

    @Override
    public void enterP_exec(EMDMLParser.P_execContext ctx) {
        super.enterP_exec(ctx);
        patternNamePurpose = PatternNamePurpose.PATTERN_EXECUTION;
        textPurpose = TextPurpose.TEMPLATE;
        //ex.setTarget(ExecutionObject.Target.PATTERN);
    }

    @Override
    public void exitP_exec(EMDMLParser.P_execContext ctx) {
        super.exitP_exec(ctx);
        textPurpose = TextPurpose.NONE;
        patternNamePurpose = PatternNamePurpose.NONE;
        this.firePatternExecutionEvent();
    }

    /*@Override
    public void exitT_exec(EMDMLParser.T_execContext ctx) {
        super.exitT_exec(ctx);
        this.fireTermExecutionEvent();
    }*/

    @Override
    public void exitX_stmt(EMDMLParser.X_stmtContext ctx) {
        super.exitX_stmt(ctx);
        pathExpressionPurpose = PathExpressionPurpose.NONE;
        termNamePurpose = TermNamePurpose.NONE;
        patternNamePurpose = PatternNamePurpose.NONE;
    }

    /********************************** LocalCubes *************************************/
    TypeLocalCube typeLocalCube = null;
    PropertyLocalCube propertyLocalCube = null;

    @Override
    public void enterSv_lc_decl(EMDMLParser.Sv_lc_declContext ctx) {
        super.enterSv_lc_decl(ctx);
        tempTargets = new LinkedList<>();
    }

    @Override
    public void enterMv_lc_decl(EMDMLParser.Mv_lc_declContext ctx) {
        super.enterMv_lc_decl(ctx);
        isMVConstraint = true;
        forVariables = new LinkedList<>();
    }

    @Override
    public void exitMv_lc_decl(EMDMLParser.Mv_lc_declContext ctx) {
        super.exitMv_lc_decl(ctx);
        isMVConstraint = false;
    }

    @Override
    public void enterType_lc_decl(EMDMLParser.Type_lc_declContext ctx) {
        super.enterType_lc_decl(ctx);
        typePurpose = TypePurpose.TYPE_LOCAL_CUBE;
        typeLocalCube = new TypeLocalCube();
        if(!isMVConstraint && context != null) {
            this.context.addLocalCube(typeLocalCube);
        }
    }

    @Override
    public void exitType_lc_decl(EMDMLParser.Type_lc_declContext ctx) {
        super.exitType_lc_decl(ctx);
        typePurpose = TypePurpose.NONE;
        if(typeLocalCube != null && tempTargets.size() == 1){
            Target element = convertToConstraintTarget(tempTargets.remove(0));
            if(element != null) {
                typeLocalCube.setElement(element);
            }
            else{
                throw new IllegalStateException("Cannot parse Type Local Cube " + ctx.getText());
            }
        }
        else if(typeLocalCube != null && constant != null){
            typeLocalCube.setElement(constant);
        }
        else{
            throw new IllegalStateException("Cannot parse Type Local Cube " + ctx.getText());
        }
    }

    @Override
    public void exitDescr_frag_decl(EMDMLParser.Descr_frag_declContext ctx) {
        super.exitDescr_frag_decl(ctx);
        if (context != null && tempTargets.size() == 4) {
            DescribedByLocalCube dba = new DescribedByLocalCube();
            Target attribute = convertToConstraintTarget(tempTargets.remove(3));
            tempTargets.remove(2); //index 2 and 0 both contain the dimension -> discard one
            Target level = convertToConstraintTarget(tempTargets.remove(1));
            Target dimension = convertToConstraintTarget(tempTargets.remove(0));

            if(attribute != null && level != null && dimension != null) {
                dba.setDimension(tempTargets.get(0));
                dba.setLevel(tempTargets.get(1));
                dba.setAttribute(tempTargets.get(3));
                context.addLocalCube(dba);
            }
            else {
                throw new IllegalStateException("Cannot parse DescribedBy Local Cube " + ctx.getText());
            }
        }
        else {
            throw new IllegalStateException("Cannot parse DescribedBy Local Cube " + ctx.getText());
        }
    }

    @Override
    public void exitDom_lc_decl(EMDMLParser.Dom_lc_declContext ctx) {
        super.exitDom_lc_decl(ctx);

        if (context != null && tempTargets.size() == 3) {
            DomainLocalCube da = new DomainLocalCube();
            Target domain = convertToConstraintTarget(tempTargets.remove(2));
            Target property = convertToConstraintTarget(tempTargets.remove(1));
            Target entity = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the local cube
            if(domain != null && property != null && entity != null) {
                da.setDomain(domain);
                da.setProperty(property);
                da.setEntity(entity);
                context.addLocalCube(da);
            }
            else{
                throw new IllegalStateException("Cannot parse Domain Local Cube " + ctx.getText());
            }
        }
        else {
            throw new IllegalStateException("Cannot parse Domain Local Cube " + ctx.getText());
        }
    }

    @Override
    public void enterProp_lc_decl(EMDMLParser.Prop_lc_declContext ctx) {
        super.enterProp_lc_decl(ctx);
        typePurpose = TypePurpose.PROPERTY_LOCAL_CUBE;
    }

    @Override
    public void exitProp_lc_decl(EMDMLParser.Prop_lc_declContext ctx) {
        super.exitProp_lc_decl(ctx);

        typePurpose = TypePurpose.NONE;
        if (propertyLocalCube != null && tempTargets.size() == 2) {
            Target property = convertToConstraintTarget(tempTargets.remove(1));
            Target entity = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the local cube
            if(property == null || entity == null) throw new IllegalStateException("Cannot parse Property Local Cube " + ctx.getText());
            propertyLocalCube.setProperty(property);
            propertyLocalCube.setEntity(entity);
        }
        else {
            throw new IllegalStateException("Cannot parse Property Local Cube " + ctx.getText());
        }
    }

    @Override
    public void exitRollup_frag_decl(EMDMLParser.Rollup_frag_declContext ctx) {
        super.exitRollup_frag_decl(ctx);

        if (context != null && tempTargets.size() == 4) {
            RollUpLocalCube ra = new RollUpLocalCube();
            Target parentLevel = convertToConstraintTarget(tempTargets.remove(3));
            tempTargets.remove(2); //index 2 and 0 both contain the dimension -> discard one
            Target childLevel = convertToConstraintTarget(tempTargets.remove(1));
            Target dimension = convertToConstraintTarget(tempTargets.remove(0));

            //Check whether one of the targets is null, i.e. an undefined variable is used in the local cube
            if(dimension != null && childLevel != null && parentLevel != null) {
                ra.setDimension(dimension);
                ra.setChildLevel(childLevel);
                ra.setParentLevel(parentLevel);
                context.addLocalCube(ra);
            }
            else {
                throw new IllegalStateException("Cannot parse RollUp Local Cube " + ctx.getText());
            }
        }
        else {
            throw new IllegalStateException("Cannot parse RollUp Local Cube " + ctx.getText());
        }
    }
}
