package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.MDM.Cube;
import at.dke.olappattern.data.MDM.Dimension;
import at.dke.olappattern.data.MDM.MDM;
import at.dke.olappattern.data.Template;
import at.dke.olappattern.data.pattern.Pattern;
import at.dke.olappattern.data.pattern.PatternDescription;
import at.dke.olappattern.data.repository.Catalogue;
import at.dke.olappattern.data.repository.Glossary;
import at.dke.olappattern.data.repository.Repository;
import at.dke.olappattern.data.term.Term;
import at.dke.olappattern.data.term.TermDescription;
import at.dke.olappattern.data.term.Type;
import at.dke.olappattern.repository_app.event.SearchEvent;

import java.util.List;

/**
 * An interface describing a component that allows to save, delete and search elements of the OLAP pattern approach
 */
public interface StorageUnit {
    /**
     * Opens a database session
     */
    void openSession();

    /**
     * Closes an active database session
     */
    void closeSession();

    /**
     * Method to persist a pattern in the database
     * @param p the pattern to be persisted
     */
    void persistPattern(Pattern p);

    /**
     * Method to persist a term in the database
     * @param t the term to be persisted
     */
    void persistTerm(Term t);

    /**
     * Method to persist a catalogue in the database
     * @param c the catalogue to be persisted
     */
    void persistCatalogue(Catalogue c);

    /**
     * Method to persist a MDM in the database
     * @param m the MDM to be persisted
     */
    void persistMDM(MDM m);

    /**
     * Method to persist a glossary in the database
     * @param glossary the glossary to be persisted
     */
    void persistGlossary(Glossary glossary);

    /**
     * Method to persist a repository in the database
     * @param r the Repository to be persisted
     */
    void persistRepository(Repository r);

    /**
     * Method that deletes a pattern (and all its templates and descriptions)
     * which is already removed from the catalogue it was in
     * @param p The pattern object to be deleted.
     */
    void deletePattern(Pattern p);

    /**
     * Method that deletes the pattern description in the given path described by the parameters
     * @param repository the repository given in the path
     * @param catalogue the catalogue given in the path
     * @param pattern the pattern given in the path
     * @param description the description to be deleted
     */
    void deletePatternDescriptions(String repository, String catalogue, String pattern, PatternDescription description);

    /**
     * Method that removes a pattern description which is already removed from the pattern it was in
     * @param description The PatternDescription object to be deleted.
     */
    void deletePatternDescriptions(PatternDescription description);

    /**
     * Method that deletes the pattern template in the given path described by the parameters
     * @param repository the repository given in the path
     * @param catalogue the catalogue given in the path
     * @param pattern the pattern given in the path
     * @param template the template to be deleted
     */
    void deletePatternTemplates(String repository, String catalogue, String pattern, Template template);

    /**
     * Method that removes a pattern template which is already removed from the pattern it was in
     * @param template
     */
    void deletePatternTemplates(Template template);

    /**
     * Method that deletes a term (and its descriptions and templates)
     * which is already removed from the glossary it was in
     * @param t The term object to be deleted
     */
    void deleteTerm(Term t);

    /**
     * Method that deletes the term description in the given path described by the parameters
     * @param repository the repository given in the path
     * @param glossary the glossary given in the path
     * @param term the term given in the path
     * @param description the description to be deleted
     */
    void deleteTermDescriptions(String repository, String glossary, String term, TermDescription description);

    /**
     * Method that deletes the term template in the given path described by the parameters
     * @param repository the repository given in the path
     * @param glossary the glossary given in the path
     * @param term the term given in the path
     * @param template the template to be deleted
     */
    void deleteTermTemplates(String repository, String glossary, String term, Template template);

    /**
     * Method to delete the catalogue and all included patterns from the given repository
     * @param repository The name of the repository that contains the catalogue
     * @param catalogue The name of the catalogue to delete
     */
    void deleteCatalogue(String repository, String catalogue);

    /**
     * Method to delete the catalogue and all included patterns that has already been removed from
     * the repository it was in.
     * @param c The name of the catalogue to delete
     */
    void deleteCatalogue(Catalogue c);

    /**
     * Method to delete the glossary and all included terms from the given repository
     * @param repository The name of the repository that contains the catalogue
     * @param glossary The name of the glossary to delete
     */
    void deleteGlossary(String repository, String glossary);

    /**
     * Method to delete the glossary and all included terms that has already been removed from
     * the repository it was in.
     * @param glossary The name of the glossary to delete
     */
    void deleteGlossary(Glossary glossary);

    /**
     * Method to delete the mdm and all included cubes and dimensions from the given repository
     * @param repository The name of the repository that contains the mdm
     * @param mdm The name of the mdm to delete
     */
    void deleteMDM(String repository, String mdm);

    /**
     * Method to delete the mdm (and all included cubes and dimensions) that has already been removed from
     * the repository it was in.
     * @param mdm The name of the mdm to delete
     */
    void deleteMDM(MDM mdm);

    /**
     * Method to delete a repository and everything it contains
     * @param repository
     */
    void deleteRepository(String repository);

    /**
     * Method to delete the cube with the given name at the path specified by the repository and mdm parameters
     * @param repository The name of the repository
     * @param mdm The name of the MDM
     * @param cube The name of the cube to be deleted
     */
    void deleteCube(String repository, String mdm, String cube);

    /**
     * Method to delete a cube instance from the database
     * @param c the cube object to be deleted
     */
    void deleteCube(Cube c);

    /**
     * Deletes the dimension with the given name in the given path specified by the repository and the mdm
     * Dimensions can only be deleted if they are not referenced by any dimension role in a cube
     * @param repository The name of the repository
     * @param mdm The name of the MDM
     * @param dimension The name of the dimension to be deleted
     */
    void deleteDimension(String repository, String mdm, String dimension);

    /**
     * Method to delete a dimension instance from the database
     * @param d the dimension object to be deleted
     */
    void deleteDimension(Dimension d);

    /**
     * Returns the type of a term or pattern with the given name
     * @param name The name of the type to be returned
     * @return the type object
     */
    Type getType(String name);

    /**
     * Method to retrieve a specific pattern from the database if it exists
     * @param repository The repository to get the pattern from
     * @param catalogue The catalogue to get the pattern from
     * @param pattern The name of the pattern to be returned
     * @return THe Pattern Object if it exists, otherwise null
     */
    Pattern getPattern(String repository, String catalogue, String pattern);

    /**
     * Method to retrieve a sepcific term from the repository in the path specified by repository and catalogue
     * @param repository The name of the Repository
     * @param catalogue The name of the Catalogue
     * @param term The term to be retrieved
     * @return The term object or null if no such term exists
     */
    Term getTerm(String repository, String catalogue, String term);

    /**
     * Method to retrieve the repository with the given name from the database
     * @param repository The name of the repository to be retrieved
     * @return The repository object or null if it does not exist
     */
    Repository getRepository(String repository);

    /**
     * Method to retrieve a specific mdm from the given repository
     * @param repository The name of the repository to search in
     * @param mdm The name of the mdm to be retrieved
     * @return The mdm object or null if it does not exist
     */
    MDM getMDM(String repository, String mdm);

    /**
     * Method to retrieve a specific dimension from the database contained in the repository and mdm given
     * @param repository The name of the repository to search in
     * @param mdm The name of the mdm the dimension must be part of
     * @param dimension The dimension to be retrieved
     * @return The dimension object or null if no such dimension exists
     */
    Dimension getDimension(String repository, String mdm, String dimension);

    /**
     * Method to retrieve a specific cube from the database contained in the repository and mdm given
     * @param repository The name of the repository to search in
     * @param mdm The name of the mdm the dimension must be part of
     * @param cube The dimension to be retrieved
     * @return The cube object or null if no such cube exists
     */
    Cube getCube(String repository, String mdm, String cube);

    /**
     * Method to retrieve a specific catalogue from the given repository
     * @param repository The name of the repository to search in
     * @param catalogue The name of the catalogue to be retrieved
     * @return The catalogue object or null if it does not exist
     */
    Catalogue getCatalogue(String repository, String catalogue);

    /**
     * Method to retrieve a specific glossary from the given repository
     * @param repository The name of the repository to search in
     * @param glossary The name of the glossary to be retrieved
     * @return The glossary object or null if it does not exist
     */
    Glossary getGlossary(String repository, String glossary);

    /**
     * Method to retrieve a specific description of the given pattern in the given catalogue contained in
     * the stated repository
     * @param repository The repository name
     * @param catalogue The catalogue the pattern must be contained in
     * @param pattern The name of the pattern to which the description belongs
     * @param description The object determining the description
     * @return The pattern description object or null if no such description exists
     */
    PatternDescription getPatternDescription(String repository, String catalogue, String pattern, String description);

    /**
     * Method to retrieve one or more specific templates of the given pattern in the given catalogue contained in
     * the stated repository
     * @param repository The repository name
     * @param catalogue The catalogue, the pattern must be contained in
     * @param pattern The name of the pattern to which the template belongs
     * @param template The object determining the template
     * @return The pattern template object or null if no such template exists
     */
    List<Template> getPatternTemplates(String repository, String catalogue, String pattern, Template template);

    /**
     * Method to retrieve one or more specific templates of the given term in the given glossary contained in
     * the stated repository
     * @param repository The repository name
     * @param glossary The glossary, the term must be contained in
     * @param term the name of the term to which the template belongs
     * @param template The object determining the template
     * @return
     */
    List<Template> getTermTemplates(String repository, String glossary, String term, Template template);

    /**
     * Method to search for patterns that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found patterns; the list is empty if no patterns where found
     */
    List<Pattern> searchPatterns(SearchEvent searchEvent);

    /**
     * Method to search for terms that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found terms; the list is empty if no terms where found
     */
    List<Term> searchTerms(SearchEvent searchEvent);

    /**
     * Method to search for repositories that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found repositories; the list is empty if no repositories where found
     */
    List<Repository> searchRepositories(SearchEvent searchEvent);

    /**
     * Method to search for glossaries that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found glossaries; the list is empty if no glossaries where found
     */
    List<Glossary> searchGlossaries(SearchEvent searchEvent);

    /**
     * Method to search for catalogue that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found catalogues; the list is empty if no catalogues where found
     */
    List<Catalogue> searchCatalogues(SearchEvent searchEvent);

    /**
     * Method to search for mdms that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found mdms; the list is empty if no mdms where found
     */
    List<MDM> searchMDMs(SearchEvent searchEvent);

    /**
     * Method to search for cubes that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found cubes; the list is empty if no cubes where found
     */
    List<Cube> searchCubes(SearchEvent searchEvent);

    /**
     * Method to search for dimensions that meet the criteria specified by the search object
     * @param searchEvent The object specifying the search criteria
     * @return A list of found dimension; the list is empty if no dimensions where found
     */
    List<Dimension> searchDimensions(SearchEvent searchEvent);

}
