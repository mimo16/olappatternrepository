package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.pattern.Pattern;

/**
 * An interface describing components that can execute and ground terms and patterns
 */
public interface PreprocessorUnit {
    /**
     * Generate an executable OLAP query from an instantiated pattern
     * @param mdm path of the MDM determining the context
     * @param glossary path of the glossary determining the context
     * @param pattern  pattern object determining the pattern to be executed
     * @param catalogue path of the catalogue to be executed
     * @return
     */
    String executePattern(String mdm, String glossary, Pattern pattern, String catalogue);
}
