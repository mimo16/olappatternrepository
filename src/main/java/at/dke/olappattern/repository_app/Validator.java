package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.MDM.*;
import at.dke.olappattern.data.Target;
import at.dke.olappattern.data.constraint.*;
import at.dke.olappattern.data.localcube.DomainLocalCube;
import at.dke.olappattern.data.localcube.LocalCube;
import at.dke.olappattern.data.localcube.PropertyLocalCube;
import at.dke.olappattern.data.localcube.TypeLocalCube;
import at.dke.olappattern.data.pattern.Pattern;
import at.dke.olappattern.data.repository.Glossary;
import at.dke.olappattern.data.term.Term;
import at.dke.olappattern.data.value.ArrayVariableEntry;
import at.dke.olappattern.data.value.SingleValue;
import at.dke.olappattern.data.variable.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Previously known as KnowledgeBasedSystem.java
 */
public class Validator implements ValidatorUnit {

    /**
     * Method that returns whether or not ground pattern is applicable, that is, it checks whether all
     * constraints are satisfied either by a pattern's local cubes, a multidimensional model's element,
     * or a vocabulary's business term.
     * @param pattern   pattern providing the local cubes to be considered
     * @param mdm       multidimensional model to be considered
     * @param glossary  glossary to be considered
     * @return          true, if all constraint are satisfied, otherwise false
     */
    @Override
    public boolean validate(Pattern pattern, MDM mdm, Glossary glossary) {

        Logger.log("START PATTERN VALIDATION: validation of pattern " + pattern.getName() + "!\r\n");

        if (!pattern.isGround())
            return false;

        boolean isApplicable = false;

        // evaluate all constraints w.r.t. to the local mdm and the associated mdm
        for(Constraint c : pattern.getConstraints()) {

            Logger.log("... check whether " + pattern.getName() + "'s " + c.getConstraintType() + " "  + c.toString() + " is satisfied?...\r\n");
            
            if (c instanceof  TypeConstraint) {
                isApplicable = checkTypeConstraint((TypeConstraint) c, pattern, mdm, glossary);
            }
            else if (c instanceof PropertyConstraint) {
                isApplicable = checkPropertyConstraint((PropertyConstraint) c, pattern, mdm);
            }
            else if (c instanceof DomainConstraint) {
                isApplicable = checkDomainConstraint((DomainConstraint) c, pattern, mdm);
            }
            else if (c instanceof ReturnConstraint) {
                isApplicable = checkReturnConstraint((ReturnConstraint)c, glossary);
            }
            else if (c instanceof RollUpConstraint) {
                isApplicable = checkRollUpToConstraint((RollUpConstraint)c, mdm);
            }
            else if (c instanceof DescribedByConstraint) {
                isApplicable = checkDescribedByConstraint((DescribedByConstraint)c, mdm);
            }
            else if (c instanceof UnaryAppConstraint) {
                isApplicable = checkUnaryAppConstraint((UnaryAppConstraint)c, pattern, mdm, glossary);
            }
            else if (c instanceof  BinaryAppConstraint) {
                isApplicable = checkBinaryAppConstraint((BinaryAppConstraint)c, pattern, mdm, glossary);
            }

            Logger.log("END PATTERN VALIDATION: " + ((isApplicable) ? " SUCCESS: " : " FAIL: ") + "check that " + c.getConstraintType() + " "  + c.toString() +  ((isApplicable) ? " is satisfied! " : " is NOT satisfied...") + "\r\n");

            if(!isApplicable)
                break;
        }

        return isApplicable;
    }

    /**
     * Method that returns whether or not a context-parameter-free business term applicable, that is,
     * it checks whether all constraints are satisfied either by a pattern's local cubes, a multidimensional
     * model's element, or a vocabulary's business term.
     * @param term      term application that is to be checked
     * @param pattern   pattern providing the local cubes to be considered
     * @param mdm       multidimensional model to be considered
     * @param glossary  glossary to be considered
     * @return          true, if all constraint are satisfied, otherwise false
     */
    private boolean validateBusinessTermApplication(Term term, Pattern pattern, MDM mdm, Glossary glossary) {

        Logger.log("START BUSINESS TERM APPLICATION VALIDATION: validation of business term's " + term.getName() + " application in the context of the pattern's " + pattern.getName() + " execution! \r\n");

        if (!term.isGround())
            return false;

        boolean isApplicable = false;

        // evaluate all constraints w.r.t. to the local mdm and the associated mdm
        for(Constraint c : term.getConstraints()) {

            Logger.log("... check whether " + term.getName() + "'s " + c.getConstraintType() + " "  + c.toString() + " is satisfied...\r\n");

            if (c instanceof  TypeConstraint) {
                isApplicable = checkTypeConstraint((TypeConstraint) c, pattern, mdm, glossary);
            }
            else if (c instanceof PropertyConstraint) {
                isApplicable = checkPropertyConstraint((PropertyConstraint) c, pattern, mdm);
            }
            else if (c instanceof DomainConstraint) {
                isApplicable = checkDomainConstraint((DomainConstraint) c, pattern, mdm);
            }
            else if (c instanceof RollUpConstraint) {
                isApplicable = checkRollUpToConstraint((RollUpConstraint)c, mdm);
            }
            else if (c instanceof DescribedByConstraint) {
                isApplicable = checkDescribedByConstraint((DescribedByConstraint)c, mdm);
            }

            Logger.log("... " + ((isApplicable) ? " SUCCESS: " : " FAIL: ") + " check that " + c.getConstraintType() + " "  + c.toString() +  ((isApplicable) ? " is satisfied! " : " is NOT satisfied...") + "\r\n");

            if(!isApplicable)
                break;
        }

        Logger.log("END BUSINESS TERM APPLICATION VALIDATION: " + ((isApplicable) ? " SUCCESS " : " FAIL ") + " of validation of business term's " + term.getName() + " application in the context of the pattern's " + pattern.getName() + " execution! \r\n");

        return isApplicable;
    }

    /**
     * Method that returns whether or not a type constraint is satisfied either by a pattern's local cubes,
     * a multidimensional model's element or a vocabulary's business term.
     * @param tc    type constraint to be checked
     * @param p     pattern providing the local cubes to be considered
     * @param mdm   multidimensional model to be considered
     * @param g     glossary to be considered
     * @return      true, if type constraint is satisfied, otherwise false
     */
    private boolean checkTypeConstraint(TypeConstraint tc, Pattern p, MDM mdm, Glossary g) {

        boolean isTypeConstraintSatisfied = false;

        // <sourceCube>:CUBE;
        // "Milking":CUBE;
        Target elementTarget = tc.getElement();
        String elementName = getTargetValue(elementTarget);

        Target typeTarget = tc.getType();
        String typeName = getTargetValue(typeTarget);

        Logger.log("... looking for element with name " + elementName + " with type " + typeName + "... \r\n");

        // check if element exists in the local cubes
        for (LocalCube lc : p.getLocalCubes()) {

            // check if local elements satisfies type constraint
            if (lc instanceof TypeLocalCube) {
                TypeLocalCube tlc = (TypeLocalCube) lc;
                String localElementName = getTargetValue(tlc.getElement());
                String localTypeName = getTargetValue(tlc.getType());

                if (elementName.equalsIgnoreCase(localElementName) && typeName.equalsIgnoreCase(localTypeName)) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: local type constraint " + tlc.toString() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                    break;
                }
            }
            // check if local property satisfies type constraint (especially for variable type constraints relevant)
            else if (lc instanceof PropertyLocalCube) {
                PropertyLocalCube plc = (PropertyLocalCube)lc;

                String localPropertyName = getTargetValue(plc.getProperty());
                String localPropertyTypeName = getTargetValue(plc.getType());

                if (elementName.equalsIgnoreCase(localPropertyName) && typeName.equalsIgnoreCase(localPropertyTypeName)) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: local property constraint " + plc.toString() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                    break;
                }
            }
        }

        // check if element exists in the multidimensional model
        if(!isTypeConstraintSatisfied) {
            if (typeName.equalsIgnoreCase("CUBE")) {
                Cube c = mdm.getCubeByName(elementName);

                if (c != null) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: cube " + c.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            } else if (typeName.equalsIgnoreCase("DIMENSION")) {
                Dimension d = mdm.getDimensionByName(elementName);

                if (d != null) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: dimension " + d.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            // currently no differentiation between STRING and NUMBER value set
            } else if (typeName.equalsIgnoreCase("STRING_VALUE_SET") ||
                    typeName.equalsIgnoreCase("NUMBER_VALUE_SET")) {
                ValueSet v = mdm.getValueSetByName(elementName);

                if (v != null) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: value set " + v.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            } else if (typeName.equalsIgnoreCase("DIMENSION_ROLE")) {
                DimensionRole dr = mdm.getDimensionRoleByName(elementName);

                if (dr != null) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: dimension role " + dr.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            } else if (typeName.equalsIgnoreCase("MEASURE")) {
                Measure m = mdm.getMeasureByName(elementName);

                if (m != null) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: measure " + m.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            } else if (typeName.equalsIgnoreCase("LEVEL")) {
                Level l = mdm.getLevelByName(elementName);

                if (l != null) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: level " + l.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            } else if (typeName.equalsIgnoreCase("ATTRIBUTE")) {
                Attribute a = mdm.getAttributeByName(elementName);

                if (a != null) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: attribute " + a.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            }
        }

        // check if element exists in the vocabulary
        if(!isTypeConstraintSatisfied) {
            Term t = g.getTermByName(elementName);

            if (t != null) {
                if (typeName.equalsIgnoreCase(t.getType().getName())) {
                    isTypeConstraintSatisfied = true;
                    Logger.log("... SUCCESS: business term " + t.getName() + " satisfies the pattern's type constraint " + tc.toString() + "! \r\n");
                }
            }
        }

        return isTypeConstraintSatisfied;
    }

    /**
     * Method that returns whether or not a property constraint is satisfied either by a pattern's local cubes
     * or by a multidimensional model's property.
     * @param pc    property constraint to be checked
     * @param p     pattern providing the local cubes to be considered
     * @param mdm   multidimensional model to be considered
     * @return      true, if property constraint is satisfied, otherwise false
     */
    private boolean checkPropertyConstraint(PropertyConstraint pc, Pattern p, MDM mdm) {

        boolean isPropertyConstraintSatisfied = false;

        // <sourceCube> HAS DIMENSION_ROLE <dimRole>;
        // "Milking" HAS DIMENSION_ROLE "Cattle";
        Target entityTarget = pc.getEntity();
        String entityName = getTargetValue(entityTarget);

        Target propTarget = pc.getProperty();
        String propName = getTargetValue(propTarget);

        Target propTypeTarget = pc.getType();
        String propTypeName = getTargetValue(propTypeTarget);

        Logger.log("... looking for property with name " + propName + " and type " + propTypeName + " of entity " + entityName + "...\r\n");

        // check if property exists in the local cubes
        for (LocalCube lc : p.getLocalCubes()) {

            // check if local property satisfies property constraint
            if (lc instanceof PropertyLocalCube) {
                PropertyLocalCube plc = (PropertyLocalCube)lc;

                String localEntityName = getTargetValue(plc.getEntity());
                String localPropertyName = getTargetValue(plc.getProperty());
                String localPropertyTypeName = getTargetValue(plc.getType());

                if (entityName.equalsIgnoreCase(localEntityName)
                        && propName.equalsIgnoreCase(localPropertyName)
                        && propTypeName.equalsIgnoreCase(localPropertyTypeName)) {
                    isPropertyConstraintSatisfied = true;
                    Logger.log("... SUCCESS: local property constraint " + plc.toString() + " satisfies the pattern's porperty constraint " + pc.toString() + "!\r\n");
                    break;
                }
            }
        }

        // check if property exists in the multidimensional model
        if(!isPropertyConstraintSatisfied) {

            // check if cube properties are considered
            if (propTypeName.equalsIgnoreCase("DIMENSION_ROLE") ||
                    propTypeName.equalsIgnoreCase("MEASURE")) {

                Cube c = mdm.getCubeByName(entityName);

                if (c != null) {
                    DimensionRole dr = mdm.getDimensionRoleByName(propName);

                    if (dr != null) {
                        isPropertyConstraintSatisfied = true;
                        Logger.log("... SUCCESS: dimension role " + dr.getName() + " satisfies the pattern's property constraint " + pc.toString() + "! \r\n");
                    }
                    else {
                        Measure m = mdm.getMeasureByName(propName);

                        if (m != null) {
                            isPropertyConstraintSatisfied = true;
                            Logger.log("... SUCCESS: measure " + m.getName() + " satisfies the pattern's property constraint " + pc.toString() + "! \r\n");
                        }
                    }
                }
            }
            // check if dimension properties are considered
            else if (propTypeName.equalsIgnoreCase("LEVEL") ||
                        propTypeName.equalsIgnoreCase("ATTRIBUTE")) {
                Dimension d = mdm.getDimensionByName(entityName);

                if(d != null) {
                    Level l = mdm.getLevelByName(propName);

                    if (l != null) {
                        isPropertyConstraintSatisfied = true;
                        Logger.log("... SUCCESS: level " + l.getName() + " satisfies the pattern's property constraint " + pc.toString() + "! \r\n");
                    }
                    else {
                        Attribute a = mdm.getAttributeByName(propName);

                        if (a != null) {
                            isPropertyConstraintSatisfied = true;
                            Logger.log("... SUCCESS: attribute " + a.getName() + " satisfies the pattern's property constraint " + pc.toString() + "! \r\n");
                        }
                    }
                }
            }
        }

        return isPropertyConstraintSatisfied;
    }

    /**
     * Method that returns whether or not a domain constraint is satisfied either by a pattern's local cubes
     * or by a multidimensional model's property.
     * @param dc    domain constraint to be checked
     * @param p     pattern providing the local cubes to be considered
     * @param mdm   multidimensional model to be considered
     * @return      true, if the domain constraint is satisfied, otherwise false
     */
    private boolean checkDomainConstraint(DomainConstraint dc, Pattern p, MDM mdm) {

        boolean isDomainConstraintSatisfied = false;

        // <sourceCube>.<dimRole>:<cubeDim>;
        // "Milking"."Cattle":"Animal";
        Target entityTarget = dc.getEntity();
        String entityName = getTargetValue(entityTarget);

        Target propTarget = dc.getProperty();
        String propName = getTargetValue(propTarget);

        Target domainTarget = dc.getDomain();
        String domainName = getTargetValue(domainTarget);

        Logger.log("... looking for property with name " + propName + " and domain " + domainName + " of entity " + entityName + "...\r\n");

        // check if property with domain exists in the local cubes
        for (LocalCube lc : p.getLocalCubes()) {

            // check if local property satisfies domain constraint
            if (lc instanceof DomainLocalCube) {
                DomainLocalCube dlc = (DomainLocalCube)lc;

                String localEntityName = getTargetValue(dlc.getEntity());
                String localPropertyName = getTargetValue(dlc.getProperty());
                String localDomainName = getTargetValue(dlc.getDomain());

                if (entityName.equalsIgnoreCase(localEntityName)
                        && propName.equalsIgnoreCase(localPropertyName)
                        && domainName.equalsIgnoreCase(localDomainName)) {
                    isDomainConstraintSatisfied = true;
                    Logger.log("... SUCCESS: local domain constraint " + dlc.toString() + " satisfies the pattern's domain constraint " + dc.toString() + "!\r\n");
                    break;
                }
            }
        }

        // check if property with domain exists in the multidimensional model
        if(!isDomainConstraintSatisfied) {

            // check if cube properties are considered
            Cube c = mdm.getCubeByName(entityName);

            if (c != null) {
                DimensionRole dr = mdm.getDimensionRoleByName(propName);

                if (dr != null) {
                    if (dr.getDimension().getName().equalsIgnoreCase(domainName)) {
                        isDomainConstraintSatisfied = true;
                        Logger.log("... SUCCESS: dimension role " + dr.getName() + " satisfies the pattern's domain constraint " + dc.toString() + "! \r\n");
                    }
                } else {
                    Measure m = mdm.getMeasureByName(propName);

                    if (m != null) {
                        if (m.getDomain().getName().equalsIgnoreCase(domainName)) {
                            isDomainConstraintSatisfied = true;
                            Logger.log("... SUCCESS: measure " + m.getName() + " satisfies the pattern's domain constraint " + dc.toString() + "! \r\n");
                        }
                    }
                }
            }
            else {
                // check if dimension properties are considered
                Dimension d = mdm.getDimensionByName(entityName);

                if (d != null) {
                    Level l = mdm.getLevelByName(propName);

                    if (l != null) {
                        if (l.getDomain().getName().equalsIgnoreCase(domainName)) {
                            isDomainConstraintSatisfied = true;
                            Logger.log("... SUCCESS: level " + l.getName() + " satisfies the pattern's domain constraint " + dc.toString() + "! \r\n");
                        }
                    } else {
                        Attribute a = mdm.getAttributeByName(propName);

                        if (a != null) {
                            if (a.getDomain().getName().equalsIgnoreCase(domainName)) {
                                isDomainConstraintSatisfied = true;
                                Logger.log("... SUCCESS: attribute " + a.getName() + " satisfies the pattern's domain constraint " + dc.toString() + "! \r\n");
                            }
                        }
                    }
                }
            }
        }

        return isDomainConstraintSatisfied;
    }

    /**
     * Method that returns whether or not a return constraint is satisfied either by a business term in a vocabulary.
     * @param rc    return constraint to be checked
     * @param g     glossary to be considered
     * @return      true, if the return constraint is satisfied, otherwise false
     */
    private boolean checkReturnConstraint(ReturnConstraint rc, Glossary g) {

        boolean isReturnConstraintSatisfied = false;

        // <cubeMeasureDom> <= <cubeMeasure>.RETURNS;
        // "Liquid In Liter" <= "Average Milk Yield".RETURNS;
        Target termTarget = rc.getEntity();
        String termName = getTargetValue(termTarget);

        Target returnTypeTarget = rc.getReturnType();
        String returnTypeName = getTargetValue(returnTypeTarget);

        Logger.log("... looking for business term with name " + termName + " and return type " + returnTypeName + "...\r\n");

        // check if business term exists in the vocabulary
        Term t = g.getTermByName(termName);

        if (t != null) {
            if (returnTypeName.equalsIgnoreCase(t.getReturnType())) {
                isReturnConstraintSatisfied = true;
                Logger.log("... SUCCESS: business term " + t.getName() + " satisfies the pattern's type constraint " + rc.toString() + "! \r\n");
            }
        }

        return isReturnConstraintSatisfied;
    }

    /**
     * Method that returns whether or not a described-by constraint is satisfied by
     * attributes of a multidimensional model.
     * @param dbc   described-by constraint to be checked
     * @param mdm   multidimensional model to be considered
     * @return      true, if the described by constraint is satisfied, otherwise false
     */
    private boolean checkDescribedByConstraint(DescribedByConstraint dbc, MDM mdm) {

        boolean isDescribedByConstraintSatisfied = false;

        // <dim>.<level> DESCRIBED_BY <dim>.<attribute>;
        // "Animal"."Animal" DESCRIBED_BY "Animal"."Animal Name";
        Target dimensionTarget = dbc.getDimension();
        String dimensionName = getTargetValue(dimensionTarget);

        Target attributeTarget = dbc.getAttribute();
        String attributeName = getTargetValue(attributeTarget);

        Target levelTarget = dbc.getLevel();
        String levelName = getTargetValue(levelTarget);

        Logger.log("... SUCCESS: looking for dimension with name " + dimensionName + "  with a level " + levelName + " that is described by a attribute " + attributeName + "...\r\n");

        // check if dimension exists in the multidimensional model
        Dimension d = mdm.getDimensionByName(dimensionName);

        if (dimensionName.equalsIgnoreCase(d.getName())) {

            for (Attribute a : d.getAttributes()) {
                String mdmAttributeName = a.getName();
                String mdmLevelName = a.getLevel().getName();

                if(attributeName.equalsIgnoreCase(mdmAttributeName) &&
                    levelName.equalsIgnoreCase((mdmLevelName))) {
                    isDescribedByConstraintSatisfied = true;
                    Logger.log("... SUCCESS: dimension " + d.getName() + " satisfies the pattern's described-by constraint " + dbc.toString() + "!");
                }
            }
        }

        return isDescribedByConstraintSatisfied;
    }

    /**
     * Method that returns whether or not a roll-up constraint is satisfied by levels of a multidimensional model.
     * @param ruc   roll-up constraint to be checked
     * @param mdm   multidimensional model to be considered
     * @return      true, if the roll up to constraint is satisfied, otherwise false
     */
    private boolean checkRollUpToConstraint(RollUpConstraint ruc, MDM mdm) {

        boolean isRollUpConstraintSatisfied = false;

        // <dim>.<childLevel> ROLLS_UP_TO <dim>.<parentLevel>;
        // "Animal"."Animal" ROLLS_UP_TO "Animal"."Main Breed";
        Target dimensionTarget = ruc.getDimension();
        String dimensionName = getTargetValue(dimensionTarget);

        Target childLevelTarget = ruc.getChildLevel();
        String childLevelName = getTargetValue(childLevelTarget);

        Target parentLevelTarget = ruc.getParentLevel();
        String parentLevelName = getTargetValue(parentLevelTarget);

        Logger.log("... SUCCESS: looking for dimension with name " + dimensionName + "  with a level " + childLevelName + " that rolls up to a level " + parentLevelName + "...\r\n");

        // check if dimension exists in the multidimensional model
        Dimension d = mdm.getDimensionByName(dimensionName);

        if (dimensionName.equalsIgnoreCase(d.getName())) {

            for (RollUp ru : d.getRollUps()) {
                String mdmChildLevelName = ru.getChildLevel().getName();
                String mdmParentLevelName = ru.getParentLevel().getName();

                if(childLevelName.equalsIgnoreCase(mdmChildLevelName) &&
                        parentLevelName.equalsIgnoreCase((mdmParentLevelName))) {
                    isRollUpConstraintSatisfied = true;
                    Logger.log("... SUCCESS: dimension " + d.getName() + " satisfies the pattern's rolls-up constraint " + ruc.toString() + "! \r\n");
                }
            }
        }

        return isRollUpConstraintSatisfied;
    }

    /**
     * Method that returns whether or not a unary applicable-to constraint is satisfied by the application of a
     * business term of a vocabulary, while considering one element that is represented by a pattern's local cube
     * or a multidimensional model's entity. The application of the business term must be valid, that is, all constraints
     * defined in the business term must be satisfied by the element bound as the context parameter.
     * @param uac   unary applicable-to constraint to be checked
     * @param p     pattern providing the local cubes to be considered
     * @param mdm   multidimensional model to be considered
     * @param g     glossary containing the business term to be applied
     * @return      true, if unary applicable-to constraint is satisfied, otherwise false
     */
    private boolean checkUnaryAppConstraint(UnaryAppConstraint uac, Pattern p, MDM mdm, Glossary g) {

        boolean isUnaryApplicableToConstraintSatisfied = false;

        // <cubeMeasure> IS_APPLICABLE_TO <sourceCube>;
        // "Average Milk Yield" IS_APPLICABLE_TO "Milking";
        Target termTarget = uac.getTerm();
        String termName = getTargetValue(termTarget);

        Target entityTarget = uac.getEntity();
        String entityName = getTargetValue(entityTarget);

        Logger.log("... looking for business term with name " + termName + " that yield a valid application by binding " + entityName + " to its <ctx> parameter ...\r\n");

        // check if business term exists in the vocabulary
        Term t = g.getTermByName(termName);

        if (t != null) {
            Logger.log("... business term with name " + termName + " found in vocabulary " + g.getName() + "...\r\n");

            // evaluated type of target entity
            String entityTypeName = "";
            String termTypeName = t.getType().getName();

            // for each unary applicable-to constraint there must be a business term of type
            // UNARY_CALCULCATED_MEASURE, UNARY_CUBE_PREDCIATE, CUBE_ORDERING or UNARY_DIMENSION_PREDICATE,
            // DIMENSION_GROUPING, DIMENSION_ORDERING
            if (termTypeName.equalsIgnoreCase("UNARY_CALCULATED_MEASURE") ||
                    termTypeName.equalsIgnoreCase("UNARY_CUBE_PREDICATE") ||
                    termTypeName.equalsIgnoreCase("CUBE_ORDERING"))
                entityTypeName = "CUBE";
            else if (termTypeName.equalsIgnoreCase("UNARY_DIMENSION_PREDICATE") ||
                    termTypeName.equalsIgnoreCase("DIMENSION_GROUPING") ||
                    termTypeName.equalsIgnoreCase("DIMENSION_ORDERING"))
                entityTypeName = "DIMENSION";
            else
                Logger.log("...FAIL: business term with name " + t.getName() + " has wrong type " + termTypeName + "!\r\n");


            // if the term provides the requested type, it is applied
            if(!entityTypeName.isEmpty()) {

                // bind the <ctx> parameter to the entity's name
                Variable ctxVar = t.getVariableByName("ctx");

                if(ctxVar instanceof SingleVariable && ctxVar.getVariableRole().equalsIgnoreCase("CONTEXT_PARAMETER")) {
                    SingleVariable ctxSingleVar = (SingleVariable)ctxVar;
                    ctxSingleVar.setValue(new SingleValue(entityName));

                    // check if binding of <ctx> parameter is valid in the current context
                    boolean isValidBinding = validateBusinessTermApplication(t, p, mdm, g);

                    if (isValidBinding) {
                        isUnaryApplicableToConstraintSatisfied = true;
                        Logger.log("... SUCCESS: business term " + t.getName() + " satisfies the pattern's unary applicable-to constraint " + uac.toString() + "! \r\n");
                    }
                }
            }
        }
        else {
            Logger.log("... FAIL: business term with name " + termName + " does not exists!\r\n");
        }

        return isUnaryApplicableToConstraintSatisfied;
    }

    /**
     * Method that returns whether or not a binary applicable-to constraint is satisfied by the application of a
     * business term of a vocabulary, while considering two elements that are represented by a pattern's local cube
     * and/or a multidimensional model's entity. The application of the business term must be valid, that is, all constraints
     * defined in the business term must be satisfied by the elements bound as the context parameters.
     * @param bac   binary applicable-to constraint to be checked
     * @param p     pattern providing the local cubes to be considered
     * @param mdm   multidimensional model to be considered
     * @param g     glossary containing the business term to be applied
     * @return      true, if unary applicable-to constraint is satisfied, otherwise false
     */
    private boolean checkBinaryAppConstraint(BinaryAppConstraint bac, Pattern p, MDM mdm, Glossary g) {

        boolean isBinaryApplicableToConstraintSatisfied = false;

        // <compMeasure> IS_APPLICABLE_TO (<interestCube>,<comparisonCube>);
        // "Average Milk Yield Ratio" IS_APPLICABLE_TO ("interestCube","comparisonCube");
        Target termTarget = bac.getTerm();
        String termName = getTargetValue(termTarget);

        Target entity1Target = bac.getEntity1();
        String entity1Name = getTargetValue(entity1Target);

        Target entity2Target = bac.getEntity2();
        String entity2Name = getTargetValue(entity2Target);

        Logger.log("... looking for business term with name " + termName + " that yield a valid application by binding " + entity1Name + " to its <ctx>[1] parameter and " + entity2Name + " to its <ctx>[2] parameter ...\r\n");

        // check if business term exists in the vocabulary
        Term termObj = g.getTermByName(termName);

        if (termObj != null) {
            Logger.log("... business term with name " + termName + " found in vocabulary " + g.getName() + "...\r\n");

            // evaluated type of target entity
            String entityTypeName = "";
            String termTypeName = termObj.getType().getName();

            // for each unary applicable-to constraint there must be a business term of type
            // BINARY_CALCULCATED_MEASURE, BINARY_CUBE_PREDCIATE, CUBE_ORDERING or UNARY_DIMENSION_PREDICATE,
            // DIMENSION_GROUPING, DIMENSION_ORDERING
            if (termTypeName.equalsIgnoreCase("BINARY_CALCULATED_MEASURE") ||
                    termTypeName.equalsIgnoreCase("BINARY_CUBE_PREDICATE"))
                entityTypeName = "CUBE";
            else if (termTypeName.equalsIgnoreCase("BINARY_DIMENSION_PREDICATE"))
                entityTypeName = "DIMENSION";
            else
                Logger.log("... FAIL: business term with name " + termObj.getName() + " has wrong type " + termTypeName + "!\r\n");


            // if the term provides the requested type, it is applied
            if(!entityTypeName.isEmpty()) {

                ArrayVariable ctxVar = termObj.getArrayVariableByName("ctx");
                // bind the <ctx>[1] parameter to the entity1's name
                ctxVar.addArrayVariableEntry(new ArrayVariableEntry(1, new SingleValue(entity1Name)));
                // bind the <ctx>[2] parameter to the entity2's name
                ctxVar.addArrayVariableEntry(new ArrayVariableEntry(2, new SingleValue(entity2Name)));

                termObj = expandConstraintsWithCollectionVariables(termObj);

                // check if binding of <ctx> parameter is valid in the current context
                boolean isValidBinding = validateBusinessTermApplication(termObj, p, mdm, g);

                if (isValidBinding) {
                    isBinaryApplicableToConstraintSatisfied = true;
                    Logger.log("... SUCCESS: business term " + termObj.getName() + " satisfies the pattern's unary applicable-to constraint " + bac.toString() + "! \r\n");
                }
            }
        }
        else {
            Logger.log("... FAIL: business term with name " + termName + " does not exists!\r\n");
        }

        return isBinaryApplicableToConstraintSatisfied;
    }

    /**
     * Method that returns the business term provided with constraints including collections being expanded.
     * Currently only ArrayVariables used as Entities are expanded, that is, for each collection constraint
     * a corresponding constraint with an ArrayVariableAccess is created instead of the ArrayVariable.
     * @param term    term, the constraints of which are to be expanded
     * @return        term with expanded collection constraints
     */
    private Term expandConstraintsWithCollectionVariables(Term term) {

        LinkedList<Constraint> expandedConstraints = new LinkedList<>();
        LinkedList<Constraint> collectionConstraints = new LinkedList<>();

        for(Constraint c : term.getConstraints()) {

            if (c instanceof  TypeConstraint) {
                Target entityTarget = ((TypeConstraint) c).getElement();

                // expand type constraint if entity is an array
                if (entityTarget instanceof ArrayVariable) {

                    ArrayVariable entityArrayVar = (ArrayVariable)entityTarget;

                    // walk through all entries of the array variable and use array variable access to reference its entries
                    for (ArrayVariableEntry arrayVariableEntry : entityArrayVar.getArrayVariableEntries()) {
                        ArrayVariableAccess arrayVariableAccess = new ArrayVariableAccess();
                        arrayVariableAccess.setVariable(entityArrayVar);
                        arrayVariableAccess.setRowIndex(arrayVariableEntry.getIndex());

                        // add the expanded type constraint to the list of new constraints
                        expandedConstraints.add(new TypeConstraint(arrayVariableAccess, ((TypeConstraint) c).getType()));
                    }

                    // add the collection type constraint to be removed
                    collectionConstraints.add(c);
                }
            }
            else if (c instanceof PropertyConstraint) {

                PropertyConstraint pc = (PropertyConstraint) c;
                Target entityTarget = pc.getEntity();

                // expand porperty constraint if entity is an array
                if (entityTarget instanceof ArrayVariable) {
                    ArrayVariable entityArrayVar = (ArrayVariable)entityTarget;

                    // walk through all entries of the array variable and use array variable access to reference its entries
                    for (ArrayVariableEntry arrayVariableEntry : entityArrayVar.getArrayVariableEntries()) {
                        ArrayVariableAccess arrayVariableAccess = new ArrayVariableAccess();
                        arrayVariableAccess.setVariable(entityArrayVar);
                        arrayVariableAccess.setRowIndex(arrayVariableEntry.getIndex());

                        // add the expanded property constraint to the list of new constraints
                        expandedConstraints.add(new PropertyConstraint(arrayVariableAccess, pc.getProperty(), pc.getType()));
                    }

                    // add the collection property constraint to be removed
                    collectionConstraints.add(c);
                }
            }
            else if (c instanceof DomainConstraint) {

                DomainConstraint dc = (DomainConstraint) c;
                Target entityTarget = dc.getEntity();

                // expand domain constraint if entity is an array
                if (entityTarget instanceof ArrayVariable) {
                    ArrayVariable entityArrayVar = (ArrayVariable)entityTarget;

                    // walk through all entries of the array variable and use array variable access to reference its entries
                    for (ArrayVariableEntry arrayVariableEntry : entityArrayVar.getArrayVariableEntries()) {
                        ArrayVariableAccess arrayVariableAccess = new ArrayVariableAccess();
                        arrayVariableAccess.setVariable(entityArrayVar);
                        arrayVariableAccess.setRowIndex(arrayVariableEntry.getIndex());

                        // add the expanded domain constraint to the list of new constraints
                        expandedConstraints.add(new DomainConstraint(arrayVariableAccess, dc.getProperty(), dc.getDomain()));
                    }

                    // add the collection domain constraint to be removed
                    collectionConstraints.add(c);
                }
            }
        }

        // remove collection constraints from term
        for(Constraint c : collectionConstraints)
            term.getConstraints().remove(c);

        // add expanded constraints to term
        for(Constraint c : expandedConstraints)
            term.getConstraints().add(c);

        return term;
    }

    /**
     * Method that evaluates all derivation rules of derived elements of a parameter-free pattern
     * by considering a pattern's local cubes, a multidimensional model, or a vocabulary. Derivation
     * rule defined over a domain are evaluated considering the pattern's local cubes and the multidimensional
     * model, while derivation rules defined over the return type of business term are evaluated considering
     * the business terms in a vocabulary
     * @param pattern   pattern to be grounded
     * @param mdm       multidimensional model to be considered
     * @param glossary  vocabulary to be considered
     * @return          ground pattern
     */
    @Override
    public Pattern ground(Pattern pattern, MDM mdm, Glossary glossary){

        Logger.log("START GROUNDING: grounding of pattern's " + pattern.getName() + "...\r\n");

        if(!pattern.isParameterFree())
            throw new IllegalStateException("Pattern " + pattern.getName() + " is not fully instantiated");

        // build stratification layers of acyclic derivation rules
        List<LinkedList<DerivationRule>> stratification = new ArrayList<LinkedList<DerivationRule>>();
        stratification = getStratification(stratification,0,pattern);

        // print stratification levels
        Logger.log("Stratification graph consists of following levels: \r\n");
        for(int i = 0; i < stratification.size(); i++) {
            Logger.log(i + ": ");
            for(DerivationRule dr : stratification.get(i)) {
                Logger.log(dr.getVariable().toString() + " " + dr.toString() + "; ");
            }
            Logger.log("\r\n");
        }

        // evaluate derivation rules in stratification graph in a bottom-up manner
        for(int i = 0; i<stratification.size(); i++) {
            Logger.log("... start evaluation of derivation rules in stratification level " + i + "...\r\n");

            for (DerivationRule derivationRule : stratification.get(i)) {
                Logger.log("... evaluate derivation rule " + derivationRule.getVariable().toString() + " " + derivationRule.toString() + "\r\n");

                if (derivationRule instanceof DomainDerivationRule) {

                    DomainDerivationRule domainDerivationRule = (DomainDerivationRule) derivationRule;

                    // example statement:
                    //     <compDim>:DIMENSION <= <sourceCube>.<compDimRole>;
                    //     <dom>:NUMBER_VALUE_SET <= <compDim>."Breed";
                    //     <cubeMeasureDom>:NUMBER_VALUE_SET <= "Milk Yield".RETURNS;
                    // example binding: <sourceCube> = "Milking"; <compDimRole> = "Cattle";

                    // case: evaluate domain derivation rule involving the domain of a property of a named entity
                    Target targetEntity = domainDerivationRule.getEntity();
                    Target targetProp = domainDerivationRule.getProperty();

                    if ((targetEntity instanceof SingleVariable) || (targetEntity instanceof SingleValue)) {

                        SingleVariable derivedElementVar = (SingleVariable) derivationRule.getVariable();

                        String entityName = "";
                        // example statement: <compDim>:DIMENSION <= <sourceCube>.<compDimRole>;
                        // target entity is a variable; bind value bound to <sourceCube> to entity name
                        // example statement: <compDim>:DIMENSION <= "Milking".<compDimRole>;
                        // target entity is a constant; bind value "Milking" to entity name
                        entityName = getTargetValue(targetEntity);

                        String propName = "";
                        // example statement: <compDim>:DIMENSION <= <sourceCube>.<compDimRole>;
                        // target property is a variable; bind value bound to <comDimRole> to property name
                        propName = getTargetValue(targetProp);

                        // find entity that matches the entity name in the local multidimensional model of the pattern
                        Logger.log("... looking for entity " + entityName + " with property " + propName + " in the local cubes of " + pattern.getName() + "!" + "\n");

                        String domainName = "";

                        for (LocalCube lc : pattern.getLocalCubes()) {

                            if (lc instanceof DomainLocalCube) {

                                Logger.log("... checking local cube statement " + lc.toString() + "!" + "\n");

                                DomainLocalCube dlc = (DomainLocalCube) lc;

                                Target localEntityTarget = dlc.getEntity();
                                String localEntityName = "";
                                // case 1: <localCube>."Average Milk Yield":<cubeMeasureDom>
                                // case 2: "interestCube"."Average Milk Yield":<cubeMeasureDom>
                                localEntityName = getTargetValue(localEntityTarget);

                                // check if local cube entity matches entity name
                                if (localEntityName.equals(entityName)) {

                                    Target localPropTarget = dlc.getProperty();
                                    String localPropName = "";
                                    // case 1:<localCube>.<localProp>:<cubeMeasureDom>
                                    // case 2: "interestCube"."Average Milk Yield":<cubeMeasureDom>
                                    localPropName = getTargetValue(localPropTarget);

                                    // check if local cube prop matches prop name
                                    if (localPropName.equals(propName)) {

                                        Target localDomainTarget = dlc.getDomain();
                                        // case 1: <localCube>.<localProp>:<cubeMeasureDom>
                                        // case 2: "interestCube"."Average Milk Yield":<cubeMeasureDom>
                                        domainName = getTargetValue(localDomainTarget);
                                    }
                                }
                            }
                        }

                        // if domain name not found, then check MDM
                        if (domainName.isEmpty()) {
                            // find entity that matches the entity name in the multidimensional model
                            MDMEntity entityObj = mdm.getEntityByName(entityName);

                            Logger.log("... looking for entity " + entityName + " with property " + propName + " in multidimensional model " + mdm.getName() + "!" + "\n");

                            // case: entity object represents a cube
                            if (entityObj instanceof Cube) {
                                Cube c = (Cube) entityObj;

                                Measure measure = c.getMeasureByName(propName);
                                DimensionRole dimRole = c.getDimensionRoleByName(propName);

                                if (measure != null) {
                                    domainName = measure.getDomain().getName();
                                    Logger.log("... " + propName + " measure's domain " + measure.getDomain().getName() + " assigned to derived element <" + derivationRule.getVariable().getName() + ">!" + "\n");
                                } else if (dimRole != null) {
                                    domainName = dimRole.getDimension().getName();
                                    Logger.log("... " + propName + " dimension role's dimension " + dimRole.getDimension().getName() + " assigned to derived element <" + derivationRule.getVariable().getName() + ">!" + "\n");
                                } else {
                                    throw new IllegalStateException("Cube " + c.getName() + " in multidimensional model " + mdm.getName() + " does not provide property " + propName + " to resolve derivation rule " + derivationRule.getVariable() + derivationRule.toString() + "!");
                                }
                            }

                            // case: entity object represents a dimension
                            if (entityObj instanceof Dimension) {
                                Dimension d = (Dimension) entityObj;

                                Level level = d.getLevelByName(propName);
                                Attribute attribute = d.getAttributeByName(propName);

                                if (level != null) {
                                    domainName = level.getDomain().getName();
                                    Logger.log("... " + propName + " level's domain " + level.getDomain().getName() + " assigned to derived element <" + derivationRule.getVariable().getName() + ">!" + "\n");
                                } else if (attribute != null) {
                                    domainName = attribute.getDomain().getName();
                                    Logger.log("... " + propName + " attribute's domain " + attribute.getDomain().getName() + " assigned to derived element <" + derivationRule.getVariable().getName() + ">!" + "\n");
                                } else {
                                    throw new IllegalStateException("Dimension " + d.getName() + " in multidimensional model " + mdm.getName() + " does not provide property " + propName + " to resolve derivation rule " + derivationRule.getVariable() + derivationRule.toString() + "!");
                                }
                            }
                        }

                        // set the value of the derived element
                        ((SingleVariable) derivationRule.getVariable()).setValue(new SingleValue(domainName));
                    }
                } else if (derivationRule instanceof ReturnTypeDerivationRule) {

                    String returnType = "";

                    ReturnTypeDerivationRule returnTypeDerivationRule = (ReturnTypeDerivationRule) derivationRule;

                    if (returnTypeDerivationRule.getVariable() instanceof SingleVariable) {

                        Target targetTerm = returnTypeDerivationRule.getBusinessTerm();

                        if (targetTerm instanceof SingleVariable || targetTerm instanceof SingleValue) {

                            // case 1: <cubeMeasureDom>:NUMBER_VALUE_SET <= <cubeMeasure>.RETURNS;
                            // case 2: <cubeMeasureDom>:NUMBER_VALUE_SET <= "Milk Yield".RETURNS;
                            String termName = getTargetValue(targetTerm);

                            Logger.log("... looking for business term " + termName + " in vocabulary " + glossary.getName() + "!" + "\n");
                            Term term = glossary.getTermByName(termName);

                            if (term != null) {

                                returnType = term.getReturnType();

                                if (returnType != null && !returnType.isEmpty()) {
                                    SingleVariable derivedElementVar = (SingleVariable) returnTypeDerivationRule.getVariable();
                                    derivedElementVar.setValue(new SingleValue(returnType));
                                    Logger.log("... business term's return type " + returnType + " assigned to derived element <" + derivationRule.getVariable().getName() + ">!" + "\n");
                                } else {
                                    throw new IllegalStateException("No return type defined for business term named \"" + termName + "\"!");
                                }
                            } else
                                throw new IllegalStateException("Business term named \"" + termName + "\" not defined in " + glossary.getName() + "!");
                        }
                    }
                }
            }
            Logger.log("... end evaluation of derivation rules in stratification level " + i + "..." + "\n");
        }
        Logger.log("END GROUNDING: grounding of pattern's " + pattern.getName() + " finished! \r\n");

        return pattern;
    }


    /**
     * Method that assigns the derivation rules to stratification levels, that is, derivation rules on lower levels
     * need to be evaluated first, as they provide bindings for derived elements that are necessary for the higher levels
     * @param stratification    structure to store the different stratification levels
     * @param level             stratification level to be populated
     * @param p                 pattern providing the derivation rules
     * @return                  structure containing stratification levels including the corresponding derivation rules
     */
    private List<LinkedList<DerivationRule>> getStratification(List<LinkedList<DerivationRule>> stratification, int level, Pattern p) {

        if(level < 0)
            throw new IllegalStateException("Level must be positive!");

        // add new linked list for the derivation rules to be added
        if(stratification.size() < level+1)
            stratification.add(new LinkedList<DerivationRule>());

        for(DerivationRule derivationRule : p.getDerivationRules()) {
            Target derivationRuleEntity = null;

            // get the entity respectively the business term variable or constants
            if(derivationRule instanceof DomainDerivationRule) {
                derivationRuleEntity = ((DomainDerivationRule)derivationRule).getEntity();
            }
            else if(derivationRule instanceof  ReturnTypeDerivationRule) {
                derivationRuleEntity = ((ReturnTypeDerivationRule)derivationRule).getBusinessTerm();
            }

            // add all entities defined by parameters or as constants to level 0
            if(level == 0) {
                if (derivationRuleEntity instanceof SingleVariable) {

                    SingleVariable entityVar = (SingleVariable) derivationRuleEntity;

                    if (entityVar.getVariableRole().equals("PARAMETER")) {
                        stratification.get(level).add(derivationRule);
                    }

                } else if (derivationRuleEntity instanceof SingleValue) {
                    stratification.get(level).add(derivationRule);
                }
            }
            // add all entities defined by derived elements that depend on other derived elements
            // that are located in a lower stratification level
            else {
                if (derivationRuleEntity instanceof SingleVariable) {

                    SingleVariable entityVar = (SingleVariable) derivationRuleEntity;

                    // check if derivation rule depends on derived element of next lower layer, if yes, then add
                    if (entityVar.getVariableRole().equals("DERIVED") &&
                            dependsOnStratificationLevel(stratification, level-1, entityVar)) {

                        stratification.get(level).add(derivationRule);
                    }
                }
            }
        }

        // stop recursion if all derivation rules are assigned
        if(getStratificationSize(stratification) < p.getDerivationRules().size())
            stratification = getStratification(stratification, level + 1, p);

        return stratification;
    }

    /**
     * Method that evaluates the number of derivation rules assigned to different levels
     * of the stratification
     * @param stratification    structure that stores the different stratification levels                                                                                                                                                        the structure to store the different stratification levels
     * @return                  number of derivation rules assigned to the stratification
     */
    private int getStratificationSize(List<LinkedList<DerivationRule>> stratification) {

        int size=0;

        for(LinkedList<DerivationRule> dr : stratification)
            size += dr.size();

        return size;
    }

    /**
     * Method that checks whether a derived element depends on a derived element
     * that is located on another stratification level
     * @param stratification    structure that stores the different stratification levels
     * @param level             stratification level to be checked
     * @param entityVar         derived element to be checked
     * @return                  true if it depends on a derived element from the specified level, otherwise false
     */
    private boolean dependsOnStratificationLevel(List<LinkedList<DerivationRule>> stratification, int level, SingleVariable entityVar) {

        if(stratification.size() < level)
            return false;

        // check whether the entity of its derivation rule is a derived element
        // that is defined by a derived rule located on the specific level
        for(DerivationRule derivationRule : stratification.get(level))
            if(derivationRule.getVariable().getId() == entityVar.getId())
                return true;

        return false;
    }

    /**
     * Method that returns the value assigned to a TargetObject. It returns for SingleVariable objects
     * its SingleValue object value, for SingleValue objects its value, and for ArrayVariableAccesses
     * the value of the accessed ArrayVariableEntry
     * @param target    target for which the value is requested
     * @return          value assigned as a string
     */
    private String getTargetValue(Target target) {
        String value = "";

        // return value assigned to a single variable, e.g., <localCube>
        if (target instanceof SingleVariable) {
            SingleVariable targetVar = (SingleVariable)target;
            value = ((SingleValue)targetVar.getValue()).getValue();
        }
        // return value assigned by a single value, e.g., "interestCube"
        else if (target instanceof SingleValue) {
            SingleValue targetVal = (SingleValue) target;
            value = targetVal.getValue();
        }
        // handle array variable access, e.g., <ctx>[1]
        else if (target instanceof ArrayVariableAccess) {
            ArrayVariableAccess targetAccess = (ArrayVariableAccess) target;
            for(ArrayVariableEntry arrayVariableEntry : targetAccess.getVariable().getArrayVariableEntries())
                if (arrayVariableEntry.getIndex().intValue() == targetAccess.getRowIndex().intValue())
                    value = arrayVariableEntry.getValue().toString();
        }

        return value;
    }
}
