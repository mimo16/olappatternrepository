package at.dke.olappattern.repository_app;

import at.dke.olappattern.data.MDM.Dimension;
import at.dke.olappattern.data.MDM.MDM;
import at.dke.olappattern.data.Target;
import at.dke.olappattern.data.Template;
import at.dke.olappattern.data.pattern.Pattern;
import at.dke.olappattern.data.repository.Glossary;
import at.dke.olappattern.data.term.Term;
import at.dke.olappattern.data.value.SingleValue;
import at.dke.olappattern.data.variable.ArrayVariable;
import at.dke.olappattern.data.variable.MapVariable;
import at.dke.olappattern.data.variable.SingleVariable;
import at.dke.olappattern.data.variable.Variable;
import at.dke.olappattern.macro.MacroLexer;
import at.dke.olappattern.macro.MacroParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A specific preprocessor unit that is responsible for processing execute commands
 * It also is a at.dke.olap_pattern_repo.MacroResolver, hence does execute macros itself
 */
public class TemplatePreprocessor implements MacroResolver, PreprocessorUnit {
    /*
    termPath and mdmPath are extracted from the execution or ground object and stored locally for as long
    as the processing takes. The member c stores the term or pattern to be instantiated for the same
    time.
     */
    private StorageUnit storageUnit;
    private ValidatorUnit validatorUnit;
    private Pattern p;
    private MDM m;
    private Glossary g;

    public TemplatePreprocessor(StorageUnit storageUnit, ValidatorUnit validatorUnit) {
        this.storageUnit = storageUnit;
        this.validatorUnit = validatorUnit;
    }

    @Override
    public String executePattern(String mdm, String glossary, Pattern pattern, String catalogue){
        Set<Template> templates = new HashSet<>();

        try {
            storageUnit.openSession();

            // load MDM
            String[] pathSplits = mdm.split("/");
             m = this.storageUnit.getMDM(pathSplits[0], pathSplits[1]);
            if(m == null) throw new IllegalStateException("MDM not found");

            // load vocabulary
            pathSplits = glossary.split("/");
            g = this.storageUnit.getGlossary(pathSplits[0], pathSplits[1]);
            if(g == null) throw new IllegalStateException("Glossary not found");

            // load pattern
            pathSplits = catalogue.split("/");
            p = this.storageUnit.getPattern(pathSplits[0], pathSplits[1], pattern.getName());
            if(p == null) throw new IllegalStateException("Pattern not found");

            // ground pattern, i.e., resolve all derivation rules by taking into account the MDM and vocabulary
            p = this.validatorUnit.ground(p, m, g);


            // check if the pattern is executable, i.e., whether all constraints are satisfied by the MDM and the vocabulary
            boolean isValid = this.validatorUnit.validate(p, m, g);

            if(!isValid)
                throw new IllegalStateException("Pattern " + p.getName() + " cannot be executed in the context of MDM " +
                    m.getName() + " using Glossary " + g.getName() + "!");

            if (pattern.getTemplates().isEmpty()) {
                //all templates from the pattern p from the database
                templates = p.getTemplates();
            } else {
                //get only the pattern(s) from p that are also included in pattern
                for (Template template : pattern.getTemplates()) {
                    List<Template> temp = storageUnit.getPatternTemplates(pathSplits[0], pathSplits[1], p.getName(), template);
                    if(temp != null) templates.addAll(temp);
                }
            }

            return processTemplates(templates, p);
        }
        catch(Exception ex){
            //System.out.println(ex.getMessage());
            throw ex;
        }
        finally {
            storageUnit.closeSession();
        }
    }

    /**
     * Method to execute a (sub)set of templates belonging to an instantiated pattern
     * @param templates the (sub)set of templates to be executed
     * @param p the instantiated pattern
     * @return  the executable queries
     */
    private String processTemplates(Set<Template> templates, Pattern p){
        String result = "";

        for (Template t : templates) {
            String instantiatedTemplate = this.substituteTemplatePlaceholders(t, p);
            //remove quotes at the beginning and end of the expression
            instantiatedTemplate = instantiatedTemplate.substring(1, instantiatedTemplate.length() - 1);
            instantiatedTemplate = instantiatedTemplate.replace("\"\"", "\"");

            MacroLexer lexer = new MacroLexer(CharStreams.fromString(instantiatedTemplate));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            MacroParser parser = new MacroParser(tokens);

            parser.addErrorListener(new BaseErrorListener() {
                @Override
                public void syntaxError(Recognizer<?, ?> recognizer,
                                        Object offendingSymbol,
                                        int line,
                                        int charPositionInLine,
                                        String msg,
                                        RecognitionException e) {
                    throw new IllegalStateException("Failed to parse at line " + line + " due to " + msg, e);
                }
            });

            ParserRuleContext ruleContext = parser.macrol();
            SemanticMacroAnalyser listener = new SemanticMacroAnalyser();
            listener.setMacroResolver(this);
            listener.setLanguage(t.getLanguage());
            listener.setDialect(t.getDialect());

            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(listener, ruleContext);

            result += listener.query + "\r\n \r\n";
        }
        return result;
    }

    /**
     * Method that replaces an expression's placeholders for the variables with their values
     * @param template the template containing the expression
     * @return The instantiated template
     */
    private String substituteTemplatePlaceholders(Template template, Pattern p){
        String result = template.getExpression();
        for (Target t : p.getTargets()) {
            if(t instanceof Variable) {
                //derived variables are handled separately
                if (t instanceof SingleVariable) {
                    SingleVariable sv = (SingleVariable) t;
                    if (sv.getValue() instanceof SingleValue) {
                        SingleValue value = (SingleValue) sv.getValue();
                        result = result.replace("<" + sv.getName() + ">", value.getValue());
                    } else {
                        // not all derived elements are also used in the template
                        if(!sv.getVariableRole().equals("DERIVED"))
                            throw new IllegalStateException("The pattern is not fully instantiated. Failed to substitute " + sv.getName());
                    }
                } else if (t instanceof MapVariable) {
                    //insert MapVariable values into template
                } else if (t instanceof ArrayVariable) {
                    //insert ArrayVariable values into template
                }
            }
        }
        return result;
    }

    @Override
    public String expr(String termName, String language, String dialect, String[] arguments) {
        if(termName == null || arguments == null) return null;

        try {
            Term term = this.g.getTermByName(termName);
            if (term != null) {
                Template template = null;
                for (Template t : term.getTemplates()) {
                    if (t.getLanguage().equals(language) && t.getDialect().equals(dialect)) {
                        template = t;
                        break;
                    }
                }

                if (template != null) {
                    String result = template.getExpression();
                    Variable v = term.getVariableByName("ctx");

                    if(v instanceof SingleVariable){
                        if(arguments.length != 1) throw new IllegalArgumentException("Too many arguments for business term " + termName);
                        else{
                            result = result.replace("<ctx>", arguments[0]);
                        }
                    }
                    else {
                        for (int i = 0; i < arguments.length; i++) {
                                result = result.replace("<ctx>[" + (i+1) + "]", arguments[i]);
                        }
                    }
                    result = result.replace("\"*{", "");
                    result = result.replace("}*\"", "");
                    result = result.replace("\"\"", "\"");
                    return result;
                }
            }
        }
        catch(Exception e){
            return null;
        }
        return null;
    }

    @Override
    public String dimKey(String dimName) {
        if(dimName == null) return null;

        try {
            Dimension d = m.getDimensionByName(dimName);
            if(d != null){
                String baseLevel = d.getBaseLevelName();
                return baseLevel;
            }
        }
        catch(Exception e){
            return null;
        }
        return null;
    }
}
