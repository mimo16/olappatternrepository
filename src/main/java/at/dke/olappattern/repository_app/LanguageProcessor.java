package at.dke.olappattern.repository_app;

import at.dke.olappattern.language.EMDMLLexer;
import at.dke.olappattern.language.EMDMLParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import at.dke.olappattern.repository_app.event.*;

public class LanguageProcessor implements LanguageProcessingUnit {
    private SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer();

    @Override
    public void process(String s) {
        if(s == null || s.equals("")){
            throw new IllegalArgumentException("Empty Command");
        }

        EMDMLLexer lexer;
        try {
            lexer = new EMDMLLexer(CharStreams.fromString(s));

            lexer.addErrorListener(new BaseErrorListener() {
                @Override
                public void syntaxError(Recognizer<?, ?> recognizer,
                                        Object offendingSymbol,
                                        int line,
                                        int charPositionInLine,
                                        String msg,
                                        RecognitionException e) {
                    throw new IllegalStateException("Failed to parse at line " + line + " due to " + msg, e);
                }
            });

            CommonTokenStream tokens = new CommonTokenStream(lexer);
            EMDMLParser parser = new EMDMLParser(tokens);

            parser.addErrorListener(new BaseErrorListener() {
                @Override
                public void syntaxError(Recognizer<?, ?> recognizer,
                                        Object offendingSymbol,
                                        int line,
                                        int charPositionInLine,
                                        String msg,
                                        RecognitionException e) {
                    throw new IllegalStateException("Failed to parse at line " + line + " due to " + msg, e);
                }
            });

            ParserRuleContext ruleContext = parser.emdm_stmt();

            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(semanticAnalyzer, ruleContext);
        }
        catch (Exception e) {
            //System.out.println(e.getMessage());
            throw e;
        }
    }

    @Override
    public void addMDMListener(MDMListener l) {
        semanticAnalyzer.addMDMListener(l);
    }

    @Override
    public void addPatternListener(PatternListener l) {
        semanticAnalyzer.addPatternListener(l);
    }

    @Override
    public void addTermListener(TermListener l) {
        semanticAnalyzer.addTermListener(l);
    }

    @Override
    public void addSearchListener(SearchListener l) {
        semanticAnalyzer.addSearchListener(l);
    }

    @Override
    public void addOrganizationElementListener(OrganizationElementListener l) {
        semanticAnalyzer.addOrganisationElementListener(l);
    }

    @Override
    public void addShowListener(ShowListener l) {
        semanticAnalyzer.addShowListener(l);
    }
}
