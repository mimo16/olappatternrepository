// Generated from C:/Daten/OLAPPatternRepository/src/main/resources\EMDML.g4 by ANTLR 4.8
package at.dke.olappattern.language;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class EMDMLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, ALIAS=20, AS=21, APPLIES=22, ATTRIBUTE=23, BINARY_TUPLE=24, 
		BINARY_CUBE_PREDICATE=25, BINARY_CALCULATED_MEASURE=26, BINARY_DIMENSION_PREDICATE=27, 
		BINDINGS=28, CATALOGUE=29, CONSTRAINTS=30, CONTAIN=31, CONTEXT=32, CREATE=33, 
		CUBE=34, CUBES=35, CUBE_ORDERING=36, CUBE_PROPERTY=37, DATA_MODEL=38, 
		DELETE=39, DERIVED=40, DESCRIBED_BY=41, DESCRIPTION=42, DIALECT=43, DIMENSION=44, 
		DIMENSION_GROUPING=45, DIMENSION_ORDERING=46, DIMENSION_PROPERTY=47, DIMENSION_ROLE=48, 
		DOMAIN=49, ELEMENTS=50, END=51, EXAMPLE=52, EXECUTE=53, EXPECTED=54, EXPRESSION=55, 
		FOR=56, FROM=57, GROUND=58, HAS=59, IN=60, INSTANCE=61, INSTANTIATE=62, 
		IS_APPLICABLE_TO=63, IS_APPLIED_TO=64, IS_OPTIONAL=65, LANGUAGE=66, LEVEL=67, 
		LOCAL=68, MEASURE=69, MULTIDIMENSIONAL_MODEL=70, NUMBER_VALUE_SET=71, 
		OR=72, OWNER=73, PARAMETERS=74, PATTERN=75, PATTERN_ALIAS=76, PROBLEM=77, 
		PROPERTIES=78, RELATED=79, REPOSITORY=80, REPLACE=81, RETURNS=82, ROLLS_UP_TO=83, 
		SEARCH=84, SHOW=85, SOLUTION=86, STRING_VALUE_SET=87, TEMPLATE=88, TERM=89, 
		TERNARY_TUPLE=90, TO=91, UNARY_CUBE_PREDICATE=92, UNARY_CALCULATED_MEASURE=93, 
		UNARY_DIMENSION_PREDICATE=94, USING=95, VALIDATE=96, VARIABLES=97, VARIANT=98, 
		GLOSSARY=99, WITH=100, QUARTERNARY_TUPLE=101, STRING=102, NAME=103, NUMBER=104, 
		SPACE=105, WS=106, COMMENT=107, LINE_COMMENT=108, COL=109;
	public static final int
		RULE_emdm_stmt = 0, RULE_c_stmt = 1, RULE_cp_stmt = 2, RULE_ct_stmt = 3, 
		RULE_cm_stmt = 4, RULE_cr_stmt = 5, RULE_d_stmt = 6, RULE_x_stmt = 7, 
		RULE_i_stmt = 8, RULE_g_stmt = 9, RULE_s_stmt = 10, RULE_f_stmt = 11, 
		RULE_s_trgt = 12, RULE_s_exp = 13, RULE_s_sct = 14, RULE_r_exp = 15, RULE_p_def = 16, 
		RULE_p_descr = 17, RULE_p_temp = 18, RULE_p_inst = 19, RULE_p_del = 20, 
		RULE_p_del_descr = 21, RULE_p_del_temp = 22, RULE_p_grnd = 23, RULE_p_exec = 24, 
		RULE_t_def = 25, RULE_t_descr = 26, RULE_t_temp = 27, RULE_t_del = 28, 
		RULE_t_del_descr = 29, RULE_t_del_temp = 30, RULE_temp_elem = 31, RULE_temp_elem_meta = 32, 
		RULE_param_decl = 33, RULE_derv_decl = 34, RULE_for_exp = 35, RULE_var_decl = 36, 
		RULE_cstr_decl = 37, RULE_mv_cstr_decl = 38, RULE_sv_cstr_decl = 39, RULE_type_cstr_decl = 40, 
		RULE_dom_cstr_decl = 41, RULE_prop_cstr_decl = 42, RULE_return_cstr_decl = 43, 
		RULE_app_cstr_decl = 44, RULE_scope_cstr_decl = 45, RULE_scope_exp = 46, 
		RULE_rollup_cstr_decl = 47, RULE_descr_cstr_decl = 48, RULE_term_cstr_decl = 49, 
		RULE_lc_decl = 50, RULE_mv_lc_decl = 51, RULE_sv_lc_decl = 52, RULE_type_lc_decl = 53, 
		RULE_dom_lc_decl = 54, RULE_prop_lc_decl = 55, RULE_rollup_frag_decl = 56, 
		RULE_descr_frag_decl = 57, RULE_binding_exp = 58, RULE_val_exp = 59, RULE_sv_exp = 60, 
		RULE_mv_exp = 61, RULE_elem_acc_exp = 62, RULE_var_acc_exp = 63, RULE_array_acc_exp = 64, 
		RULE_map_simple_acc_exp = 65, RULE_tuple_acc_exp = 66, RULE_elem_exp = 67, 
		RULE_var_exp = 68, RULE_tuple_exp = 69, RULE_array_exp = 70, RULE_map_exp = 71, 
		RULE_map_acc_exp = 72, RULE_idx_exp = 73, RULE_const_exp = 74, RULE_op_exp = 75, 
		RULE_path_exp = 76, RULE_cube_def = 77, RULE_dim_def = 78, RULE_m_del = 79, 
		RULE_meas_decl = 80, RULE_dim_role_decl = 81, RULE_lvl_decl = 82, RULE_attr_decl = 83, 
		RULE_roll_up_rel_decl = 84, RULE_descr_by_rel_decl = 85, RULE_type = 86, 
		RULE_t_type = 87, RULE_m_entity_type = 88, RULE_m_prop_type = 89, RULE_v_type = 90, 
		RULE_a_type = 91, RULE_descr_txt = 92, RULE_temp_txt = 93, RULE_prob_txt = 94, 
		RULE_sol_txt = 95, RULE_ex_txt = 96, RULE_var_label = 97, RULE_alias_name = 98, 
		RULE_model_name = 99, RULE_variant_name = 100, RULE_lang_name = 101, RULE_dialect_name = 102, 
		RULE_const_name = 103, RULE_meas_name = 104, RULE_dim_role_name = 105, 
		RULE_lvl_name = 106, RULE_attr_name = 107, RULE_val_set_name = 108, RULE_idx_name = 109, 
		RULE_elem_name = 110, RULE_value_set_name = 111, RULE_idx_no = 112, RULE_s_str = 113, 
		RULE_p_loc_name = 114, RULE_dim_loc_name = 115, RULE_p_name = 116, RULE_t_name = 117, 
		RULE_cube_name = 118, RULE_dim_name = 119, RULE_mdm_name = 120, RULE_voc_name = 121, 
		RULE_s_name = 122;
	private static String[] makeRuleNames() {
		return new String[] {
			"emdm_stmt", "c_stmt", "cp_stmt", "ct_stmt", "cm_stmt", "cr_stmt", "d_stmt", 
			"x_stmt", "i_stmt", "g_stmt", "s_stmt", "f_stmt", "s_trgt", "s_exp", 
			"s_sct", "r_exp", "p_def", "p_descr", "p_temp", "p_inst", "p_del", "p_del_descr", 
			"p_del_temp", "p_grnd", "p_exec", "t_def", "t_descr", "t_temp", "t_del", 
			"t_del_descr", "t_del_temp", "temp_elem", "temp_elem_meta", "param_decl", 
			"derv_decl", "for_exp", "var_decl", "cstr_decl", "mv_cstr_decl", "sv_cstr_decl", 
			"type_cstr_decl", "dom_cstr_decl", "prop_cstr_decl", "return_cstr_decl", 
			"app_cstr_decl", "scope_cstr_decl", "scope_exp", "rollup_cstr_decl", 
			"descr_cstr_decl", "term_cstr_decl", "lc_decl", "mv_lc_decl", "sv_lc_decl", 
			"type_lc_decl", "dom_lc_decl", "prop_lc_decl", "rollup_frag_decl", "descr_frag_decl", 
			"binding_exp", "val_exp", "sv_exp", "mv_exp", "elem_acc_exp", "var_acc_exp", 
			"array_acc_exp", "map_simple_acc_exp", "tuple_acc_exp", "elem_exp", "var_exp", 
			"tuple_exp", "array_exp", "map_exp", "map_acc_exp", "idx_exp", "const_exp", 
			"op_exp", "path_exp", "cube_def", "dim_def", "m_del", "meas_decl", "dim_role_decl", 
			"lvl_decl", "attr_decl", "roll_up_rel_decl", "descr_by_rel_decl", "type", 
			"t_type", "m_entity_type", "m_prop_type", "v_type", "a_type", "descr_txt", 
			"temp_txt", "prob_txt", "sol_txt", "ex_txt", "var_label", "alias_name", 
			"model_name", "variant_name", "lang_name", "dialect_name", "const_name", 
			"meas_name", "dim_role_name", "lvl_name", "attr_name", "val_set_name", 
			"idx_name", "elem_name", "value_set_name", "idx_no", "s_str", "p_loc_name", 
			"dim_loc_name", "p_name", "t_name", "cube_name", "dim_name", "mdm_name", 
			"voc_name", "s_name"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "','", "'='", "'<='", "'.'", "'('", "')'", "':'", "'+'", "'{'", 
			"'}'", "'<'", "'>'", "'()'", "'['", "']'", "'*'", "'.['", "'[]'", "'/'", 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, "';'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, "ALIAS", "AS", "APPLIES", 
			"ATTRIBUTE", "BINARY_TUPLE", "BINARY_CUBE_PREDICATE", "BINARY_CALCULATED_MEASURE", 
			"BINARY_DIMENSION_PREDICATE", "BINDINGS", "CATALOGUE", "CONSTRAINTS", 
			"CONTAIN", "CONTEXT", "CREATE", "CUBE", "CUBES", "CUBE_ORDERING", "CUBE_PROPERTY", 
			"DATA_MODEL", "DELETE", "DERIVED", "DESCRIBED_BY", "DESCRIPTION", "DIALECT", 
			"DIMENSION", "DIMENSION_GROUPING", "DIMENSION_ORDERING", "DIMENSION_PROPERTY", 
			"DIMENSION_ROLE", "DOMAIN", "ELEMENTS", "END", "EXAMPLE", "EXECUTE", 
			"EXPECTED", "EXPRESSION", "FOR", "FROM", "GROUND", "HAS", "IN", "INSTANCE", 
			"INSTANTIATE", "IS_APPLICABLE_TO", "IS_APPLIED_TO", "IS_OPTIONAL", "LANGUAGE", 
			"LEVEL", "LOCAL", "MEASURE", "MULTIDIMENSIONAL_MODEL", "NUMBER_VALUE_SET", 
			"OR", "OWNER", "PARAMETERS", "PATTERN", "PATTERN_ALIAS", "PROBLEM", "PROPERTIES", 
			"RELATED", "REPOSITORY", "REPLACE", "RETURNS", "ROLLS_UP_TO", "SEARCH", 
			"SHOW", "SOLUTION", "STRING_VALUE_SET", "TEMPLATE", "TERM", "TERNARY_TUPLE", 
			"TO", "UNARY_CUBE_PREDICATE", "UNARY_CALCULATED_MEASURE", "UNARY_DIMENSION_PREDICATE", 
			"USING", "VALIDATE", "VARIABLES", "VARIANT", "GLOSSARY", "WITH", "QUARTERNARY_TUPLE", 
			"STRING", "NAME", "NUMBER", "SPACE", "WS", "COMMENT", "LINE_COMMENT", 
			"COL"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "EMDML.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public EMDMLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class Emdm_stmtContext extends ParserRuleContext {
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<C_stmtContext> c_stmt() {
			return getRuleContexts(C_stmtContext.class);
		}
		public C_stmtContext c_stmt(int i) {
			return getRuleContext(C_stmtContext.class,i);
		}
		public List<D_stmtContext> d_stmt() {
			return getRuleContexts(D_stmtContext.class);
		}
		public D_stmtContext d_stmt(int i) {
			return getRuleContext(D_stmtContext.class,i);
		}
		public List<G_stmtContext> g_stmt() {
			return getRuleContexts(G_stmtContext.class);
		}
		public G_stmtContext g_stmt(int i) {
			return getRuleContext(G_stmtContext.class,i);
		}
		public List<I_stmtContext> i_stmt() {
			return getRuleContexts(I_stmtContext.class);
		}
		public I_stmtContext i_stmt(int i) {
			return getRuleContext(I_stmtContext.class,i);
		}
		public List<X_stmtContext> x_stmt() {
			return getRuleContexts(X_stmtContext.class);
		}
		public X_stmtContext x_stmt(int i) {
			return getRuleContext(X_stmtContext.class,i);
		}
		public List<S_stmtContext> s_stmt() {
			return getRuleContexts(S_stmtContext.class);
		}
		public S_stmtContext s_stmt(int i) {
			return getRuleContext(S_stmtContext.class,i);
		}
		public List<F_stmtContext> f_stmt() {
			return getRuleContexts(F_stmtContext.class);
		}
		public F_stmtContext f_stmt(int i) {
			return getRuleContext(F_stmtContext.class,i);
		}
		public Emdm_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_emdm_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterEmdm_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitEmdm_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitEmdm_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Emdm_stmtContext emdm_stmt() throws RecognitionException {
		Emdm_stmtContext _localctx = new Emdm_stmtContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_emdm_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(257); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(253);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CREATE:
					{
					setState(246);
					c_stmt();
					}
					break;
				case DELETE:
					{
					setState(247);
					d_stmt();
					}
					break;
				case GROUND:
					{
					setState(248);
					g_stmt();
					}
					break;
				case INSTANTIATE:
					{
					setState(249);
					i_stmt();
					}
					break;
				case EXECUTE:
					{
					setState(250);
					x_stmt();
					}
					break;
				case SHOW:
					{
					setState(251);
					s_stmt();
					}
					break;
				case SEARCH:
					{
					setState(252);
					f_stmt();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(255);
				match(COL);
				}
				}
				setState(259); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 33)) & ~0x3f) == 0 && ((1L << (_la - 33)) & ((1L << (CREATE - 33)) | (1L << (DELETE - 33)) | (1L << (EXECUTE - 33)) | (1L << (GROUND - 33)) | (1L << (INSTANTIATE - 33)) | (1L << (SEARCH - 33)) | (1L << (SHOW - 33)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class C_stmtContext extends ParserRuleContext {
		public TerminalNode CREATE() { return getToken(EMDMLParser.CREATE, 0); }
		public Cp_stmtContext cp_stmt() {
			return getRuleContext(Cp_stmtContext.class,0);
		}
		public Ct_stmtContext ct_stmt() {
			return getRuleContext(Ct_stmtContext.class,0);
		}
		public Cm_stmtContext cm_stmt() {
			return getRuleContext(Cm_stmtContext.class,0);
		}
		public Cr_stmtContext cr_stmt() {
			return getRuleContext(Cr_stmtContext.class,0);
		}
		public TerminalNode OR() { return getToken(EMDMLParser.OR, 0); }
		public TerminalNode REPLACE() { return getToken(EMDMLParser.REPLACE, 0); }
		public C_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_c_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterC_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitC_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitC_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final C_stmtContext c_stmt() throws RecognitionException {
		C_stmtContext _localctx = new C_stmtContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_c_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			match(CREATE);
			setState(264);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OR) {
				{
				setState(262);
				match(OR);
				setState(263);
				match(REPLACE);
				}
			}

			setState(271);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(266);
				cp_stmt();
				}
				break;
			case 2:
				{
				setState(267);
				ct_stmt();
				}
				break;
			case 3:
				{
				setState(268);
				ct_stmt();
				}
				break;
			case 4:
				{
				setState(269);
				cm_stmt();
				}
				break;
			case 5:
				{
				setState(270);
				cr_stmt();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cp_stmtContext extends ParserRuleContext {
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public P_defContext p_def() {
			return getRuleContext(P_defContext.class,0);
		}
		public P_descrContext p_descr() {
			return getRuleContext(P_descrContext.class,0);
		}
		public P_tempContext p_temp() {
			return getRuleContext(P_tempContext.class,0);
		}
		public Cp_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cp_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterCp_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitCp_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitCp_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cp_stmtContext cp_stmt() throws RecognitionException {
		Cp_stmtContext _localctx = new Cp_stmtContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_cp_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(273);
			match(PATTERN);
			setState(277);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING:
				{
				setState(274);
				p_def();
				}
				break;
			case DESCRIPTION:
				{
				setState(275);
				p_descr();
				}
				break;
			case TEMPLATE:
				{
				setState(276);
				p_temp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ct_stmtContext extends ParserRuleContext {
		public T_defContext t_def() {
			return getRuleContext(T_defContext.class,0);
		}
		public TerminalNode TERM() { return getToken(EMDMLParser.TERM, 0); }
		public T_descrContext t_descr() {
			return getRuleContext(T_descrContext.class,0);
		}
		public T_tempContext t_temp() {
			return getRuleContext(T_tempContext.class,0);
		}
		public Ct_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ct_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterCt_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitCt_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitCt_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ct_stmtContext ct_stmt() throws RecognitionException {
		Ct_stmtContext _localctx = new Ct_stmtContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_ct_stmt);
		try {
			setState(285);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BINARY_CUBE_PREDICATE:
			case BINARY_CALCULATED_MEASURE:
			case BINARY_DIMENSION_PREDICATE:
			case CUBE_ORDERING:
			case DIMENSION_GROUPING:
			case DIMENSION_ORDERING:
			case UNARY_CUBE_PREDICATE:
			case UNARY_CALCULATED_MEASURE:
			case UNARY_DIMENSION_PREDICATE:
				enterOuterAlt(_localctx, 1);
				{
				setState(279);
				t_def();
				}
				break;
			case TERM:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(280);
				match(TERM);
				setState(283);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case DESCRIPTION:
					{
					setState(281);
					t_descr();
					}
					break;
				case TEMPLATE:
					{
					setState(282);
					t_temp();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cm_stmtContext extends ParserRuleContext {
		public Cube_defContext cube_def() {
			return getRuleContext(Cube_defContext.class,0);
		}
		public Dim_defContext dim_def() {
			return getRuleContext(Dim_defContext.class,0);
		}
		public Cm_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cm_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterCm_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitCm_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitCm_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cm_stmtContext cm_stmt() throws RecognitionException {
		Cm_stmtContext _localctx = new Cm_stmtContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_cm_stmt);
		try {
			setState(289);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CUBE:
				enterOuterAlt(_localctx, 1);
				{
				setState(287);
				cube_def();
				}
				break;
			case DIMENSION:
				enterOuterAlt(_localctx, 2);
				{
				setState(288);
				dim_def();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cr_stmtContext extends ParserRuleContext {
		public R_expContext r_exp() {
			return getRuleContext(R_expContext.class,0);
		}
		public Cr_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cr_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterCr_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitCr_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitCr_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cr_stmtContext cr_stmt() throws RecognitionException {
		Cr_stmtContext _localctx = new Cr_stmtContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_cr_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(291);
			r_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class D_stmtContext extends ParserRuleContext {
		public TerminalNode DELETE() { return getToken(EMDMLParser.DELETE, 0); }
		public P_delContext p_del() {
			return getRuleContext(P_delContext.class,0);
		}
		public T_delContext t_del() {
			return getRuleContext(T_delContext.class,0);
		}
		public M_delContext m_del() {
			return getRuleContext(M_delContext.class,0);
		}
		public R_expContext r_exp() {
			return getRuleContext(R_expContext.class,0);
		}
		public D_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_d_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterD_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitD_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitD_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final D_stmtContext d_stmt() throws RecognitionException {
		D_stmtContext _localctx = new D_stmtContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_d_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(293);
			match(DELETE);
			setState(298);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PATTERN:
				{
				setState(294);
				p_del();
				}
				break;
			case TERM:
				{
				setState(295);
				t_del();
				}
				break;
			case CUBE:
			case DIMENSION:
				{
				setState(296);
				m_del();
				}
				break;
			case CATALOGUE:
			case MULTIDIMENSIONAL_MODEL:
			case REPOSITORY:
			case GLOSSARY:
				{
				setState(297);
				r_exp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class X_stmtContext extends ParserRuleContext {
		public TerminalNode EXECUTE() { return getToken(EMDMLParser.EXECUTE, 0); }
		public P_execContext p_exec() {
			return getRuleContext(P_execContext.class,0);
		}
		public X_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_x_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterX_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitX_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitX_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final X_stmtContext x_stmt() throws RecognitionException {
		X_stmtContext _localctx = new X_stmtContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_x_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(300);
			match(EXECUTE);
			{
			setState(301);
			p_exec();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class I_stmtContext extends ParserRuleContext {
		public TerminalNode INSTANTIATE() { return getToken(EMDMLParser.INSTANTIATE, 0); }
		public P_instContext p_inst() {
			return getRuleContext(P_instContext.class,0);
		}
		public I_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_i_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterI_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitI_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitI_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final I_stmtContext i_stmt() throws RecognitionException {
		I_stmtContext _localctx = new I_stmtContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_i_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(303);
			match(INSTANTIATE);
			{
			setState(304);
			p_inst();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class G_stmtContext extends ParserRuleContext {
		public TerminalNode GROUND() { return getToken(EMDMLParser.GROUND, 0); }
		public P_grndContext p_grnd() {
			return getRuleContext(P_grndContext.class,0);
		}
		public G_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_g_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterG_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitG_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitG_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final G_stmtContext g_stmt() throws RecognitionException {
		G_stmtContext _localctx = new G_stmtContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_g_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(306);
			match(GROUND);
			{
			setState(307);
			p_grnd();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class S_stmtContext extends ParserRuleContext {
		public TerminalNode SHOW() { return getToken(EMDMLParser.SHOW, 0); }
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public S_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_s_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterS_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitS_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitS_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final S_stmtContext s_stmt() throws RecognitionException {
		S_stmtContext _localctx = new S_stmtContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_s_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(309);
			match(SHOW);
			setState(310);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class F_stmtContext extends ParserRuleContext {
		public TerminalNode SEARCH() { return getToken(EMDMLParser.SEARCH, 0); }
		public S_trgtContext s_trgt() {
			return getRuleContext(S_trgtContext.class,0);
		}
		public TerminalNode IN() { return getToken(EMDMLParser.IN, 0); }
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public List<S_expContext> s_exp() {
			return getRuleContexts(S_expContext.class);
		}
		public S_expContext s_exp(int i) {
			return getRuleContext(S_expContext.class,i);
		}
		public F_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_f_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterF_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitF_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitF_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final F_stmtContext f_stmt() throws RecognitionException {
		F_stmtContext _localctx = new F_stmtContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_f_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(312);
			match(SEARCH);
			setState(313);
			s_trgt();
			setState(316);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IN) {
				{
				setState(314);
				match(IN);
				setState(315);
				path_exp();
				}
			}

			setState(319); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(318);
				s_exp();
				}
				}
				setState(321); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CONTAIN );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class S_trgtContext extends ParserRuleContext {
		public TerminalNode REPOSITORY() { return getToken(EMDMLParser.REPOSITORY, 0); }
		public TerminalNode CATALOGUE() { return getToken(EMDMLParser.CATALOGUE, 0); }
		public TerminalNode GLOSSARY() { return getToken(EMDMLParser.GLOSSARY, 0); }
		public TerminalNode MULTIDIMENSIONAL_MODEL() { return getToken(EMDMLParser.MULTIDIMENSIONAL_MODEL, 0); }
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public TerminalNode TERM() { return getToken(EMDMLParser.TERM, 0); }
		public TerminalNode CUBE() { return getToken(EMDMLParser.CUBE, 0); }
		public TerminalNode DIMENSION() { return getToken(EMDMLParser.DIMENSION, 0); }
		public S_trgtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_s_trgt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterS_trgt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitS_trgt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitS_trgt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final S_trgtContext s_trgt() throws RecognitionException {
		S_trgtContext _localctx = new S_trgtContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_s_trgt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(323);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CATALOGUE) | (1L << CUBE) | (1L << DIMENSION))) != 0) || ((((_la - 70)) & ~0x3f) == 0 && ((1L << (_la - 70)) & ((1L << (MULTIDIMENSIONAL_MODEL - 70)) | (1L << (PATTERN - 70)) | (1L << (REPOSITORY - 70)) | (1L << (TERM - 70)) | (1L << (GLOSSARY - 70)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class S_expContext extends ParserRuleContext {
		public TerminalNode CONTAIN() { return getToken(EMDMLParser.CONTAIN, 0); }
		public S_strContext s_str() {
			return getRuleContext(S_strContext.class,0);
		}
		public TerminalNode IN() { return getToken(EMDMLParser.IN, 0); }
		public List<S_sctContext> s_sct() {
			return getRuleContexts(S_sctContext.class);
		}
		public S_sctContext s_sct(int i) {
			return getRuleContext(S_sctContext.class,i);
		}
		public S_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_s_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterS_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitS_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitS_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final S_expContext s_exp() throws RecognitionException {
		S_expContext _localctx = new S_expContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_s_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(325);
			match(CONTAIN);
			setState(326);
			s_str();
			setState(327);
			match(IN);
			setState(336); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(328);
				s_sct();
				setState(333);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(329);
					match(T__0);
					setState(330);
					s_sct();
					}
					}
					setState(335);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				setState(338); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ALIAS || _la==EXAMPLE || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (LANGUAGE - 66)) | (1L << (PROBLEM - 66)) | (1L << (RELATED - 66)) | (1L << (SOLUTION - 66)) | (1L << (NAME - 66)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class S_sctContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(EMDMLParser.NAME, 0); }
		public TerminalNode LANGUAGE() { return getToken(EMDMLParser.LANGUAGE, 0); }
		public TerminalNode ALIAS() { return getToken(EMDMLParser.ALIAS, 0); }
		public TerminalNode PROBLEM() { return getToken(EMDMLParser.PROBLEM, 0); }
		public TerminalNode SOLUTION() { return getToken(EMDMLParser.SOLUTION, 0); }
		public TerminalNode EXAMPLE() { return getToken(EMDMLParser.EXAMPLE, 0); }
		public TerminalNode RELATED() { return getToken(EMDMLParser.RELATED, 0); }
		public S_sctContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_s_sct; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterS_sct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitS_sct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitS_sct(this);
			else return visitor.visitChildren(this);
		}
	}

	public final S_sctContext s_sct() throws RecognitionException {
		S_sctContext _localctx = new S_sctContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_s_sct);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(340);
			_la = _input.LA(1);
			if ( !(_la==ALIAS || _la==EXAMPLE || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (LANGUAGE - 66)) | (1L << (PROBLEM - 66)) | (1L << (RELATED - 66)) | (1L << (SOLUTION - 66)) | (1L << (NAME - 66)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class R_expContext extends ParserRuleContext {
		public S_nameContext s_name() {
			return getRuleContext(S_nameContext.class,0);
		}
		public TerminalNode REPOSITORY() { return getToken(EMDMLParser.REPOSITORY, 0); }
		public TerminalNode CATALOGUE() { return getToken(EMDMLParser.CATALOGUE, 0); }
		public TerminalNode GLOSSARY() { return getToken(EMDMLParser.GLOSSARY, 0); }
		public TerminalNode MULTIDIMENSIONAL_MODEL() { return getToken(EMDMLParser.MULTIDIMENSIONAL_MODEL, 0); }
		public R_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_r_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterR_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitR_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitR_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final R_expContext r_exp() throws RecognitionException {
		R_expContext _localctx = new R_expContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_r_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(342);
			_la = _input.LA(1);
			if ( !(_la==CATALOGUE || ((((_la - 70)) & ~0x3f) == 0 && ((1L << (_la - 70)) & ((1L << (MULTIDIMENSIONAL_MODEL - 70)) | (1L << (REPOSITORY - 70)) | (1L << (GLOSSARY - 70)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(343);
			s_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_defContext extends ParserRuleContext {
		public P_nameContext p_name() {
			return getRuleContext(P_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<TerminalNode> END() { return getTokens(EMDMLParser.END); }
		public TerminalNode END(int i) {
			return getToken(EMDMLParser.END, i);
		}
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public List<TerminalNode> PARAMETERS() { return getTokens(EMDMLParser.PARAMETERS); }
		public TerminalNode PARAMETERS(int i) {
			return getToken(EMDMLParser.PARAMETERS, i);
		}
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<TerminalNode> DERIVED() { return getTokens(EMDMLParser.DERIVED); }
		public TerminalNode DERIVED(int i) {
			return getToken(EMDMLParser.DERIVED, i);
		}
		public List<TerminalNode> ELEMENTS() { return getTokens(EMDMLParser.ELEMENTS); }
		public TerminalNode ELEMENTS(int i) {
			return getToken(EMDMLParser.ELEMENTS, i);
		}
		public List<TerminalNode> LOCAL() { return getTokens(EMDMLParser.LOCAL); }
		public TerminalNode LOCAL(int i) {
			return getToken(EMDMLParser.LOCAL, i);
		}
		public List<TerminalNode> CUBES() { return getTokens(EMDMLParser.CUBES); }
		public TerminalNode CUBES(int i) {
			return getToken(EMDMLParser.CUBES, i);
		}
		public List<TerminalNode> CONSTRAINTS() { return getTokens(EMDMLParser.CONSTRAINTS); }
		public TerminalNode CONSTRAINTS(int i) {
			return getToken(EMDMLParser.CONSTRAINTS, i);
		}
		public List<Param_declContext> param_decl() {
			return getRuleContexts(Param_declContext.class);
		}
		public Param_declContext param_decl(int i) {
			return getRuleContext(Param_declContext.class,i);
		}
		public List<Derv_declContext> derv_decl() {
			return getRuleContexts(Derv_declContext.class);
		}
		public Derv_declContext derv_decl(int i) {
			return getRuleContext(Derv_declContext.class,i);
		}
		public List<Lc_declContext> lc_decl() {
			return getRuleContexts(Lc_declContext.class);
		}
		public Lc_declContext lc_decl(int i) {
			return getRuleContext(Lc_declContext.class,i);
		}
		public List<Cstr_declContext> cstr_decl() {
			return getRuleContexts(Cstr_declContext.class);
		}
		public Cstr_declContext cstr_decl(int i) {
			return getRuleContext(Cstr_declContext.class,i);
		}
		public P_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_defContext p_def() throws RecognitionException {
		P_defContext _localctx = new P_defContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_p_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(345);
			p_name();
			setState(346);
			match(WITH);
			setState(357);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARAMETERS) {
				{
				setState(347);
				match(PARAMETERS);
				setState(349); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(348);
					param_decl();
					}
					}
					setState(351); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 );
				setState(353);
				match(END);
				setState(354);
				match(PARAMETERS);
				setState(355);
				match(COL);
				}
			}

			setState(371);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DERIVED) {
				{
				setState(359);
				match(DERIVED);
				setState(360);
				match(ELEMENTS);
				setState(362); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(361);
					derv_decl();
					}
					}
					setState(364); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 );
				setState(366);
				match(END);
				setState(367);
				match(DERIVED);
				setState(368);
				match(ELEMENTS);
				setState(369);
				match(COL);
				}
			}

			setState(385);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LOCAL) {
				{
				setState(373);
				match(LOCAL);
				setState(374);
				match(CUBES);
				setState(376); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(375);
					lc_decl();
					}
					}
					setState(378); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 || _la==FOR || _la==STRING );
				setState(380);
				match(END);
				setState(381);
				match(LOCAL);
				setState(382);
				match(CUBES);
				setState(383);
				match(COL);
				}
			}

			setState(397);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONSTRAINTS) {
				{
				setState(387);
				match(CONSTRAINTS);
				setState(389); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(388);
					cstr_decl();
					}
					}
					setState(391); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 || _la==FOR || _la==TERM || _la==STRING );
				setState(393);
				match(END);
				setState(394);
				match(CONSTRAINTS);
				setState(395);
				match(COL);
				}
			}

			setState(399);
			match(END);
			setState(400);
			match(PATTERN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_descrContext extends ParserRuleContext {
		public List<TerminalNode> DESCRIPTION() { return getTokens(EMDMLParser.DESCRIPTION); }
		public TerminalNode DESCRIPTION(int i) {
			return getToken(EMDMLParser.DESCRIPTION, i);
		}
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public P_nameContext p_name() {
			return getRuleContext(P_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public TerminalNode END() { return getToken(EMDMLParser.END, 0); }
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<TerminalNode> LANGUAGE() { return getTokens(EMDMLParser.LANGUAGE); }
		public TerminalNode LANGUAGE(int i) {
			return getToken(EMDMLParser.LANGUAGE, i);
		}
		public List<Lang_nameContext> lang_name() {
			return getRuleContexts(Lang_nameContext.class);
		}
		public Lang_nameContext lang_name(int i) {
			return getRuleContext(Lang_nameContext.class,i);
		}
		public List<TerminalNode> ALIAS() { return getTokens(EMDMLParser.ALIAS); }
		public TerminalNode ALIAS(int i) {
			return getToken(EMDMLParser.ALIAS, i);
		}
		public List<Alias_nameContext> alias_name() {
			return getRuleContexts(Alias_nameContext.class);
		}
		public Alias_nameContext alias_name(int i) {
			return getRuleContext(Alias_nameContext.class,i);
		}
		public List<TerminalNode> PROBLEM() { return getTokens(EMDMLParser.PROBLEM); }
		public TerminalNode PROBLEM(int i) {
			return getToken(EMDMLParser.PROBLEM, i);
		}
		public List<Prob_txtContext> prob_txt() {
			return getRuleContexts(Prob_txtContext.class);
		}
		public Prob_txtContext prob_txt(int i) {
			return getRuleContext(Prob_txtContext.class,i);
		}
		public List<TerminalNode> SOLUTION() { return getTokens(EMDMLParser.SOLUTION); }
		public TerminalNode SOLUTION(int i) {
			return getToken(EMDMLParser.SOLUTION, i);
		}
		public List<Sol_txtContext> sol_txt() {
			return getRuleContexts(Sol_txtContext.class);
		}
		public Sol_txtContext sol_txt(int i) {
			return getRuleContext(Sol_txtContext.class,i);
		}
		public List<TerminalNode> EXAMPLE() { return getTokens(EMDMLParser.EXAMPLE); }
		public TerminalNode EXAMPLE(int i) {
			return getToken(EMDMLParser.EXAMPLE, i);
		}
		public List<Ex_txtContext> ex_txt() {
			return getRuleContexts(Ex_txtContext.class);
		}
		public Ex_txtContext ex_txt(int i) {
			return getRuleContext(Ex_txtContext.class,i);
		}
		public List<TerminalNode> RELATED() { return getTokens(EMDMLParser.RELATED); }
		public TerminalNode RELATED(int i) {
			return getToken(EMDMLParser.RELATED, i);
		}
		public List<P_loc_nameContext> p_loc_name() {
			return getRuleContexts(P_loc_nameContext.class);
		}
		public P_loc_nameContext p_loc_name(int i) {
			return getRuleContext(P_loc_nameContext.class,i);
		}
		public P_descrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_descr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_descr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_descr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_descr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_descrContext p_descr() throws RecognitionException {
		P_descrContext _localctx = new P_descrContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_p_descr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(402);
			match(DESCRIPTION);
			setState(403);
			match(FOR);
			setState(404);
			p_name();
			setState(405);
			match(WITH);
			setState(442); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(438);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LANGUAGE:
					{
					setState(406);
					match(LANGUAGE);
					setState(407);
					match(T__1);
					setState(408);
					lang_name();
					}
					break;
				case ALIAS:
					{
					setState(409);
					match(ALIAS);
					setState(410);
					match(T__1);
					setState(411);
					alias_name();
					setState(416);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__0) {
						{
						{
						setState(412);
						match(T__0);
						setState(413);
						alias_name();
						}
						}
						setState(418);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				case PROBLEM:
					{
					setState(419);
					match(PROBLEM);
					setState(420);
					match(T__1);
					setState(421);
					prob_txt();
					}
					break;
				case SOLUTION:
					{
					setState(422);
					match(SOLUTION);
					setState(423);
					match(T__1);
					setState(424);
					sol_txt();
					}
					break;
				case EXAMPLE:
					{
					setState(425);
					match(EXAMPLE);
					setState(426);
					match(T__1);
					setState(427);
					ex_txt();
					}
					break;
				case RELATED:
					{
					setState(428);
					match(RELATED);
					setState(429);
					match(T__1);
					setState(430);
					p_loc_name();
					setState(435);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__0) {
						{
						{
						setState(431);
						match(T__0);
						setState(432);
						p_loc_name();
						}
						}
						setState(437);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(440);
				match(COL);
				}
				}
				setState(444); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ALIAS || _la==EXAMPLE || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (LANGUAGE - 66)) | (1L << (PROBLEM - 66)) | (1L << (RELATED - 66)) | (1L << (SOLUTION - 66)))) != 0) );
			setState(446);
			match(END);
			setState(447);
			match(PATTERN);
			setState(448);
			match(DESCRIPTION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_tempContext extends ParserRuleContext {
		public List<TerminalNode> TEMPLATE() { return getTokens(EMDMLParser.TEMPLATE); }
		public TerminalNode TEMPLATE(int i) {
			return getToken(EMDMLParser.TEMPLATE, i);
		}
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public P_nameContext p_name() {
			return getRuleContext(P_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public TerminalNode END() { return getToken(EMDMLParser.END, 0); }
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public List<Temp_elemContext> temp_elem() {
			return getRuleContexts(Temp_elemContext.class);
		}
		public Temp_elemContext temp_elem(int i) {
			return getRuleContext(Temp_elemContext.class,i);
		}
		public P_tempContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_temp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_temp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_temp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_temp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_tempContext p_temp() throws RecognitionException {
		P_tempContext _localctx = new P_tempContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_p_temp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(450);
			match(TEMPLATE);
			setState(451);
			match(FOR);
			setState(452);
			p_name();
			setState(453);
			match(WITH);
			setState(455); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(454);
				temp_elem();
				}
				}
				setState(457); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 38)) & ~0x3f) == 0 && ((1L << (_la - 38)) & ((1L << (DATA_MODEL - 38)) | (1L << (DIALECT - 38)) | (1L << (EXPRESSION - 38)) | (1L << (LANGUAGE - 38)) | (1L << (VARIANT - 38)))) != 0) );
			setState(459);
			match(END);
			setState(460);
			match(PATTERN);
			setState(461);
			match(TEMPLATE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_instContext extends ParserRuleContext {
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public List<P_nameContext> p_name() {
			return getRuleContexts(P_nameContext.class);
		}
		public P_nameContext p_name(int i) {
			return getRuleContext(P_nameContext.class,i);
		}
		public TerminalNode AS() { return getToken(EMDMLParser.AS, 0); }
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<Binding_expContext> binding_exp() {
			return getRuleContexts(Binding_expContext.class);
		}
		public Binding_expContext binding_exp(int i) {
			return getRuleContext(Binding_expContext.class,i);
		}
		public List<TerminalNode> BINDINGS() { return getTokens(EMDMLParser.BINDINGS); }
		public TerminalNode BINDINGS(int i) {
			return getToken(EMDMLParser.BINDINGS, i);
		}
		public TerminalNode END() { return getToken(EMDMLParser.END, 0); }
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public P_instContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_inst; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_inst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_inst(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_inst(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_instContext p_inst() throws RecognitionException {
		P_instContext _localctx = new P_instContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_p_inst);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(463);
			match(PATTERN);
			setState(464);
			p_name();
			setState(465);
			match(AS);
			setState(466);
			p_name();
			setState(467);
			match(WITH);
			setState(469);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BINDINGS) {
				{
				setState(468);
				match(BINDINGS);
				}
			}

			setState(471);
			binding_exp();
			setState(476);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(472);
				match(T__0);
				setState(473);
				binding_exp();
				}
				}
				setState(478);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(481);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==END) {
				{
				setState(479);
				match(END);
				setState(480);
				match(BINDINGS);
				}
			}

			setState(485);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FOR) {
				{
				setState(483);
				match(FOR);
				setState(484);
				path_exp();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_delContext extends ParserRuleContext {
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public P_del_descrContext p_del_descr() {
			return getRuleContext(P_del_descrContext.class,0);
		}
		public P_del_tempContext p_del_temp() {
			return getRuleContext(P_del_tempContext.class,0);
		}
		public P_nameContext p_name() {
			return getRuleContext(P_nameContext.class,0);
		}
		public P_delContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_del; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_del(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_del(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_del(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_delContext p_del() throws RecognitionException {
		P_delContext _localctx = new P_delContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_p_del);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(487);
			match(PATTERN);
			setState(491);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DESCRIPTION:
				{
				setState(488);
				p_del_descr();
				}
				break;
			case TEMPLATE:
				{
				setState(489);
				p_del_temp();
				}
				break;
			case STRING:
				{
				setState(490);
				p_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_del_descrContext extends ParserRuleContext {
		public TerminalNode DESCRIPTION() { return getToken(EMDMLParser.DESCRIPTION, 0); }
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public P_nameContext p_name() {
			return getRuleContext(P_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public TerminalNode LANGUAGE() { return getToken(EMDMLParser.LANGUAGE, 0); }
		public Lang_nameContext lang_name() {
			return getRuleContext(Lang_nameContext.class,0);
		}
		public P_del_descrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_del_descr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_del_descr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_del_descr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_del_descr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_del_descrContext p_del_descr() throws RecognitionException {
		P_del_descrContext _localctx = new P_del_descrContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_p_del_descr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(493);
			match(DESCRIPTION);
			setState(494);
			match(FOR);
			setState(495);
			p_name();
			setState(500);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(496);
				match(WITH);
				setState(497);
				match(LANGUAGE);
				setState(498);
				match(T__1);
				setState(499);
				lang_name();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_del_tempContext extends ParserRuleContext {
		public TerminalNode TEMPLATE() { return getToken(EMDMLParser.TEMPLATE, 0); }
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public P_nameContext p_name() {
			return getRuleContext(P_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<Temp_elem_metaContext> temp_elem_meta() {
			return getRuleContexts(Temp_elem_metaContext.class);
		}
		public Temp_elem_metaContext temp_elem_meta(int i) {
			return getRuleContext(Temp_elem_metaContext.class,i);
		}
		public P_del_tempContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_del_temp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_del_temp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_del_temp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_del_temp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_del_tempContext p_del_temp() throws RecognitionException {
		P_del_tempContext _localctx = new P_del_tempContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_p_del_temp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(502);
			match(TEMPLATE);
			setState(503);
			match(FOR);
			setState(504);
			p_name();
			setState(518);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(505);
				match(WITH);
				setState(507); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(506);
					temp_elem_meta();
					}
					}
					setState(509); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 38)) & ~0x3f) == 0 && ((1L << (_la - 38)) & ((1L << (DATA_MODEL - 38)) | (1L << (DIALECT - 38)) | (1L << (LANGUAGE - 38)) | (1L << (VARIANT - 38)))) != 0) );
				setState(515);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(511);
					match(T__0);
					setState(512);
					temp_elem_meta();
					}
					}
					setState(517);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_grndContext extends ParserRuleContext {
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public List<P_nameContext> p_name() {
			return getRuleContexts(P_nameContext.class);
		}
		public P_nameContext p_name(int i) {
			return getRuleContext(P_nameContext.class,i);
		}
		public TerminalNode AS() { return getToken(EMDMLParser.AS, 0); }
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public Mdm_nameContext mdm_name() {
			return getRuleContext(Mdm_nameContext.class,0);
		}
		public P_grndContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_grnd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_grnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_grnd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_grnd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_grndContext p_grnd() throws RecognitionException {
		P_grndContext _localctx = new P_grndContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_p_grnd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(520);
			match(PATTERN);
			setState(521);
			p_name();
			setState(522);
			match(AS);
			setState(523);
			p_name();
			setState(524);
			match(FOR);
			setState(525);
			mdm_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_execContext extends ParserRuleContext {
		public TerminalNode PATTERN() { return getToken(EMDMLParser.PATTERN, 0); }
		public P_nameContext p_name() {
			return getRuleContext(P_nameContext.class,0);
		}
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public Mdm_nameContext mdm_name() {
			return getRuleContext(Mdm_nameContext.class,0);
		}
		public TerminalNode USING() { return getToken(EMDMLParser.USING, 0); }
		public Voc_nameContext voc_name() {
			return getRuleContext(Voc_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public TerminalNode TEMPLATE() { return getToken(EMDMLParser.TEMPLATE, 0); }
		public List<Temp_elem_metaContext> temp_elem_meta() {
			return getRuleContexts(Temp_elem_metaContext.class);
		}
		public Temp_elem_metaContext temp_elem_meta(int i) {
			return getRuleContext(Temp_elem_metaContext.class,i);
		}
		public P_execContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_exec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_exec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_exec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_exec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_execContext p_exec() throws RecognitionException {
		P_execContext _localctx = new P_execContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_p_exec);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(527);
			match(PATTERN);
			setState(528);
			p_name();
			setState(529);
			match(FOR);
			setState(530);
			mdm_name();
			setState(531);
			match(USING);
			setState(532);
			voc_name();
			setState(547);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(533);
				match(WITH);
				setState(534);
				match(TEMPLATE);
				setState(536); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(535);
					temp_elem_meta();
					}
					}
					setState(538); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 38)) & ~0x3f) == 0 && ((1L << (_la - 38)) & ((1L << (DATA_MODEL - 38)) | (1L << (DIALECT - 38)) | (1L << (LANGUAGE - 38)) | (1L << (VARIANT - 38)))) != 0) );
				setState(544);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(540);
					match(T__0);
					setState(541);
					temp_elem_meta();
					}
					}
					setState(546);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_defContext extends ParserRuleContext {
		public List<T_typeContext> t_type() {
			return getRuleContexts(T_typeContext.class);
		}
		public T_typeContext t_type(int i) {
			return getRuleContext(T_typeContext.class,i);
		}
		public T_nameContext t_name() {
			return getRuleContext(T_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<TerminalNode> END() { return getTokens(EMDMLParser.END); }
		public TerminalNode END(int i) {
			return getToken(EMDMLParser.END, i);
		}
		public List<TerminalNode> PARAMETERS() { return getTokens(EMDMLParser.PARAMETERS); }
		public TerminalNode PARAMETERS(int i) {
			return getToken(EMDMLParser.PARAMETERS, i);
		}
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<TerminalNode> CONSTRAINTS() { return getTokens(EMDMLParser.CONSTRAINTS); }
		public TerminalNode CONSTRAINTS(int i) {
			return getToken(EMDMLParser.CONSTRAINTS, i);
		}
		public TerminalNode RETURNS() { return getToken(EMDMLParser.RETURNS, 0); }
		public Value_set_nameContext value_set_name() {
			return getRuleContext(Value_set_nameContext.class,0);
		}
		public List<Param_declContext> param_decl() {
			return getRuleContexts(Param_declContext.class);
		}
		public Param_declContext param_decl(int i) {
			return getRuleContext(Param_declContext.class,i);
		}
		public List<Cstr_declContext> cstr_decl() {
			return getRuleContexts(Cstr_declContext.class);
		}
		public Cstr_declContext cstr_decl(int i) {
			return getRuleContext(Cstr_declContext.class,i);
		}
		public T_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_defContext t_def() throws RecognitionException {
		T_defContext _localctx = new T_defContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_t_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(549);
			t_type();
			setState(550);
			t_name();
			setState(551);
			match(WITH);
			setState(562);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARAMETERS) {
				{
				setState(552);
				match(PARAMETERS);
				setState(554); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(553);
					param_decl();
					}
					}
					setState(556); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 );
				setState(558);
				match(END);
				setState(559);
				match(PARAMETERS);
				setState(560);
				match(COL);
				}
			}

			setState(574);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONSTRAINTS) {
				{
				setState(564);
				match(CONSTRAINTS);
				setState(566); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(565);
					cstr_decl();
					}
					}
					setState(568); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 || _la==FOR || _la==TERM || _la==STRING );
				setState(570);
				match(END);
				setState(571);
				match(CONSTRAINTS);
				setState(572);
				match(COL);
				}
			}

			setState(580);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURNS) {
				{
				setState(576);
				match(RETURNS);
				setState(577);
				value_set_name();
				setState(578);
				match(COL);
				}
			}

			setState(582);
			match(END);
			setState(583);
			t_type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_descrContext extends ParserRuleContext {
		public List<TerminalNode> DESCRIPTION() { return getTokens(EMDMLParser.DESCRIPTION); }
		public TerminalNode DESCRIPTION(int i) {
			return getToken(EMDMLParser.DESCRIPTION, i);
		}
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public List<T_nameContext> t_name() {
			return getRuleContexts(T_nameContext.class);
		}
		public T_nameContext t_name(int i) {
			return getRuleContext(T_nameContext.class,i);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public TerminalNode END() { return getToken(EMDMLParser.END, 0); }
		public TerminalNode TERM() { return getToken(EMDMLParser.TERM, 0); }
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<TerminalNode> LANGUAGE() { return getTokens(EMDMLParser.LANGUAGE); }
		public TerminalNode LANGUAGE(int i) {
			return getToken(EMDMLParser.LANGUAGE, i);
		}
		public List<Lang_nameContext> lang_name() {
			return getRuleContexts(Lang_nameContext.class);
		}
		public Lang_nameContext lang_name(int i) {
			return getRuleContext(Lang_nameContext.class,i);
		}
		public List<TerminalNode> ALIAS() { return getTokens(EMDMLParser.ALIAS); }
		public TerminalNode ALIAS(int i) {
			return getToken(EMDMLParser.ALIAS, i);
		}
		public List<Descr_txtContext> descr_txt() {
			return getRuleContexts(Descr_txtContext.class);
		}
		public Descr_txtContext descr_txt(int i) {
			return getRuleContext(Descr_txtContext.class,i);
		}
		public T_descrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_descr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_descr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_descr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_descr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_descrContext t_descr() throws RecognitionException {
		T_descrContext _localctx = new T_descrContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_t_descr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(585);
			match(DESCRIPTION);
			setState(586);
			match(FOR);
			setState(587);
			t_name();
			setState(588);
			match(WITH);
			setState(609); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(605);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LANGUAGE:
					{
					setState(589);
					match(LANGUAGE);
					setState(590);
					match(T__1);
					setState(591);
					lang_name();
					}
					break;
				case ALIAS:
					{
					setState(592);
					match(ALIAS);
					setState(593);
					match(T__1);
					setState(594);
					t_name();
					setState(599);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__0) {
						{
						{
						setState(595);
						match(T__0);
						setState(596);
						t_name();
						}
						}
						setState(601);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				case DESCRIPTION:
					{
					setState(602);
					match(DESCRIPTION);
					setState(603);
					match(T__1);
					setState(604);
					descr_txt();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(607);
				match(COL);
				}
				}
				setState(611); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 20)) & ~0x3f) == 0 && ((1L << (_la - 20)) & ((1L << (ALIAS - 20)) | (1L << (DESCRIPTION - 20)) | (1L << (LANGUAGE - 20)))) != 0) );
			setState(613);
			match(END);
			setState(614);
			match(TERM);
			setState(615);
			match(DESCRIPTION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_tempContext extends ParserRuleContext {
		public List<TerminalNode> TEMPLATE() { return getTokens(EMDMLParser.TEMPLATE); }
		public TerminalNode TEMPLATE(int i) {
			return getToken(EMDMLParser.TEMPLATE, i);
		}
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public T_nameContext t_name() {
			return getRuleContext(T_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public TerminalNode END() { return getToken(EMDMLParser.END, 0); }
		public TerminalNode TERM() { return getToken(EMDMLParser.TERM, 0); }
		public List<Temp_elemContext> temp_elem() {
			return getRuleContexts(Temp_elemContext.class);
		}
		public Temp_elemContext temp_elem(int i) {
			return getRuleContext(Temp_elemContext.class,i);
		}
		public T_tempContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_temp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_temp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_temp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_temp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_tempContext t_temp() throws RecognitionException {
		T_tempContext _localctx = new T_tempContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_t_temp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(617);
			match(TEMPLATE);
			setState(618);
			match(FOR);
			setState(619);
			t_name();
			setState(620);
			match(WITH);
			setState(622); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(621);
				temp_elem();
				}
				}
				setState(624); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 38)) & ~0x3f) == 0 && ((1L << (_la - 38)) & ((1L << (DATA_MODEL - 38)) | (1L << (DIALECT - 38)) | (1L << (EXPRESSION - 38)) | (1L << (LANGUAGE - 38)) | (1L << (VARIANT - 38)))) != 0) );
			setState(626);
			match(END);
			setState(627);
			match(TERM);
			setState(628);
			match(TEMPLATE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_delContext extends ParserRuleContext {
		public TerminalNode TERM() { return getToken(EMDMLParser.TERM, 0); }
		public T_del_descrContext t_del_descr() {
			return getRuleContext(T_del_descrContext.class,0);
		}
		public T_del_tempContext t_del_temp() {
			return getRuleContext(T_del_tempContext.class,0);
		}
		public T_nameContext t_name() {
			return getRuleContext(T_nameContext.class,0);
		}
		public T_delContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_del; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_del(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_del(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_del(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_delContext t_del() throws RecognitionException {
		T_delContext _localctx = new T_delContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_t_del);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(630);
			match(TERM);
			setState(634);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DESCRIPTION:
				{
				setState(631);
				t_del_descr();
				}
				break;
			case TEMPLATE:
				{
				setState(632);
				t_del_temp();
				}
				break;
			case STRING:
				{
				setState(633);
				t_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_del_descrContext extends ParserRuleContext {
		public TerminalNode DESCRIPTION() { return getToken(EMDMLParser.DESCRIPTION, 0); }
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public T_nameContext t_name() {
			return getRuleContext(T_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public TerminalNode LANGUAGE() { return getToken(EMDMLParser.LANGUAGE, 0); }
		public Lang_nameContext lang_name() {
			return getRuleContext(Lang_nameContext.class,0);
		}
		public T_del_descrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_del_descr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_del_descr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_del_descr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_del_descr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_del_descrContext t_del_descr() throws RecognitionException {
		T_del_descrContext _localctx = new T_del_descrContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_t_del_descr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(636);
			match(DESCRIPTION);
			setState(637);
			match(FOR);
			setState(638);
			t_name();
			setState(643);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(639);
				match(WITH);
				setState(640);
				match(LANGUAGE);
				setState(641);
				match(T__1);
				setState(642);
				lang_name();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_del_tempContext extends ParserRuleContext {
		public TerminalNode TEMPLATE() { return getToken(EMDMLParser.TEMPLATE, 0); }
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public T_nameContext t_name() {
			return getRuleContext(T_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<Temp_elem_metaContext> temp_elem_meta() {
			return getRuleContexts(Temp_elem_metaContext.class);
		}
		public Temp_elem_metaContext temp_elem_meta(int i) {
			return getRuleContext(Temp_elem_metaContext.class,i);
		}
		public T_del_tempContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_del_temp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_del_temp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_del_temp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_del_temp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_del_tempContext t_del_temp() throws RecognitionException {
		T_del_tempContext _localctx = new T_del_tempContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_t_del_temp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(645);
			match(TEMPLATE);
			setState(646);
			match(FOR);
			setState(647);
			t_name();
			setState(661);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(648);
				match(WITH);
				setState(650); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(649);
					temp_elem_meta();
					}
					}
					setState(652); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 38)) & ~0x3f) == 0 && ((1L << (_la - 38)) & ((1L << (DATA_MODEL - 38)) | (1L << (DIALECT - 38)) | (1L << (LANGUAGE - 38)) | (1L << (VARIANT - 38)))) != 0) );
				setState(658);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(654);
					match(T__0);
					setState(655);
					temp_elem_meta();
					}
					}
					setState(660);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Temp_elemContext extends ParserRuleContext {
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Temp_elem_metaContext temp_elem_meta() {
			return getRuleContext(Temp_elem_metaContext.class,0);
		}
		public TerminalNode EXPRESSION() { return getToken(EMDMLParser.EXPRESSION, 0); }
		public Temp_txtContext temp_txt() {
			return getRuleContext(Temp_txtContext.class,0);
		}
		public Temp_elemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_temp_elem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterTemp_elem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitTemp_elem(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitTemp_elem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Temp_elemContext temp_elem() throws RecognitionException {
		Temp_elemContext _localctx = new Temp_elemContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_temp_elem);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(667);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DATA_MODEL:
			case DIALECT:
			case LANGUAGE:
			case VARIANT:
				{
				setState(663);
				temp_elem_meta();
				}
				break;
			case EXPRESSION:
				{
				setState(664);
				match(EXPRESSION);
				setState(665);
				match(T__1);
				setState(666);
				temp_txt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(669);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Temp_elem_metaContext extends ParserRuleContext {
		public TerminalNode DATA_MODEL() { return getToken(EMDMLParser.DATA_MODEL, 0); }
		public Model_nameContext model_name() {
			return getRuleContext(Model_nameContext.class,0);
		}
		public TerminalNode VARIANT() { return getToken(EMDMLParser.VARIANT, 0); }
		public Variant_nameContext variant_name() {
			return getRuleContext(Variant_nameContext.class,0);
		}
		public TerminalNode LANGUAGE() { return getToken(EMDMLParser.LANGUAGE, 0); }
		public Lang_nameContext lang_name() {
			return getRuleContext(Lang_nameContext.class,0);
		}
		public TerminalNode DIALECT() { return getToken(EMDMLParser.DIALECT, 0); }
		public Dialect_nameContext dialect_name() {
			return getRuleContext(Dialect_nameContext.class,0);
		}
		public Temp_elem_metaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_temp_elem_meta; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterTemp_elem_meta(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitTemp_elem_meta(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitTemp_elem_meta(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Temp_elem_metaContext temp_elem_meta() throws RecognitionException {
		Temp_elem_metaContext _localctx = new Temp_elem_metaContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_temp_elem_meta);
		try {
			setState(683);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DATA_MODEL:
				enterOuterAlt(_localctx, 1);
				{
				setState(671);
				match(DATA_MODEL);
				setState(672);
				match(T__1);
				setState(673);
				model_name();
				}
				break;
			case VARIANT:
				enterOuterAlt(_localctx, 2);
				{
				setState(674);
				match(VARIANT);
				setState(675);
				match(T__1);
				setState(676);
				variant_name();
				}
				break;
			case LANGUAGE:
				enterOuterAlt(_localctx, 3);
				{
				setState(677);
				match(LANGUAGE);
				setState(678);
				match(T__1);
				setState(679);
				lang_name();
				}
				break;
			case DIALECT:
				enterOuterAlt(_localctx, 4);
				{
				setState(680);
				match(DIALECT);
				setState(681);
				match(T__1);
				setState(682);
				dialect_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_declContext extends ParserRuleContext {
		public Var_declContext var_decl() {
			return getRuleContext(Var_declContext.class,0);
		}
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Param_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterParam_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitParam_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitParam_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Param_declContext param_decl() throws RecognitionException {
		Param_declContext _localctx = new Param_declContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_param_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(685);
			var_decl();
			setState(686);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Derv_declContext extends ParserRuleContext {
		public Var_declContext var_decl() {
			return getRuleContext(Var_declContext.class,0);
		}
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public TerminalNode RETURNS() { return getToken(EMDMLParser.RETURNS, 0); }
		public For_expContext for_exp() {
			return getRuleContext(For_expContext.class,0);
		}
		public Var_expContext var_exp() {
			return getRuleContext(Var_expContext.class,0);
		}
		public Derv_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_derv_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDerv_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDerv_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDerv_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Derv_declContext derv_decl() throws RecognitionException {
		Derv_declContext _localctx = new Derv_declContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_derv_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(688);
			var_decl();
			setState(692);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FOR) {
				{
				setState(689);
				for_exp();
				setState(690);
				var_exp();
				}
			}

			setState(694);
			match(T__2);
			setState(695);
			elem_exp();
			setState(696);
			match(T__3);
			setState(699);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__10:
			case STRING:
				{
				setState(697);
				elem_exp();
				}
				break;
			case RETURNS:
				{
				setState(698);
				match(RETURNS);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(701);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_expContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(EMDMLParser.FOR, 0); }
		public TerminalNode IN() { return getToken(EMDMLParser.IN, 0); }
		public List<Var_expContext> var_exp() {
			return getRuleContexts(Var_expContext.class);
		}
		public Var_expContext var_exp(int i) {
			return getRuleContext(Var_expContext.class,i);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public For_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterFor_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitFor_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitFor_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_expContext for_exp() throws RecognitionException {
		For_expContext _localctx = new For_expContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_for_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(703);
			match(FOR);
			setState(715);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__10:
				{
				setState(704);
				var_exp();
				}
				break;
			case T__4:
				{
				setState(705);
				match(T__4);
				setState(706);
				var_exp();
				setState(709); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(707);
					match(T__0);
					setState(708);
					var_exp();
					}
					}
					setState(711); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__0 );
				setState(713);
				match(T__5);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(717);
			match(IN);
			setState(718);
			var_exp();
			setState(719);
			match(WITH);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_declContext extends ParserRuleContext {
		public Var_expContext var_exp() {
			return getRuleContext(Var_expContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Op_expContext op_exp() {
			return getRuleContext(Op_expContext.class,0);
		}
		public Var_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVar_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVar_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVar_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_declContext var_decl() throws RecognitionException {
		Var_declContext _localctx = new Var_declContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_var_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(721);
			var_exp();
			setState(722);
			match(T__6);
			setState(723);
			type();
			setState(725);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IS_OPTIONAL) {
				{
				setState(724);
				op_exp();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cstr_declContext extends ParserRuleContext {
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Sv_cstr_declContext sv_cstr_decl() {
			return getRuleContext(Sv_cstr_declContext.class,0);
		}
		public Mv_cstr_declContext mv_cstr_decl() {
			return getRuleContext(Mv_cstr_declContext.class,0);
		}
		public Cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterCstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitCstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitCstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cstr_declContext cstr_decl() throws RecognitionException {
		Cstr_declContext _localctx = new Cstr_declContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(729);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__10:
			case TERM:
			case STRING:
				{
				setState(727);
				sv_cstr_decl();
				}
				break;
			case FOR:
				{
				setState(728);
				mv_cstr_decl();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(731);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Mv_cstr_declContext extends ParserRuleContext {
		public For_expContext for_exp() {
			return getRuleContext(For_expContext.class,0);
		}
		public List<Sv_cstr_declContext> sv_cstr_decl() {
			return getRuleContexts(Sv_cstr_declContext.class);
		}
		public Sv_cstr_declContext sv_cstr_decl(int i) {
			return getRuleContext(Sv_cstr_declContext.class,i);
		}
		public Mv_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mv_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMv_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMv_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMv_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Mv_cstr_declContext mv_cstr_decl() throws RecognitionException {
		Mv_cstr_declContext _localctx = new Mv_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_mv_cstr_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(733);
			for_exp();
			setState(734);
			sv_cstr_decl();
			setState(739);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(735);
				match(T__0);
				setState(736);
				sv_cstr_decl();
				}
				}
				setState(741);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sv_cstr_declContext extends ParserRuleContext {
		public Type_cstr_declContext type_cstr_decl() {
			return getRuleContext(Type_cstr_declContext.class,0);
		}
		public Dom_cstr_declContext dom_cstr_decl() {
			return getRuleContext(Dom_cstr_declContext.class,0);
		}
		public Prop_cstr_declContext prop_cstr_decl() {
			return getRuleContext(Prop_cstr_declContext.class,0);
		}
		public Return_cstr_declContext return_cstr_decl() {
			return getRuleContext(Return_cstr_declContext.class,0);
		}
		public App_cstr_declContext app_cstr_decl() {
			return getRuleContext(App_cstr_declContext.class,0);
		}
		public Scope_cstr_declContext scope_cstr_decl() {
			return getRuleContext(Scope_cstr_declContext.class,0);
		}
		public Rollup_cstr_declContext rollup_cstr_decl() {
			return getRuleContext(Rollup_cstr_declContext.class,0);
		}
		public Descr_cstr_declContext descr_cstr_decl() {
			return getRuleContext(Descr_cstr_declContext.class,0);
		}
		public Term_cstr_declContext term_cstr_decl() {
			return getRuleContext(Term_cstr_declContext.class,0);
		}
		public Sv_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sv_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterSv_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitSv_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitSv_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sv_cstr_declContext sv_cstr_decl() throws RecognitionException {
		Sv_cstr_declContext _localctx = new Sv_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_sv_cstr_decl);
		try {
			setState(751);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(742);
				type_cstr_decl();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(743);
				dom_cstr_decl();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(744);
				prop_cstr_decl();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(745);
				return_cstr_decl();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(746);
				app_cstr_decl();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(747);
				scope_cstr_decl();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(748);
				rollup_cstr_decl();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(749);
				descr_cstr_decl();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(750);
				term_cstr_decl();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_cstr_declContext extends ParserRuleContext {
		public Elem_expContext elem_exp() {
			return getRuleContext(Elem_expContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Type_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterType_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitType_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitType_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_cstr_declContext type_cstr_decl() throws RecognitionException {
		Type_cstr_declContext _localctx = new Type_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_type_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(753);
			elem_exp();
			setState(754);
			match(T__6);
			setState(755);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dom_cstr_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public Dom_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dom_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDom_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDom_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDom_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dom_cstr_declContext dom_cstr_decl() throws RecognitionException {
		Dom_cstr_declContext _localctx = new Dom_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_dom_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(757);
			elem_exp();
			setState(758);
			match(T__3);
			setState(759);
			elem_exp();
			setState(760);
			match(T__6);
			setState(761);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Prop_cstr_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode HAS() { return getToken(EMDMLParser.HAS, 0); }
		public M_prop_typeContext m_prop_type() {
			return getRuleContext(M_prop_typeContext.class,0);
		}
		public Prop_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prop_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterProp_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitProp_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitProp_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Prop_cstr_declContext prop_cstr_decl() throws RecognitionException {
		Prop_cstr_declContext _localctx = new Prop_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_prop_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(763);
			elem_exp();
			setState(764);
			match(HAS);
			setState(765);
			m_prop_type();
			setState(766);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_cstr_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode RETURNS() { return getToken(EMDMLParser.RETURNS, 0); }
		public Return_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterReturn_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitReturn_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitReturn_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_cstr_declContext return_cstr_decl() throws RecognitionException {
		Return_cstr_declContext _localctx = new Return_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_return_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(768);
			elem_exp();
			setState(769);
			match(RETURNS);
			setState(770);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class App_cstr_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode IS_APPLICABLE_TO() { return getToken(EMDMLParser.IS_APPLICABLE_TO, 0); }
		public App_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_app_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterApp_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitApp_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitApp_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final App_cstr_declContext app_cstr_decl() throws RecognitionException {
		App_cstr_declContext _localctx = new App_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_app_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(772);
			elem_exp();
			setState(773);
			match(IS_APPLICABLE_TO);
			setState(781);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__10:
			case STRING:
				{
				setState(774);
				elem_exp();
				}
				break;
			case T__4:
				{
				setState(775);
				match(T__4);
				setState(776);
				elem_exp();
				setState(777);
				match(T__0);
				setState(778);
				elem_exp();
				setState(779);
				match(T__5);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Scope_cstr_declContext extends ParserRuleContext {
		public List<Scope_expContext> scope_exp() {
			return getRuleContexts(Scope_expContext.class);
		}
		public Scope_expContext scope_exp(int i) {
			return getRuleContext(Scope_expContext.class,i);
		}
		public Var_expContext var_exp() {
			return getRuleContext(Var_expContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public Scope_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scope_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterScope_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitScope_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitScope_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Scope_cstr_declContext scope_cstr_decl() throws RecognitionException {
		Scope_cstr_declContext _localctx = new Scope_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_scope_cstr_decl);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(786);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
			case 1:
				{
				setState(783);
				var_exp();
				setState(784);
				match(WITH);
				}
				break;
			}
			setState(788);
			scope_exp();
			setState(793);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,64,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(789);
					match(T__0);
					setState(790);
					scope_exp();
					}
					} 
				}
				setState(795);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,64,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Scope_expContext extends ParserRuleContext {
		public List<Var_expContext> var_exp() {
			return getRuleContexts(Var_expContext.class);
		}
		public Var_expContext var_exp(int i) {
			return getRuleContext(Var_expContext.class,i);
		}
		public TerminalNode IN() { return getToken(EMDMLParser.IN, 0); }
		public Scope_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scope_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterScope_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitScope_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitScope_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Scope_expContext scope_exp() throws RecognitionException {
		Scope_expContext _localctx = new Scope_expContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_scope_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(796);
			var_exp();
			setState(797);
			match(IN);
			setState(798);
			var_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rollup_cstr_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode ROLLS_UP_TO() { return getToken(EMDMLParser.ROLLS_UP_TO, 0); }
		public Rollup_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rollup_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterRollup_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitRollup_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitRollup_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Rollup_cstr_declContext rollup_cstr_decl() throws RecognitionException {
		Rollup_cstr_declContext _localctx = new Rollup_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_rollup_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(800);
			elem_exp();
			setState(801);
			match(T__3);
			setState(802);
			elem_exp();
			setState(803);
			match(ROLLS_UP_TO);
			setState(804);
			elem_exp();
			setState(805);
			match(T__3);
			setState(806);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Descr_cstr_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode DESCRIBED_BY() { return getToken(EMDMLParser.DESCRIBED_BY, 0); }
		public Descr_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_descr_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDescr_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDescr_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDescr_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Descr_cstr_declContext descr_cstr_decl() throws RecognitionException {
		Descr_cstr_declContext _localctx = new Descr_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_descr_cstr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(808);
			elem_exp();
			setState(809);
			match(T__3);
			setState(810);
			elem_exp();
			setState(811);
			match(DESCRIBED_BY);
			setState(812);
			elem_exp();
			setState(813);
			match(T__3);
			setState(814);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Term_cstr_declContext extends ParserRuleContext {
		public List<TerminalNode> TERM() { return getTokens(EMDMLParser.TERM); }
		public TerminalNode TERM(int i) {
			return getToken(EMDMLParser.TERM, i);
		}
		public Elem_expContext elem_exp() {
			return getRuleContext(Elem_expContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<TerminalNode> END() { return getTokens(EMDMLParser.END); }
		public TerminalNode END(int i) {
			return getToken(EMDMLParser.END, i);
		}
		public List<TerminalNode> EXPECTED() { return getTokens(EMDMLParser.EXPECTED); }
		public TerminalNode EXPECTED(int i) {
			return getToken(EMDMLParser.EXPECTED, i);
		}
		public List<TerminalNode> PARAMETERS() { return getTokens(EMDMLParser.PARAMETERS); }
		public TerminalNode PARAMETERS(int i) {
			return getToken(EMDMLParser.PARAMETERS, i);
		}
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<TerminalNode> CONSTRAINTS() { return getTokens(EMDMLParser.CONSTRAINTS); }
		public TerminalNode CONSTRAINTS(int i) {
			return getToken(EMDMLParser.CONSTRAINTS, i);
		}
		public List<Param_declContext> param_decl() {
			return getRuleContexts(Param_declContext.class);
		}
		public Param_declContext param_decl(int i) {
			return getRuleContext(Param_declContext.class,i);
		}
		public List<Sv_cstr_declContext> sv_cstr_decl() {
			return getRuleContexts(Sv_cstr_declContext.class);
		}
		public Sv_cstr_declContext sv_cstr_decl(int i) {
			return getRuleContext(Sv_cstr_declContext.class,i);
		}
		public Term_cstr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term_cstr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterTerm_cstr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitTerm_cstr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitTerm_cstr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Term_cstr_declContext term_cstr_decl() throws RecognitionException {
		Term_cstr_declContext _localctx = new Term_cstr_declContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_term_cstr_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(816);
			match(TERM);
			setState(817);
			elem_exp();
			setState(818);
			match(WITH);
			setState(831);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,66,_ctx) ) {
			case 1:
				{
				setState(819);
				match(EXPECTED);
				setState(820);
				match(PARAMETERS);
				setState(822); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(821);
					param_decl();
					}
					}
					setState(824); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 );
				setState(826);
				match(END);
				setState(827);
				match(EXPECTED);
				setState(828);
				match(PARAMETERS);
				setState(829);
				match(COL);
				}
				break;
			}
			setState(847);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EXPECTED) {
				{
				setState(833);
				match(EXPECTED);
				setState(834);
				match(CONSTRAINTS);
				setState(838); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(835);
					sv_cstr_decl();
					setState(836);
					match(COL);
					}
					}
					setState(840); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__10 || _la==TERM || _la==STRING );
				setState(842);
				match(END);
				setState(843);
				match(EXPECTED);
				setState(844);
				match(CONSTRAINTS);
				setState(845);
				match(COL);
				}
			}

			setState(849);
			match(END);
			setState(850);
			match(TERM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lc_declContext extends ParserRuleContext {
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Sv_lc_declContext sv_lc_decl() {
			return getRuleContext(Sv_lc_declContext.class,0);
		}
		public Mv_lc_declContext mv_lc_decl() {
			return getRuleContext(Mv_lc_declContext.class,0);
		}
		public Lc_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lc_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterLc_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitLc_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitLc_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Lc_declContext lc_decl() throws RecognitionException {
		Lc_declContext _localctx = new Lc_declContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_lc_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(854);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__10:
			case STRING:
				{
				setState(852);
				sv_lc_decl();
				}
				break;
			case FOR:
				{
				setState(853);
				mv_lc_decl();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(856);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Mv_lc_declContext extends ParserRuleContext {
		public For_expContext for_exp() {
			return getRuleContext(For_expContext.class,0);
		}
		public List<Sv_lc_declContext> sv_lc_decl() {
			return getRuleContexts(Sv_lc_declContext.class);
		}
		public Sv_lc_declContext sv_lc_decl(int i) {
			return getRuleContext(Sv_lc_declContext.class,i);
		}
		public Mv_lc_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mv_lc_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMv_lc_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMv_lc_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMv_lc_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Mv_lc_declContext mv_lc_decl() throws RecognitionException {
		Mv_lc_declContext _localctx = new Mv_lc_declContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_mv_lc_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(858);
			for_exp();
			setState(859);
			sv_lc_decl();
			setState(864);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(860);
				match(T__0);
				setState(861);
				sv_lc_decl();
				}
				}
				setState(866);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sv_lc_declContext extends ParserRuleContext {
		public Type_lc_declContext type_lc_decl() {
			return getRuleContext(Type_lc_declContext.class,0);
		}
		public Dom_lc_declContext dom_lc_decl() {
			return getRuleContext(Dom_lc_declContext.class,0);
		}
		public Prop_lc_declContext prop_lc_decl() {
			return getRuleContext(Prop_lc_declContext.class,0);
		}
		public Sv_lc_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sv_lc_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterSv_lc_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitSv_lc_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitSv_lc_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sv_lc_declContext sv_lc_decl() throws RecognitionException {
		Sv_lc_declContext _localctx = new Sv_lc_declContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_sv_lc_decl);
		try {
			setState(870);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,71,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(867);
				type_lc_decl();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(868);
				dom_lc_decl();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(869);
				prop_lc_decl();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_lc_declContext extends ParserRuleContext {
		public Const_expContext const_exp() {
			return getRuleContext(Const_expContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Type_lc_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_lc_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterType_lc_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitType_lc_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitType_lc_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_lc_declContext type_lc_decl() throws RecognitionException {
		Type_lc_declContext _localctx = new Type_lc_declContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_type_lc_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(872);
			const_exp();
			setState(873);
			match(T__6);
			setState(874);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dom_lc_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public Dom_lc_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dom_lc_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDom_lc_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDom_lc_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDom_lc_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dom_lc_declContext dom_lc_decl() throws RecognitionException {
		Dom_lc_declContext _localctx = new Dom_lc_declContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_dom_lc_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(876);
			elem_exp();
			setState(877);
			match(T__3);
			setState(878);
			elem_exp();
			setState(879);
			match(T__6);
			{
			setState(880);
			elem_exp();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Prop_lc_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode HAS() { return getToken(EMDMLParser.HAS, 0); }
		public M_prop_typeContext m_prop_type() {
			return getRuleContext(M_prop_typeContext.class,0);
		}
		public Prop_lc_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prop_lc_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterProp_lc_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitProp_lc_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitProp_lc_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Prop_lc_declContext prop_lc_decl() throws RecognitionException {
		Prop_lc_declContext _localctx = new Prop_lc_declContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_prop_lc_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(882);
			elem_exp();
			setState(883);
			match(HAS);
			setState(884);
			m_prop_type();
			setState(885);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rollup_frag_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode ROLLS_UP_TO() { return getToken(EMDMLParser.ROLLS_UP_TO, 0); }
		public Rollup_frag_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rollup_frag_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterRollup_frag_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitRollup_frag_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitRollup_frag_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Rollup_frag_declContext rollup_frag_decl() throws RecognitionException {
		Rollup_frag_declContext _localctx = new Rollup_frag_declContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_rollup_frag_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(887);
			elem_exp();
			setState(888);
			match(T__3);
			setState(889);
			elem_exp();
			setState(890);
			match(ROLLS_UP_TO);
			setState(891);
			elem_exp();
			setState(892);
			match(T__3);
			setState(893);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Descr_frag_declContext extends ParserRuleContext {
		public List<Elem_expContext> elem_exp() {
			return getRuleContexts(Elem_expContext.class);
		}
		public Elem_expContext elem_exp(int i) {
			return getRuleContext(Elem_expContext.class,i);
		}
		public TerminalNode DESCRIBED_BY() { return getToken(EMDMLParser.DESCRIBED_BY, 0); }
		public Descr_frag_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_descr_frag_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDescr_frag_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDescr_frag_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDescr_frag_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Descr_frag_declContext descr_frag_decl() throws RecognitionException {
		Descr_frag_declContext _localctx = new Descr_frag_declContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_descr_frag_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(895);
			elem_exp();
			setState(896);
			match(T__3);
			setState(897);
			elem_exp();
			setState(898);
			match(DESCRIBED_BY);
			setState(899);
			elem_exp();
			setState(900);
			match(T__3);
			setState(901);
			elem_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Binding_expContext extends ParserRuleContext {
		public Var_expContext var_exp() {
			return getRuleContext(Var_expContext.class,0);
		}
		public Val_expContext val_exp() {
			return getRuleContext(Val_expContext.class,0);
		}
		public Binding_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binding_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterBinding_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitBinding_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitBinding_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Binding_expContext binding_exp() throws RecognitionException {
		Binding_expContext _localctx = new Binding_expContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_binding_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(903);
			var_exp();
			setState(905);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__7) {
				{
				setState(904);
				match(T__7);
				}
			}

			setState(907);
			match(T__1);
			setState(908);
			val_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Val_expContext extends ParserRuleContext {
		public Sv_expContext sv_exp() {
			return getRuleContext(Sv_expContext.class,0);
		}
		public Mv_expContext mv_exp() {
			return getRuleContext(Mv_expContext.class,0);
		}
		public Val_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_val_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVal_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVal_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVal_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Val_expContext val_exp() throws RecognitionException {
		Val_expContext _localctx = new Val_expContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_val_exp);
		try {
			setState(912);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(910);
				sv_exp();
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 2);
				{
				setState(911);
				mv_exp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sv_expContext extends ParserRuleContext {
		public Const_expContext const_exp() {
			return getRuleContext(Const_expContext.class,0);
		}
		public List<Elem_acc_expContext> elem_acc_exp() {
			return getRuleContexts(Elem_acc_expContext.class);
		}
		public Elem_acc_expContext elem_acc_exp(int i) {
			return getRuleContext(Elem_acc_expContext.class,i);
		}
		public Sv_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sv_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterSv_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitSv_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitSv_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sv_expContext sv_exp() throws RecognitionException {
		Sv_expContext _localctx = new Sv_expContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_sv_exp);
		int _la;
		try {
			setState(925);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(914);
				const_exp();
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(915);
				match(T__4);
				setState(916);
				elem_acc_exp();
				setState(919); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(917);
					match(T__0);
					setState(918);
					elem_acc_exp();
					}
					}
					setState(921); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__0 );
				setState(923);
				match(T__5);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Mv_expContext extends ParserRuleContext {
		public List<Sv_expContext> sv_exp() {
			return getRuleContexts(Sv_expContext.class);
		}
		public Sv_expContext sv_exp(int i) {
			return getRuleContext(Sv_expContext.class,i);
		}
		public Mv_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mv_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMv_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMv_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMv_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Mv_expContext mv_exp() throws RecognitionException {
		Mv_expContext _localctx = new Mv_expContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_mv_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(927);
			match(T__8);
			setState(928);
			sv_exp();
			setState(933);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(929);
				match(T__0);
				setState(930);
				sv_exp();
				}
				}
				setState(935);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(936);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Elem_acc_expContext extends ParserRuleContext {
		public Const_expContext const_exp() {
			return getRuleContext(Const_expContext.class,0);
		}
		public Var_acc_expContext var_acc_exp() {
			return getRuleContext(Var_acc_expContext.class,0);
		}
		public Elem_acc_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elem_acc_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterElem_acc_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitElem_acc_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitElem_acc_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Elem_acc_expContext elem_acc_exp() throws RecognitionException {
		Elem_acc_expContext _localctx = new Elem_acc_expContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_elem_acc_exp);
		try {
			setState(940);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(938);
				const_exp();
				}
				break;
			case T__10:
				enterOuterAlt(_localctx, 2);
				{
				setState(939);
				var_acc_exp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_acc_expContext extends ParserRuleContext {
		public Var_labelContext var_label() {
			return getRuleContext(Var_labelContext.class,0);
		}
		public Array_acc_expContext array_acc_exp() {
			return getRuleContext(Array_acc_expContext.class,0);
		}
		public Map_simple_acc_expContext map_simple_acc_exp() {
			return getRuleContext(Map_simple_acc_expContext.class,0);
		}
		public Tuple_acc_expContext tuple_acc_exp() {
			return getRuleContext(Tuple_acc_expContext.class,0);
		}
		public Var_acc_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_acc_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVar_acc_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVar_acc_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVar_acc_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_acc_expContext var_acc_exp() throws RecognitionException {
		Var_acc_expContext _localctx = new Var_acc_expContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_var_acc_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(942);
			match(T__10);
			setState(943);
			var_label();
			setState(944);
			match(T__11);
			setState(949);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__12) {
				{
				setState(945);
				match(T__12);
				setState(947);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__16) {
					{
					setState(946);
					tuple_acc_exp();
					}
				}

				}
			}

			setState(953);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__13:
				{
				setState(951);
				array_acc_exp();
				}
				break;
			case T__15:
				{
				setState(952);
				map_simple_acc_exp();
				}
				break;
			case T__0:
			case T__5:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_acc_expContext extends ParserRuleContext {
		public Idx_noContext idx_no() {
			return getRuleContext(Idx_noContext.class,0);
		}
		public Array_acc_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_acc_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterArray_acc_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitArray_acc_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitArray_acc_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_acc_expContext array_acc_exp() throws RecognitionException {
		Array_acc_expContext _localctx = new Array_acc_expContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_array_acc_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(955);
			match(T__13);
			setState(956);
			idx_no();
			setState(957);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Map_simple_acc_expContext extends ParserRuleContext {
		public List<Idx_nameContext> idx_name() {
			return getRuleContexts(Idx_nameContext.class);
		}
		public Idx_nameContext idx_name(int i) {
			return getRuleContext(Idx_nameContext.class,i);
		}
		public Map_simple_acc_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_map_simple_acc_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMap_simple_acc_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMap_simple_acc_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMap_simple_acc_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Map_simple_acc_expContext map_simple_acc_exp() throws RecognitionException {
		Map_simple_acc_expContext _localctx = new Map_simple_acc_expContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_map_simple_acc_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(959);
			match(T__15);
			setState(960);
			match(T__13);
			setState(972);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING:
				{
				setState(961);
				idx_name();
				}
				break;
			case T__4:
				{
				{
				setState(962);
				match(T__4);
				setState(963);
				idx_name();
				setState(966); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(964);
					match(T__0);
					setState(965);
					idx_name();
					}
					}
					setState(968); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__0 );
				setState(970);
				match(T__5);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(974);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Tuple_acc_expContext extends ParserRuleContext {
		public Idx_noContext idx_no() {
			return getRuleContext(Idx_noContext.class,0);
		}
		public Tuple_acc_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tuple_acc_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterTuple_acc_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitTuple_acc_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitTuple_acc_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tuple_acc_expContext tuple_acc_exp() throws RecognitionException {
		Tuple_acc_expContext _localctx = new Tuple_acc_expContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_tuple_acc_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(976);
			match(T__16);
			setState(977);
			idx_no();
			setState(978);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Elem_expContext extends ParserRuleContext {
		public Const_expContext const_exp() {
			return getRuleContext(Const_expContext.class,0);
		}
		public Var_expContext var_exp() {
			return getRuleContext(Var_expContext.class,0);
		}
		public Elem_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elem_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterElem_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitElem_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitElem_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Elem_expContext elem_exp() throws RecognitionException {
		Elem_expContext _localctx = new Elem_expContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_elem_exp);
		try {
			setState(982);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(980);
				const_exp();
				}
				break;
			case T__10:
				enterOuterAlt(_localctx, 2);
				{
				setState(981);
				var_exp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_expContext extends ParserRuleContext {
		public Var_labelContext var_label() {
			return getRuleContext(Var_labelContext.class,0);
		}
		public Tuple_expContext tuple_exp() {
			return getRuleContext(Tuple_expContext.class,0);
		}
		public Map_expContext map_exp() {
			return getRuleContext(Map_expContext.class,0);
		}
		public Array_expContext array_exp() {
			return getRuleContext(Array_expContext.class,0);
		}
		public Var_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVar_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVar_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVar_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_expContext var_exp() throws RecognitionException {
		Var_expContext _localctx = new Var_expContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_var_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(984);
			match(T__10);
			setState(985);
			var_label();
			setState(986);
			match(T__11);
			setState(988);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__4 || _la==T__12) {
				{
				setState(987);
				tuple_exp();
				}
			}

			setState(992);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__15:
				{
				setState(990);
				map_exp();
				}
				break;
			case T__13:
			case T__17:
				{
				setState(991);
				array_exp();
				}
				break;
			case EOF:
			case T__0:
			case T__1:
			case T__2:
			case T__3:
			case T__5:
			case T__6:
			case T__7:
			case T__14:
			case DESCRIBED_BY:
			case HAS:
			case IN:
			case IS_APPLICABLE_TO:
			case RETURNS:
			case ROLLS_UP_TO:
			case WITH:
			case COL:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Tuple_expContext extends ParserRuleContext {
		public Tuple_acc_expContext tuple_acc_exp() {
			return getRuleContext(Tuple_acc_expContext.class,0);
		}
		public List<Idx_expContext> idx_exp() {
			return getRuleContexts(Idx_expContext.class);
		}
		public Idx_expContext idx_exp(int i) {
			return getRuleContext(Idx_expContext.class,i);
		}
		public Tuple_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tuple_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterTuple_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitTuple_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitTuple_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tuple_expContext tuple_exp() throws RecognitionException {
		Tuple_expContext _localctx = new Tuple_expContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_tuple_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1009);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__12:
				{
				setState(994);
				match(T__12);
				setState(996);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__16) {
					{
					setState(995);
					tuple_acc_exp();
					}
				}

				}
				break;
			case T__4:
				{
				setState(998);
				match(T__4);
				setState(1006);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__4 || _la==T__10 || _la==STRING || _la==NUMBER) {
					{
					setState(999);
					idx_exp();
					setState(1002); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1000);
						match(T__0);
						setState(1001);
						idx_exp();
						}
						}
						setState(1004); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==T__0 );
					}
				}

				setState(1008);
				match(T__5);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_expContext extends ParserRuleContext {
		public Array_acc_expContext array_acc_exp() {
			return getRuleContext(Array_acc_expContext.class,0);
		}
		public Array_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterArray_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitArray_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitArray_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_expContext array_exp() throws RecognitionException {
		Array_expContext _localctx = new Array_expContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_array_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1013);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__17:
				{
				setState(1011);
				match(T__17);
				}
				break;
			case T__13:
				{
				setState(1012);
				array_acc_exp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Map_expContext extends ParserRuleContext {
		public Map_acc_expContext map_acc_exp() {
			return getRuleContext(Map_acc_expContext.class,0);
		}
		public Map_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_map_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMap_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMap_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMap_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Map_expContext map_exp() throws RecognitionException {
		Map_expContext _localctx = new Map_expContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_map_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1015);
			match(T__15);
			setState(1017);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__13) {
				{
				setState(1016);
				map_acc_exp();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Map_acc_expContext extends ParserRuleContext {
		public Idx_expContext idx_exp() {
			return getRuleContext(Idx_expContext.class,0);
		}
		public Map_acc_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_map_acc_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMap_acc_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMap_acc_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMap_acc_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Map_acc_expContext map_acc_exp() throws RecognitionException {
		Map_acc_expContext _localctx = new Map_acc_expContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_map_acc_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1019);
			match(T__13);
			setState(1020);
			idx_exp();
			setState(1021);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Idx_expContext extends ParserRuleContext {
		public Idx_nameContext idx_name() {
			return getRuleContext(Idx_nameContext.class,0);
		}
		public Idx_noContext idx_no() {
			return getRuleContext(Idx_noContext.class,0);
		}
		public Sv_expContext sv_exp() {
			return getRuleContext(Sv_expContext.class,0);
		}
		public Var_expContext var_exp() {
			return getRuleContext(Var_expContext.class,0);
		}
		public Idx_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idx_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterIdx_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitIdx_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitIdx_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Idx_expContext idx_exp() throws RecognitionException {
		Idx_expContext _localctx = new Idx_expContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_idx_exp);
		try {
			setState(1027);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,92,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1023);
				idx_name();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1024);
				idx_no();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1025);
				sv_exp();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1026);
				var_exp();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Const_expContext extends ParserRuleContext {
		public Const_nameContext const_name() {
			return getRuleContext(Const_nameContext.class,0);
		}
		public Const_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_const_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterConst_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitConst_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitConst_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Const_expContext const_exp() throws RecognitionException {
		Const_expContext _localctx = new Const_expContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_const_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1029);
			const_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Op_expContext extends ParserRuleContext {
		public TerminalNode IS_OPTIONAL() { return getToken(EMDMLParser.IS_OPTIONAL, 0); }
		public Op_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_op_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterOp_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitOp_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitOp_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Op_expContext op_exp() throws RecognitionException {
		Op_expContext _localctx = new Op_expContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_op_exp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1031);
			match(IS_OPTIONAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Path_expContext extends ParserRuleContext {
		public List<Elem_nameContext> elem_name() {
			return getRuleContexts(Elem_nameContext.class);
		}
		public Elem_nameContext elem_name(int i) {
			return getRuleContext(Elem_nameContext.class,i);
		}
		public Path_expContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path_exp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterPath_exp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitPath_exp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitPath_exp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Path_expContext path_exp() throws RecognitionException {
		Path_expContext _localctx = new Path_expContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_path_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1033);
			elem_name();
			setState(1038);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__18) {
				{
				{
				setState(1034);
				match(T__18);
				setState(1035);
				elem_name();
				}
				}
				setState(1040);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cube_defContext extends ParserRuleContext {
		public List<TerminalNode> CUBE() { return getTokens(EMDMLParser.CUBE); }
		public TerminalNode CUBE(int i) {
			return getToken(EMDMLParser.CUBE, i);
		}
		public Cube_nameContext cube_name() {
			return getRuleContext(Cube_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<TerminalNode> MEASURE() { return getTokens(EMDMLParser.MEASURE); }
		public TerminalNode MEASURE(int i) {
			return getToken(EMDMLParser.MEASURE, i);
		}
		public List<TerminalNode> PROPERTIES() { return getTokens(EMDMLParser.PROPERTIES); }
		public TerminalNode PROPERTIES(int i) {
			return getToken(EMDMLParser.PROPERTIES, i);
		}
		public List<TerminalNode> END() { return getTokens(EMDMLParser.END); }
		public TerminalNode END(int i) {
			return getToken(EMDMLParser.END, i);
		}
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<TerminalNode> DIMENSION_ROLE() { return getTokens(EMDMLParser.DIMENSION_ROLE); }
		public TerminalNode DIMENSION_ROLE(int i) {
			return getToken(EMDMLParser.DIMENSION_ROLE, i);
		}
		public List<Meas_declContext> meas_decl() {
			return getRuleContexts(Meas_declContext.class);
		}
		public Meas_declContext meas_decl(int i) {
			return getRuleContext(Meas_declContext.class,i);
		}
		public List<Dim_role_declContext> dim_role_decl() {
			return getRuleContexts(Dim_role_declContext.class);
		}
		public Dim_role_declContext dim_role_decl(int i) {
			return getRuleContext(Dim_role_declContext.class,i);
		}
		public Cube_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cube_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterCube_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitCube_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitCube_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cube_defContext cube_def() throws RecognitionException {
		Cube_defContext _localctx = new Cube_defContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_cube_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1041);
			match(CUBE);
			setState(1042);
			cube_name();
			setState(1043);
			match(WITH);
			setState(1044);
			match(MEASURE);
			setState(1045);
			match(PROPERTIES);
			setState(1047); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1046);
				meas_decl();
				}
				}
				setState(1049); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==STRING );
			setState(1051);
			match(END);
			setState(1052);
			match(MEASURE);
			setState(1053);
			match(PROPERTIES);
			setState(1054);
			match(COL);
			setState(1055);
			match(DIMENSION_ROLE);
			setState(1056);
			match(PROPERTIES);
			setState(1058); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1057);
				dim_role_decl();
				}
				}
				setState(1060); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==STRING );
			setState(1062);
			match(END);
			setState(1063);
			match(DIMENSION_ROLE);
			setState(1064);
			match(PROPERTIES);
			setState(1065);
			match(COL);
			setState(1066);
			match(END);
			setState(1067);
			match(CUBE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dim_defContext extends ParserRuleContext {
		public List<TerminalNode> DIMENSION() { return getTokens(EMDMLParser.DIMENSION); }
		public TerminalNode DIMENSION(int i) {
			return getToken(EMDMLParser.DIMENSION, i);
		}
		public Dim_nameContext dim_name() {
			return getRuleContext(Dim_nameContext.class,0);
		}
		public TerminalNode WITH() { return getToken(EMDMLParser.WITH, 0); }
		public List<TerminalNode> END() { return getTokens(EMDMLParser.END); }
		public TerminalNode END(int i) {
			return getToken(EMDMLParser.END, i);
		}
		public List<TerminalNode> LEVEL() { return getTokens(EMDMLParser.LEVEL); }
		public TerminalNode LEVEL(int i) {
			return getToken(EMDMLParser.LEVEL, i);
		}
		public List<TerminalNode> PROPERTIES() { return getTokens(EMDMLParser.PROPERTIES); }
		public TerminalNode PROPERTIES(int i) {
			return getToken(EMDMLParser.PROPERTIES, i);
		}
		public List<TerminalNode> COL() { return getTokens(EMDMLParser.COL); }
		public TerminalNode COL(int i) {
			return getToken(EMDMLParser.COL, i);
		}
		public List<TerminalNode> ATTRIBUTE() { return getTokens(EMDMLParser.ATTRIBUTE); }
		public TerminalNode ATTRIBUTE(int i) {
			return getToken(EMDMLParser.ATTRIBUTE, i);
		}
		public List<TerminalNode> CONSTRAINTS() { return getTokens(EMDMLParser.CONSTRAINTS); }
		public TerminalNode CONSTRAINTS(int i) {
			return getToken(EMDMLParser.CONSTRAINTS, i);
		}
		public List<Lvl_declContext> lvl_decl() {
			return getRuleContexts(Lvl_declContext.class);
		}
		public Lvl_declContext lvl_decl(int i) {
			return getRuleContext(Lvl_declContext.class,i);
		}
		public List<Attr_declContext> attr_decl() {
			return getRuleContexts(Attr_declContext.class);
		}
		public Attr_declContext attr_decl(int i) {
			return getRuleContext(Attr_declContext.class,i);
		}
		public List<Roll_up_rel_declContext> roll_up_rel_decl() {
			return getRuleContexts(Roll_up_rel_declContext.class);
		}
		public Roll_up_rel_declContext roll_up_rel_decl(int i) {
			return getRuleContext(Roll_up_rel_declContext.class,i);
		}
		public List<Descr_by_rel_declContext> descr_by_rel_decl() {
			return getRuleContexts(Descr_by_rel_declContext.class);
		}
		public Descr_by_rel_declContext descr_by_rel_decl(int i) {
			return getRuleContext(Descr_by_rel_declContext.class,i);
		}
		public Dim_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dim_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDim_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDim_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDim_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dim_defContext dim_def() throws RecognitionException {
		Dim_defContext _localctx = new Dim_defContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_dim_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1069);
			match(DIMENSION);
			setState(1070);
			dim_name();
			setState(1071);
			match(WITH);
			setState(1107); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(1107);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LEVEL:
					{
					{
					setState(1072);
					match(LEVEL);
					setState(1073);
					match(PROPERTIES);
					setState(1075); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1074);
						lvl_decl();
						}
						}
						setState(1077); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==STRING );
					setState(1079);
					match(END);
					setState(1080);
					match(LEVEL);
					setState(1081);
					match(PROPERTIES);
					setState(1082);
					match(COL);
					}
					}
					break;
				case ATTRIBUTE:
					{
					{
					setState(1084);
					match(ATTRIBUTE);
					setState(1085);
					match(PROPERTIES);
					setState(1087); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1086);
						attr_decl();
						}
						}
						setState(1089); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==STRING );
					setState(1091);
					match(END);
					setState(1092);
					match(ATTRIBUTE);
					setState(1093);
					match(PROPERTIES);
					setState(1094);
					match(COL);
					}
					}
					break;
				case CONSTRAINTS:
					{
					{
					setState(1096);
					match(CONSTRAINTS);
					setState(1099); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						setState(1099);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,98,_ctx) ) {
						case 1:
							{
							setState(1097);
							roll_up_rel_decl();
							}
							break;
						case 2:
							{
							setState(1098);
							descr_by_rel_decl();
							}
							break;
						}
						}
						setState(1101); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==STRING );
					setState(1103);
					match(END);
					setState(1104);
					match(CONSTRAINTS);
					setState(1105);
					match(COL);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(1109); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 23)) & ~0x3f) == 0 && ((1L << (_la - 23)) & ((1L << (ATTRIBUTE - 23)) | (1L << (CONSTRAINTS - 23)) | (1L << (LEVEL - 23)))) != 0) );
			setState(1111);
			match(END);
			setState(1112);
			match(DIMENSION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class M_delContext extends ParserRuleContext {
		public TerminalNode CUBE() { return getToken(EMDMLParser.CUBE, 0); }
		public Cube_nameContext cube_name() {
			return getRuleContext(Cube_nameContext.class,0);
		}
		public TerminalNode DIMENSION() { return getToken(EMDMLParser.DIMENSION, 0); }
		public Dim_nameContext dim_name() {
			return getRuleContext(Dim_nameContext.class,0);
		}
		public M_delContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_m_del; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterM_del(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitM_del(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitM_del(this);
			else return visitor.visitChildren(this);
		}
	}

	public final M_delContext m_del() throws RecognitionException {
		M_delContext _localctx = new M_delContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_m_del);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1118);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CUBE:
				{
				setState(1114);
				match(CUBE);
				setState(1115);
				cube_name();
				}
				break;
			case DIMENSION:
				{
				setState(1116);
				match(DIMENSION);
				setState(1117);
				dim_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Meas_declContext extends ParserRuleContext {
		public Meas_nameContext meas_name() {
			return getRuleContext(Meas_nameContext.class,0);
		}
		public Val_set_nameContext val_set_name() {
			return getRuleContext(Val_set_nameContext.class,0);
		}
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Meas_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_meas_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMeas_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMeas_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMeas_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Meas_declContext meas_decl() throws RecognitionException {
		Meas_declContext _localctx = new Meas_declContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_meas_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1120);
			meas_name();
			setState(1121);
			match(T__6);
			setState(1122);
			val_set_name();
			setState(1123);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dim_role_declContext extends ParserRuleContext {
		public Dim_role_nameContext dim_role_name() {
			return getRuleContext(Dim_role_nameContext.class,0);
		}
		public Dim_loc_nameContext dim_loc_name() {
			return getRuleContext(Dim_loc_nameContext.class,0);
		}
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Dim_role_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dim_role_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDim_role_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDim_role_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDim_role_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dim_role_declContext dim_role_decl() throws RecognitionException {
		Dim_role_declContext _localctx = new Dim_role_declContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_dim_role_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1125);
			dim_role_name();
			setState(1126);
			match(T__6);
			setState(1127);
			dim_loc_name();
			setState(1128);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lvl_declContext extends ParserRuleContext {
		public Lvl_nameContext lvl_name() {
			return getRuleContext(Lvl_nameContext.class,0);
		}
		public Val_set_nameContext val_set_name() {
			return getRuleContext(Val_set_nameContext.class,0);
		}
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Lvl_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lvl_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterLvl_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitLvl_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitLvl_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Lvl_declContext lvl_decl() throws RecognitionException {
		Lvl_declContext _localctx = new Lvl_declContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_lvl_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1130);
			lvl_name();
			setState(1131);
			match(T__6);
			setState(1132);
			val_set_name();
			setState(1133);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Attr_declContext extends ParserRuleContext {
		public Attr_nameContext attr_name() {
			return getRuleContext(Attr_nameContext.class,0);
		}
		public Val_set_nameContext val_set_name() {
			return getRuleContext(Val_set_nameContext.class,0);
		}
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Attr_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attr_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterAttr_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitAttr_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitAttr_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Attr_declContext attr_decl() throws RecognitionException {
		Attr_declContext _localctx = new Attr_declContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_attr_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1135);
			attr_name();
			setState(1136);
			match(T__6);
			setState(1137);
			val_set_name();
			setState(1138);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Roll_up_rel_declContext extends ParserRuleContext {
		public List<Lvl_nameContext> lvl_name() {
			return getRuleContexts(Lvl_nameContext.class);
		}
		public Lvl_nameContext lvl_name(int i) {
			return getRuleContext(Lvl_nameContext.class,i);
		}
		public TerminalNode ROLLS_UP_TO() { return getToken(EMDMLParser.ROLLS_UP_TO, 0); }
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Roll_up_rel_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_roll_up_rel_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterRoll_up_rel_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitRoll_up_rel_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitRoll_up_rel_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Roll_up_rel_declContext roll_up_rel_decl() throws RecognitionException {
		Roll_up_rel_declContext _localctx = new Roll_up_rel_declContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_roll_up_rel_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1140);
			lvl_name();
			setState(1141);
			match(ROLLS_UP_TO);
			setState(1142);
			lvl_name();
			setState(1143);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Descr_by_rel_declContext extends ParserRuleContext {
		public Lvl_nameContext lvl_name() {
			return getRuleContext(Lvl_nameContext.class,0);
		}
		public TerminalNode DESCRIBED_BY() { return getToken(EMDMLParser.DESCRIBED_BY, 0); }
		public Attr_nameContext attr_name() {
			return getRuleContext(Attr_nameContext.class,0);
		}
		public TerminalNode COL() { return getToken(EMDMLParser.COL, 0); }
		public Descr_by_rel_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_descr_by_rel_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDescr_by_rel_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDescr_by_rel_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDescr_by_rel_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Descr_by_rel_declContext descr_by_rel_decl() throws RecognitionException {
		Descr_by_rel_declContext _localctx = new Descr_by_rel_declContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_descr_by_rel_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1145);
			lvl_name();
			setState(1146);
			match(DESCRIBED_BY);
			setState(1147);
			attr_name();
			setState(1148);
			match(COL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public M_entity_typeContext m_entity_type() {
			return getRuleContext(M_entity_typeContext.class,0);
		}
		public M_prop_typeContext m_prop_type() {
			return getRuleContext(M_prop_typeContext.class,0);
		}
		public T_typeContext t_type() {
			return getRuleContext(T_typeContext.class,0);
		}
		public V_typeContext v_type() {
			return getRuleContext(V_typeContext.class,0);
		}
		public A_typeContext a_type() {
			return getRuleContext(A_typeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_type);
		try {
			setState(1155);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CUBE:
			case DIMENSION:
				enterOuterAlt(_localctx, 1);
				{
				setState(1150);
				m_entity_type();
				}
				break;
			case ATTRIBUTE:
			case CUBE_PROPERTY:
			case DIMENSION_PROPERTY:
			case DIMENSION_ROLE:
			case LEVEL:
			case MEASURE:
				enterOuterAlt(_localctx, 2);
				{
				setState(1151);
				m_prop_type();
				}
				break;
			case BINARY_CUBE_PREDICATE:
			case BINARY_CALCULATED_MEASURE:
			case BINARY_DIMENSION_PREDICATE:
			case CUBE_ORDERING:
			case DIMENSION_GROUPING:
			case DIMENSION_ORDERING:
			case UNARY_CUBE_PREDICATE:
			case UNARY_CALCULATED_MEASURE:
			case UNARY_DIMENSION_PREDICATE:
				enterOuterAlt(_localctx, 3);
				{
				setState(1152);
				t_type();
				}
				break;
			case NUMBER_VALUE_SET:
			case STRING_VALUE_SET:
			case STRING:
				enterOuterAlt(_localctx, 4);
				{
				setState(1153);
				v_type();
				}
				break;
			case BINARY_TUPLE:
			case TERNARY_TUPLE:
			case QUARTERNARY_TUPLE:
				enterOuterAlt(_localctx, 5);
				{
				setState(1154);
				a_type();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_typeContext extends ParserRuleContext {
		public TerminalNode UNARY_CUBE_PREDICATE() { return getToken(EMDMLParser.UNARY_CUBE_PREDICATE, 0); }
		public TerminalNode BINARY_CUBE_PREDICATE() { return getToken(EMDMLParser.BINARY_CUBE_PREDICATE, 0); }
		public TerminalNode UNARY_CALCULATED_MEASURE() { return getToken(EMDMLParser.UNARY_CALCULATED_MEASURE, 0); }
		public TerminalNode BINARY_CALCULATED_MEASURE() { return getToken(EMDMLParser.BINARY_CALCULATED_MEASURE, 0); }
		public TerminalNode CUBE_ORDERING() { return getToken(EMDMLParser.CUBE_ORDERING, 0); }
		public TerminalNode UNARY_DIMENSION_PREDICATE() { return getToken(EMDMLParser.UNARY_DIMENSION_PREDICATE, 0); }
		public TerminalNode BINARY_DIMENSION_PREDICATE() { return getToken(EMDMLParser.BINARY_DIMENSION_PREDICATE, 0); }
		public TerminalNode DIMENSION_GROUPING() { return getToken(EMDMLParser.DIMENSION_GROUPING, 0); }
		public TerminalNode DIMENSION_ORDERING() { return getToken(EMDMLParser.DIMENSION_ORDERING, 0); }
		public T_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_typeContext t_type() throws RecognitionException {
		T_typeContext _localctx = new T_typeContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_t_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1157);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BINARY_CUBE_PREDICATE) | (1L << BINARY_CALCULATED_MEASURE) | (1L << BINARY_DIMENSION_PREDICATE) | (1L << CUBE_ORDERING) | (1L << DIMENSION_GROUPING) | (1L << DIMENSION_ORDERING))) != 0) || ((((_la - 92)) & ~0x3f) == 0 && ((1L << (_la - 92)) & ((1L << (UNARY_CUBE_PREDICATE - 92)) | (1L << (UNARY_CALCULATED_MEASURE - 92)) | (1L << (UNARY_DIMENSION_PREDICATE - 92)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class M_entity_typeContext extends ParserRuleContext {
		public TerminalNode CUBE() { return getToken(EMDMLParser.CUBE, 0); }
		public TerminalNode DIMENSION() { return getToken(EMDMLParser.DIMENSION, 0); }
		public M_entity_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_m_entity_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterM_entity_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitM_entity_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitM_entity_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final M_entity_typeContext m_entity_type() throws RecognitionException {
		M_entity_typeContext _localctx = new M_entity_typeContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_m_entity_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1159);
			_la = _input.LA(1);
			if ( !(_la==CUBE || _la==DIMENSION) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class M_prop_typeContext extends ParserRuleContext {
		public TerminalNode MEASURE() { return getToken(EMDMLParser.MEASURE, 0); }
		public TerminalNode DIMENSION_ROLE() { return getToken(EMDMLParser.DIMENSION_ROLE, 0); }
		public TerminalNode CUBE_PROPERTY() { return getToken(EMDMLParser.CUBE_PROPERTY, 0); }
		public TerminalNode LEVEL() { return getToken(EMDMLParser.LEVEL, 0); }
		public TerminalNode ATTRIBUTE() { return getToken(EMDMLParser.ATTRIBUTE, 0); }
		public TerminalNode DIMENSION_PROPERTY() { return getToken(EMDMLParser.DIMENSION_PROPERTY, 0); }
		public M_prop_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_m_prop_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterM_prop_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitM_prop_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitM_prop_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final M_prop_typeContext m_prop_type() throws RecognitionException {
		M_prop_typeContext _localctx = new M_prop_typeContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_m_prop_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1161);
			_la = _input.LA(1);
			if ( !(((((_la - 23)) & ~0x3f) == 0 && ((1L << (_la - 23)) & ((1L << (ATTRIBUTE - 23)) | (1L << (CUBE_PROPERTY - 23)) | (1L << (DIMENSION_PROPERTY - 23)) | (1L << (DIMENSION_ROLE - 23)) | (1L << (LEVEL - 23)) | (1L << (MEASURE - 23)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class V_typeContext extends ParserRuleContext {
		public TerminalNode NUMBER_VALUE_SET() { return getToken(EMDMLParser.NUMBER_VALUE_SET, 0); }
		public TerminalNode STRING_VALUE_SET() { return getToken(EMDMLParser.STRING_VALUE_SET, 0); }
		public Val_set_nameContext val_set_name() {
			return getRuleContext(Val_set_nameContext.class,0);
		}
		public V_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_v_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterV_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitV_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitV_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final V_typeContext v_type() throws RecognitionException {
		V_typeContext _localctx = new V_typeContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_v_type);
		try {
			setState(1166);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMBER_VALUE_SET:
				enterOuterAlt(_localctx, 1);
				{
				setState(1163);
				match(NUMBER_VALUE_SET);
				}
				break;
			case STRING_VALUE_SET:
				enterOuterAlt(_localctx, 2);
				{
				setState(1164);
				match(STRING_VALUE_SET);
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 3);
				{
				setState(1165);
				val_set_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class A_typeContext extends ParserRuleContext {
		public TerminalNode BINARY_TUPLE() { return getToken(EMDMLParser.BINARY_TUPLE, 0); }
		public TerminalNode TERNARY_TUPLE() { return getToken(EMDMLParser.TERNARY_TUPLE, 0); }
		public TerminalNode QUARTERNARY_TUPLE() { return getToken(EMDMLParser.QUARTERNARY_TUPLE, 0); }
		public A_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_a_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterA_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitA_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitA_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final A_typeContext a_type() throws RecognitionException {
		A_typeContext _localctx = new A_typeContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_a_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1168);
			_la = _input.LA(1);
			if ( !(_la==BINARY_TUPLE || _la==TERNARY_TUPLE || _la==QUARTERNARY_TUPLE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Descr_txtContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Descr_txtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_descr_txt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDescr_txt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDescr_txt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDescr_txt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Descr_txtContext descr_txt() throws RecognitionException {
		Descr_txtContext _localctx = new Descr_txtContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_descr_txt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1170);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Temp_txtContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Temp_txtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_temp_txt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterTemp_txt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitTemp_txt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitTemp_txt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Temp_txtContext temp_txt() throws RecognitionException {
		Temp_txtContext _localctx = new Temp_txtContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_temp_txt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1172);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Prob_txtContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Prob_txtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prob_txt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterProb_txt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitProb_txt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitProb_txt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Prob_txtContext prob_txt() throws RecognitionException {
		Prob_txtContext _localctx = new Prob_txtContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_prob_txt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1174);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sol_txtContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Sol_txtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sol_txt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterSol_txt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitSol_txt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitSol_txt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sol_txtContext sol_txt() throws RecognitionException {
		Sol_txtContext _localctx = new Sol_txtContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_sol_txt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1176);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ex_txtContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Ex_txtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ex_txt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterEx_txt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitEx_txt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitEx_txt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ex_txtContext ex_txt() throws RecognitionException {
		Ex_txtContext _localctx = new Ex_txtContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_ex_txt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1178);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_labelContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(EMDMLParser.NAME, 0); }
		public Var_labelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_label; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVar_label(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVar_label(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVar_label(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_labelContext var_label() throws RecognitionException {
		Var_labelContext _localctx = new Var_labelContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_var_label);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1180);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alias_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Alias_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alias_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterAlias_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitAlias_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitAlias_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alias_nameContext alias_name() throws RecognitionException {
		Alias_nameContext _localctx = new Alias_nameContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_alias_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1182);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Model_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Model_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_model_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterModel_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitModel_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitModel_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Model_nameContext model_name() throws RecognitionException {
		Model_nameContext _localctx = new Model_nameContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_model_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1184);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variant_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Variant_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variant_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVariant_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVariant_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVariant_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variant_nameContext variant_name() throws RecognitionException {
		Variant_nameContext _localctx = new Variant_nameContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_variant_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1186);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lang_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Lang_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lang_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterLang_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitLang_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitLang_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Lang_nameContext lang_name() throws RecognitionException {
		Lang_nameContext _localctx = new Lang_nameContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_lang_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1188);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dialect_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Dialect_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dialect_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDialect_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDialect_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDialect_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dialect_nameContext dialect_name() throws RecognitionException {
		Dialect_nameContext _localctx = new Dialect_nameContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_dialect_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1190);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Const_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Const_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_const_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterConst_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitConst_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitConst_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Const_nameContext const_name() throws RecognitionException {
		Const_nameContext _localctx = new Const_nameContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_const_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1192);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Meas_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Meas_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_meas_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMeas_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMeas_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMeas_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Meas_nameContext meas_name() throws RecognitionException {
		Meas_nameContext _localctx = new Meas_nameContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_meas_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1194);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dim_role_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Dim_role_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dim_role_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDim_role_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDim_role_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDim_role_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dim_role_nameContext dim_role_name() throws RecognitionException {
		Dim_role_nameContext _localctx = new Dim_role_nameContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_dim_role_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1196);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lvl_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Lvl_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lvl_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterLvl_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitLvl_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitLvl_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Lvl_nameContext lvl_name() throws RecognitionException {
		Lvl_nameContext _localctx = new Lvl_nameContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_lvl_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1198);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Attr_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Attr_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attr_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterAttr_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitAttr_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitAttr_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Attr_nameContext attr_name() throws RecognitionException {
		Attr_nameContext _localctx = new Attr_nameContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_attr_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1200);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Val_set_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Val_set_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_val_set_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVal_set_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVal_set_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVal_set_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Val_set_nameContext val_set_name() throws RecognitionException {
		Val_set_nameContext _localctx = new Val_set_nameContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_val_set_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1202);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Idx_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Idx_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idx_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterIdx_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitIdx_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitIdx_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Idx_nameContext idx_name() throws RecognitionException {
		Idx_nameContext _localctx = new Idx_nameContext(_ctx, getState());
		enterRule(_localctx, 218, RULE_idx_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1204);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Elem_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Elem_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elem_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterElem_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitElem_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitElem_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Elem_nameContext elem_name() throws RecognitionException {
		Elem_nameContext _localctx = new Elem_nameContext(_ctx, getState());
		enterRule(_localctx, 220, RULE_elem_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1206);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Value_set_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Value_set_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value_set_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterValue_set_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitValue_set_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitValue_set_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Value_set_nameContext value_set_name() throws RecognitionException {
		Value_set_nameContext _localctx = new Value_set_nameContext(_ctx, getState());
		enterRule(_localctx, 222, RULE_value_set_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1208);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Idx_noContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(EMDMLParser.NUMBER, 0); }
		public Idx_noContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idx_no; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterIdx_no(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitIdx_no(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitIdx_no(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Idx_noContext idx_no() throws RecognitionException {
		Idx_noContext _localctx = new Idx_noContext(_ctx, getState());
		enterRule(_localctx, 224, RULE_idx_no);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1210);
			match(NUMBER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class S_strContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public S_strContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_s_str; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterS_str(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitS_str(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitS_str(this);
			else return visitor.visitChildren(this);
		}
	}

	public final S_strContext s_str() throws RecognitionException {
		S_strContext _localctx = new S_strContext(_ctx, getState());
		enterRule(_localctx, 226, RULE_s_str);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1212);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_loc_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public P_loc_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_loc_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_loc_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_loc_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_loc_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_loc_nameContext p_loc_name() throws RecognitionException {
		P_loc_nameContext _localctx = new P_loc_nameContext(_ctx, getState());
		enterRule(_localctx, 228, RULE_p_loc_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1214);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dim_loc_nameContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(EMDMLParser.STRING, 0); }
		public Dim_loc_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dim_loc_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDim_loc_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDim_loc_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDim_loc_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dim_loc_nameContext dim_loc_name() throws RecognitionException {
		Dim_loc_nameContext _localctx = new Dim_loc_nameContext(_ctx, getState());
		enterRule(_localctx, 230, RULE_dim_loc_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1216);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class P_nameContext extends ParserRuleContext {
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public P_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_p_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterP_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitP_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitP_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final P_nameContext p_name() throws RecognitionException {
		P_nameContext _localctx = new P_nameContext(_ctx, getState());
		enterRule(_localctx, 232, RULE_p_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1218);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class T_nameContext extends ParserRuleContext {
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public T_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterT_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitT_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitT_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final T_nameContext t_name() throws RecognitionException {
		T_nameContext _localctx = new T_nameContext(_ctx, getState());
		enterRule(_localctx, 234, RULE_t_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1220);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cube_nameContext extends ParserRuleContext {
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public Cube_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cube_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterCube_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitCube_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitCube_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cube_nameContext cube_name() throws RecognitionException {
		Cube_nameContext _localctx = new Cube_nameContext(_ctx, getState());
		enterRule(_localctx, 236, RULE_cube_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1222);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dim_nameContext extends ParserRuleContext {
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public Dim_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dim_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterDim_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitDim_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitDim_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dim_nameContext dim_name() throws RecognitionException {
		Dim_nameContext _localctx = new Dim_nameContext(_ctx, getState());
		enterRule(_localctx, 238, RULE_dim_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1224);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Mdm_nameContext extends ParserRuleContext {
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public Mdm_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mdm_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterMdm_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitMdm_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitMdm_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Mdm_nameContext mdm_name() throws RecognitionException {
		Mdm_nameContext _localctx = new Mdm_nameContext(_ctx, getState());
		enterRule(_localctx, 240, RULE_mdm_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1226);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Voc_nameContext extends ParserRuleContext {
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public Voc_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_voc_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterVoc_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitVoc_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitVoc_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Voc_nameContext voc_name() throws RecognitionException {
		Voc_nameContext _localctx = new Voc_nameContext(_ctx, getState());
		enterRule(_localctx, 242, RULE_voc_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1228);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class S_nameContext extends ParserRuleContext {
		public Path_expContext path_exp() {
			return getRuleContext(Path_expContext.class,0);
		}
		public S_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_s_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).enterS_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof EMDMLListener ) ((EMDMLListener)listener).exitS_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof EMDMLVisitor ) return ((EMDMLVisitor<? extends T>)visitor).visitS_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final S_nameContext s_name() throws RecognitionException {
		S_nameContext _localctx = new S_nameContext(_ctx, getState());
		enterRule(_localctx, 244, RULE_s_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1230);
			path_exp();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3o\u04d3\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4"+
		"w\tw\4x\tx\4y\ty\4z\tz\4{\t{\4|\t|\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\u0100"+
		"\n\2\3\2\3\2\6\2\u0104\n\2\r\2\16\2\u0105\3\3\3\3\3\3\5\3\u010b\n\3\3"+
		"\3\3\3\3\3\3\3\3\3\5\3\u0112\n\3\3\4\3\4\3\4\3\4\5\4\u0118\n\4\3\5\3\5"+
		"\3\5\3\5\5\5\u011e\n\5\5\5\u0120\n\5\3\6\3\6\5\6\u0124\n\6\3\7\3\7\3\b"+
		"\3\b\3\b\3\b\3\b\5\b\u012d\n\b\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13"+
		"\3\f\3\f\3\f\3\r\3\r\3\r\3\r\5\r\u013f\n\r\3\r\6\r\u0142\n\r\r\r\16\r"+
		"\u0143\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\7\17\u014e\n\17\f\17\16"+
		"\17\u0151\13\17\6\17\u0153\n\17\r\17\16\17\u0154\3\20\3\20\3\21\3\21\3"+
		"\21\3\22\3\22\3\22\3\22\6\22\u0160\n\22\r\22\16\22\u0161\3\22\3\22\3\22"+
		"\3\22\5\22\u0168\n\22\3\22\3\22\3\22\6\22\u016d\n\22\r\22\16\22\u016e"+
		"\3\22\3\22\3\22\3\22\3\22\5\22\u0176\n\22\3\22\3\22\3\22\6\22\u017b\n"+
		"\22\r\22\16\22\u017c\3\22\3\22\3\22\3\22\3\22\5\22\u0184\n\22\3\22\3\22"+
		"\6\22\u0188\n\22\r\22\16\22\u0189\3\22\3\22\3\22\3\22\5\22\u0190\n\22"+
		"\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\7\23\u01a1\n\23\f\23\16\23\u01a4\13\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u01b4\n\23\f\23\16"+
		"\23\u01b7\13\23\5\23\u01b9\n\23\3\23\3\23\6\23\u01bd\n\23\r\23\16\23\u01be"+
		"\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\6\24\u01ca\n\24\r\24\16"+
		"\24\u01cb\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u01d8"+
		"\n\25\3\25\3\25\3\25\7\25\u01dd\n\25\f\25\16\25\u01e0\13\25\3\25\3\25"+
		"\5\25\u01e4\n\25\3\25\3\25\5\25\u01e8\n\25\3\26\3\26\3\26\3\26\5\26\u01ee"+
		"\n\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u01f7\n\27\3\30\3\30\3\30"+
		"\3\30\3\30\6\30\u01fe\n\30\r\30\16\30\u01ff\3\30\3\30\7\30\u0204\n\30"+
		"\f\30\16\30\u0207\13\30\5\30\u0209\n\30\3\31\3\31\3\31\3\31\3\31\3\31"+
		"\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\6\32\u021b\n\32\r\32"+
		"\16\32\u021c\3\32\3\32\7\32\u0221\n\32\f\32\16\32\u0224\13\32\5\32\u0226"+
		"\n\32\3\33\3\33\3\33\3\33\3\33\6\33\u022d\n\33\r\33\16\33\u022e\3\33\3"+
		"\33\3\33\3\33\5\33\u0235\n\33\3\33\3\33\6\33\u0239\n\33\r\33\16\33\u023a"+
		"\3\33\3\33\3\33\3\33\5\33\u0241\n\33\3\33\3\33\3\33\3\33\5\33\u0247\n"+
		"\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3"+
		"\34\3\34\7\34\u0258\n\34\f\34\16\34\u025b\13\34\3\34\3\34\3\34\5\34\u0260"+
		"\n\34\3\34\3\34\6\34\u0264\n\34\r\34\16\34\u0265\3\34\3\34\3\34\3\34\3"+
		"\35\3\35\3\35\3\35\3\35\6\35\u0271\n\35\r\35\16\35\u0272\3\35\3\35\3\35"+
		"\3\35\3\36\3\36\3\36\3\36\5\36\u027d\n\36\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\5\37\u0286\n\37\3 \3 \3 \3 \3 \6 \u028d\n \r \16 \u028e\3 \3 \7"+
		" \u0293\n \f \16 \u0296\13 \5 \u0298\n \3!\3!\3!\3!\5!\u029e\n!\3!\3!"+
		"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\5\"\u02ae\n\"\3#\3#\3"+
		"#\3$\3$\3$\3$\5$\u02b7\n$\3$\3$\3$\3$\3$\5$\u02be\n$\3$\3$\3%\3%\3%\3"+
		"%\3%\3%\6%\u02c8\n%\r%\16%\u02c9\3%\3%\5%\u02ce\n%\3%\3%\3%\3%\3&\3&\3"+
		"&\3&\5&\u02d8\n&\3\'\3\'\5\'\u02dc\n\'\3\'\3\'\3(\3(\3(\3(\7(\u02e4\n"+
		"(\f(\16(\u02e7\13(\3)\3)\3)\3)\3)\3)\3)\3)\3)\5)\u02f2\n)\3*\3*\3*\3*"+
		"\3+\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3-\3-\3-\3-\3.\3.\3.\3.\3.\3.\3.\3."+
		"\3.\5.\u0310\n.\3/\3/\3/\5/\u0315\n/\3/\3/\3/\7/\u031a\n/\f/\16/\u031d"+
		"\13/\3\60\3\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\62"+
		"\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\63\3\63\3\63\3\63\3\63\3\63\6\63"+
		"\u0339\n\63\r\63\16\63\u033a\3\63\3\63\3\63\3\63\3\63\5\63\u0342\n\63"+
		"\3\63\3\63\3\63\3\63\3\63\6\63\u0349\n\63\r\63\16\63\u034a\3\63\3\63\3"+
		"\63\3\63\3\63\5\63\u0352\n\63\3\63\3\63\3\63\3\64\3\64\5\64\u0359\n\64"+
		"\3\64\3\64\3\65\3\65\3\65\3\65\7\65\u0361\n\65\f\65\16\65\u0364\13\65"+
		"\3\66\3\66\3\66\5\66\u0369\n\66\3\67\3\67\3\67\3\67\38\38\38\38\38\38"+
		"\39\39\39\39\39\3:\3:\3:\3:\3:\3:\3:\3:\3;\3;\3;\3;\3;\3;\3;\3;\3<\3<"+
		"\5<\u038c\n<\3<\3<\3<\3=\3=\5=\u0393\n=\3>\3>\3>\3>\3>\6>\u039a\n>\r>"+
		"\16>\u039b\3>\3>\5>\u03a0\n>\3?\3?\3?\3?\7?\u03a6\n?\f?\16?\u03a9\13?"+
		"\3?\3?\3@\3@\5@\u03af\n@\3A\3A\3A\3A\3A\5A\u03b6\nA\5A\u03b8\nA\3A\3A"+
		"\5A\u03bc\nA\3B\3B\3B\3B\3C\3C\3C\3C\3C\3C\3C\6C\u03c9\nC\rC\16C\u03ca"+
		"\3C\3C\5C\u03cf\nC\3C\3C\3D\3D\3D\3D\3E\3E\5E\u03d9\nE\3F\3F\3F\3F\5F"+
		"\u03df\nF\3F\3F\5F\u03e3\nF\3G\3G\5G\u03e7\nG\3G\3G\3G\3G\6G\u03ed\nG"+
		"\rG\16G\u03ee\5G\u03f1\nG\3G\5G\u03f4\nG\3H\3H\5H\u03f8\nH\3I\3I\5I\u03fc"+
		"\nI\3J\3J\3J\3J\3K\3K\3K\3K\5K\u0406\nK\3L\3L\3M\3M\3N\3N\3N\7N\u040f"+
		"\nN\fN\16N\u0412\13N\3O\3O\3O\3O\3O\3O\6O\u041a\nO\rO\16O\u041b\3O\3O"+
		"\3O\3O\3O\3O\3O\6O\u0425\nO\rO\16O\u0426\3O\3O\3O\3O\3O\3O\3O\3P\3P\3"+
		"P\3P\3P\3P\6P\u0436\nP\rP\16P\u0437\3P\3P\3P\3P\3P\3P\3P\3P\6P\u0442\n"+
		"P\rP\16P\u0443\3P\3P\3P\3P\3P\3P\3P\3P\6P\u044e\nP\rP\16P\u044f\3P\3P"+
		"\3P\3P\6P\u0456\nP\rP\16P\u0457\3P\3P\3P\3Q\3Q\3Q\3Q\5Q\u0461\nQ\3R\3"+
		"R\3R\3R\3R\3S\3S\3S\3S\3S\3T\3T\3T\3T\3T\3U\3U\3U\3U\3U\3V\3V\3V\3V\3"+
		"V\3W\3W\3W\3W\3W\3X\3X\3X\3X\3X\5X\u0486\nX\3Y\3Y\3Z\3Z\3[\3[\3\\\3\\"+
		"\3\\\5\\\u0491\n\\\3]\3]\3^\3^\3_\3_\3`\3`\3a\3a\3b\3b\3c\3c\3d\3d\3e"+
		"\3e\3f\3f\3g\3g\3h\3h\3i\3i\3j\3j\3k\3k\3l\3l\3m\3m\3n\3n\3o\3o\3p\3p"+
		"\3q\3q\3r\3r\3s\3s\3t\3t\3u\3u\3v\3v\3w\3w\3x\3x\3y\3y\3z\3z\3{\3{\3|"+
		"\3|\3|\2\2}\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\66"+
		"8:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a"+
		"\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2"+
		"\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\u00b8\u00ba"+
		"\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc\u00ce\u00d0\u00d2"+
		"\u00d4\u00d6\u00d8\u00da\u00dc\u00de\u00e0\u00e2\u00e4\u00e6\u00e8\u00ea"+
		"\u00ec\u00ee\u00f0\u00f2\u00f4\u00f6\2\t\n\2\37\37$$..HHMMRR[[ee\t\2\26"+
		"\26\66\66DDOOQQXXii\6\2\37\37HHRRee\6\2\33\35&&/\60^`\4\2$$..\7\2\31\31"+
		"\'\'\61\62EEGG\5\2\32\32\\\\gg\2\u04e5\2\u0103\3\2\2\2\4\u0107\3\2\2\2"+
		"\6\u0113\3\2\2\2\b\u011f\3\2\2\2\n\u0123\3\2\2\2\f\u0125\3\2\2\2\16\u0127"+
		"\3\2\2\2\20\u012e\3\2\2\2\22\u0131\3\2\2\2\24\u0134\3\2\2\2\26\u0137\3"+
		"\2\2\2\30\u013a\3\2\2\2\32\u0145\3\2\2\2\34\u0147\3\2\2\2\36\u0156\3\2"+
		"\2\2 \u0158\3\2\2\2\"\u015b\3\2\2\2$\u0194\3\2\2\2&\u01c4\3\2\2\2(\u01d1"+
		"\3\2\2\2*\u01e9\3\2\2\2,\u01ef\3\2\2\2.\u01f8\3\2\2\2\60\u020a\3\2\2\2"+
		"\62\u0211\3\2\2\2\64\u0227\3\2\2\2\66\u024b\3\2\2\28\u026b\3\2\2\2:\u0278"+
		"\3\2\2\2<\u027e\3\2\2\2>\u0287\3\2\2\2@\u029d\3\2\2\2B\u02ad\3\2\2\2D"+
		"\u02af\3\2\2\2F\u02b2\3\2\2\2H\u02c1\3\2\2\2J\u02d3\3\2\2\2L\u02db\3\2"+
		"\2\2N\u02df\3\2\2\2P\u02f1\3\2\2\2R\u02f3\3\2\2\2T\u02f7\3\2\2\2V\u02fd"+
		"\3\2\2\2X\u0302\3\2\2\2Z\u0306\3\2\2\2\\\u0314\3\2\2\2^\u031e\3\2\2\2"+
		"`\u0322\3\2\2\2b\u032a\3\2\2\2d\u0332\3\2\2\2f\u0358\3\2\2\2h\u035c\3"+
		"\2\2\2j\u0368\3\2\2\2l\u036a\3\2\2\2n\u036e\3\2\2\2p\u0374\3\2\2\2r\u0379"+
		"\3\2\2\2t\u0381\3\2\2\2v\u0389\3\2\2\2x\u0392\3\2\2\2z\u039f\3\2\2\2|"+
		"\u03a1\3\2\2\2~\u03ae\3\2\2\2\u0080\u03b0\3\2\2\2\u0082\u03bd\3\2\2\2"+
		"\u0084\u03c1\3\2\2\2\u0086\u03d2\3\2\2\2\u0088\u03d8\3\2\2\2\u008a\u03da"+
		"\3\2\2\2\u008c\u03f3\3\2\2\2\u008e\u03f7\3\2\2\2\u0090\u03f9\3\2\2\2\u0092"+
		"\u03fd\3\2\2\2\u0094\u0405\3\2\2\2\u0096\u0407\3\2\2\2\u0098\u0409\3\2"+
		"\2\2\u009a\u040b\3\2\2\2\u009c\u0413\3\2\2\2\u009e\u042f\3\2\2\2\u00a0"+
		"\u0460\3\2\2\2\u00a2\u0462\3\2\2\2\u00a4\u0467\3\2\2\2\u00a6\u046c\3\2"+
		"\2\2\u00a8\u0471\3\2\2\2\u00aa\u0476\3\2\2\2\u00ac\u047b\3\2\2\2\u00ae"+
		"\u0485\3\2\2\2\u00b0\u0487\3\2\2\2\u00b2\u0489\3\2\2\2\u00b4\u048b\3\2"+
		"\2\2\u00b6\u0490\3\2\2\2\u00b8\u0492\3\2\2\2\u00ba\u0494\3\2\2\2\u00bc"+
		"\u0496\3\2\2\2\u00be\u0498\3\2\2\2\u00c0\u049a\3\2\2\2\u00c2\u049c\3\2"+
		"\2\2\u00c4\u049e\3\2\2\2\u00c6\u04a0\3\2\2\2\u00c8\u04a2\3\2\2\2\u00ca"+
		"\u04a4\3\2\2\2\u00cc\u04a6\3\2\2\2\u00ce\u04a8\3\2\2\2\u00d0\u04aa\3\2"+
		"\2\2\u00d2\u04ac\3\2\2\2\u00d4\u04ae\3\2\2\2\u00d6\u04b0\3\2\2\2\u00d8"+
		"\u04b2\3\2\2\2\u00da\u04b4\3\2\2\2\u00dc\u04b6\3\2\2\2\u00de\u04b8\3\2"+
		"\2\2\u00e0\u04ba\3\2\2\2\u00e2\u04bc\3\2\2\2\u00e4\u04be\3\2\2\2\u00e6"+
		"\u04c0\3\2\2\2\u00e8\u04c2\3\2\2\2\u00ea\u04c4\3\2\2\2\u00ec\u04c6\3\2"+
		"\2\2\u00ee\u04c8\3\2\2\2\u00f0\u04ca\3\2\2\2\u00f2\u04cc\3\2\2\2\u00f4"+
		"\u04ce\3\2\2\2\u00f6\u04d0\3\2\2\2\u00f8\u0100\5\4\3\2\u00f9\u0100\5\16"+
		"\b\2\u00fa\u0100\5\24\13\2\u00fb\u0100\5\22\n\2\u00fc\u0100\5\20\t\2\u00fd"+
		"\u0100\5\26\f\2\u00fe\u0100\5\30\r\2\u00ff\u00f8\3\2\2\2\u00ff\u00f9\3"+
		"\2\2\2\u00ff\u00fa\3\2\2\2\u00ff\u00fb\3\2\2\2\u00ff\u00fc\3\2\2\2\u00ff"+
		"\u00fd\3\2\2\2\u00ff\u00fe\3\2\2\2\u0100\u0101\3\2\2\2\u0101\u0102\7o"+
		"\2\2\u0102\u0104\3\2\2\2\u0103\u00ff\3\2\2\2\u0104\u0105\3\2\2\2\u0105"+
		"\u0103\3\2\2\2\u0105\u0106\3\2\2\2\u0106\3\3\2\2\2\u0107\u010a\7#\2\2"+
		"\u0108\u0109\7J\2\2\u0109\u010b\7S\2\2\u010a\u0108\3\2\2\2\u010a\u010b"+
		"\3\2\2\2\u010b\u0111\3\2\2\2\u010c\u0112\5\6\4\2\u010d\u0112\5\b\5\2\u010e"+
		"\u0112\5\b\5\2\u010f\u0112\5\n\6\2\u0110\u0112\5\f\7\2\u0111\u010c\3\2"+
		"\2\2\u0111\u010d\3\2\2\2\u0111\u010e\3\2\2\2\u0111\u010f\3\2\2\2\u0111"+
		"\u0110\3\2\2\2\u0112\5\3\2\2\2\u0113\u0117\7M\2\2\u0114\u0118\5\"\22\2"+
		"\u0115\u0118\5$\23\2\u0116\u0118\5&\24\2\u0117\u0114\3\2\2\2\u0117\u0115"+
		"\3\2\2\2\u0117\u0116\3\2\2\2\u0118\7\3\2\2\2\u0119\u0120\5\64\33\2\u011a"+
		"\u011d\7[\2\2\u011b\u011e\5\66\34\2\u011c\u011e\58\35\2\u011d\u011b\3"+
		"\2\2\2\u011d\u011c\3\2\2\2\u011e\u0120\3\2\2\2\u011f\u0119\3\2\2\2\u011f"+
		"\u011a\3\2\2\2\u0120\t\3\2\2\2\u0121\u0124\5\u009cO\2\u0122\u0124\5\u009e"+
		"P\2\u0123\u0121\3\2\2\2\u0123\u0122\3\2\2\2\u0124\13\3\2\2\2\u0125\u0126"+
		"\5 \21\2\u0126\r\3\2\2\2\u0127\u012c\7)\2\2\u0128\u012d\5*\26\2\u0129"+
		"\u012d\5:\36\2\u012a\u012d\5\u00a0Q\2\u012b\u012d\5 \21\2\u012c\u0128"+
		"\3\2\2\2\u012c\u0129\3\2\2\2\u012c\u012a\3\2\2\2\u012c\u012b\3\2\2\2\u012d"+
		"\17\3\2\2\2\u012e\u012f\7\67\2\2\u012f\u0130\5\62\32\2\u0130\21\3\2\2"+
		"\2\u0131\u0132\7@\2\2\u0132\u0133\5(\25\2\u0133\23\3\2\2\2\u0134\u0135"+
		"\7<\2\2\u0135\u0136\5\60\31\2\u0136\25\3\2\2\2\u0137\u0138\7W\2\2\u0138"+
		"\u0139\5\u009aN\2\u0139\27\3\2\2\2\u013a\u013b\7V\2\2\u013b\u013e\5\32"+
		"\16\2\u013c\u013d\7>\2\2\u013d\u013f\5\u009aN\2\u013e\u013c\3\2\2\2\u013e"+
		"\u013f\3\2\2\2\u013f\u0141\3\2\2\2\u0140\u0142\5\34\17\2\u0141\u0140\3"+
		"\2\2\2\u0142\u0143\3\2\2\2\u0143\u0141\3\2\2\2\u0143\u0144\3\2\2\2\u0144"+
		"\31\3\2\2\2\u0145\u0146\t\2\2\2\u0146\33\3\2\2\2\u0147\u0148\7!\2\2\u0148"+
		"\u0149\5\u00e4s\2\u0149\u0152\7>\2\2\u014a\u014f\5\36\20\2\u014b\u014c"+
		"\7\3\2\2\u014c\u014e\5\36\20\2\u014d\u014b\3\2\2\2\u014e\u0151\3\2\2\2"+
		"\u014f\u014d\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0153\3\2\2\2\u0151\u014f"+
		"\3\2\2\2\u0152\u014a\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0152\3\2\2\2\u0154"+
		"\u0155\3\2\2\2\u0155\35\3\2\2\2\u0156\u0157\t\3\2\2\u0157\37\3\2\2\2\u0158"+
		"\u0159\t\4\2\2\u0159\u015a\5\u00f6|\2\u015a!\3\2\2\2\u015b\u015c\5\u00ea"+
		"v\2\u015c\u0167\7f\2\2\u015d\u015f\7L\2\2\u015e\u0160\5D#\2\u015f\u015e"+
		"\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u015f\3\2\2\2\u0161\u0162\3\2\2\2\u0162"+
		"\u0163\3\2\2\2\u0163\u0164\7\65\2\2\u0164\u0165\7L\2\2\u0165\u0166\7o"+
		"\2\2\u0166\u0168\3\2\2\2\u0167\u015d\3\2\2\2\u0167\u0168\3\2\2\2\u0168"+
		"\u0175\3\2\2\2\u0169\u016a\7*\2\2\u016a\u016c\7\64\2\2\u016b\u016d\5F"+
		"$\2\u016c\u016b\3\2\2\2\u016d\u016e\3\2\2\2\u016e\u016c\3\2\2\2\u016e"+
		"\u016f\3\2\2\2\u016f\u0170\3\2\2\2\u0170\u0171\7\65\2\2\u0171\u0172\7"+
		"*\2\2\u0172\u0173\7\64\2\2\u0173\u0174\7o\2\2\u0174\u0176\3\2\2\2\u0175"+
		"\u0169\3\2\2\2\u0175\u0176\3\2\2\2\u0176\u0183\3\2\2\2\u0177\u0178\7F"+
		"\2\2\u0178\u017a\7%\2\2\u0179\u017b\5f\64\2\u017a\u0179\3\2\2\2\u017b"+
		"\u017c\3\2\2\2\u017c\u017a\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u017e\3\2"+
		"\2\2\u017e\u017f\7\65\2\2\u017f\u0180\7F\2\2\u0180\u0181\7%\2\2\u0181"+
		"\u0182\7o\2\2\u0182\u0184\3\2\2\2\u0183\u0177\3\2\2\2\u0183\u0184\3\2"+
		"\2\2\u0184\u018f\3\2\2\2\u0185\u0187\7 \2\2\u0186\u0188\5L\'\2\u0187\u0186"+
		"\3\2\2\2\u0188\u0189\3\2\2\2\u0189\u0187\3\2\2\2\u0189\u018a\3\2\2\2\u018a"+
		"\u018b\3\2\2\2\u018b\u018c\7\65\2\2\u018c\u018d\7 \2\2\u018d\u018e\7o"+
		"\2\2\u018e\u0190\3\2\2\2\u018f\u0185\3\2\2\2\u018f\u0190\3\2\2\2\u0190"+
		"\u0191\3\2\2\2\u0191\u0192\7\65\2\2\u0192\u0193\7M\2\2\u0193#\3\2\2\2"+
		"\u0194\u0195\7,\2\2\u0195\u0196\7:\2\2\u0196\u0197\5\u00eav\2\u0197\u01bc"+
		"\7f\2\2\u0198\u0199\7D\2\2\u0199\u019a\7\4\2\2\u019a\u01b9\5\u00ccg\2"+
		"\u019b\u019c\7\26\2\2\u019c\u019d\7\4\2\2\u019d\u01a2\5\u00c6d\2\u019e"+
		"\u019f\7\3\2\2\u019f\u01a1\5\u00c6d\2\u01a0\u019e\3\2\2\2\u01a1\u01a4"+
		"\3\2\2\2\u01a2\u01a0\3\2\2\2\u01a2\u01a3\3\2\2\2\u01a3\u01b9\3\2\2\2\u01a4"+
		"\u01a2\3\2\2\2\u01a5\u01a6\7O\2\2\u01a6\u01a7\7\4\2\2\u01a7\u01b9\5\u00be"+
		"`\2\u01a8\u01a9\7X\2\2\u01a9\u01aa\7\4\2\2\u01aa\u01b9\5\u00c0a\2\u01ab"+
		"\u01ac\7\66\2\2\u01ac\u01ad\7\4\2\2\u01ad\u01b9\5\u00c2b\2\u01ae\u01af"+
		"\7Q\2\2\u01af\u01b0\7\4\2\2\u01b0\u01b5\5\u00e6t\2\u01b1\u01b2\7\3\2\2"+
		"\u01b2\u01b4\5\u00e6t\2\u01b3\u01b1\3\2\2\2\u01b4\u01b7\3\2\2\2\u01b5"+
		"\u01b3\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6\u01b9\3\2\2\2\u01b7\u01b5\3\2"+
		"\2\2\u01b8\u0198\3\2\2\2\u01b8\u019b\3\2\2\2\u01b8\u01a5\3\2\2\2\u01b8"+
		"\u01a8\3\2\2\2\u01b8\u01ab\3\2\2\2\u01b8\u01ae\3\2\2\2\u01b9\u01ba\3\2"+
		"\2\2\u01ba\u01bb\7o\2\2\u01bb\u01bd\3\2\2\2\u01bc\u01b8\3\2\2\2\u01bd"+
		"\u01be\3\2\2\2\u01be\u01bc\3\2\2\2\u01be\u01bf\3\2\2\2\u01bf\u01c0\3\2"+
		"\2\2\u01c0\u01c1\7\65\2\2\u01c1\u01c2\7M\2\2\u01c2\u01c3\7,\2\2\u01c3"+
		"%\3\2\2\2\u01c4\u01c5\7Z\2\2\u01c5\u01c6\7:\2\2\u01c6\u01c7\5\u00eav\2"+
		"\u01c7\u01c9\7f\2\2\u01c8\u01ca\5@!\2\u01c9\u01c8\3\2\2\2\u01ca\u01cb"+
		"\3\2\2\2\u01cb\u01c9\3\2\2\2\u01cb\u01cc\3\2\2\2\u01cc\u01cd\3\2\2\2\u01cd"+
		"\u01ce\7\65\2\2\u01ce\u01cf\7M\2\2\u01cf\u01d0\7Z\2\2\u01d0\'\3\2\2\2"+
		"\u01d1\u01d2\7M\2\2\u01d2\u01d3\5\u00eav\2\u01d3\u01d4\7\27\2\2\u01d4"+
		"\u01d5\5\u00eav\2\u01d5\u01d7\7f\2\2\u01d6\u01d8\7\36\2\2\u01d7\u01d6"+
		"\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d9\u01de\5v<\2\u01da"+
		"\u01db\7\3\2\2\u01db\u01dd\5v<\2\u01dc\u01da\3\2\2\2\u01dd\u01e0\3\2\2"+
		"\2\u01de\u01dc\3\2\2\2\u01de\u01df\3\2\2\2\u01df\u01e3\3\2\2\2\u01e0\u01de"+
		"\3\2\2\2\u01e1\u01e2\7\65\2\2\u01e2\u01e4\7\36\2\2\u01e3\u01e1\3\2\2\2"+
		"\u01e3\u01e4\3\2\2\2\u01e4\u01e7\3\2\2\2\u01e5\u01e6\7:\2\2\u01e6\u01e8"+
		"\5\u009aN\2\u01e7\u01e5\3\2\2\2\u01e7\u01e8\3\2\2\2\u01e8)\3\2\2\2\u01e9"+
		"\u01ed\7M\2\2\u01ea\u01ee\5,\27\2\u01eb\u01ee\5.\30\2\u01ec\u01ee\5\u00ea"+
		"v\2\u01ed\u01ea\3\2\2\2\u01ed\u01eb\3\2\2\2\u01ed\u01ec\3\2\2\2\u01ee"+
		"+\3\2\2\2\u01ef\u01f0\7,\2\2\u01f0\u01f1\7:\2\2\u01f1\u01f6\5\u00eav\2"+
		"\u01f2\u01f3\7f\2\2\u01f3\u01f4\7D\2\2\u01f4\u01f5\7\4\2\2\u01f5\u01f7"+
		"\5\u00ccg\2\u01f6\u01f2\3\2\2\2\u01f6\u01f7\3\2\2\2\u01f7-\3\2\2\2\u01f8"+
		"\u01f9\7Z\2\2\u01f9\u01fa\7:\2\2\u01fa\u0208\5\u00eav\2\u01fb\u01fd\7"+
		"f\2\2\u01fc\u01fe\5B\"\2\u01fd\u01fc\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff"+
		"\u01fd\3\2\2\2\u01ff\u0200\3\2\2\2\u0200\u0205\3\2\2\2\u0201\u0202\7\3"+
		"\2\2\u0202\u0204\5B\"\2\u0203\u0201\3\2\2\2\u0204\u0207\3\2\2\2\u0205"+
		"\u0203\3\2\2\2\u0205\u0206\3\2\2\2\u0206\u0209\3\2\2\2\u0207\u0205\3\2"+
		"\2\2\u0208\u01fb\3\2\2\2\u0208\u0209\3\2\2\2\u0209/\3\2\2\2\u020a\u020b"+
		"\7M\2\2\u020b\u020c\5\u00eav\2\u020c\u020d\7\27\2\2\u020d\u020e\5\u00ea"+
		"v\2\u020e\u020f\7:\2\2\u020f\u0210\5\u00f2z\2\u0210\61\3\2\2\2\u0211\u0212"+
		"\7M\2\2\u0212\u0213\5\u00eav\2\u0213\u0214\7:\2\2\u0214\u0215\5\u00f2"+
		"z\2\u0215\u0216\7a\2\2\u0216\u0225\5\u00f4{\2\u0217\u0218\7f\2\2\u0218"+
		"\u021a\7Z\2\2\u0219\u021b\5B\"\2\u021a\u0219\3\2\2\2\u021b\u021c\3\2\2"+
		"\2\u021c\u021a\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u0222\3\2\2\2\u021e\u021f"+
		"\7\3\2\2\u021f\u0221\5B\"\2\u0220\u021e\3\2\2\2\u0221\u0224\3\2\2\2\u0222"+
		"\u0220\3\2\2\2\u0222\u0223\3\2\2\2\u0223\u0226\3\2\2\2\u0224\u0222\3\2"+
		"\2\2\u0225\u0217\3\2\2\2\u0225\u0226\3\2\2\2\u0226\63\3\2\2\2\u0227\u0228"+
		"\5\u00b0Y\2\u0228\u0229\5\u00ecw\2\u0229\u0234\7f\2\2\u022a\u022c\7L\2"+
		"\2\u022b\u022d\5D#\2\u022c\u022b\3\2\2\2\u022d\u022e\3\2\2\2\u022e\u022c"+
		"\3\2\2\2\u022e\u022f\3\2\2\2\u022f\u0230\3\2\2\2\u0230\u0231\7\65\2\2"+
		"\u0231\u0232\7L\2\2\u0232\u0233\7o\2\2\u0233\u0235\3\2\2\2\u0234\u022a"+
		"\3\2\2\2\u0234\u0235\3\2\2\2\u0235\u0240\3\2\2\2\u0236\u0238\7 \2\2\u0237"+
		"\u0239\5L\'\2\u0238\u0237\3\2\2\2\u0239\u023a\3\2\2\2\u023a\u0238\3\2"+
		"\2\2\u023a\u023b\3\2\2\2\u023b\u023c\3\2\2\2\u023c\u023d\7\65\2\2\u023d"+
		"\u023e\7 \2\2\u023e\u023f\7o\2\2\u023f\u0241\3\2\2\2\u0240\u0236\3\2\2"+
		"\2\u0240\u0241\3\2\2\2\u0241\u0246\3\2\2\2\u0242\u0243\7T\2\2\u0243\u0244"+
		"\5\u00e0q\2\u0244\u0245\7o\2\2\u0245\u0247\3\2\2\2\u0246\u0242\3\2\2\2"+
		"\u0246\u0247\3\2\2\2\u0247\u0248\3\2\2\2\u0248\u0249\7\65\2\2\u0249\u024a"+
		"\5\u00b0Y\2\u024a\65\3\2\2\2\u024b\u024c\7,\2\2\u024c\u024d\7:\2\2\u024d"+
		"\u024e\5\u00ecw\2\u024e\u0263\7f\2\2\u024f\u0250\7D\2\2\u0250\u0251\7"+
		"\4\2\2\u0251\u0260\5\u00ccg\2\u0252\u0253\7\26\2\2\u0253\u0254\7\4\2\2"+
		"\u0254\u0259\5\u00ecw\2\u0255\u0256\7\3\2\2\u0256\u0258\5\u00ecw\2\u0257"+
		"\u0255\3\2\2\2\u0258\u025b\3\2\2\2\u0259\u0257\3\2\2\2\u0259\u025a\3\2"+
		"\2\2\u025a\u0260\3\2\2\2\u025b\u0259\3\2\2\2\u025c\u025d\7,\2\2\u025d"+
		"\u025e\7\4\2\2\u025e\u0260\5\u00ba^\2\u025f\u024f\3\2\2\2\u025f\u0252"+
		"\3\2\2\2\u025f\u025c\3\2\2\2\u0260\u0261\3\2\2\2\u0261\u0262\7o\2\2\u0262"+
		"\u0264\3\2\2\2\u0263\u025f\3\2\2\2\u0264\u0265\3\2\2\2\u0265\u0263\3\2"+
		"\2\2\u0265\u0266\3\2\2\2\u0266\u0267\3\2\2\2\u0267\u0268\7\65\2\2\u0268"+
		"\u0269\7[\2\2\u0269\u026a\7,\2\2\u026a\67\3\2\2\2\u026b\u026c\7Z\2\2\u026c"+
		"\u026d\7:\2\2\u026d\u026e\5\u00ecw\2\u026e\u0270\7f\2\2\u026f\u0271\5"+
		"@!\2\u0270\u026f\3\2\2\2\u0271\u0272\3\2\2\2\u0272\u0270\3\2\2\2\u0272"+
		"\u0273\3\2\2\2\u0273\u0274\3\2\2\2\u0274\u0275\7\65\2\2\u0275\u0276\7"+
		"[\2\2\u0276\u0277\7Z\2\2\u02779\3\2\2\2\u0278\u027c\7[\2\2\u0279\u027d"+
		"\5<\37\2\u027a\u027d\5> \2\u027b\u027d\5\u00ecw\2\u027c\u0279\3\2\2\2"+
		"\u027c\u027a\3\2\2\2\u027c\u027b\3\2\2\2\u027d;\3\2\2\2\u027e\u027f\7"+
		",\2\2\u027f\u0280\7:\2\2\u0280\u0285\5\u00ecw\2\u0281\u0282\7f\2\2\u0282"+
		"\u0283\7D\2\2\u0283\u0284\7\4\2\2\u0284\u0286\5\u00ccg\2\u0285\u0281\3"+
		"\2\2\2\u0285\u0286\3\2\2\2\u0286=\3\2\2\2\u0287\u0288\7Z\2\2\u0288\u0289"+
		"\7:\2\2\u0289\u0297\5\u00ecw\2\u028a\u028c\7f\2\2\u028b\u028d\5B\"\2\u028c"+
		"\u028b\3\2\2\2\u028d\u028e\3\2\2\2\u028e\u028c\3\2\2\2\u028e\u028f\3\2"+
		"\2\2\u028f\u0294\3\2\2\2\u0290\u0291\7\3\2\2\u0291\u0293\5B\"\2\u0292"+
		"\u0290\3\2\2\2\u0293\u0296\3\2\2\2\u0294\u0292\3\2\2\2\u0294\u0295\3\2"+
		"\2\2\u0295\u0298\3\2\2\2\u0296\u0294\3\2\2\2\u0297\u028a\3\2\2\2\u0297"+
		"\u0298\3\2\2\2\u0298?\3\2\2\2\u0299\u029e\5B\"\2\u029a\u029b\79\2\2\u029b"+
		"\u029c\7\4\2\2\u029c\u029e\5\u00bc_\2\u029d\u0299\3\2\2\2\u029d\u029a"+
		"\3\2\2\2\u029e\u029f\3\2\2\2\u029f\u02a0\7o\2\2\u02a0A\3\2\2\2\u02a1\u02a2"+
		"\7(\2\2\u02a2\u02a3\7\4\2\2\u02a3\u02ae\5\u00c8e\2\u02a4\u02a5\7d\2\2"+
		"\u02a5\u02a6\7\4\2\2\u02a6\u02ae\5\u00caf\2\u02a7\u02a8\7D\2\2\u02a8\u02a9"+
		"\7\4\2\2\u02a9\u02ae\5\u00ccg\2\u02aa\u02ab\7-\2\2\u02ab\u02ac\7\4\2\2"+
		"\u02ac\u02ae\5\u00ceh\2\u02ad\u02a1\3\2\2\2\u02ad\u02a4\3\2\2\2\u02ad"+
		"\u02a7\3\2\2\2\u02ad\u02aa\3\2\2\2\u02aeC\3\2\2\2\u02af\u02b0\5J&\2\u02b0"+
		"\u02b1\7o\2\2\u02b1E\3\2\2\2\u02b2\u02b6\5J&\2\u02b3\u02b4\5H%\2\u02b4"+
		"\u02b5\5\u008aF\2\u02b5\u02b7\3\2\2\2\u02b6\u02b3\3\2\2\2\u02b6\u02b7"+
		"\3\2\2\2\u02b7\u02b8\3\2\2\2\u02b8\u02b9\7\5\2\2\u02b9\u02ba\5\u0088E"+
		"\2\u02ba\u02bd\7\6\2\2\u02bb\u02be\5\u0088E\2\u02bc\u02be\7T\2\2\u02bd"+
		"\u02bb\3\2\2\2\u02bd\u02bc\3\2\2\2\u02be\u02bf\3\2\2\2\u02bf\u02c0\7o"+
		"\2\2\u02c0G\3\2\2\2\u02c1\u02cd\7:\2\2\u02c2\u02ce\5\u008aF\2\u02c3\u02c4"+
		"\7\7\2\2\u02c4\u02c7\5\u008aF\2\u02c5\u02c6\7\3\2\2\u02c6\u02c8\5\u008a"+
		"F\2\u02c7\u02c5\3\2\2\2\u02c8\u02c9\3\2\2\2\u02c9\u02c7\3\2\2\2\u02c9"+
		"\u02ca\3\2\2\2\u02ca\u02cb\3\2\2\2\u02cb\u02cc\7\b\2\2\u02cc\u02ce\3\2"+
		"\2\2\u02cd\u02c2\3\2\2\2\u02cd\u02c3\3\2\2\2\u02ce\u02cf\3\2\2\2\u02cf"+
		"\u02d0\7>\2\2\u02d0\u02d1\5\u008aF\2\u02d1\u02d2\7f\2\2\u02d2I\3\2\2\2"+
		"\u02d3\u02d4\5\u008aF\2\u02d4\u02d5\7\t\2\2\u02d5\u02d7\5\u00aeX\2\u02d6"+
		"\u02d8\5\u0098M\2\u02d7\u02d6\3\2\2\2\u02d7\u02d8\3\2\2\2\u02d8K\3\2\2"+
		"\2\u02d9\u02dc\5P)\2\u02da\u02dc\5N(\2\u02db\u02d9\3\2\2\2\u02db\u02da"+
		"\3\2\2\2\u02dc\u02dd\3\2\2\2\u02dd\u02de\7o\2\2\u02deM\3\2\2\2\u02df\u02e0"+
		"\5H%\2\u02e0\u02e5\5P)\2\u02e1\u02e2\7\3\2\2\u02e2\u02e4\5P)\2\u02e3\u02e1"+
		"\3\2\2\2\u02e4\u02e7\3\2\2\2\u02e5\u02e3\3\2\2\2\u02e5\u02e6\3\2\2\2\u02e6"+
		"O\3\2\2\2\u02e7\u02e5\3\2\2\2\u02e8\u02f2\5R*\2\u02e9\u02f2\5T+\2\u02ea"+
		"\u02f2\5V,\2\u02eb\u02f2\5X-\2\u02ec\u02f2\5Z.\2\u02ed\u02f2\5\\/\2\u02ee"+
		"\u02f2\5`\61\2\u02ef\u02f2\5b\62\2\u02f0\u02f2\5d\63\2\u02f1\u02e8\3\2"+
		"\2\2\u02f1\u02e9\3\2\2\2\u02f1\u02ea\3\2\2\2\u02f1\u02eb\3\2\2\2\u02f1"+
		"\u02ec\3\2\2\2\u02f1\u02ed\3\2\2\2\u02f1\u02ee\3\2\2\2\u02f1\u02ef\3\2"+
		"\2\2\u02f1\u02f0\3\2\2\2\u02f2Q\3\2\2\2\u02f3\u02f4\5\u0088E\2\u02f4\u02f5"+
		"\7\t\2\2\u02f5\u02f6\5\u00aeX\2\u02f6S\3\2\2\2\u02f7\u02f8\5\u0088E\2"+
		"\u02f8\u02f9\7\6\2\2\u02f9\u02fa\5\u0088E\2\u02fa\u02fb\7\t\2\2\u02fb"+
		"\u02fc\5\u0088E\2\u02fcU\3\2\2\2\u02fd\u02fe\5\u0088E\2\u02fe\u02ff\7"+
		"=\2\2\u02ff\u0300\5\u00b4[\2\u0300\u0301\5\u0088E\2\u0301W\3\2\2\2\u0302"+
		"\u0303\5\u0088E\2\u0303\u0304\7T\2\2\u0304\u0305\5\u0088E\2\u0305Y\3\2"+
		"\2\2\u0306\u0307\5\u0088E\2\u0307\u030f\7A\2\2\u0308\u0310\5\u0088E\2"+
		"\u0309\u030a\7\7\2\2\u030a\u030b\5\u0088E\2\u030b\u030c\7\3\2\2\u030c"+
		"\u030d\5\u0088E\2\u030d\u030e\7\b\2\2\u030e\u0310\3\2\2\2\u030f\u0308"+
		"\3\2\2\2\u030f\u0309\3\2\2\2\u0310[\3\2\2\2\u0311\u0312\5\u008aF\2\u0312"+
		"\u0313\7f\2\2\u0313\u0315\3\2\2\2\u0314\u0311\3\2\2\2\u0314\u0315\3\2"+
		"\2\2\u0315\u0316\3\2\2\2\u0316\u031b\5^\60\2\u0317\u0318\7\3\2\2\u0318"+
		"\u031a\5^\60\2\u0319\u0317\3\2\2\2\u031a\u031d\3\2\2\2\u031b\u0319\3\2"+
		"\2\2\u031b\u031c\3\2\2\2\u031c]\3\2\2\2\u031d\u031b\3\2\2\2\u031e\u031f"+
		"\5\u008aF\2\u031f\u0320\7>\2\2\u0320\u0321\5\u008aF\2\u0321_\3\2\2\2\u0322"+
		"\u0323\5\u0088E\2\u0323\u0324\7\6\2\2\u0324\u0325\5\u0088E\2\u0325\u0326"+
		"\7U\2\2\u0326\u0327\5\u0088E\2\u0327\u0328\7\6\2\2\u0328\u0329\5\u0088"+
		"E\2\u0329a\3\2\2\2\u032a\u032b\5\u0088E\2\u032b\u032c\7\6\2\2\u032c\u032d"+
		"\5\u0088E\2\u032d\u032e\7+\2\2\u032e\u032f\5\u0088E\2\u032f\u0330\7\6"+
		"\2\2\u0330\u0331\5\u0088E\2\u0331c\3\2\2\2\u0332\u0333\7[\2\2\u0333\u0334"+
		"\5\u0088E\2\u0334\u0341\7f\2\2\u0335\u0336\78\2\2\u0336\u0338\7L\2\2\u0337"+
		"\u0339\5D#\2\u0338\u0337\3\2\2\2\u0339\u033a\3\2\2\2\u033a\u0338\3\2\2"+
		"\2\u033a\u033b\3\2\2\2\u033b\u033c\3\2\2\2\u033c\u033d\7\65\2\2\u033d"+
		"\u033e\78\2\2\u033e\u033f\7L\2\2\u033f\u0340\7o\2\2\u0340\u0342\3\2\2"+
		"\2\u0341\u0335\3\2\2\2\u0341\u0342\3\2\2\2\u0342\u0351\3\2\2\2\u0343\u0344"+
		"\78\2\2\u0344\u0348\7 \2\2\u0345\u0346\5P)\2\u0346\u0347\7o\2\2\u0347"+
		"\u0349\3\2\2\2\u0348\u0345\3\2\2\2\u0349\u034a\3\2\2\2\u034a\u0348\3\2"+
		"\2\2\u034a\u034b\3\2\2\2\u034b\u034c\3\2\2\2\u034c\u034d\7\65\2\2\u034d"+
		"\u034e\78\2\2\u034e\u034f\7 \2\2\u034f\u0350\7o\2\2\u0350\u0352\3\2\2"+
		"\2\u0351\u0343\3\2\2\2\u0351\u0352\3\2\2\2\u0352\u0353\3\2\2\2\u0353\u0354"+
		"\7\65\2\2\u0354\u0355\7[\2\2\u0355e\3\2\2\2\u0356\u0359\5j\66\2\u0357"+
		"\u0359\5h\65\2\u0358\u0356\3\2\2\2\u0358\u0357\3\2\2\2\u0359\u035a\3\2"+
		"\2\2\u035a\u035b\7o\2\2\u035bg\3\2\2\2\u035c\u035d\5H%\2\u035d\u0362\5"+
		"j\66\2\u035e\u035f\7\3\2\2\u035f\u0361\5j\66\2\u0360\u035e\3\2\2\2\u0361"+
		"\u0364\3\2\2\2\u0362\u0360\3\2\2\2\u0362\u0363\3\2\2\2\u0363i\3\2\2\2"+
		"\u0364\u0362\3\2\2\2\u0365\u0369\5l\67\2\u0366\u0369\5n8\2\u0367\u0369"+
		"\5p9\2\u0368\u0365\3\2\2\2\u0368\u0366\3\2\2\2\u0368\u0367\3\2\2\2\u0369"+
		"k\3\2\2\2\u036a\u036b\5\u0096L\2\u036b\u036c\7\t\2\2\u036c\u036d\5\u00ae"+
		"X\2\u036dm\3\2\2\2\u036e\u036f\5\u0088E\2\u036f\u0370\7\6\2\2\u0370\u0371"+
		"\5\u0088E\2\u0371\u0372\7\t\2\2\u0372\u0373\5\u0088E\2\u0373o\3\2\2\2"+
		"\u0374\u0375\5\u0088E\2\u0375\u0376\7=\2\2\u0376\u0377\5\u00b4[\2\u0377"+
		"\u0378\5\u0088E\2\u0378q\3\2\2\2\u0379\u037a\5\u0088E\2\u037a\u037b\7"+
		"\6\2\2\u037b\u037c\5\u0088E\2\u037c\u037d\7U\2\2\u037d\u037e\5\u0088E"+
		"\2\u037e\u037f\7\6\2\2\u037f\u0380\5\u0088E\2\u0380s\3\2\2\2\u0381\u0382"+
		"\5\u0088E\2\u0382\u0383\7\6\2\2\u0383\u0384\5\u0088E\2\u0384\u0385\7+"+
		"\2\2\u0385\u0386\5\u0088E\2\u0386\u0387\7\6\2\2\u0387\u0388\5\u0088E\2"+
		"\u0388u\3\2\2\2\u0389\u038b\5\u008aF\2\u038a\u038c\7\n\2\2\u038b\u038a"+
		"\3\2\2\2\u038b\u038c\3\2\2\2\u038c\u038d\3\2\2\2\u038d\u038e\7\4\2\2\u038e"+
		"\u038f\5x=\2\u038fw\3\2\2\2\u0390\u0393\5z>\2\u0391\u0393\5|?\2\u0392"+
		"\u0390\3\2\2\2\u0392\u0391\3\2\2\2\u0393y\3\2\2\2\u0394\u03a0\5\u0096"+
		"L\2\u0395\u0396\7\7\2\2\u0396\u0399\5~@\2\u0397\u0398\7\3\2\2\u0398\u039a"+
		"\5~@\2\u0399\u0397\3\2\2\2\u039a\u039b\3\2\2\2\u039b\u0399\3\2\2\2\u039b"+
		"\u039c\3\2\2\2\u039c\u039d\3\2\2\2\u039d\u039e\7\b\2\2\u039e\u03a0\3\2"+
		"\2\2\u039f\u0394\3\2\2\2\u039f\u0395\3\2\2\2\u03a0{\3\2\2\2\u03a1\u03a2"+
		"\7\13\2\2\u03a2\u03a7\5z>\2\u03a3\u03a4\7\3\2\2\u03a4\u03a6\5z>\2\u03a5"+
		"\u03a3\3\2\2\2\u03a6\u03a9\3\2\2\2\u03a7\u03a5\3\2\2\2\u03a7\u03a8\3\2"+
		"\2\2\u03a8\u03aa\3\2\2\2\u03a9\u03a7\3\2\2\2\u03aa\u03ab\7\f\2\2\u03ab"+
		"}\3\2\2\2\u03ac\u03af\5\u0096L\2\u03ad\u03af\5\u0080A\2\u03ae\u03ac\3"+
		"\2\2\2\u03ae\u03ad\3\2\2\2\u03af\177\3\2\2\2\u03b0\u03b1\7\r\2\2\u03b1"+
		"\u03b2\5\u00c4c\2\u03b2\u03b7\7\16\2\2\u03b3\u03b5\7\17\2\2\u03b4\u03b6"+
		"\5\u0086D\2\u03b5\u03b4\3\2\2\2\u03b5\u03b6\3\2\2\2\u03b6\u03b8\3\2\2"+
		"\2\u03b7\u03b3\3\2\2\2\u03b7\u03b8\3\2\2\2\u03b8\u03bb\3\2\2\2\u03b9\u03bc"+
		"\5\u0082B\2\u03ba\u03bc\5\u0084C\2\u03bb\u03b9\3\2\2\2\u03bb\u03ba\3\2"+
		"\2\2\u03bb\u03bc\3\2\2\2\u03bc\u0081\3\2\2\2\u03bd\u03be\7\20\2\2\u03be"+
		"\u03bf\5\u00e2r\2\u03bf\u03c0\7\21\2\2\u03c0\u0083\3\2\2\2\u03c1\u03c2"+
		"\7\22\2\2\u03c2\u03ce\7\20\2\2\u03c3\u03cf\5\u00dco\2\u03c4\u03c5\7\7"+
		"\2\2\u03c5\u03c8\5\u00dco\2\u03c6\u03c7\7\3\2\2\u03c7\u03c9\5\u00dco\2"+
		"\u03c8\u03c6\3\2\2\2\u03c9\u03ca\3\2\2\2\u03ca\u03c8\3\2\2\2\u03ca\u03cb"+
		"\3\2\2\2\u03cb\u03cc\3\2\2\2\u03cc\u03cd\7\b\2\2\u03cd\u03cf\3\2\2\2\u03ce"+
		"\u03c3\3\2\2\2\u03ce\u03c4\3\2\2\2\u03cf\u03d0\3\2\2\2\u03d0\u03d1\7\21"+
		"\2\2\u03d1\u0085\3\2\2\2\u03d2\u03d3\7\23\2\2\u03d3\u03d4\5\u00e2r\2\u03d4"+
		"\u03d5\7\21\2\2\u03d5\u0087\3\2\2\2\u03d6\u03d9\5\u0096L\2\u03d7\u03d9"+
		"\5\u008aF\2\u03d8\u03d6\3\2\2\2\u03d8\u03d7\3\2\2\2\u03d9\u0089\3\2\2"+
		"\2\u03da\u03db\7\r\2\2\u03db\u03dc\5\u00c4c\2\u03dc\u03de\7\16\2\2\u03dd"+
		"\u03df\5\u008cG\2\u03de\u03dd\3\2\2\2\u03de\u03df\3\2\2\2\u03df\u03e2"+
		"\3\2\2\2\u03e0\u03e3\5\u0090I\2\u03e1\u03e3\5\u008eH\2\u03e2\u03e0\3\2"+
		"\2\2\u03e2\u03e1\3\2\2\2\u03e2\u03e3\3\2\2\2\u03e3\u008b\3\2\2\2\u03e4"+
		"\u03e6\7\17\2\2\u03e5\u03e7\5\u0086D\2\u03e6\u03e5\3\2\2\2\u03e6\u03e7"+
		"\3\2\2\2\u03e7\u03f4\3\2\2\2\u03e8\u03f0\7\7\2\2\u03e9\u03ec\5\u0094K"+
		"\2\u03ea\u03eb\7\3\2\2\u03eb\u03ed\5\u0094K\2\u03ec\u03ea\3\2\2\2\u03ed"+
		"\u03ee\3\2\2\2\u03ee\u03ec\3\2\2\2\u03ee\u03ef\3\2\2\2\u03ef\u03f1\3\2"+
		"\2\2\u03f0\u03e9\3\2\2\2\u03f0\u03f1\3\2\2\2\u03f1\u03f2\3\2\2\2\u03f2"+
		"\u03f4\7\b\2\2\u03f3\u03e4\3\2\2\2\u03f3\u03e8\3\2\2\2\u03f4\u008d\3\2"+
		"\2\2\u03f5\u03f8\7\24\2\2\u03f6\u03f8\5\u0082B\2\u03f7\u03f5\3\2\2\2\u03f7"+
		"\u03f6\3\2\2\2\u03f8\u008f\3\2\2\2\u03f9\u03fb\7\22\2\2\u03fa\u03fc\5"+
		"\u0092J\2\u03fb\u03fa\3\2\2\2\u03fb\u03fc\3\2\2\2\u03fc\u0091\3\2\2\2"+
		"\u03fd\u03fe\7\20\2\2\u03fe\u03ff\5\u0094K\2\u03ff\u0400\7\21\2\2\u0400"+
		"\u0093\3\2\2\2\u0401\u0406\5\u00dco\2\u0402\u0406\5\u00e2r\2\u0403\u0406"+
		"\5z>\2\u0404\u0406\5\u008aF\2\u0405\u0401\3\2\2\2\u0405\u0402\3\2\2\2"+
		"\u0405\u0403\3\2\2\2\u0405\u0404\3\2\2\2\u0406\u0095\3\2\2\2\u0407\u0408"+
		"\5\u00d0i\2\u0408\u0097\3\2\2\2\u0409\u040a\7C\2\2\u040a\u0099\3\2\2\2"+
		"\u040b\u0410\5\u00dep\2\u040c\u040d\7\25\2\2\u040d\u040f\5\u00dep\2\u040e"+
		"\u040c\3\2\2\2\u040f\u0412\3\2\2\2\u0410\u040e\3\2\2\2\u0410\u0411\3\2"+
		"\2\2\u0411\u009b\3\2\2\2\u0412\u0410\3\2\2\2\u0413\u0414\7$\2\2\u0414"+
		"\u0415\5\u00eex\2\u0415\u0416\7f\2\2\u0416\u0417\7G\2\2\u0417\u0419\7"+
		"P\2\2\u0418\u041a\5\u00a2R\2\u0419\u0418\3\2\2\2\u041a\u041b\3\2\2\2\u041b"+
		"\u0419\3\2\2\2\u041b\u041c\3\2\2\2\u041c\u041d\3\2\2\2\u041d\u041e\7\65"+
		"\2\2\u041e\u041f\7G\2\2\u041f\u0420\7P\2\2\u0420\u0421\7o\2\2\u0421\u0422"+
		"\7\62\2\2\u0422\u0424\7P\2\2\u0423\u0425\5\u00a4S\2\u0424\u0423\3\2\2"+
		"\2\u0425\u0426\3\2\2\2\u0426\u0424\3\2\2\2\u0426\u0427\3\2\2\2\u0427\u0428"+
		"\3\2\2\2\u0428\u0429\7\65\2\2\u0429\u042a\7\62\2\2\u042a\u042b\7P\2\2"+
		"\u042b\u042c\7o\2\2\u042c\u042d\7\65\2\2\u042d\u042e\7$\2\2\u042e\u009d"+
		"\3\2\2\2\u042f\u0430\7.\2\2\u0430\u0431\5\u00f0y\2\u0431\u0455\7f\2\2"+
		"\u0432\u0433\7E\2\2\u0433\u0435\7P\2\2\u0434\u0436\5\u00a6T\2\u0435\u0434"+
		"\3\2\2\2\u0436\u0437\3\2\2\2\u0437\u0435\3\2\2\2\u0437\u0438\3\2\2\2\u0438"+
		"\u0439\3\2\2\2\u0439\u043a\7\65\2\2\u043a\u043b\7E\2\2\u043b\u043c\7P"+
		"\2\2\u043c\u043d\7o\2\2\u043d\u0456\3\2\2\2\u043e\u043f\7\31\2\2\u043f"+
		"\u0441\7P\2\2\u0440\u0442\5\u00a8U\2\u0441\u0440\3\2\2\2\u0442\u0443\3"+
		"\2\2\2\u0443\u0441\3\2\2\2\u0443\u0444\3\2\2\2\u0444\u0445\3\2\2\2\u0445"+
		"\u0446\7\65\2\2\u0446\u0447\7\31\2\2\u0447\u0448\7P\2\2\u0448\u0449\7"+
		"o\2\2\u0449\u0456\3\2\2\2\u044a\u044d\7 \2\2\u044b\u044e\5\u00aaV\2\u044c"+
		"\u044e\5\u00acW\2\u044d\u044b\3\2\2\2\u044d\u044c\3\2\2\2\u044e\u044f"+
		"\3\2\2\2\u044f\u044d\3\2\2\2\u044f\u0450\3\2\2\2\u0450\u0451\3\2\2\2\u0451"+
		"\u0452\7\65\2\2\u0452\u0453\7 \2\2\u0453\u0454\7o\2\2\u0454\u0456\3\2"+
		"\2\2\u0455\u0432\3\2\2\2\u0455\u043e\3\2\2\2\u0455\u044a\3\2\2\2\u0456"+
		"\u0457\3\2\2\2\u0457\u0455\3\2\2\2\u0457\u0458\3\2\2\2\u0458\u0459\3\2"+
		"\2\2\u0459\u045a\7\65\2\2\u045a\u045b\7.\2\2\u045b\u009f\3\2\2\2\u045c"+
		"\u045d\7$\2\2\u045d\u0461\5\u00eex\2\u045e\u045f\7.\2\2\u045f\u0461\5"+
		"\u00f0y\2\u0460\u045c\3\2\2\2\u0460\u045e\3\2\2\2\u0461\u00a1\3\2\2\2"+
		"\u0462\u0463\5\u00d2j\2\u0463\u0464\7\t\2\2\u0464\u0465\5\u00dan\2\u0465"+
		"\u0466\7o\2\2\u0466\u00a3\3\2\2\2\u0467\u0468\5\u00d4k\2\u0468\u0469\7"+
		"\t\2\2\u0469\u046a\5\u00e8u\2\u046a\u046b\7o\2\2\u046b\u00a5\3\2\2\2\u046c"+
		"\u046d\5\u00d6l\2\u046d\u046e\7\t\2\2\u046e\u046f\5\u00dan\2\u046f\u0470"+
		"\7o\2\2\u0470\u00a7\3\2\2\2\u0471\u0472\5\u00d8m\2\u0472\u0473\7\t\2\2"+
		"\u0473\u0474\5\u00dan\2\u0474\u0475\7o\2\2\u0475\u00a9\3\2\2\2\u0476\u0477"+
		"\5\u00d6l\2\u0477\u0478\7U\2\2\u0478\u0479\5\u00d6l\2\u0479\u047a\7o\2"+
		"\2\u047a\u00ab\3\2\2\2\u047b\u047c\5\u00d6l\2\u047c\u047d\7+\2\2\u047d"+
		"\u047e\5\u00d8m\2\u047e\u047f\7o\2\2\u047f\u00ad\3\2\2\2\u0480\u0486\5"+
		"\u00b2Z\2\u0481\u0486\5\u00b4[\2\u0482\u0486\5\u00b0Y\2\u0483\u0486\5"+
		"\u00b6\\\2\u0484\u0486\5\u00b8]\2\u0485\u0480\3\2\2\2\u0485\u0481\3\2"+
		"\2\2\u0485\u0482\3\2\2\2\u0485\u0483\3\2\2\2\u0485\u0484\3\2\2\2\u0486"+
		"\u00af\3\2\2\2\u0487\u0488\t\5\2\2\u0488\u00b1\3\2\2\2\u0489\u048a\t\6"+
		"\2\2\u048a\u00b3\3\2\2\2\u048b\u048c\t\7\2\2\u048c\u00b5\3\2\2\2\u048d"+
		"\u0491\7I\2\2\u048e\u0491\7Y\2\2\u048f\u0491\5\u00dan\2\u0490\u048d\3"+
		"\2\2\2\u0490\u048e\3\2\2\2\u0490\u048f\3\2\2\2\u0491\u00b7\3\2\2\2\u0492"+
		"\u0493\t\b\2\2\u0493\u00b9\3\2\2\2\u0494\u0495\7h\2\2\u0495\u00bb\3\2"+
		"\2\2\u0496\u0497\7h\2\2\u0497\u00bd\3\2\2\2\u0498\u0499\7h\2\2\u0499\u00bf"+
		"\3\2\2\2\u049a\u049b\7h\2\2\u049b\u00c1\3\2\2\2\u049c\u049d\7h\2\2\u049d"+
		"\u00c3\3\2\2\2\u049e\u049f\7i\2\2\u049f\u00c5\3\2\2\2\u04a0\u04a1\7h\2"+
		"\2\u04a1\u00c7\3\2\2\2\u04a2\u04a3\7h\2\2\u04a3\u00c9\3\2\2\2\u04a4\u04a5"+
		"\7h\2\2\u04a5\u00cb\3\2\2\2\u04a6\u04a7\7h\2\2\u04a7\u00cd\3\2\2\2\u04a8"+
		"\u04a9\7h\2\2\u04a9\u00cf\3\2\2\2\u04aa\u04ab\7h\2\2\u04ab\u00d1\3\2\2"+
		"\2\u04ac\u04ad\7h\2\2\u04ad\u00d3\3\2\2\2\u04ae\u04af\7h\2\2\u04af\u00d5"+
		"\3\2\2\2\u04b0\u04b1\7h\2\2\u04b1\u00d7\3\2\2\2\u04b2\u04b3\7h\2\2\u04b3"+
		"\u00d9\3\2\2\2\u04b4\u04b5\7h\2\2\u04b5\u00db\3\2\2\2\u04b6\u04b7\7h\2"+
		"\2\u04b7\u00dd\3\2\2\2\u04b8\u04b9\7h\2\2\u04b9\u00df\3\2\2\2\u04ba\u04bb"+
		"\7h\2\2\u04bb\u00e1\3\2\2\2\u04bc\u04bd\7j\2\2\u04bd\u00e3\3\2\2\2\u04be"+
		"\u04bf\7h\2\2\u04bf\u00e5\3\2\2\2\u04c0\u04c1\7h\2\2\u04c1\u00e7\3\2\2"+
		"\2\u04c2\u04c3\7h\2\2\u04c3\u00e9\3\2\2\2\u04c4\u04c5\5\u009aN\2\u04c5"+
		"\u00eb\3\2\2\2\u04c6\u04c7\5\u009aN\2\u04c7\u00ed\3\2\2\2\u04c8\u04c9"+
		"\5\u009aN\2\u04c9\u00ef\3\2\2\2\u04ca\u04cb\5\u009aN\2\u04cb\u00f1\3\2"+
		"\2\2\u04cc\u04cd\5\u009aN\2\u04cd\u00f3\3\2\2\2\u04ce\u04cf\5\u009aN\2"+
		"\u04cf\u00f5\3\2\2\2\u04d0\u04d1\5\u009aN\2\u04d1\u00f7\3\2\2\2k\u00ff"+
		"\u0105\u010a\u0111\u0117\u011d\u011f\u0123\u012c\u013e\u0143\u014f\u0154"+
		"\u0161\u0167\u016e\u0175\u017c\u0183\u0189\u018f\u01a2\u01b5\u01b8\u01be"+
		"\u01cb\u01d7\u01de\u01e3\u01e7\u01ed\u01f6\u01ff\u0205\u0208\u021c\u0222"+
		"\u0225\u022e\u0234\u023a\u0240\u0246\u0259\u025f\u0265\u0272\u027c\u0285"+
		"\u028e\u0294\u0297\u029d\u02ad\u02b6\u02bd\u02c9\u02cd\u02d7\u02db\u02e5"+
		"\u02f1\u030f\u0314\u031b\u033a\u0341\u034a\u0351\u0358\u0362\u0368\u038b"+
		"\u0392\u039b\u039f\u03a7\u03ae\u03b5\u03b7\u03bb\u03ca\u03ce\u03d8\u03de"+
		"\u03e2\u03e6\u03ee\u03f0\u03f3\u03f7\u03fb\u0405\u0410\u041b\u0426\u0437"+
		"\u0443\u044d\u044f\u0455\u0457\u0460\u0485\u0490";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}