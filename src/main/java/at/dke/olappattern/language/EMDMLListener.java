// Generated from C:/Daten/OLAPPatternRepository/src/main/resources\EMDML.g4 by ANTLR 4.8
package at.dke.olappattern.language;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link EMDMLParser}.
 */
public interface EMDMLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#emdm_stmt}.
	 * @param ctx the parse tree
	 */
	void enterEmdm_stmt(EMDMLParser.Emdm_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#emdm_stmt}.
	 * @param ctx the parse tree
	 */
	void exitEmdm_stmt(EMDMLParser.Emdm_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#c_stmt}.
	 * @param ctx the parse tree
	 */
	void enterC_stmt(EMDMLParser.C_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#c_stmt}.
	 * @param ctx the parse tree
	 */
	void exitC_stmt(EMDMLParser.C_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#cp_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCp_stmt(EMDMLParser.Cp_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#cp_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCp_stmt(EMDMLParser.Cp_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#ct_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCt_stmt(EMDMLParser.Ct_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#ct_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCt_stmt(EMDMLParser.Ct_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#cm_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCm_stmt(EMDMLParser.Cm_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#cm_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCm_stmt(EMDMLParser.Cm_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#cr_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCr_stmt(EMDMLParser.Cr_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#cr_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCr_stmt(EMDMLParser.Cr_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#d_stmt}.
	 * @param ctx the parse tree
	 */
	void enterD_stmt(EMDMLParser.D_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#d_stmt}.
	 * @param ctx the parse tree
	 */
	void exitD_stmt(EMDMLParser.D_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#x_stmt}.
	 * @param ctx the parse tree
	 */
	void enterX_stmt(EMDMLParser.X_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#x_stmt}.
	 * @param ctx the parse tree
	 */
	void exitX_stmt(EMDMLParser.X_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#i_stmt}.
	 * @param ctx the parse tree
	 */
	void enterI_stmt(EMDMLParser.I_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#i_stmt}.
	 * @param ctx the parse tree
	 */
	void exitI_stmt(EMDMLParser.I_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#g_stmt}.
	 * @param ctx the parse tree
	 */
	void enterG_stmt(EMDMLParser.G_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#g_stmt}.
	 * @param ctx the parse tree
	 */
	void exitG_stmt(EMDMLParser.G_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#s_stmt}.
	 * @param ctx the parse tree
	 */
	void enterS_stmt(EMDMLParser.S_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#s_stmt}.
	 * @param ctx the parse tree
	 */
	void exitS_stmt(EMDMLParser.S_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#f_stmt}.
	 * @param ctx the parse tree
	 */
	void enterF_stmt(EMDMLParser.F_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#f_stmt}.
	 * @param ctx the parse tree
	 */
	void exitF_stmt(EMDMLParser.F_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#s_trgt}.
	 * @param ctx the parse tree
	 */
	void enterS_trgt(EMDMLParser.S_trgtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#s_trgt}.
	 * @param ctx the parse tree
	 */
	void exitS_trgt(EMDMLParser.S_trgtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#s_exp}.
	 * @param ctx the parse tree
	 */
	void enterS_exp(EMDMLParser.S_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#s_exp}.
	 * @param ctx the parse tree
	 */
	void exitS_exp(EMDMLParser.S_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#s_sct}.
	 * @param ctx the parse tree
	 */
	void enterS_sct(EMDMLParser.S_sctContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#s_sct}.
	 * @param ctx the parse tree
	 */
	void exitS_sct(EMDMLParser.S_sctContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#r_exp}.
	 * @param ctx the parse tree
	 */
	void enterR_exp(EMDMLParser.R_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#r_exp}.
	 * @param ctx the parse tree
	 */
	void exitR_exp(EMDMLParser.R_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_def}.
	 * @param ctx the parse tree
	 */
	void enterP_def(EMDMLParser.P_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_def}.
	 * @param ctx the parse tree
	 */
	void exitP_def(EMDMLParser.P_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_descr}.
	 * @param ctx the parse tree
	 */
	void enterP_descr(EMDMLParser.P_descrContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_descr}.
	 * @param ctx the parse tree
	 */
	void exitP_descr(EMDMLParser.P_descrContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_temp}.
	 * @param ctx the parse tree
	 */
	void enterP_temp(EMDMLParser.P_tempContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_temp}.
	 * @param ctx the parse tree
	 */
	void exitP_temp(EMDMLParser.P_tempContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_inst}.
	 * @param ctx the parse tree
	 */
	void enterP_inst(EMDMLParser.P_instContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_inst}.
	 * @param ctx the parse tree
	 */
	void exitP_inst(EMDMLParser.P_instContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_del}.
	 * @param ctx the parse tree
	 */
	void enterP_del(EMDMLParser.P_delContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_del}.
	 * @param ctx the parse tree
	 */
	void exitP_del(EMDMLParser.P_delContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_del_descr}.
	 * @param ctx the parse tree
	 */
	void enterP_del_descr(EMDMLParser.P_del_descrContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_del_descr}.
	 * @param ctx the parse tree
	 */
	void exitP_del_descr(EMDMLParser.P_del_descrContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_del_temp}.
	 * @param ctx the parse tree
	 */
	void enterP_del_temp(EMDMLParser.P_del_tempContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_del_temp}.
	 * @param ctx the parse tree
	 */
	void exitP_del_temp(EMDMLParser.P_del_tempContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_grnd}.
	 * @param ctx the parse tree
	 */
	void enterP_grnd(EMDMLParser.P_grndContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_grnd}.
	 * @param ctx the parse tree
	 */
	void exitP_grnd(EMDMLParser.P_grndContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_exec}.
	 * @param ctx the parse tree
	 */
	void enterP_exec(EMDMLParser.P_execContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_exec}.
	 * @param ctx the parse tree
	 */
	void exitP_exec(EMDMLParser.P_execContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_def}.
	 * @param ctx the parse tree
	 */
	void enterT_def(EMDMLParser.T_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_def}.
	 * @param ctx the parse tree
	 */
	void exitT_def(EMDMLParser.T_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_descr}.
	 * @param ctx the parse tree
	 */
	void enterT_descr(EMDMLParser.T_descrContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_descr}.
	 * @param ctx the parse tree
	 */
	void exitT_descr(EMDMLParser.T_descrContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_temp}.
	 * @param ctx the parse tree
	 */
	void enterT_temp(EMDMLParser.T_tempContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_temp}.
	 * @param ctx the parse tree
	 */
	void exitT_temp(EMDMLParser.T_tempContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_del}.
	 * @param ctx the parse tree
	 */
	void enterT_del(EMDMLParser.T_delContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_del}.
	 * @param ctx the parse tree
	 */
	void exitT_del(EMDMLParser.T_delContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_del_descr}.
	 * @param ctx the parse tree
	 */
	void enterT_del_descr(EMDMLParser.T_del_descrContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_del_descr}.
	 * @param ctx the parse tree
	 */
	void exitT_del_descr(EMDMLParser.T_del_descrContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_del_temp}.
	 * @param ctx the parse tree
	 */
	void enterT_del_temp(EMDMLParser.T_del_tempContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_del_temp}.
	 * @param ctx the parse tree
	 */
	void exitT_del_temp(EMDMLParser.T_del_tempContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#temp_elem}.
	 * @param ctx the parse tree
	 */
	void enterTemp_elem(EMDMLParser.Temp_elemContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#temp_elem}.
	 * @param ctx the parse tree
	 */
	void exitTemp_elem(EMDMLParser.Temp_elemContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#temp_elem_meta}.
	 * @param ctx the parse tree
	 */
	void enterTemp_elem_meta(EMDMLParser.Temp_elem_metaContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#temp_elem_meta}.
	 * @param ctx the parse tree
	 */
	void exitTemp_elem_meta(EMDMLParser.Temp_elem_metaContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#param_decl}.
	 * @param ctx the parse tree
	 */
	void enterParam_decl(EMDMLParser.Param_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#param_decl}.
	 * @param ctx the parse tree
	 */
	void exitParam_decl(EMDMLParser.Param_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#derv_decl}.
	 * @param ctx the parse tree
	 */
	void enterDerv_decl(EMDMLParser.Derv_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#derv_decl}.
	 * @param ctx the parse tree
	 */
	void exitDerv_decl(EMDMLParser.Derv_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#for_exp}.
	 * @param ctx the parse tree
	 */
	void enterFor_exp(EMDMLParser.For_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#for_exp}.
	 * @param ctx the parse tree
	 */
	void exitFor_exp(EMDMLParser.For_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void enterVar_decl(EMDMLParser.Var_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void exitVar_decl(EMDMLParser.Var_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterCstr_decl(EMDMLParser.Cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitCstr_decl(EMDMLParser.Cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#mv_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterMv_cstr_decl(EMDMLParser.Mv_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#mv_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitMv_cstr_decl(EMDMLParser.Mv_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#sv_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterSv_cstr_decl(EMDMLParser.Sv_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#sv_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitSv_cstr_decl(EMDMLParser.Sv_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#type_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterType_cstr_decl(EMDMLParser.Type_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#type_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitType_cstr_decl(EMDMLParser.Type_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dom_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterDom_cstr_decl(EMDMLParser.Dom_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dom_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitDom_cstr_decl(EMDMLParser.Dom_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#prop_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterProp_cstr_decl(EMDMLParser.Prop_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#prop_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitProp_cstr_decl(EMDMLParser.Prop_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#return_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterReturn_cstr_decl(EMDMLParser.Return_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#return_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitReturn_cstr_decl(EMDMLParser.Return_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#app_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterApp_cstr_decl(EMDMLParser.App_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#app_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitApp_cstr_decl(EMDMLParser.App_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#scope_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterScope_cstr_decl(EMDMLParser.Scope_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#scope_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitScope_cstr_decl(EMDMLParser.Scope_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#scope_exp}.
	 * @param ctx the parse tree
	 */
	void enterScope_exp(EMDMLParser.Scope_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#scope_exp}.
	 * @param ctx the parse tree
	 */
	void exitScope_exp(EMDMLParser.Scope_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#rollup_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterRollup_cstr_decl(EMDMLParser.Rollup_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#rollup_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitRollup_cstr_decl(EMDMLParser.Rollup_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#descr_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterDescr_cstr_decl(EMDMLParser.Descr_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#descr_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitDescr_cstr_decl(EMDMLParser.Descr_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#term_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void enterTerm_cstr_decl(EMDMLParser.Term_cstr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#term_cstr_decl}.
	 * @param ctx the parse tree
	 */
	void exitTerm_cstr_decl(EMDMLParser.Term_cstr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#lc_decl}.
	 * @param ctx the parse tree
	 */
	void enterLc_decl(EMDMLParser.Lc_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#lc_decl}.
	 * @param ctx the parse tree
	 */
	void exitLc_decl(EMDMLParser.Lc_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#mv_lc_decl}.
	 * @param ctx the parse tree
	 */
	void enterMv_lc_decl(EMDMLParser.Mv_lc_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#mv_lc_decl}.
	 * @param ctx the parse tree
	 */
	void exitMv_lc_decl(EMDMLParser.Mv_lc_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#sv_lc_decl}.
	 * @param ctx the parse tree
	 */
	void enterSv_lc_decl(EMDMLParser.Sv_lc_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#sv_lc_decl}.
	 * @param ctx the parse tree
	 */
	void exitSv_lc_decl(EMDMLParser.Sv_lc_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#type_lc_decl}.
	 * @param ctx the parse tree
	 */
	void enterType_lc_decl(EMDMLParser.Type_lc_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#type_lc_decl}.
	 * @param ctx the parse tree
	 */
	void exitType_lc_decl(EMDMLParser.Type_lc_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dom_lc_decl}.
	 * @param ctx the parse tree
	 */
	void enterDom_lc_decl(EMDMLParser.Dom_lc_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dom_lc_decl}.
	 * @param ctx the parse tree
	 */
	void exitDom_lc_decl(EMDMLParser.Dom_lc_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#prop_lc_decl}.
	 * @param ctx the parse tree
	 */
	void enterProp_lc_decl(EMDMLParser.Prop_lc_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#prop_lc_decl}.
	 * @param ctx the parse tree
	 */
	void exitProp_lc_decl(EMDMLParser.Prop_lc_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#rollup_frag_decl}.
	 * @param ctx the parse tree
	 */
	void enterRollup_frag_decl(EMDMLParser.Rollup_frag_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#rollup_frag_decl}.
	 * @param ctx the parse tree
	 */
	void exitRollup_frag_decl(EMDMLParser.Rollup_frag_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#descr_frag_decl}.
	 * @param ctx the parse tree
	 */
	void enterDescr_frag_decl(EMDMLParser.Descr_frag_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#descr_frag_decl}.
	 * @param ctx the parse tree
	 */
	void exitDescr_frag_decl(EMDMLParser.Descr_frag_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#binding_exp}.
	 * @param ctx the parse tree
	 */
	void enterBinding_exp(EMDMLParser.Binding_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#binding_exp}.
	 * @param ctx the parse tree
	 */
	void exitBinding_exp(EMDMLParser.Binding_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#val_exp}.
	 * @param ctx the parse tree
	 */
	void enterVal_exp(EMDMLParser.Val_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#val_exp}.
	 * @param ctx the parse tree
	 */
	void exitVal_exp(EMDMLParser.Val_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#sv_exp}.
	 * @param ctx the parse tree
	 */
	void enterSv_exp(EMDMLParser.Sv_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#sv_exp}.
	 * @param ctx the parse tree
	 */
	void exitSv_exp(EMDMLParser.Sv_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#mv_exp}.
	 * @param ctx the parse tree
	 */
	void enterMv_exp(EMDMLParser.Mv_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#mv_exp}.
	 * @param ctx the parse tree
	 */
	void exitMv_exp(EMDMLParser.Mv_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#elem_acc_exp}.
	 * @param ctx the parse tree
	 */
	void enterElem_acc_exp(EMDMLParser.Elem_acc_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#elem_acc_exp}.
	 * @param ctx the parse tree
	 */
	void exitElem_acc_exp(EMDMLParser.Elem_acc_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#var_acc_exp}.
	 * @param ctx the parse tree
	 */
	void enterVar_acc_exp(EMDMLParser.Var_acc_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#var_acc_exp}.
	 * @param ctx the parse tree
	 */
	void exitVar_acc_exp(EMDMLParser.Var_acc_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#array_acc_exp}.
	 * @param ctx the parse tree
	 */
	void enterArray_acc_exp(EMDMLParser.Array_acc_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#array_acc_exp}.
	 * @param ctx the parse tree
	 */
	void exitArray_acc_exp(EMDMLParser.Array_acc_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#map_simple_acc_exp}.
	 * @param ctx the parse tree
	 */
	void enterMap_simple_acc_exp(EMDMLParser.Map_simple_acc_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#map_simple_acc_exp}.
	 * @param ctx the parse tree
	 */
	void exitMap_simple_acc_exp(EMDMLParser.Map_simple_acc_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#tuple_acc_exp}.
	 * @param ctx the parse tree
	 */
	void enterTuple_acc_exp(EMDMLParser.Tuple_acc_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#tuple_acc_exp}.
	 * @param ctx the parse tree
	 */
	void exitTuple_acc_exp(EMDMLParser.Tuple_acc_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#elem_exp}.
	 * @param ctx the parse tree
	 */
	void enterElem_exp(EMDMLParser.Elem_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#elem_exp}.
	 * @param ctx the parse tree
	 */
	void exitElem_exp(EMDMLParser.Elem_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#var_exp}.
	 * @param ctx the parse tree
	 */
	void enterVar_exp(EMDMLParser.Var_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#var_exp}.
	 * @param ctx the parse tree
	 */
	void exitVar_exp(EMDMLParser.Var_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#tuple_exp}.
	 * @param ctx the parse tree
	 */
	void enterTuple_exp(EMDMLParser.Tuple_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#tuple_exp}.
	 * @param ctx the parse tree
	 */
	void exitTuple_exp(EMDMLParser.Tuple_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#array_exp}.
	 * @param ctx the parse tree
	 */
	void enterArray_exp(EMDMLParser.Array_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#array_exp}.
	 * @param ctx the parse tree
	 */
	void exitArray_exp(EMDMLParser.Array_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#map_exp}.
	 * @param ctx the parse tree
	 */
	void enterMap_exp(EMDMLParser.Map_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#map_exp}.
	 * @param ctx the parse tree
	 */
	void exitMap_exp(EMDMLParser.Map_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#map_acc_exp}.
	 * @param ctx the parse tree
	 */
	void enterMap_acc_exp(EMDMLParser.Map_acc_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#map_acc_exp}.
	 * @param ctx the parse tree
	 */
	void exitMap_acc_exp(EMDMLParser.Map_acc_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#idx_exp}.
	 * @param ctx the parse tree
	 */
	void enterIdx_exp(EMDMLParser.Idx_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#idx_exp}.
	 * @param ctx the parse tree
	 */
	void exitIdx_exp(EMDMLParser.Idx_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#const_exp}.
	 * @param ctx the parse tree
	 */
	void enterConst_exp(EMDMLParser.Const_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#const_exp}.
	 * @param ctx the parse tree
	 */
	void exitConst_exp(EMDMLParser.Const_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#op_exp}.
	 * @param ctx the parse tree
	 */
	void enterOp_exp(EMDMLParser.Op_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#op_exp}.
	 * @param ctx the parse tree
	 */
	void exitOp_exp(EMDMLParser.Op_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#path_exp}.
	 * @param ctx the parse tree
	 */
	void enterPath_exp(EMDMLParser.Path_expContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#path_exp}.
	 * @param ctx the parse tree
	 */
	void exitPath_exp(EMDMLParser.Path_expContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#cube_def}.
	 * @param ctx the parse tree
	 */
	void enterCube_def(EMDMLParser.Cube_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#cube_def}.
	 * @param ctx the parse tree
	 */
	void exitCube_def(EMDMLParser.Cube_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dim_def}.
	 * @param ctx the parse tree
	 */
	void enterDim_def(EMDMLParser.Dim_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dim_def}.
	 * @param ctx the parse tree
	 */
	void exitDim_def(EMDMLParser.Dim_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#m_del}.
	 * @param ctx the parse tree
	 */
	void enterM_del(EMDMLParser.M_delContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#m_del}.
	 * @param ctx the parse tree
	 */
	void exitM_del(EMDMLParser.M_delContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#meas_decl}.
	 * @param ctx the parse tree
	 */
	void enterMeas_decl(EMDMLParser.Meas_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#meas_decl}.
	 * @param ctx the parse tree
	 */
	void exitMeas_decl(EMDMLParser.Meas_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dim_role_decl}.
	 * @param ctx the parse tree
	 */
	void enterDim_role_decl(EMDMLParser.Dim_role_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dim_role_decl}.
	 * @param ctx the parse tree
	 */
	void exitDim_role_decl(EMDMLParser.Dim_role_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#lvl_decl}.
	 * @param ctx the parse tree
	 */
	void enterLvl_decl(EMDMLParser.Lvl_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#lvl_decl}.
	 * @param ctx the parse tree
	 */
	void exitLvl_decl(EMDMLParser.Lvl_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#attr_decl}.
	 * @param ctx the parse tree
	 */
	void enterAttr_decl(EMDMLParser.Attr_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#attr_decl}.
	 * @param ctx the parse tree
	 */
	void exitAttr_decl(EMDMLParser.Attr_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#roll_up_rel_decl}.
	 * @param ctx the parse tree
	 */
	void enterRoll_up_rel_decl(EMDMLParser.Roll_up_rel_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#roll_up_rel_decl}.
	 * @param ctx the parse tree
	 */
	void exitRoll_up_rel_decl(EMDMLParser.Roll_up_rel_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#descr_by_rel_decl}.
	 * @param ctx the parse tree
	 */
	void enterDescr_by_rel_decl(EMDMLParser.Descr_by_rel_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#descr_by_rel_decl}.
	 * @param ctx the parse tree
	 */
	void exitDescr_by_rel_decl(EMDMLParser.Descr_by_rel_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(EMDMLParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(EMDMLParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_type}.
	 * @param ctx the parse tree
	 */
	void enterT_type(EMDMLParser.T_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_type}.
	 * @param ctx the parse tree
	 */
	void exitT_type(EMDMLParser.T_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#m_entity_type}.
	 * @param ctx the parse tree
	 */
	void enterM_entity_type(EMDMLParser.M_entity_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#m_entity_type}.
	 * @param ctx the parse tree
	 */
	void exitM_entity_type(EMDMLParser.M_entity_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#m_prop_type}.
	 * @param ctx the parse tree
	 */
	void enterM_prop_type(EMDMLParser.M_prop_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#m_prop_type}.
	 * @param ctx the parse tree
	 */
	void exitM_prop_type(EMDMLParser.M_prop_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#v_type}.
	 * @param ctx the parse tree
	 */
	void enterV_type(EMDMLParser.V_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#v_type}.
	 * @param ctx the parse tree
	 */
	void exitV_type(EMDMLParser.V_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#a_type}.
	 * @param ctx the parse tree
	 */
	void enterA_type(EMDMLParser.A_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#a_type}.
	 * @param ctx the parse tree
	 */
	void exitA_type(EMDMLParser.A_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#descr_txt}.
	 * @param ctx the parse tree
	 */
	void enterDescr_txt(EMDMLParser.Descr_txtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#descr_txt}.
	 * @param ctx the parse tree
	 */
	void exitDescr_txt(EMDMLParser.Descr_txtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#temp_txt}.
	 * @param ctx the parse tree
	 */
	void enterTemp_txt(EMDMLParser.Temp_txtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#temp_txt}.
	 * @param ctx the parse tree
	 */
	void exitTemp_txt(EMDMLParser.Temp_txtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#prob_txt}.
	 * @param ctx the parse tree
	 */
	void enterProb_txt(EMDMLParser.Prob_txtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#prob_txt}.
	 * @param ctx the parse tree
	 */
	void exitProb_txt(EMDMLParser.Prob_txtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#sol_txt}.
	 * @param ctx the parse tree
	 */
	void enterSol_txt(EMDMLParser.Sol_txtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#sol_txt}.
	 * @param ctx the parse tree
	 */
	void exitSol_txt(EMDMLParser.Sol_txtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#ex_txt}.
	 * @param ctx the parse tree
	 */
	void enterEx_txt(EMDMLParser.Ex_txtContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#ex_txt}.
	 * @param ctx the parse tree
	 */
	void exitEx_txt(EMDMLParser.Ex_txtContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#var_label}.
	 * @param ctx the parse tree
	 */
	void enterVar_label(EMDMLParser.Var_labelContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#var_label}.
	 * @param ctx the parse tree
	 */
	void exitVar_label(EMDMLParser.Var_labelContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#alias_name}.
	 * @param ctx the parse tree
	 */
	void enterAlias_name(EMDMLParser.Alias_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#alias_name}.
	 * @param ctx the parse tree
	 */
	void exitAlias_name(EMDMLParser.Alias_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#model_name}.
	 * @param ctx the parse tree
	 */
	void enterModel_name(EMDMLParser.Model_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#model_name}.
	 * @param ctx the parse tree
	 */
	void exitModel_name(EMDMLParser.Model_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#variant_name}.
	 * @param ctx the parse tree
	 */
	void enterVariant_name(EMDMLParser.Variant_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#variant_name}.
	 * @param ctx the parse tree
	 */
	void exitVariant_name(EMDMLParser.Variant_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#lang_name}.
	 * @param ctx the parse tree
	 */
	void enterLang_name(EMDMLParser.Lang_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#lang_name}.
	 * @param ctx the parse tree
	 */
	void exitLang_name(EMDMLParser.Lang_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dialect_name}.
	 * @param ctx the parse tree
	 */
	void enterDialect_name(EMDMLParser.Dialect_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dialect_name}.
	 * @param ctx the parse tree
	 */
	void exitDialect_name(EMDMLParser.Dialect_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#const_name}.
	 * @param ctx the parse tree
	 */
	void enterConst_name(EMDMLParser.Const_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#const_name}.
	 * @param ctx the parse tree
	 */
	void exitConst_name(EMDMLParser.Const_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#meas_name}.
	 * @param ctx the parse tree
	 */
	void enterMeas_name(EMDMLParser.Meas_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#meas_name}.
	 * @param ctx the parse tree
	 */
	void exitMeas_name(EMDMLParser.Meas_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dim_role_name}.
	 * @param ctx the parse tree
	 */
	void enterDim_role_name(EMDMLParser.Dim_role_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dim_role_name}.
	 * @param ctx the parse tree
	 */
	void exitDim_role_name(EMDMLParser.Dim_role_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#lvl_name}.
	 * @param ctx the parse tree
	 */
	void enterLvl_name(EMDMLParser.Lvl_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#lvl_name}.
	 * @param ctx the parse tree
	 */
	void exitLvl_name(EMDMLParser.Lvl_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#attr_name}.
	 * @param ctx the parse tree
	 */
	void enterAttr_name(EMDMLParser.Attr_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#attr_name}.
	 * @param ctx the parse tree
	 */
	void exitAttr_name(EMDMLParser.Attr_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#val_set_name}.
	 * @param ctx the parse tree
	 */
	void enterVal_set_name(EMDMLParser.Val_set_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#val_set_name}.
	 * @param ctx the parse tree
	 */
	void exitVal_set_name(EMDMLParser.Val_set_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#idx_name}.
	 * @param ctx the parse tree
	 */
	void enterIdx_name(EMDMLParser.Idx_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#idx_name}.
	 * @param ctx the parse tree
	 */
	void exitIdx_name(EMDMLParser.Idx_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#elem_name}.
	 * @param ctx the parse tree
	 */
	void enterElem_name(EMDMLParser.Elem_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#elem_name}.
	 * @param ctx the parse tree
	 */
	void exitElem_name(EMDMLParser.Elem_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#value_set_name}.
	 * @param ctx the parse tree
	 */
	void enterValue_set_name(EMDMLParser.Value_set_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#value_set_name}.
	 * @param ctx the parse tree
	 */
	void exitValue_set_name(EMDMLParser.Value_set_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#idx_no}.
	 * @param ctx the parse tree
	 */
	void enterIdx_no(EMDMLParser.Idx_noContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#idx_no}.
	 * @param ctx the parse tree
	 */
	void exitIdx_no(EMDMLParser.Idx_noContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#s_str}.
	 * @param ctx the parse tree
	 */
	void enterS_str(EMDMLParser.S_strContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#s_str}.
	 * @param ctx the parse tree
	 */
	void exitS_str(EMDMLParser.S_strContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_loc_name}.
	 * @param ctx the parse tree
	 */
	void enterP_loc_name(EMDMLParser.P_loc_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_loc_name}.
	 * @param ctx the parse tree
	 */
	void exitP_loc_name(EMDMLParser.P_loc_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dim_loc_name}.
	 * @param ctx the parse tree
	 */
	void enterDim_loc_name(EMDMLParser.Dim_loc_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dim_loc_name}.
	 * @param ctx the parse tree
	 */
	void exitDim_loc_name(EMDMLParser.Dim_loc_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#p_name}.
	 * @param ctx the parse tree
	 */
	void enterP_name(EMDMLParser.P_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#p_name}.
	 * @param ctx the parse tree
	 */
	void exitP_name(EMDMLParser.P_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#t_name}.
	 * @param ctx the parse tree
	 */
	void enterT_name(EMDMLParser.T_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#t_name}.
	 * @param ctx the parse tree
	 */
	void exitT_name(EMDMLParser.T_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#cube_name}.
	 * @param ctx the parse tree
	 */
	void enterCube_name(EMDMLParser.Cube_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#cube_name}.
	 * @param ctx the parse tree
	 */
	void exitCube_name(EMDMLParser.Cube_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#dim_name}.
	 * @param ctx the parse tree
	 */
	void enterDim_name(EMDMLParser.Dim_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#dim_name}.
	 * @param ctx the parse tree
	 */
	void exitDim_name(EMDMLParser.Dim_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#mdm_name}.
	 * @param ctx the parse tree
	 */
	void enterMdm_name(EMDMLParser.Mdm_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#mdm_name}.
	 * @param ctx the parse tree
	 */
	void exitMdm_name(EMDMLParser.Mdm_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#voc_name}.
	 * @param ctx the parse tree
	 */
	void enterVoc_name(EMDMLParser.Voc_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#voc_name}.
	 * @param ctx the parse tree
	 */
	void exitVoc_name(EMDMLParser.Voc_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link EMDMLParser#s_name}.
	 * @param ctx the parse tree
	 */
	void enterS_name(EMDMLParser.S_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link EMDMLParser#s_name}.
	 * @param ctx the parse tree
	 */
	void exitS_name(EMDMLParser.S_nameContext ctx);
}