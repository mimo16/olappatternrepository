// Generated from C:/Daten/OLAPPatternRepository/src/main/resources\EMDML.g4 by ANTLR 4.8
package at.dke.olappattern.language;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link EMDMLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface EMDMLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#emdm_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmdm_stmt(EMDMLParser.Emdm_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#c_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitC_stmt(EMDMLParser.C_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#cp_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCp_stmt(EMDMLParser.Cp_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#ct_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCt_stmt(EMDMLParser.Ct_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#cm_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCm_stmt(EMDMLParser.Cm_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#cr_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCr_stmt(EMDMLParser.Cr_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#d_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitD_stmt(EMDMLParser.D_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#x_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitX_stmt(EMDMLParser.X_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#i_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitI_stmt(EMDMLParser.I_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#g_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitG_stmt(EMDMLParser.G_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#s_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitS_stmt(EMDMLParser.S_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#f_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitF_stmt(EMDMLParser.F_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#s_trgt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitS_trgt(EMDMLParser.S_trgtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#s_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitS_exp(EMDMLParser.S_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#s_sct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitS_sct(EMDMLParser.S_sctContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#r_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitR_exp(EMDMLParser.R_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_def(EMDMLParser.P_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_descr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_descr(EMDMLParser.P_descrContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_temp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_temp(EMDMLParser.P_tempContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_inst}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_inst(EMDMLParser.P_instContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_del}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_del(EMDMLParser.P_delContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_del_descr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_del_descr(EMDMLParser.P_del_descrContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_del_temp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_del_temp(EMDMLParser.P_del_tempContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_grnd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_grnd(EMDMLParser.P_grndContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_exec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_exec(EMDMLParser.P_execContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_def(EMDMLParser.T_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_descr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_descr(EMDMLParser.T_descrContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_temp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_temp(EMDMLParser.T_tempContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_del}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_del(EMDMLParser.T_delContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_del_descr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_del_descr(EMDMLParser.T_del_descrContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_del_temp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_del_temp(EMDMLParser.T_del_tempContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#temp_elem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemp_elem(EMDMLParser.Temp_elemContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#temp_elem_meta}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemp_elem_meta(EMDMLParser.Temp_elem_metaContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#param_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam_decl(EMDMLParser.Param_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#derv_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDerv_decl(EMDMLParser.Derv_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#for_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_exp(EMDMLParser.For_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#var_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_decl(EMDMLParser.Var_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCstr_decl(EMDMLParser.Cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#mv_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMv_cstr_decl(EMDMLParser.Mv_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#sv_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSv_cstr_decl(EMDMLParser.Sv_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#type_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_cstr_decl(EMDMLParser.Type_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dom_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDom_cstr_decl(EMDMLParser.Dom_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#prop_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProp_cstr_decl(EMDMLParser.Prop_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#return_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_cstr_decl(EMDMLParser.Return_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#app_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitApp_cstr_decl(EMDMLParser.App_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#scope_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScope_cstr_decl(EMDMLParser.Scope_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#scope_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScope_exp(EMDMLParser.Scope_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#rollup_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRollup_cstr_decl(EMDMLParser.Rollup_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#descr_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescr_cstr_decl(EMDMLParser.Descr_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#term_cstr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm_cstr_decl(EMDMLParser.Term_cstr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#lc_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLc_decl(EMDMLParser.Lc_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#mv_lc_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMv_lc_decl(EMDMLParser.Mv_lc_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#sv_lc_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSv_lc_decl(EMDMLParser.Sv_lc_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#type_lc_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_lc_decl(EMDMLParser.Type_lc_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dom_lc_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDom_lc_decl(EMDMLParser.Dom_lc_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#prop_lc_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProp_lc_decl(EMDMLParser.Prop_lc_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#rollup_frag_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRollup_frag_decl(EMDMLParser.Rollup_frag_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#descr_frag_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescr_frag_decl(EMDMLParser.Descr_frag_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#binding_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinding_exp(EMDMLParser.Binding_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#val_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVal_exp(EMDMLParser.Val_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#sv_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSv_exp(EMDMLParser.Sv_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#mv_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMv_exp(EMDMLParser.Mv_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#elem_acc_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElem_acc_exp(EMDMLParser.Elem_acc_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#var_acc_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_acc_exp(EMDMLParser.Var_acc_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#array_acc_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_acc_exp(EMDMLParser.Array_acc_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#map_simple_acc_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMap_simple_acc_exp(EMDMLParser.Map_simple_acc_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#tuple_acc_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTuple_acc_exp(EMDMLParser.Tuple_acc_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#elem_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElem_exp(EMDMLParser.Elem_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#var_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_exp(EMDMLParser.Var_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#tuple_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTuple_exp(EMDMLParser.Tuple_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#array_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_exp(EMDMLParser.Array_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#map_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMap_exp(EMDMLParser.Map_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#map_acc_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMap_acc_exp(EMDMLParser.Map_acc_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#idx_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdx_exp(EMDMLParser.Idx_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#const_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst_exp(EMDMLParser.Const_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#op_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp_exp(EMDMLParser.Op_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#path_exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPath_exp(EMDMLParser.Path_expContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#cube_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCube_def(EMDMLParser.Cube_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dim_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_def(EMDMLParser.Dim_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#m_del}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitM_del(EMDMLParser.M_delContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#meas_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMeas_decl(EMDMLParser.Meas_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dim_role_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_role_decl(EMDMLParser.Dim_role_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#lvl_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLvl_decl(EMDMLParser.Lvl_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#attr_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttr_decl(EMDMLParser.Attr_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#roll_up_rel_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoll_up_rel_decl(EMDMLParser.Roll_up_rel_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#descr_by_rel_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescr_by_rel_decl(EMDMLParser.Descr_by_rel_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(EMDMLParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_type(EMDMLParser.T_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#m_entity_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitM_entity_type(EMDMLParser.M_entity_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#m_prop_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitM_prop_type(EMDMLParser.M_prop_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#v_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitV_type(EMDMLParser.V_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#a_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitA_type(EMDMLParser.A_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#descr_txt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescr_txt(EMDMLParser.Descr_txtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#temp_txt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemp_txt(EMDMLParser.Temp_txtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#prob_txt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProb_txt(EMDMLParser.Prob_txtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#sol_txt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSol_txt(EMDMLParser.Sol_txtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#ex_txt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEx_txt(EMDMLParser.Ex_txtContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#var_label}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_label(EMDMLParser.Var_labelContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#alias_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlias_name(EMDMLParser.Alias_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#model_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModel_name(EMDMLParser.Model_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#variant_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariant_name(EMDMLParser.Variant_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#lang_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLang_name(EMDMLParser.Lang_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dialect_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDialect_name(EMDMLParser.Dialect_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#const_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConst_name(EMDMLParser.Const_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#meas_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMeas_name(EMDMLParser.Meas_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dim_role_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_role_name(EMDMLParser.Dim_role_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#lvl_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLvl_name(EMDMLParser.Lvl_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#attr_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttr_name(EMDMLParser.Attr_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#val_set_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVal_set_name(EMDMLParser.Val_set_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#idx_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdx_name(EMDMLParser.Idx_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#elem_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElem_name(EMDMLParser.Elem_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#value_set_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_set_name(EMDMLParser.Value_set_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#idx_no}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdx_no(EMDMLParser.Idx_noContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#s_str}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitS_str(EMDMLParser.S_strContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_loc_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_loc_name(EMDMLParser.P_loc_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dim_loc_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_loc_name(EMDMLParser.Dim_loc_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#p_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitP_name(EMDMLParser.P_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#t_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitT_name(EMDMLParser.T_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#cube_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCube_name(EMDMLParser.Cube_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#dim_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDim_name(EMDMLParser.Dim_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#mdm_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMdm_name(EMDMLParser.Mdm_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#voc_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoc_name(EMDMLParser.Voc_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link EMDMLParser#s_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitS_name(EMDMLParser.S_nameContext ctx);
}