package at.dke.olappattern.macro;// Generated from C:/Daten/OLAPPatternRepository/src/main/resources\Macro.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MacroParser}.
 */
public interface MacroListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MacroParser#macrol}.
	 * @param ctx the parse tree
	 */
	void enterMacrol(MacroParser.MacrolContext ctx);
	/**
	 * Exit a parse tree produced by {@link MacroParser#macrol}.
	 * @param ctx the parse tree
	 */
	void exitMacrol(MacroParser.MacrolContext ctx);
	/**
	 * Enter a parse tree produced by {@link MacroParser#txt}.
	 * @param ctx the parse tree
	 */
	void enterTxt(MacroParser.TxtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MacroParser#txt}.
	 * @param ctx the parse tree
	 */
	void exitTxt(MacroParser.TxtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MacroParser#macro_call}.
	 * @param ctx the parse tree
	 */
	void enterMacro_call(MacroParser.Macro_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link MacroParser#macro_call}.
	 * @param ctx the parse tree
	 */
	void exitMacro_call(MacroParser.Macro_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link MacroParser#macro_name}.
	 * @param ctx the parse tree
	 */
	void enterMacro_name(MacroParser.Macro_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MacroParser#macro_name}.
	 * @param ctx the parse tree
	 */
	void exitMacro_name(MacroParser.Macro_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link MacroParser#macro_body}.
	 * @param ctx the parse tree
	 */
	void enterMacro_body(MacroParser.Macro_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link MacroParser#macro_body}.
	 * @param ctx the parse tree
	 */
	void exitMacro_body(MacroParser.Macro_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link MacroParser#macro_call_arguments}.
	 * @param ctx the parse tree
	 */
	void enterMacro_call_arguments(MacroParser.Macro_call_argumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MacroParser#macro_call_arguments}.
	 * @param ctx the parse tree
	 */
	void exitMacro_call_arguments(MacroParser.Macro_call_argumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MacroParser#macro_call_argument}.
	 * @param ctx the parse tree
	 */
	void enterMacro_call_argument(MacroParser.Macro_call_argumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link MacroParser#macro_call_argument}.
	 * @param ctx the parse tree
	 */
	void exitMacro_call_argument(MacroParser.Macro_call_argumentContext ctx);
}
