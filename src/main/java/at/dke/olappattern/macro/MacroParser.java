package at.dke.olappattern.macro;// Generated from C:/Daten/OLAPPatternRepository/src/main/resources\Macro.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MacroParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, EXPR=6, DIMKEY=7, QUOTED_NAME=8, 
		UNQUOTED_NAME=9, QS=10, QE=11, NAME=12, QUERY_TEXT=13, SPACE=14, WS=15;
	public static final int
		RULE_macrol = 0, RULE_txt = 1, RULE_macro_call = 2, RULE_macro_name = 3, 
		RULE_macro_body = 4, RULE_macro_call_arguments = 5, RULE_macro_call_argument = 6;
	private static String[] makeRuleNames() {
		return new String[] {
			"macrol", "txt", "macro_call", "macro_name", "macro_body", "macro_call_arguments", 
			"macro_call_argument"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'('", "')'", "'{'", "'}'", "','", null, null, null, null, "'*{'", 
			"'}*'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, "EXPR", "DIMKEY", "QUOTED_NAME", 
			"UNQUOTED_NAME", "QS", "QE", "NAME", "QUERY_TEXT", "SPACE", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Macro.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MacroParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class MacrolContext extends ParserRuleContext {
		public List<Macro_callContext> macro_call() {
			return getRuleContexts(Macro_callContext.class);
		}
		public Macro_callContext macro_call(int i) {
			return getRuleContext(Macro_callContext.class,i);
		}
		public List<TxtContext> txt() {
			return getRuleContexts(TxtContext.class);
		}
		public TxtContext txt(int i) {
			return getRuleContext(TxtContext.class,i);
		}
		public MacrolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_macrol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).enterMacrol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).exitMacrol(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MacroVisitor ) return ((MacroVisitor<? extends T>)visitor).visitMacrol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MacrolContext macrol() throws RecognitionException {
		MacrolContext _localctx = new MacrolContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_macrol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(18);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EXPR) | (1L << DIMKEY) | (1L << QUERY_TEXT))) != 0)) {
				{
				setState(16);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case EXPR:
				case DIMKEY:
					{
					setState(14);
					macro_call();
					}
					break;
				case QUERY_TEXT:
					{
					setState(15);
					txt();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(20);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TxtContext extends ParserRuleContext {
		public TerminalNode QUERY_TEXT() { return getToken(MacroParser.QUERY_TEXT, 0); }
		public TxtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_txt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).enterTxt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).exitTxt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MacroVisitor ) return ((MacroVisitor<? extends T>)visitor).visitTxt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TxtContext txt() throws RecognitionException {
		TxtContext _localctx = new TxtContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_txt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(21);
			match(QUERY_TEXT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Macro_callContext extends ParserRuleContext {
		public Macro_nameContext macro_name() {
			return getRuleContext(Macro_nameContext.class,0);
		}
		public Macro_call_argumentsContext macro_call_arguments() {
			return getRuleContext(Macro_call_argumentsContext.class,0);
		}
		public Macro_bodyContext macro_body() {
			return getRuleContext(Macro_bodyContext.class,0);
		}
		public Macro_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_macro_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).enterMacro_call(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).exitMacro_call(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MacroVisitor ) return ((MacroVisitor<? extends T>)visitor).visitMacro_call(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Macro_callContext macro_call() throws RecognitionException {
		Macro_callContext _localctx = new Macro_callContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_macro_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23);
			macro_name();
			setState(24);
			match(T__0);
			setState(25);
			macro_call_arguments();
			setState(26);
			match(T__1);
			setState(31);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(27);
				match(T__2);
				setState(28);
				macro_body();
				setState(29);
				match(T__3);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Macro_nameContext extends ParserRuleContext {
		public TerminalNode DIMKEY() { return getToken(MacroParser.DIMKEY, 0); }
		public TerminalNode EXPR() { return getToken(MacroParser.EXPR, 0); }
		public Macro_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_macro_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).enterMacro_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).exitMacro_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MacroVisitor ) return ((MacroVisitor<? extends T>)visitor).visitMacro_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Macro_nameContext macro_name() throws RecognitionException {
		Macro_nameContext _localctx = new Macro_nameContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_macro_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33);
			_la = _input.LA(1);
			if ( !(_la==EXPR || _la==DIMKEY) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Macro_bodyContext extends ParserRuleContext {
		public MacrolContext macrol() {
			return getRuleContext(MacrolContext.class,0);
		}
		public Macro_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_macro_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).enterMacro_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).exitMacro_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MacroVisitor ) return ((MacroVisitor<? extends T>)visitor).visitMacro_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Macro_bodyContext macro_body() throws RecognitionException {
		Macro_bodyContext _localctx = new Macro_bodyContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_macro_body);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			macrol();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Macro_call_argumentsContext extends ParserRuleContext {
		public List<Macro_call_argumentContext> macro_call_argument() {
			return getRuleContexts(Macro_call_argumentContext.class);
		}
		public Macro_call_argumentContext macro_call_argument(int i) {
			return getRuleContext(Macro_call_argumentContext.class,i);
		}
		public Macro_call_argumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_macro_call_arguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).enterMacro_call_arguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).exitMacro_call_arguments(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MacroVisitor ) return ((MacroVisitor<? extends T>)visitor).visitMacro_call_arguments(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Macro_call_argumentsContext macro_call_arguments() throws RecognitionException {
		Macro_call_argumentsContext _localctx = new Macro_call_argumentsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_macro_call_arguments);
		int _la;
		try {
			setState(46);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				enterOuterAlt(_localctx, 1);
				{
				}
				break;
			case QUOTED_NAME:
			case UNQUOTED_NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(38);
				macro_call_argument();
				setState(43);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(39);
					match(T__4);
					setState(40);
					macro_call_argument();
					}
					}
					setState(45);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Macro_call_argumentContext extends ParserRuleContext {
		public TerminalNode QUOTED_NAME() { return getToken(MacroParser.QUOTED_NAME, 0); }
		public TerminalNode UNQUOTED_NAME() { return getToken(MacroParser.UNQUOTED_NAME, 0); }
		public Macro_call_argumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_macro_call_argument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).enterMacro_call_argument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MacroListener ) ((MacroListener)listener).exitMacro_call_argument(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MacroVisitor ) return ((MacroVisitor<? extends T>)visitor).visitMacro_call_argument(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Macro_call_argumentContext macro_call_argument() throws RecognitionException {
		Macro_call_argumentContext _localctx = new Macro_call_argumentContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_macro_call_argument);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			_la = _input.LA(1);
			if ( !(_la==QUOTED_NAME || _la==UNQUOTED_NAME) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\21\65\4\2\t\2\4\3"+
		"\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\7\2\23\n\2\f\2\16"+
		"\2\26\13\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\"\n\4\3\5\3\5\3"+
		"\6\3\6\3\7\3\7\3\7\3\7\7\7,\n\7\f\7\16\7/\13\7\5\7\61\n\7\3\b\3\b\3\b"+
		"\2\2\t\2\4\6\b\n\f\16\2\4\3\2\b\t\3\2\n\13\2\62\2\24\3\2\2\2\4\27\3\2"+
		"\2\2\6\31\3\2\2\2\b#\3\2\2\2\n%\3\2\2\2\f\60\3\2\2\2\16\62\3\2\2\2\20"+
		"\23\5\6\4\2\21\23\5\4\3\2\22\20\3\2\2\2\22\21\3\2\2\2\23\26\3\2\2\2\24"+
		"\22\3\2\2\2\24\25\3\2\2\2\25\3\3\2\2\2\26\24\3\2\2\2\27\30\7\17\2\2\30"+
		"\5\3\2\2\2\31\32\5\b\5\2\32\33\7\3\2\2\33\34\5\f\7\2\34!\7\4\2\2\35\36"+
		"\7\5\2\2\36\37\5\n\6\2\37 \7\6\2\2 \"\3\2\2\2!\35\3\2\2\2!\"\3\2\2\2\""+
		"\7\3\2\2\2#$\t\2\2\2$\t\3\2\2\2%&\5\2\2\2&\13\3\2\2\2\'\61\3\2\2\2(-\5"+
		"\16\b\2)*\7\7\2\2*,\5\16\b\2+)\3\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2."+
		"\61\3\2\2\2/-\3\2\2\2\60\'\3\2\2\2\60(\3\2\2\2\61\r\3\2\2\2\62\63\t\3"+
		"\2\2\63\17\3\2\2\2\7\22\24!-\60";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
