package at.dke.olappattern.macro;// Generated from C:/Daten/OLAPPatternRepository/src/main/resources\Macro.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MacroParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MacroVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MacroParser#macrol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMacrol(MacroParser.MacrolContext ctx);
	/**
	 * Visit a parse tree produced by {@link MacroParser#txt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTxt(MacroParser.TxtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MacroParser#macro_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMacro_call(MacroParser.Macro_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link MacroParser#macro_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMacro_name(MacroParser.Macro_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link MacroParser#macro_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMacro_body(MacroParser.Macro_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link MacroParser#macro_call_arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMacro_call_arguments(MacroParser.Macro_call_argumentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MacroParser#macro_call_argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMacro_call_argument(MacroParser.Macro_call_argumentContext ctx);
}
