package at.dke.olappattern.data.localcube;

/**
 * An Assertion for pattern elements
 */
public abstract class LocalCube {
    private int id;
    private String fragmentType;

    public LocalCube(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFragmentType() {
        return fragmentType;
    }

    public void setFragmentType(String assertionType) {
        this.fragmentType = assertionType;
    }

    /**
     * Removes all object properties of the Assertions and sets them to null
     */
    public abstract void removeObjects();

    /**
     * Generates a copy of this Assertion
     * @return a new Assertion object identical to this one
     */
    public abstract LocalCube copy();
}
