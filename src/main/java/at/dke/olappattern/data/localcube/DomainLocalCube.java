package at.dke.olappattern.data.localcube;

import at.dke.olappattern.data.Target;

public class DomainLocalCube extends LocalCube {
    private Target entity = null;
    private Target property = null;
    private Target domain = null;

    public DomainLocalCube() {
        super();
        this.setFragmentType("DOMAIN_LOCAL_CUBE");
    }

    @Override
    public void removeObjects() {
        this.entity = null;
        this.property = null;
        this.domain = null;
    }

    @Override
    public DomainLocalCube copy() {
        DomainLocalCube c = new DomainLocalCube();
        c.setDomain(this.domain.copy());
        c.setEntity(this.entity.copy());
        c.setProperty(this.property.copy());
        return c;
    }

    public Target getEntity() {
        return entity;
    }

    public void setEntity(Target entity) {
        this.entity = entity;
    }

    public Target getProperty() {
        return property;
    }

    public void setProperty(Target property) {
        this.property = property;
    }

    public Target getDomain() {
        return domain;
    }

    public void setDomain(Target domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return this.entity + "." + this.property + ":" + this.domain;
    }
}
