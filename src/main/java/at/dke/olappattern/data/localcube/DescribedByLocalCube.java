package at.dke.olappattern.data.localcube;

import at.dke.olappattern.data.Target;

public class DescribedByLocalCube extends LocalCube {
    private Target dimension = null;
    private Target level = null;
    private Target attribute = null;

    public DescribedByLocalCube() {
        this.setFragmentType("DESCRIBED_BY_LOCAL_CUBE");
    }

    public DescribedByLocalCube(Target dimension){
        this();
        this.dimension = dimension;
    }

    @Override
    public void removeObjects() {
        this.dimension = null;
        this.level = null;
        this.attribute = null;
    }

    @Override
    public DescribedByLocalCube copy() {
        DescribedByLocalCube c = new DescribedByLocalCube();
        c.setAttribute(this.attribute.copy());
        c.setLevel(this.attribute.copy());
        c.setDimension(this.dimension.copy());
        return c;
    }

    public Target getDimension() {
        return dimension;
    }

    public void setDimension(Target dimension) {
        this.dimension = dimension;
    }

    public Target getLevel() {
        return level;
    }

    public void setLevel(Target level) {
        this.level = level;
    }

    public Target getAttribute() {
        return attribute;
    }

    public void setAttribute(Target attribute) {
        this.attribute = attribute;
    }

    @Override
    public String toString() {
        return this.dimension + "." + this.level + " DESCRIBED_BY " + this.dimension + "." + this.attribute;
    }
}
