package at.dke.olappattern.data.localcube;

import at.dke.olappattern.data.Target;

public class TypeLocalCube extends LocalCube {
    private Target element = null;
    private Target type = null;

    public TypeLocalCube(){
        super();
        this.setFragmentType("TYPE_LOCAL_CUBE");
    }

    @Override
    public void removeObjects() {
        this.element = null;
        this.type = null;
    }

    @Override
    public TypeLocalCube copy() {
        TypeLocalCube c = new TypeLocalCube();
        c.setType(this.type.copy());
        c.setElement(this.element.copy());
        return c;
    }

    public Target getElement() {
        return element;
    }

    public void setElement(Target element) {
        this.element = element;
    }

    public Target getType() {
        return type;
    }

    public void setType(Target type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return this.element + ":" + this.type;
    }
}
