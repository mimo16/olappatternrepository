package at.dke.olappattern.data.localcube;

import at.dke.olappattern.data.Target;

public class PropertyLocalCube extends LocalCube {
    private Target entity = null;
    private Target type = null;
    private Target property = null;

    public PropertyLocalCube() {
        super();
        this.setFragmentType("PROPERTY_LOCAL_CUBE");
    }

    @Override
    public void removeObjects() {
        this.entity = null;
        this.type = null;
        this.property = null;
    }

    @Override
    public PropertyLocalCube copy() {
        PropertyLocalCube c = new PropertyLocalCube();
        c.setType(this.type.copy());
        c.setEntity(this.entity.copy());
        c.setProperty(this.property.copy());
        return c;
    }

    public Target getEntity() {
        return entity;
    }

    public void setEntity(Target entity) {
        this.entity = entity;
    }

    public Target getType() {
        return type;
    }

    public void setType(Target type) {
        this.type = type;
    }

    public Target getProperty() {
        return property;
    }

    public void setProperty(Target property) {
        this.property = property;
    }

    @Override
    public String toString() {
        return this.entity + " HAS " + this.type + " " + this.property;
    }
}
