package at.dke.olappattern.data.localcube;

import at.dke.olappattern.data.Target;

public class RollUpLocalCube extends LocalCube {
    private Target dimension;
    private Target childLevel;
    private Target parentLevel;

    public RollUpLocalCube() {
        this.setFragmentType("ROLL_UP_LOCAL_CUBE");
    }

    @Override
    public void removeObjects() {
        this.dimension = null;
        this.childLevel = null;
        this.parentLevel = null;
    }

    @Override
    public RollUpLocalCube copy() {
        RollUpLocalCube c = new RollUpLocalCube();
        c.setChildLevel(this.childLevel.copy());
        c.setParentLevel(this.parentLevel.copy());
        c.setDimension(this.dimension.copy());
        return c;
    }

    public Target getDimension() {
        return dimension;
    }

    public void setDimension(Target dimension) {
        this.dimension = dimension;
    }

    public Target getChildLevel() {
        return childLevel;
    }

    public void setChildLevel(Target childLevel) {
        this.childLevel = childLevel;
    }

    public Target getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Target parentLevel) {
        this.parentLevel = parentLevel;
    }

    @Override
    public String toString() {
        return this.dimension + "." + this.childLevel + " ROLLS_UP_TO " + this.dimension + "." + this.parentLevel;
    }
}
