package at.dke.olappattern.data.pattern;

import at.dke.olappattern.data.BasicDescription;

import java.util.HashSet;
import java.util.Set;

/**
 * Description for an OLAP Pattern
 */
public class PatternDescription extends BasicDescription {
    private String problem = null;
    private String solution = null;
    private String example = null;
    private Set<String> relatedPatterns = null;

    public PatternDescription() {
        super();
        relatedPatterns = new HashSet<>();
    }

    /*public PatternDescription(String name){
        this();
        this.setName(name);
    }*/

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public Set<String> getRelatedPatterns() {
        return relatedPatterns;
    }

    public void setRelatedPatterns(Set<String> relatedPatterns) {
        this.relatedPatterns = relatedPatterns;
    }

    public boolean addRelatedPattern(String pattern) {
        return relatedPatterns.add(pattern);
    }
}
