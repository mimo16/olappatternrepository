package at.dke.olappattern.data.pattern;

import at.dke.olappattern.data.Context;
import at.dke.olappattern.data.Template;
import at.dke.olappattern.data.term.Type;

import java.util.HashSet;
import java.util.Set;

/**
 * A representation of an OLAP Pattern
 */
public class Pattern extends Context {
    private int id;
    private String name = null;
    private Set<PatternDescription> descriptions = null;
    private Type type;

    public Pattern(){
        super();
        this.descriptions = new HashSet<PatternDescription>();
    }

    public Pattern(String name) {
        this();
        this.name = name;
    }

    public boolean addPatternDescription(PatternDescription patternDescription) {
        return descriptions.add(patternDescription);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PatternDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Set<PatternDescription> descriptions) {
        this.descriptions = descriptions;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Method to generate a string representation for the description with the given name
     * @param @param repo the name of the repository the pattern is contained in
     * @param catalogue the name of the catalogue the pattern is contained in
     * @return The string corresponding to the description's create statement
     */
    public String descriptionStatements(String repo, String catalogue){
        String str = "";
        for(PatternDescription pd : this.getDescriptions()){
            str += "CREATE OR REPLACE PATTERN DESCRIPTION FOR " + repo + "/" + catalogue + "/" + this.name + " WITH\r\n";
            if(pd.getLanguage() != null) str += "\tLANGUAGE = " + pd.getLanguage() + ";\r\n";
            if(pd.getAliases() != null && pd.getAliases().size()>0){
                str += "\tALIAS = ";
                for(String a : pd.getAliases()){
                    str += a + ", ";
                }
                str = str.substring(0, str.length()-2);
                str += ";\r\n";
            }
            if(pd.getProblem() != null) str += "\tPROBLEM = " + pd.getProblem() + ";\r\n";
            if(pd.getSolution() != null) str += "\tSOLUTION = " + pd.getSolution() + ";\r\n";
            if(pd.getExample() != null) str += "\tEXAMPLE = " + pd.getExample() + ";\r\n";
            if(pd.getRelatedPatterns() != null && pd.getRelatedPatterns().size()>0){
                str += "\tRELATED = ";
                for(String p : pd.getRelatedPatterns()){
                    str += p + ", ";
                }
                str = str.substring(0, str.length()-2);
                str += ";\r\n";
            }

            str += "END PATTERN DESCRIPTION; \r\n \r\n";
        }

        return str;
    }

    /**
     * Method to generate a string representation for all the pattern's templates
     * @param repo the name of the repository the pattern is contained in
     * @param catalogue the name of the catalogue the pattern is contained in
     * @return The string corresponding to the template's create statement
     */
    public String templateStatements(String repo, String catalogue){
        String str = "";
        for(Template pt : this.getTemplates()){
            str += "CREATE OR REPLACE PATTERN TEMPLATE FOR " + repo + "/" + catalogue + "/" + this.name + " WITH\r\n";
            if(pt.getDataModel() != null) str += "\tDATA_MODEL = " + pt.getDataModel() + ";\r\n";
            if(pt.getVariant() != null) str += "\tVARIANT = " + pt.getVariant() + ";\r\n";
            if(pt.getLanguage() != null) str += "\tLANGUAGE = " + pt.getLanguage() + ";\r\n";
            if(pt.getDialect() != null) str += "\tDIALECT = " + pt.getDialect() + ";\r\n";
            if(pt.getExpression() != null) str += "\tEXPRESSION = " + pt.getExpression() + ";\r\n";

            str += "END PATTERN TEMPLATE;  \r\n \r\n";
        }

        return str;
    }

    /**
     * Method generating a string representation of the pattern
     * The string includes the pattern name and the lists of available descriptions and templates
     * @return the string representation
     */
    public String printPath(){
        String str = "Pattern " + this.getName() + ":\r\n";
        /*str += "------------------------------------------------------\r\n";
        if(!this.descriptions.isEmpty()){
            str += "Descriptions: \r\n";
            for(PatternDescription pd : this.descriptions){
                str += "\t" + pd.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(!this.getTemplates().isEmpty()){
            str += "Templates: \r\n";
            for(Template t : this.getTemplates()){
                str += "\t" + t.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(descriptions.isEmpty() && this.getTemplates().isEmpty()){
            str += "Empty\r\n";
            str += "------------------------------------------------------\r\n";
        }*/

        return str;
    }

    @Override
    public String toString() {
        String str = "CREATE OR REPLACE PATTERN " + this.name + " WITH\r\n";
        str += super.toString();

        str += "END PATTERN;";
        return str;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Pattern)) return false;

        final Pattern o = (Pattern)obj;

        if(this.name != null && !this.name.equals(o.getName())) return false;
        if(this.name == null && o.getName() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.name != null) result += name.hashCode();
        return result;
    }

    public String generateCreateStatement(String repository, String catalogue){
        String str = "CREATE OR REPLACE PATTERN " + repository + "/" + catalogue + "/" + this.name + " WITH\r\n";
        str += super.toString();

        str += "END PATTERN;";
        return str;
    }
}
