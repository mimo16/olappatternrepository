package at.dke.olappattern.data;

/**
 * A template providing a generic query structure for a term or pattern
 */
public class Template {
    private int id;
    private String dataModel = null;
    private String language = null;
    private String dialect = null;
    private String variant = null;
    private String expression = null;

    public Template(){    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDataModel() {
        return dataModel;
    }

    public void setDataModel(String dataModel) {
        this.dataModel = dataModel;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDialect() {
        return dialect;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    /**
     * Creates a copy of this Template and returns an identical one
     * @return The copied Template
     */
    public Template copy(){
        Template t = new Template();
        //t.setName(this.getName());
        t.setDataModel(this.getDataModel());
        t.setLanguage(this.getLanguage());
        t.setDialect(this.getDialect());
        t.setVariant(this.getVariant());
        t.setExpression(this.getExpression());
        return t;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Template)) return false;

        final Template o = (Template)obj;

        //if(this.name != null && !this.name.equals(o.getName())) return false;
        //if(this.name == null && o.getName() != null) return false;
        if(this.language != null && !this.language.equals(o.getLanguage())) return false;
        if(this.language == null && o.getLanguage() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.language != null) result += language.hashCode();
        return result;
    }
}
