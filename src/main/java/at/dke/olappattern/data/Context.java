package at.dke.olappattern.data;

import at.dke.olappattern.data.constraint.Constraint;
import at.dke.olappattern.data.constraint.TypeConstraint;
import at.dke.olappattern.data.localcube.LocalCube;
import at.dke.olappattern.data.value.SingleValue;
import at.dke.olappattern.data.value.TupleEntry;
import at.dke.olappattern.data.value.TupleValue;
import at.dke.olappattern.data.variable.*;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Abstract superclass for terms and patterns.
 * A Context is an object containing Targets and their constraints as well as templates and local cubes
 * Further, derivation rules are included allowing to derive calculated measures from the parameters.
 */
public abstract class Context {
    private Set<Constraint> constraints = null;
    private Set<DerivationRule> derivationRules = null;
    private Set<Target> targets = null;
    private Set<Template> templates = null;
    private Set<LocalCube> localCubes = null;

    public Context(){
        this.constraints = new HashSet<Constraint>();
        this.derivationRules = new HashSet<DerivationRule>();
        this.targets = new HashSet<Target>();
        this.templates = new HashSet<Template>();
        this.localCubes = new HashSet<>();
    }

    //region add methods

    public boolean addConstraint(Constraint constraint){
        return this.constraints.add(constraint);
    }

    public boolean addDerivationRule(DerivationRule derivationRule) {
        return derivationRules.add(derivationRule);
    }

    public boolean addTarget(Target target) {
        return targets.add(target);
    }

    public boolean addTemplate(Template template) {
        return templates.add(template);
    }

    public boolean addLocalCube(LocalCube localCube) { return localCubes.add(localCube); }

    //endregion

    //region getters and setters
    public Set<Constraint> getConstraints() {
        return this.constraints;
    }

    public void setConstraints(Set<Constraint> constraints) {
        this.constraints = constraints;
    }

    public Set<Target> getTargets() {
        return targets;
    }

    public void setTargets(Set<Target> targets) {
        this.targets = targets;
    }

    public Set<DerivationRule> getDerivationRules() {
        return derivationRules;
    }

    public void setDerivationRules(Set<DerivationRule> derivationRules) {
        this.derivationRules = derivationRules;
    }

    public Set<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(Set<Template> templates) {
        this.templates = templates;
    }

    public Set<LocalCube> getLocalCubes() {
        return localCubes;
    }

    public void setLocalCubes(Set<LocalCube> localCubes) {
        this.localCubes = localCubes;
    }

    //endregion

    /**
     * Method to get only the TypeConstraints
     * @return A List of all TypeConstraints
     */
    public List<TypeConstraint> getTypeConstraints(){
        List<TypeConstraint> typeConstraints = new LinkedList<TypeConstraint>();

        for(Constraint c : this.constraints){
            if(c instanceof TypeConstraint){
                TypeConstraint tc = (TypeConstraint) c;
                typeConstraints.add(tc);
            }
        }

        return typeConstraints;
    }

    /**
     * Returns all the constraints except of Typeonstraints
     * @return A List of Constraints
     */
    public List<Constraint> getNonTypeConstraints(){
        List<Constraint> constraintList = new LinkedList<Constraint>();

        for(Constraint c : this.constraints){
            if(!(c instanceof TypeConstraint)){
                constraintList.add(c);
            }
        }

        return constraintList;
    }


    /**
     * Method to get a SingleVariable with a certain name
     * @param name The name of the Variable
     * @return The SingleVariable object if it exists, otherwise null
     */
    public SingleVariable getSingleVariableByName(String name){
        for(Target t : this.targets){
            if(t instanceof SingleVariable){
                SingleVariable v = (SingleVariable)t;
                if(v.getName().equals(name)) return v;
            }

        }

        return null;
    }

    /**
     * Method to get an ArrayVariable with a certain name
     * @param name The name of the Variable
     * @return The ArrayVariable object if it exists, otherwise null
     */
    public ArrayVariable getArrayVariableByName(String name){
        for(Target t : this.targets){
            if(t instanceof ArrayVariable){
                ArrayVariable v = (ArrayVariable)t;
                if(v.getName().equals(name)) return v;
            }

        }

        return null;
    }

    /**
     * Method to get a MapVariable with a certain name
     * @param name The name of the Variable
     * @return The MapVariable object if it exists, otherwise null
     */
    public MapVariable getMapVariableByName(String name){
        for(Target t : this.targets){
            if(t instanceof MapVariable){
                MapVariable v = (MapVariable)t;
                if(v.getName().equals(name)) return v;
            }

        }

        return null;
    }

    /**
     * Method to get the Variable with the given name independent of its type
     * @param name The name of the Variable
     * @return The Variable object if it exists, otherwise null
     */
    public Variable getVariableByName(String name){
        for(Target t : this.targets){
            if(t instanceof Variable){
                Variable v = (Variable)t;
                if(v.getName().equals(name)) return v;
            }

        }

        return null;
    }

    /**
     * Method to get the parameter Variable with the given position
     * @param position The position number
     * @return The Variable object if the position is valid, otherwise null
     */
    public Variable getVariableByPosition(int position){
        for(Target t : this.targets){
            if(t instanceof Variable){
                Variable v = (Variable)t;
                if(v.getPosition() == position) return v;
            }

        }
        return null;
    }

    /**
     * Adds a SingleVariableAccess if no identical already exists, otherwise the existing is returned
     * @param singleVariableAccess The SingleVariableAccess to be added
     * @return The added or existing SingleVariableAccess
     */
    public SingleVariableAccess addSingleVariableAccessIfNotExists(SingleVariableAccess singleVariableAccess){
        for(Target t : this.targets){
            if(t instanceof SingleVariableAccess) {
                SingleVariableAccess sva = (SingleVariableAccess)t;
                if (sva.getColumnIndex() == singleVariableAccess.getColumnIndex() &&
                        sva.getVariable() == singleVariableAccess.getVariable()) {
                    return sva;
                }
            }
        }
        this.addTarget(singleVariableAccess);
        return singleVariableAccess;
    }

    /**
     * Adds a ArrayVariableAccess if no identical already exists, otherwise the existing is returned
     * @param arrayVariableAccess The ArrayVariableAccess to be added
     * @return The added or existing ArrayVariableAccess
     */
    public ArrayVariableAccess addArrayVariableAccessIfNotExists(ArrayVariableAccess arrayVariableAccess){
        for(Target t : this.targets){
            if(t instanceof ArrayVariableAccess) {
                ArrayVariableAccess sva = (ArrayVariableAccess)t;
                if (sva.getColumnIndex() == arrayVariableAccess.getColumnIndex() &&
                        sva.getVariable() == arrayVariableAccess.getVariable() &&
                        sva.getRowIndex() == arrayVariableAccess.getRowIndex()) {
                    return sva;
                }
            }
        }
        this.addTarget(arrayVariableAccess);
        return arrayVariableAccess;
    }

    /**
     * Adds a MapVariableAccess if no identical already exists, otherwise the existing is returned
     * @param mapVariableAccess The MapVariableAccess to be added
     * @return The added or existing MapVariableAccess
     */
    public MapVariableAccess addMapVariableAccessIfNotExists(MapVariableAccess mapVariableAccess){
        for(Target t : this.targets){
            if(t instanceof MapVariableAccess) {
                MapVariableAccess sva = (MapVariableAccess)t;
                if (sva.getColumnIndex() == mapVariableAccess.getColumnIndex() &&
                        sva.getVariable() == mapVariableAccess.getVariable() && sva.getRowIndex() == mapVariableAccess.getRowIndex()) {
                    return sva;
                }
            }
        }
        this.addTarget(mapVariableAccess);
        return mapVariableAccess;
    }

    @Override
    public String toString() {
        String str = "";
        List<TypeConstraint> temp = new LinkedList<>(); //List to store all Type constraints regarding instantiated variables

        List<TypeConstraint> typeConstraints = this.getTypeConstraints();
        if(!typeConstraints.isEmpty()){
            int numberOfParameters = 0;

            str+= "\tPARAMETERS \r\n";
            for(TypeConstraint t : typeConstraints){
                if(t.getElement() instanceof Variable){
                    Variable v = (Variable)t.getElement();
                    //check if v is instantiated and if so, add it to temp
                    if(v.getVariableRole().equals("PARAMETER")){
                        numberOfParameters++;

                        if(v instanceof SingleVariable && ((SingleVariable) v).getValue() != null) {
                            temp.add(t);
                            continue;
                        }
                        if(v instanceof MapVariable && !((MapVariable) v).getMapVariableEntries().isEmpty()){
                            temp.add(t);
                            continue;
                        }
                        if(v instanceof ArrayVariable && !((ArrayVariable) v).getArrayVariableEntries().isEmpty()) {
                            temp.add(t);
                            continue;
                        }
                        str += "\t \t" + t;
                        if(v.getOptional() == 'Y') str += " IS_OPTIONAL;\r\n";
                        else str += ";\r\n";
                    }
                }
            }
            str += "\tEND PARAMETERS; \r\n \r\n";

            if(temp.size() == numberOfParameters){
                str = "";
            }
        }

        if(!this.derivationRules.isEmpty()){
            str += "\tDERIVED ELEMENTS\r\n";

            for(DerivationRule dr : this.getDerivationRules()){
                for(TypeConstraint t : this.getTypeConstraints()){
                    if(t.getElement() == dr.getVariable()){
                        Variable v = (Variable)t.getElement();
                        if(v.getVariableRole().equals("DERIVED")) {
                            str += "\t \t" + t;
                            if (v.getOptional() == 'Y') str += " IS_OPTIONAL\r\n";
                            else str += "\r\n";
                            break;
                        }
                    }
                }
                str += "\t\t\t" + dr + ";\r\n";
            }

            str += "\tEND DERIVED ELEMENTS; \r\n\r\n";
        }

        if(!this.localCubes.isEmpty()){
            str += "\tLOCAL CUBES \r\n";

            for(LocalCube a : localCubes){
                str += "\t\t" + a + "; \r\n";
            }

            str += "\tEND LOCAL CUBES; \r\n \r\n";
        }

        List<Constraint> constraints = this.getNonTypeConstraints();
        if(constraints.size() > 0 || temp.size() > 0){
            str += "\tCONSTRAINTS \r\n ";

            for(Constraint c : temp){
                str += "\t\t" + c + "; \r\n";
            }

            for(Constraint c : constraints){
                str += "\t\t" + c + "; \r\n";
            }

            str += "\tEND CONSTRAINTS; \r\n";
        }

        return str;
    }

    /**
     * Adds any VariableAccess if no identical already exists or returns the existing one.
     * @param variableAccess The VariableAccess to be added
     * @return The added or existing VariableAccess
     */
    public VariableAccess addVariableAccessIfNotExists(VariableAccess variableAccess){
        if(variableAccess instanceof ArrayVariableAccess) return addArrayVariableAccessIfNotExists((ArrayVariableAccess) variableAccess);
        if(variableAccess instanceof MapVariableAccess) return addMapVariableAccessIfNotExists((MapVariableAccess) variableAccess);
        else return addSingleVariableAccessIfNotExists((SingleVariableAccess) variableAccess);
    }

    /**
     * Adds a Variable if no identical already exits, otherwise the existing is returned
     * @param v the Variable to be added
     * @return The added Variable v or the existing one
     */
    public Variable addVariableIfNotExists(Variable v){
        Variable temp = getVariableByName(v.getName());
        if(temp != null) return temp;
        else{
            this.targets.add(v);
            return v;
        }
    }

    /**
     * Adds a SingleValue if no identical already exits, otherwise the existing is returned
     * @param sv the SingleValue to be added
     * @return The added SingleValue sv or the existing one
     */
    public SingleValue addSingleValueIfNotExist(SingleValue sv){
        for(Target t : this.targets){
            if(t instanceof SingleValue){
                if(((SingleValue) t).getValue().equals(sv.getValue())){
                    return (SingleValue) t;
                }
            }
        }
        this.targets.add(sv);
        return sv;
    }

    /**
     * Adds a TupleValue if no identical already exits, otherwise the existing is returned
     * @param tv the TupleValue to be added
     * @return The added TupleValue tv or the existing one
     */
    public TupleValue addTupleValueIfNotExists(TupleValue tv){
        boolean entryExists = false;
        boolean tupleExists = false;
        for(Target t : this.targets){
            if(t instanceof TupleValue){
                tupleExists = true;
                TupleValue existingTuple = (TupleValue)t;
                for(TupleEntry te : tv.getTupleEntries()){
                    entryExists = false;
                    for(TupleEntry te2 : existingTuple.getTupleEntries()){
                        if(te.getValue() == te2.getValue()){
                            entryExists = true;
                            break;
                        }
                    }
                    if(!entryExists){
                        tupleExists = false;
                    }
                }
                if(tupleExists && tv.getTupleEntries().size() == existingTuple.getTupleEntries().size()){
                    return existingTuple;
                }
            }
        }
        for(TupleEntry te : tv.getTupleEntries()){
            te.setValue(addTargetIfNotExists(te.getValue()));
        }
        this.targets.add(tv);
        return tv;
    }

    /**
     * Adds a Target if no identical already exits, otherwise the existing is returned
     * @param t the Target to be added
     * @return The added Target t or the existing one
     */
    public Target addTargetIfNotExists(Target t){
        if(t instanceof  Variable){
            return addVariableIfNotExists((Variable) t);
        }
        else if(t instanceof VariableAccess){
            return addVariableAccessIfNotExists((VariableAccess) t);
        }
        else if(t instanceof SingleValue){
            return addSingleValueIfNotExist((SingleValue) t);
        }
        else if(t instanceof TupleValue){
            return addTupleValueIfNotExists((TupleValue) t);
        }
        //as a Target must match one of the four previous conditions, the following return is only to
        //avoid a syntax error.
        return t;
    }

    /**
     * Checks whether all SingleVariables representing parameters have a value assigned
     * @return true, if all for each variable a value is bound, otherwise false
     */
    public boolean isParameterFree() {

        boolean isParameterFree = true;

        for(Target t : targets) {

            if(t instanceof SingleVariable) {

                SingleVariable sv = (SingleVariable) t;

                if(sv.getVariableRole().equals("PARAMETER") && sv.getValue() == null) {
                    isParameterFree = false;
                    System.out.println(sv.getName());
                }
            }
        }

        return isParameterFree;
    }

    /**
     * Checks whether all SingleVariables representing parameters and derived elements have a value assigned
     * @return true, if all for each variable a value is bound, otherwise false
     */
    public boolean isGround() {

        boolean isGround = true;

        for(Target t : targets) {

            if(t instanceof SingleVariable) {

                SingleVariable sv = (SingleVariable) t;

                if(sv.getVariableRole().equals("DERIVED") && sv.getValue() == null) {
                    isGround = false;
                    break;
                }
            }
        }

        if(isParameterFree() && isGround)
            return true;
        else
            return false;
    }
}
