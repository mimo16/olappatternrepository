package at.dke.olappattern.data.repository;

import at.dke.olappattern.data.pattern.Pattern;

import java.util.HashSet;
import java.util.Set;

public class Catalogue extends OrganizationElement {
    private Set<Pattern> patterns = null;

    public Catalogue() {
        this.patterns = new HashSet<Pattern>();
    }

    public Catalogue(String name) {
        super(name);
        this.patterns = new HashSet<Pattern>();
    }

    /**
     * Method to find a pattern with the given name if it exists
     * @param name The name of the pattern
     * @return A Pattern object with the given name otherwise null
     */
    public Pattern getPatternByName(String name){
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for (Pattern p : this.patterns) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public boolean addPattern(Pattern pattern) {
        return patterns.add(pattern);
    }

    public boolean removePattern(Object o) {
        return patterns.remove(o);
    }

    public Set<Pattern> getPatterns() {
        return patterns;
    }

    public void setPatterns(Set<Pattern> patterns) {
        this.patterns = patterns;
    }

    public String generateCreateStatement(String repo){
        return "CREATE CATALOGUE " + repo + "/" + this.getName() + ";";
    }

    /**
     * Method to generate a string representation of the catalogue viewed as a path
     * @return the string representation
     */
    public String printPath(){
        String str = "Catalogue " + this.getName() + ":\r\n";
        str += "------------------------------------------------------\r\n";
        if(!this.patterns.isEmpty()){
            str += "Patterns: \r\n";
            for(Pattern pattern : this.patterns){
                str += "\t" + pattern.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        else{
            str += "Empty\r\n";
            str += "------------------------------------------------------\r\n";
        }

        return str;
    }
}
