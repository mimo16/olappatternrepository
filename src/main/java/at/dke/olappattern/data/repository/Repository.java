package at.dke.olappattern.data.repository;

import at.dke.olappattern.data.MDM.MDM;

import java.util.HashSet;
import java.util.Set;

/**
 * Root element of the organization structure of OLAP Patterns
 * Consits of a set of catalogues, a set of glossaries and a set of MDMs
 */
public class Repository extends OrganizationElement {
    private Set<Catalogue> catalogues = null;
    private Set<Glossary> glossaries = null;
    private Set<MDM> mdms = null;

    public Repository() {
    }

    public Repository(String name) {
        super(name);
        this.catalogues = new HashSet<Catalogue>();
        this.glossaries = new HashSet<Glossary>();
        this.mdms = new HashSet<MDM>();
    }

    public boolean addCatalogue(Catalogue catalogue) {
        return catalogues.add(catalogue);
    }

    public boolean addGlossary(Glossary glossary) {
        return glossaries.add(glossary);
    }

    public boolean addMDM(MDM mdm) {
        return mdms.add(mdm);
    }

    public Set<Catalogue> getCatalogues() {
        return catalogues;
    }

    public Set<Glossary> getGlossaries() {
        return glossaries;
    }

    public Set<MDM> getMdms() {
        return mdms;
    }

    public void setCatalogues(Set<Catalogue> catalogues) {
        this.catalogues = catalogues;
    }

    public void setGlossaries(Set<Glossary> glossaries) {
        this.glossaries = glossaries;
    }

    public void setMdms(Set<MDM> mdms) {
        this.mdms = mdms;
    }

    /**
     * Method to get the MDM with the given name if it exists
     * @param name The name of the MDM to be found
     * @return The MDM object or null if it does not exist
     */
    public MDM getMDMByName(String name){
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(MDM mdm : this.mdms){
            if(mdm.getName().equals(name)){
                return mdm;
            }
        }
        return null;
    }

    public Catalogue getCatalogueByName(String name){
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Catalogue c : this.catalogues){
            if(c.getName().equals(name)){
                return c;
            }
        }
        return null;
    }

    public Glossary getGlossaryByName(String name){
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Glossary g : this.glossaries){
            if(g.getName().equals(name)){
                return g;
            }
        }
        return null;
    }

    public boolean removeMDM(Object o) {
        return mdms.remove(o);
    }

    public String generateCreateStatement(){
        return "CREATE REPOSITORY " + this.getName() + ";";
    }

    public String printPath(){
        String str = "Repository " + this.getName() + ":\r\n";
        str += "------------------------------------------------------\r\n";
        if(!this.mdms.isEmpty()){
            str += "Multidimensional Models: \r\n";
            for(MDM mdm : this.mdms){
                str += "\t* " + mdm.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(!this.glossaries.isEmpty()){
            str += "Glossaries: \r\n";
            for(Glossary v : this.glossaries){
                str += "\t* " + v.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(!this.catalogues.isEmpty()){
            str += "Catalogues: \r\n";
            for(Catalogue c : this.catalogues){
                str += "\t* " + c.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(catalogues.isEmpty() && glossaries.isEmpty() && mdms.isEmpty()){
            str += "Empty\r\n";
            str += "------------------------------------------------------\r\n";
        }

        return str;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Repository)) return false;

        final Repository o = (Repository)obj;

        if(this.getName() != null && !this.getName().equals(o.getName())) return false;
        if(this.getName() == null && o.getName() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.getName() != null) result += getName().hashCode();
        return result;
    }
}
