package at.dke.olappattern.data.repository;

import at.dke.olappattern.data.term.Term;

import java.util.HashSet;
import java.util.Set;

/**
 * Organizational structure element for grouping terms
 */
public class Glossary extends OrganizationElement {
    private Set<Term> terms = null;

    public Glossary() {
        this.terms = new HashSet<Term>();
    }

    public Glossary(String name) {
        this.terms = new HashSet<Term>();
    }

    public boolean addTerm(Term term) {
        return terms.add(term);
    }

    public Set<Term> getTerms() {
        return terms;
    }

    public void setTerms(Set<Term> terms) {
        this.terms = terms;
    }

    /**
     * Method to get the term with the given name if it exists
     * @param name The name of the term to be found
     * @return The term object or null if it does not exist
     */
    public Term getTermByName(String name){
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Term t : this.terms){
            if(t.getName().equals(name)){
                return t;
            }
        }
        return null;
    }

    public boolean removeTerm(Object o) {
        return terms.remove(o);
    }

    public String generateCreateStatement(String repo){
        return "CREATE GLOSSARY " + repo + "/" + this.getName() + ";";
    }

    /**
     * Method to generate a string representation of the vocabulary viewed as a path
     * @return the string representation of the vocabulary
     */
    public String printPath(){
        String str = "Vocabulary " + this.getName() + ":\r\n";
        str += "------------------------------------------------------\r\n";
        if(!this.terms.isEmpty()){
            str += "Terms: \r\n";
            for(Term term : this.terms){
                str += "\t" + term.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        else{
            str += "Empty\r\n";
            str += "------------------------------------------------------\r\n";
        }

        return str;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Glossary)) return false;

        final Glossary o = (Glossary)obj;

        if(this.getName() != null && !this.getName().equals(o.getName())) return false;
        if(this.getName() == null && o.getName() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.getName() != null) result += getName().hashCode();
        return result;
    }
}
