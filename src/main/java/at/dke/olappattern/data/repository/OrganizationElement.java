package at.dke.olappattern.data.repository;

/**
 * Abstract superclass for all elements part of the organizational structure of OLAP patterns
 */
public abstract class OrganizationElement {
    private int id;
    private String name;

    public OrganizationElement() {
    }

    public OrganizationElement(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
