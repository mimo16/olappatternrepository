package at.dke.olappattern.data.value;

import at.dke.olappattern.data.Target;

/**
 * A entry of a map characterised by an index (also referred to as key) and the value
 */
public class MapVariableEntry {
    public Integer id = null;
    private Integer index = null;
    private Target value = null;

    public MapVariableEntry() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MapVariableEntry(Target value){
        this.value = value;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Target getValue() {
        return value;
    }

    public void setValue(Target value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof MapVariableEntry)) return false;

        final MapVariableEntry o = (MapVariableEntry)obj;

        if(!this.value.equals(o.getValue())) return false;
        if(!(this.index == o.getIndex())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = value.hashCode();
        result = 29 * result + index.hashCode();
        return result;
    }

    /**
     * Copies this MapVariableEntry and returns an identical one
     * @return The copy
     */
    public MapVariableEntry copy(){
        MapVariableEntry ave = new MapVariableEntry();
        ave.setIndex(this.index);
        ave.setValue(this.value.copy());
        return ave;
    }
}
