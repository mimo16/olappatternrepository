package at.dke.olappattern.data.value;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A value consisting of multiple columns (Tuple Entries)
 */
public class TupleValue extends Value {
    private Set<TupleEntry> tupleEntries = null;
    private int index = 1;

    public TupleValue() {
        setKind("TUPLE_VALUE");
        tupleEntries = new HashSet<>();
    }

    public boolean addTupleEntry(TupleEntry tupleEntry) {
        tupleEntry.setIndex(this.index++);
        return tupleEntries.add(tupleEntry);
    }

    public Set<TupleEntry> getTupleEntries() {
        return tupleEntries;
    }

    public void setTupleEntries(Set<TupleEntry> tupleEntries) {
        this.tupleEntries = tupleEntries;
    }

    public void removeObjects(){
        for(TupleEntry te : this.tupleEntries){
            te.setValue(null);
        }
        this.tupleEntries.clear();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof TupleValue)) return false;

        final TupleValue o = (TupleValue)obj;

        if(!this.tupleEntries.containsAll(o.getTupleEntries())) return false;
        if(!o.getTupleEntries().containsAll(this.tupleEntries)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        if(this.tupleEntries != null) return super.hashCode()*29 + this.tupleEntries.hashCode();
        return super.hashCode()*29;
    }

    @Override
    public String toString() {
        List<TupleEntry> entries = this.tupleEntries.stream().sorted().collect(Collectors.toList());

        String str = "(";
        if(entries != null && !entries.isEmpty()) {
            for (TupleEntry t : entries) {
                str += t + ", ";
            }
            str = str.substring(0, str.length() - 2);
        }
        str += ")";
        return str;
    }

    @Override
    public TupleValue copy() {
        TupleValue tv = new TupleValue();
        tv.setKind(this.getKind());
        for(TupleEntry te : this.tupleEntries){
            tv.addTupleEntry(te.copy());
        }
        return tv;
    }
}
