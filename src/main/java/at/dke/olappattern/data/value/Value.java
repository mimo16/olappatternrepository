package at.dke.olappattern.data.value;

import at.dke.olappattern.data.Target;

/**
 * An abstract superclass for all
 */
public abstract class Value extends Target {
    public Value(){}

    @Override
    public int hashCode() {
        return super.hashCode()*29;
    }

    @Override
    public abstract Value copy();

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Value)) return false;

        final Value o = (Value)obj;

        if(this.getId() != null && this.getId().equals(o.getId())) return true;
        if(this.getKind() != null && !this.getKind().equals(o.getKind())) return false;

        return true;
    }
}
