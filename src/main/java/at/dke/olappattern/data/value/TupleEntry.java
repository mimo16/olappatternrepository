package at.dke.olappattern.data.value;

import at.dke.olappattern.data.Target;

/*
An entry being part of a tuple characterised by the index within the tuple and the actual value
 */
public class TupleEntry implements Comparable<TupleEntry> {
    private int id;
    private Integer index;
    private Target value;

    public TupleEntry() {
    }

    public TupleEntry(Target value){
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Target getValue() {
        return value;
    }

    public void setValue(Target value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof TupleEntry)) return false;

        final TupleEntry tupleEntry = (TupleEntry)obj;

        if(!(this.index == tupleEntry.getIndex())) return false;
        if(!this.value.equals(tupleEntry.getValue())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(value != null) result += value.hashCode();
        if(index != null) result = result + index;
        return result;
    }

    @Override
    public String toString() {
        return value.toString();
    }


    @Override
    public int compareTo(TupleEntry o) {
        return this.index.compareTo(o.getIndex());
    }

    /**
     * Copies this TupleEntry and returns an identical one
     * @return The copy
     */
    public TupleEntry copy(){
        TupleEntry te = new TupleEntry();
        te.setIndex(this.getIndex());
        te.setValue(this.getValue().copy());
        return te;
    }
}
