package at.dke.olappattern.data.value;

/**
 * A value consisting only of one string
 */
public class SingleValue extends Value {
    private String value = null;

    public SingleValue() {
        setKind("SINGLE_VALUE");
    }

    public SingleValue(String value){
        this();
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof SingleValue)) return false;

        final SingleValue o = (SingleValue)obj;

        if(!super.equals(obj)) return false;
        if(this.value == null && o.getValue() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        if(value != null) return super.hashCode()*29 + value.hashCode();
        return super.hashCode()*29;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public SingleValue copy() {
        SingleValue sv = new SingleValue();
        sv.setKind(this.getKind());
        sv.setValue(this.value);
        return sv;
    }
}
