package at.dke.olappattern.data.value;

import at.dke.olappattern.data.Target;

/**
 * A entry of an array characterised by an index and the value
 */
public class ArrayVariableEntry {
    public Integer id;
    private Integer index;
    private Target value = null;

    public ArrayVariableEntry(int index, Target value) {
        this.index = index;
        this.value = value;
    }

    public ArrayVariableEntry() {
    }

    public ArrayVariableEntry(Target value){
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Target getValue() {
        return value;
    }

    public void setValue(Target value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof ArrayVariableEntry)) return false;

        final ArrayVariableEntry o = (ArrayVariableEntry)obj;

        if(!this.value.equals(o.getValue())) return false;
        if(!(this.index == o.getIndex())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = value.hashCode();
        result = 29 * result + index;
        return result;
    }

    /**
     * Copies this ArrayVariableEntry and returns an identical one
     * @return The copy
     */
    public ArrayVariableEntry copy(){
        ArrayVariableEntry sve = new ArrayVariableEntry();
        sve.setIndex(this.index);
        sve.setValue(this.value.copy());
        return sve;
    }
}
