package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;
import at.dke.olappattern.data.variable.ArrayVariableAccess;
import at.dke.olappattern.data.variable.SingleVariable;
import at.dke.olappattern.data.variable.Variable;

public class TypeConstraint extends Constraint {
    private Target element = null;
    private Target type = null;

    public TypeConstraint(Target element, Target type){
        super();
        this.setConstraintType("TYPE_CONSTRAINT");
        this.element = element;
        this.type = type;
    }

    public TypeConstraint(){
        super();
        this.setConstraintType("TYPE_CONSTRAINT");
    }

    @Override
    public void removeObjects() {
        this.element = null;
        this.type = null;
    }

    @Override
    public TypeConstraint copy() {
        TypeConstraint c = new TypeConstraint();
        c.setType(this.type.copy());
        c.setElement(this.element.copy());
        return c;
    }

    public Target getElement() {
        return element;
    }

    public void setElement(Target element) {
        this.element = element;
    }

    public Target getType() {
        return type;
    }

    public void setType(Target type) {
        this.type = type;
    }

    @Override
    public String toString() {
        String variable = "";
        if(this.element instanceof Variable){
            if(this.element instanceof SingleVariable && ((SingleVariable) this.element).getValue() != null){
                variable = ((SingleVariable) this.element).getValue().toString();
            }
            else {
                variable = ((Variable) element).getDefinition();
            }
        }
        else if(this.element instanceof ArrayVariableAccess) {
            variable = ((ArrayVariableAccess)element).toString();
        }
        return variable + ":" + this.type;
    }
}
