package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;

public class ScopeConstraint extends Constraint {
    private int id;
    private Target tuple;
    private Target scope;

    public ScopeConstraint(){
        super();
        this.setConstraintType("SCOPE_CONSTRAINT");
    }
    public ScopeConstraint(Target scope){
        this();
        this.scope = scope;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void removeObjects() {
        this.tuple = null;
        this.scope = null;
    }

    @Override
    public ScopeConstraint copy() {
        ScopeConstraint c = new ScopeConstraint();
        c.setScope(this.scope.copy());
        c.setTuple(this.tuple.copy());
        return c;
    }

    public Target getTuple() {
        return tuple;
    }

    public void setTuple(Target tuple) {
        this.tuple = tuple;
    }

    public Target getScope() {
        return scope;
    }

    public void setScope(Target scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return tuple + " IN " + scope;
    }
}
