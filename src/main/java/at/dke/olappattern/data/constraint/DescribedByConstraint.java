package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;

public class DescribedByConstraint extends Constraint {
    private Target dimension = null;
    private Target level = null;
    private Target attribute = null;

    public DescribedByConstraint() {
        this.setConstraintType("DESCRIBED_BY_CONSTRAINT");
    }

    public DescribedByConstraint(Target dimension){
        this();
        this.dimension = dimension;
    }

    @Override
    public void removeObjects() {
        this.dimension = null;
        this.level = null;
        this.attribute = null;
    }

    @Override
    public DescribedByConstraint copy() {
        DescribedByConstraint c = new DescribedByConstraint();
        c.setAttribute(this.attribute.copy());
        c.setLevel(this.attribute.copy());
        c.setDimension(this.dimension.copy());
        return c;
    }

    public Target getDimension() {
        return dimension;
    }

    public void setDimension(Target dimension) {
        this.dimension = dimension;
    }

    public Target getLevel() {
        return level;
    }

    public void setLevel(Target level) {
        this.level = level;
    }

    public Target getAttribute() {
        return attribute;
    }

    public void setAttribute(Target attribute) {
        this.attribute = attribute;
    }

    @Override
    public String toString() {
        return this.dimension + "." + this.level + " DESCRIBED_BY " + this.dimension + "." + this.attribute;
    }
}
