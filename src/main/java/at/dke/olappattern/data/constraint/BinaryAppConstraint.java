package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;

public class BinaryAppConstraint extends Constraint {
    private Target term = null;
    private Target entity1 = null;
    private  Target entity2 = null;

    public BinaryAppConstraint() {
        super();
        this.setConstraintType("BINARY_APPLICABILITY_CONSTRAINT");
    }

    @Override
    public void removeObjects() {
        this.term = null;
        this.entity1 = null;
        this.entity2 = null;
    }

    @Override
    public BinaryAppConstraint copy() {
        BinaryAppConstraint c = new BinaryAppConstraint();
        c.setTerm(this.term.copy());
        c.setEntity1(this.entity1.copy());
        c.setEntity2(this.entity2.copy());
        return c;
    }

    public Target getTerm() {
        return term;
    }

    public void setTerm(Target term) {
        this.term = term;
    }

    public Target getEntity1() {
        return entity1;
    }

    public void setEntity1(Target entity1) {
        this.entity1 = entity1;
    }

    public Target getEntity2() {
        return entity2;
    }

    public void setEntity2(Target entity2) {
        this.entity2 = entity2;
    }

    @Override
    public String toString() {
        return term + " IS_APPLICABLE_TO (" + entity1 + ", " + entity2 + ")";
    }
}
