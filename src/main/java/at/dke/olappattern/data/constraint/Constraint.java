package at.dke.olappattern.data.constraint;

/**
 * A Constraint that restricts pattern variables
 */
public abstract class Constraint {
    private int id;
    private String constraintType;

    public Constraint(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConstraintType() {
        return constraintType;
    }

    public void setConstraintType(String constraintType) {
        this.constraintType = constraintType;
    }

    /**
     * Removes all object properties of the Assertions and sets them to null
     */
    public abstract void removeObjects();

    /**
     * Generates a copy of this Constraint
     * @return a new Constraint object identical to this one
     */
    public abstract Constraint copy();
}
