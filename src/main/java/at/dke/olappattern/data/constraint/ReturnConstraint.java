package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;


public class ReturnConstraint extends Constraint {
    private Target entity = null;
    private Target returnType = null;

    public ReturnConstraint() {
        super();
        this.setConstraintType("RETURN_CONSTRAINT");
    }

    public Target getEntity() {
        return entity;
    }

    public void setEntity(Target entity) {
        this.entity = entity;
    }

    public Target getReturnType() {
        return returnType;
    }

    public void setReturnType(Target returnType) {
        this.returnType = returnType;
    }

    @Override
    public void removeObjects() {
        this.entity = null;
        this.returnType = null;
    }

    @Override
    public ReturnConstraint copy() {
        ReturnConstraint c = new ReturnConstraint();
        c.setEntity(this.entity.copy());
        c.setReturnType(this.returnType.copy());
        return c;
    }

    @Override
    public String toString() {
        return this.entity + " RETURNS " + this.returnType;
    }
}
