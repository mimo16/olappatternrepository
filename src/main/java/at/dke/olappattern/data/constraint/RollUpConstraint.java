package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;

public class RollUpConstraint extends Constraint {
    private Target dimension;
    private Target childLevel;
    private Target parentLevel;

    public RollUpConstraint() {
        this.setConstraintType("ROLL_UP_CONSTRAINT");
    }

    @Override
    public void removeObjects() {
        this.dimension = null;
        this.childLevel = null;
        this.parentLevel = null;
    }

    @Override
    public RollUpConstraint copy() {
        RollUpConstraint c = new RollUpConstraint();
        c.setChildLevel(this.childLevel.copy());
        c.setParentLevel(this.parentLevel.copy());
        c.setDimension(this.dimension.copy());
        return c;
    }

    public Target getDimension() {
        return dimension;
    }

    public void setDimension(Target dimension) {
        this.dimension = dimension;
    }

    public Target getChildLevel() {
        return childLevel;
    }

    public void setChildLevel(Target childLevel) {
        this.childLevel = childLevel;
    }

    public Target getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Target parentLevel) {
        this.parentLevel = parentLevel;
    }

    @Override
    public String toString() {
        return this.dimension + "." + this.childLevel + " ROLLS_UP_TO " + this.dimension + "." + this.parentLevel;
    }
}
