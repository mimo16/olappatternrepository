package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;

public class DomainConstraint extends Constraint {
    private Target entity = null;
    private Target property = null;
    private Target domain = null;

    public DomainConstraint(Target entity, Target property, Target domain) {
        super();
        this.setConstraintType("DOMAIN_CONSTRAINT");
        this.entity = entity;
        this.property = property;
        this.domain = domain;
    }

    public DomainConstraint() {
        super();
        this.setConstraintType("DOMAIN_CONSTRAINT");
    }

    @Override
    public void removeObjects() {
        this.entity = null;
        this.property = null;
        this.domain = null;
    }

    @Override
    public DomainConstraint copy() {
        DomainConstraint c = new DomainConstraint();
        c.setDomain(this.domain.copy());
        c.setEntity(this.entity.copy());
        c.setProperty(this.property.copy());
        return c;
    }

    public Target getEntity() {
        return entity;
    }

    public void setEntity(Target entity) {
        this.entity = entity;
    }

    public Target getProperty() {
        return property;
    }

    public void setProperty(Target property) {
        this.property = property;
    }

    public Target getDomain() {
        return domain;
    }

    public void setDomain(Target domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return this.entity + "." + this.property + ":" + this.domain;
    }
}
