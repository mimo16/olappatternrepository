package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;

public class UnaryAppConstraint extends Constraint {
    private Target term = null;
    private Target entity = null;

    public UnaryAppConstraint() {
        super();
        this.setConstraintType("UNARY_APPLICABILITY_CONSTRAINT");
    }

    @Override
    public void removeObjects() {
        this.term = null;
        this.entity = null;
    }

    @Override
    public UnaryAppConstraint copy() {
        UnaryAppConstraint c = new UnaryAppConstraint();
        c.setTerm(this.term.copy());
        c.setEntity(this.entity.copy());
        return c;
    }

    public Target getTerm() {
        return term;
    }

    public void setTerm(Target term) {
        this.term = term;
    }

    public Target getEntity() {
        return entity;
    }

    public void setEntity(Target entity) {
        this.entity = entity;
    }

    @Override
    public String toString() {
        return term + " IS_APPLICABLE_TO " + entity;
    }
}
