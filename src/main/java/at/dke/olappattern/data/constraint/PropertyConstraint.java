package at.dke.olappattern.data.constraint;

import at.dke.olappattern.data.Target;

public class PropertyConstraint extends Constraint{
    private Target entity = null;
    private Target type = null;
    private Target property = null;

    public PropertyConstraint(Target entity, Target type, Target property) {
        super();
        this.setConstraintType("PROPERTY_CONSTRAINT");
        this.entity = entity;
        this.type = type;
        this.property = property;
    }

    public PropertyConstraint() {
        super();
        this.setConstraintType("PROPERTY_CONSTRAINT");
    }

    @Override
    public void removeObjects() {
        this.entity = null;
        this.type = null;
        this.property = null;
    }

    @Override
    public PropertyConstraint copy() {
        PropertyConstraint c = new PropertyConstraint();
        c.setType(this.type.copy());
        c.setEntity(this.entity.copy());
        c.setProperty(this.property.copy());
        return c;
    }

    public Target getEntity() {
        return entity;
    }

    public void setEntity(Target entity) {
        this.entity = entity;
    }

    public Target getType() {
        return type;
    }

    public void setType(Target type) {
        this.type = type;
    }

    public Target getProperty() {
        return property;
    }

    public void setProperty(Target property) {
        this.property = property;
    }

    @Override
    public String toString() {
        return this.entity + " HAS " + this.type + " " + this.property;
    }
}
