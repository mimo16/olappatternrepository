package at.dke.olappattern.data.MDM;

/**
 * A DimensionProperty is an abstraction for the properties of data cube dimensions
 */
public abstract class DimensionProperty extends MDMProperty{
    private ValueSet domain = null;

    public DimensionProperty(String name){
        super(name);
    }

    public DimensionProperty(){
        super();
    }

    public ValueSet getDomain() {
        return this.domain;
    }

    public void setDomain(ValueSet domain) {
        this.domain = domain;
    }
}
