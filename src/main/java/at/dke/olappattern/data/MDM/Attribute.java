package at.dke.olappattern.data.MDM;

/**
 * Representation of an attribute describing a level of a Dimension in a data warehouse.
 */
public class Attribute extends DimensionProperty {
    private int id;
    Level level = null;

    public Attribute(){
        super();
    }

    public Attribute(String name){
        super(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return this.getName() + ":" + this.getDomain();
    }
}
