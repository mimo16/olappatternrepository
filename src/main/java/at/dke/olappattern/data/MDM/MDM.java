package at.dke.olappattern.data.MDM;

import at.dke.olappattern.data.repository.OrganizationElement;

import java.util.HashSet;
import java.util.Set;

public class MDM extends OrganizationElement {
    private Set<Cube> cubes = null;
    private Set<Dimension> dimensions = null;
    private Set<ValueSet> valueSets = null;

    public MDM() {
        this.cubes = new HashSet<Cube>();
        this.dimensions = new HashSet<Dimension>();
        this.valueSets = new HashSet<ValueSet>();
    }

    public MDM(String name){
        this();
        this.setName(name);
    }

    public boolean addCube(Cube cube) {
        return cubes.add(cube);
    }

    public boolean addDimension(Dimension dimension) {
        return dimensions.add(dimension);
    }

    public boolean removeDimension(Object o) {
        return dimensions.remove(o);
    }

    public boolean removeCube(Object o) {
        return cubes.remove(o);
    }

    public Set<Cube> getCubes() {
        return cubes;
    }
    public void setCubes(Set<Cube> cubes) {
        this.cubes = cubes;
    }

    public Set<Dimension> getDimensions() {
        return dimensions;
    }

    public void setDimensions(Set<Dimension> dimensions) {
        this.dimensions = dimensions;
    }

    public Set<ValueSet> getValueSets() {
        return valueSets;
    }

    public void setValueSets(Set<ValueSet> valueSets) {
        this.valueSets = valueSets;
    }

    public boolean addValueSet(ValueSet valueSet) {
        return getValueSets().add(valueSet);
    }

    /**
     * Method to find the entities with the given name if it exists within the MDM
     * @param name the entity name to search for
     * @return The entity object if found, otherwise null
     */
    public MDMEntity getEntityByName(String name){

        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";

        Dimension d = getDimensionByName(name);
        if(d != null) return d;

        Cube c = getCubeByName(name);
        if(c != null) return c;

        return null;
    }

    /**
     * Method to find the dimension with the given name if it exists within the MDM
     * @param name the dimension name to search for
     * @return The dimension object if found, otherwise null
     */
    public Dimension getDimensionByName(String name){
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Dimension d: this.dimensions){
            if(d.getName().equals(name)){
                return d;
            }
        }
        return null;
    }

    /**
     * Method to find the cube with the given name if it exists within the MDM
     * @param name the cube name to search for
     * @return The cube object if found, otherwise null
     */
    public Cube getCubeByName(String name){
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Cube c : this.cubes){
            if(c.getName().equals(name)){
                return c;
            }
        }
        return null;
    }

    public Measure getMeasureByName(String name) {
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Cube c : this.cubes){
            for(Measure m : c.getMeasures())
                if(m.getName().equals(name)){
                    return m;
            }
        }
        return null;
    }

    public DimensionRole getDimensionRoleByName(String name) {
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Cube c : this.cubes){
            for(DimensionRole dr : c.getDimensionRoles())
                if(dr.getName().equals(name)){
                    return dr;
                }
        }
        return null;
    }

    public Level getLevelByName(String name) {
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Dimension d : this.dimensions){
            for(Level l : d.getLevels())
                if(l.getName().equals(name)){
                    return l;
                }
        }
        return null;
    }

    public Attribute getAttributeByName(String name) {
        if(!name.startsWith("\"") && !name.endsWith("\"")) name = "\"" + name + "\"";
        for(Dimension d : this.dimensions){
            for(Attribute a : d.getAttributes())
                if(a.getName().equals(name)){
                    return a;
                }
        }
        return null;
    }

    /**
     * Method to find the value set with the given name if it exists within the MDM
     * @param name the value set name to search for
     * @return The value set object if found, otherwise null
     */
    public ValueSet getValueSetByName(String name){
        for(ValueSet vs : this.valueSets){
            if(vs.getName().equals(name)){
                return vs;
            }
        }
        return null;
    }

    /**
     * Adds a ValueSet if none with the same name already exists
     * @param valueSet the ValueSet to be added
     * @return The ValueSet parameter if it was added, otherwise the already existing with the identical name
     */
    public ValueSet addValueSetIfNotExists(ValueSet valueSet){
        ValueSet vs = this.getValueSetByName(valueSet.getName());
        if(vs == null){
            this.addValueSet(valueSet);
            return valueSet;
        }
        else return vs;
    }

    public String generateCreateStatement(String repo){
        return "CREATE MULTIDIMENSIONAL_MODEL " + repo + "/" + this.getName() + ";";
    }

    public String printPath(){
        String str = "Multidimensional Model " + this.getName() + ":\r\n";
        str += "------------------------------------------------------\r\n";
        if(!this.cubes.isEmpty()){
            str += "Cubes: \r\n";
            for(Cube c : this.cubes){
                str += "\t" + c.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(!this.dimensions.isEmpty()){
            str += "Glossaries: \r\n";
            for(Dimension d : this.dimensions){
                str += "\t" + d.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(!this.valueSets.isEmpty()){
            str += "Catalogues: \r\n";
            for(ValueSet vs : this.valueSets){
                str += "\t" + vs.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(cubes.isEmpty() && dimensions.isEmpty() && valueSets.isEmpty()){
            str += "Empty\r\n";
            str += "------------------------------------------------------\r\n";
        }

        return str;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!this.getClass().equals(obj.getClass())) return false;

        MDM mdm2 = (MDM)obj;
        if(this.getId() == mdm2.getId() && this.getName().equals(mdm2.getName())){
            return true;
        }

        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        int tmp = 0;
        tmp = ( this.getId() + this.getName() ).hashCode();
        return tmp;
    }

    @Override
    public String toString() {
        String str = "MDM (ID: " + this.getId() + ", Name: " + this.getName() + ")\r\n";

        return str;
    }
}
