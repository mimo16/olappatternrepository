package at.dke.olappattern.data.MDM;

/**
 * A class representing the roll up relation between a parent and a child level
 */
public class RollUp {
    private int id;
    private Level childLevel = null;
    private Level parentLevel = null;

    public RollUp() {
    }

    public Level getChildLevel() {
        return childLevel;
    }

    public void setChildLevel(Level childLevel) {
        this.childLevel = childLevel;
    }

    public Level getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Level parentLevel) {
        this.parentLevel = parentLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return childLevel.getName() + " ROLLS_UP_TO " + parentLevel.getName();
    }
}
