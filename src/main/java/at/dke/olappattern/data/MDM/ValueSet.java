package at.dke.olappattern.data.MDM;

/**
 * A ValueSet defines domain restrictions for different MDMElements
 */
public abstract class ValueSet {
    private int id;
    private String name;

    public ValueSet(){}

    public ValueSet(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
