package at.dke.olappattern.data.MDM;

import java.util.HashSet;
import java.util.Set;

/**
 * A Cube is a class representing a data cube in the context of data warehousing consisting of multiple dimensions in
 * different roles and Measures.
 */
public class Cube extends MDMEntity{
    private int id;
    private Set<DimensionRole> dimensionRoles = null; //The DimensionRoles of the cube
    private Set<Measure> measures = null; //The Measures of the cube

    public Cube(){
        super();
        this.dimensionRoles = new HashSet<DimensionRole>();
        this.measures = new HashSet<Measure>();
    }

    public Cube(String name){
        super(name);
        this.dimensionRoles = new HashSet<DimensionRole>();
        this.measures = new HashSet<Measure>();
    }

    public boolean addDimensionRole(DimensionRole dimensionRole){
        return this.dimensionRoles.add(dimensionRole);
    }

    public boolean addMeasure(Measure measure) {
        if(measure != null){ return this.measures.add(measure); }
        return false;
    }

    public DimensionRole getDimensionRoleByName(String name) {

        for(DimensionRole dr : dimensionRoles)
            if(dr.getName().equals(name))
                return dr;

        return null;
    }

    public Measure getMeasureByName(String name) {

        for(Measure m : measures)
            if(m.getName().equals(name))
                return m;

        return null;
    }

    public Set<DimensionRole> getDimensionRoles() { return dimensionRoles; }

    public Set<Measure> getMeasures() { return measures; }

    public void setDimensionRoles(Set<DimensionRole> dimensionRoles) {
        this.dimensionRoles = dimensionRoles;
    }

    public void setMeasures(Set<Measure> measures) {
        this.measures = measures;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String str = "CREATE OR REPLACE CUBE " + this.getName() + " WITH\r\n";

        if(!this.measures.isEmpty()){
            str += "\tMEASURE PROPERTIES\r\n";
            for(Measure m : this.measures){
                str += "\t\t" + m + ";\r\n";
            }
            str += "\tEND MEASURE PROPERTIES;\r\n";
        }

        Set<DimensionRole> roles;
        if(!this.dimensionRoles.isEmpty()){
            str += "\tDIMENSION_ROLE PROPERTIES\r\n";
            for(DimensionRole dr : this.dimensionRoles){
                str += "\t\t" + dr + ";\r\n";
            }
            str += "\tEND DIMENSION_ROLE PROPERTIES;\r\n";
        }
        str += "END CUBE;";

        return str;
    }

    public String generateCreateStatement(String repository, String mdm){
        String str = "CREATE OR REPLACE CUBE " + repository + "/" + mdm + "/" + this.getName() + " WITH\r\n";

        if(!this.measures.isEmpty()){
            str += "\tMEASURE PROPERTIES\r\n";
            for(Measure m : this.measures){
                str += "\t\t" + m + ";\r\n";
            }
            str += "\tEND MEASURE PROPERTIES;\r\n";
        }

        Set<DimensionRole> roles;
        if(!this.dimensionRoles.isEmpty()){
            str += "\tDIMENSION_ROLE PROPERTIES\r\n";
            for(DimensionRole dr : this.dimensionRoles){
                str += "\t\t" + dr + ";\r\n";
            }
            str += "\tEND DIMENSION_ROLE PROPERTIES;\r\n";
        }
        str += "END CUBE;";

        return str;
    }
}
