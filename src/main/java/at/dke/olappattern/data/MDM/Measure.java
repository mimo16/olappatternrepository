package at.dke.olappattern.data.MDM;

import java.io.Serializable;

/**
 * A Measure is a data element that appears within a data warehouse Cube. It is defined by a name and a domain.
 */
public class Measure extends CubeProperty implements Serializable {
    private int id;
    private ValueSet domain = null; //The domain restriction for the measure

    public Measure(){
        super();
    }

    /**
     * Getter for the Measure's domain
     * @return The ValueSet representing the domain
     */
    public ValueSet getDomain() {
        return domain;
    }

    /**
     * Method to set the domain of the measure
     * @param domain The ValueSet representing the domain
     */
    public void setDomain(ValueSet domain) {
        this.domain = domain;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.getName() + ":" + this.getDomain();
    }
}
