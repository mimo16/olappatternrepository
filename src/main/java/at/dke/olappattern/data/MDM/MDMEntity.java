package at.dke.olappattern.data.MDM;

/**
 * An abstract type representing entities of a multidimensional data model, which is identified by its name
 */
public abstract class MDMEntity extends MDMElement {
    private String name;

    public MDMEntity(){}

    public MDMEntity(String name){
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
