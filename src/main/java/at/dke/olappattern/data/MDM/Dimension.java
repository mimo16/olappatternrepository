package at.dke.olappattern.data.MDM;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A Dimension of a multi-dimensional Cube in the context of data warehousing consisting of multiple Levels,
 * Attributes and their relations
 */
public class Dimension extends MDMEntity {
    private int id;
    private Set<Level> levels = null;
    private Set<RollUp> rollUps = null;
    private Set<Attribute> attributes = null;
    private Map<Level, Attribute> describedByRelations = null;

    public Dimension(){
        super();
        this.levels = new HashSet<Level>();
        this.rollUps = new HashSet<RollUp>();
        this.attributes = new HashSet<Attribute>();
        this.describedByRelations = new HashMap<>();
    }

    public Dimension(String name){
        this();
        this.setName(name);
    }

    public Set<Level> getLevels() {
        return levels;
    }

    public boolean addLevel(Level level){
        return this.levels.add(level);
    }

    public void setLevels(Set<Level> levels) {
        this.levels = levels;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<RollUp> getRollUps() {
        return rollUps;
    }

    public void setRollUps(Set<RollUp> rollUps) {
        this.rollUps = rollUps;
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }

    public boolean addRollUp(RollUp rollUp) {
        return getRollUps().add(rollUp);
    }

    public boolean addAttribute(Attribute attribute){
        if(attribute != null && attribute.getLevel() != null){
            this.addDescribedByRelation(attribute, attribute.getLevel());
        }
        return attributes.add(attribute);
    }

    public Attribute getAttributeByName(String name){
        for(Attribute a : this.attributes){
            if(a.getName().equals(name)){
                return a;
            }
        }
        return null;
    }

    public boolean addDescribedByRelation(Attribute attribute, Level level){
        if(attribute != null && level != null) {
            this.describedByRelations.put(level, attribute);
            return true;
        }
        return false;
    }

    /**
     * Method to get the Level object with the given name
     * @param name The name of the Level to be returned
     * @return The Level object
     */
    public Level getLevelByName(String name){
        if(name != null) {
            for(Level l : this.levels){
                if(l.getName().equals(name)) return l;
            }
        }
        return null;
    }

    /**
     * Method to find the base level of the domain, i.e. the level not occuring as parent level in any
     * roll up relation
     * @return the name of the base level
     */
    public String getBaseLevelName(){
        boolean isBaseLevel = false;
        for(RollUp ru : rollUps){
            isBaseLevel = true;
            for(RollUp other : rollUps){
                if(ru.getChildLevel().getName().equals(other.getParentLevel().getName())){
                    isBaseLevel = false;
                    break;
                }
            }
            if(isBaseLevel) return ru.getChildLevel().getName();
        }
        return null;
    }

    @Override
    public String toString() {
        String str = "CREATE OR REPLACE DIMENSION " + this.getName() + " WITH\r\n";

        if(!this.levels.isEmpty()){
            str += "\tLEVEL PROPERTIES\r\n";

            for(Level l : this.levels){
                str += "\t\t" + l + ";\r\n";
            }

            str += "\tEND LEVEL PROPERTIES;\r\n \r\n";
        }

        if(!this.attributes.isEmpty()){
            str += "\tATTRIBUTE PROPERTIES\r\n";

            for(Attribute a : this.attributes){
                str += "\t\t" + a + ";\r\n";
            }

            str += "\tEND ATTRIBUTE PROPERTIES;\r\n \r\n";
        }

        if(!this.rollUps.isEmpty() || !this.describedByRelations.isEmpty()){
            str += "\tCONSTRAINTS\r\n";

            for(RollUp r : this.rollUps){
                str += "\t\t" + r + ";\r\n";
            }
            for(Map.Entry<Level, Attribute> e : this.describedByRelations.entrySet()){
                str += "\t\t" + e.getKey().getName() + " DESCRIBED_BY " + e.getValue().getName() + ";\r\n";
            }

            str += "\tEND CONSTRAINTS\r\n";
        }

        str += "END DIMENSION;";
        return str;
    }

    public String generateCreateStatement(String repository, String mdm) {
        String str = "CREATE OR REPLACE DIMENSION " + repository + "/" + mdm + "/" + this.getName() + " WITH\r\n";

        if(!this.levels.isEmpty()){
            str += "\tLEVEL PROPERTIES\r\n";

            for(Level l : this.levels){
                str += "\t\t" + l + ";\r\n";
            }

            str += "\tEND LEVEL PROPERTIES;\r\n \r\n";
        }

        if(!this.attributes.isEmpty()){
            str += "\tATTRIBUTE PROPERTIES\r\n";

            for(Attribute a : this.attributes){
                str += "\t\t" + a + ";\r\n";
            }

            str += "\tEND ATTRIBUTE PROPERTIES;\r\n \r\n";
        }

        if(!this.rollUps.isEmpty() || !this.describedByRelations.isEmpty()){
            str += "\tCONSTRAINTS\r\n";

            for(RollUp r : this.rollUps){
                str += "\t\t" + r + ";\r\n";
            }
            for(Map.Entry<Level, Attribute> e : this.describedByRelations.entrySet()){
                str += "\t\t" + e.getKey().getName() + " DESCRIBED_BY " + e.getValue().getName() + ";\r\n";
            }

            str += "\tEND CONSTRAINTS\r\n";
        }

        str += "END DIMENSION;";
        return str;
    }
}
