package at.dke.olappattern.data.MDM;

/**
 * An abstraction for properties within a multidimensional data model with a specific name
 */
public abstract class MDMProperty extends MDMElement{
    private String name = null;

    public MDMProperty(){
        super();
    }

    public MDMProperty(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
