package at.dke.olappattern.data.MDM;

import java.io.Serializable;

/**
 * A Level of a Dimension in a Cube. Potentially, it can have multiple attributes and defined roll-up Levels
 */
public class Level extends DimensionProperty implements Serializable {
    private int id;

    public Level(){
        super();
    }

    public Level(String name){
        super(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.getName() + ":" + this.getDomain();
    }
}
