package at.dke.olappattern.data.MDM;

import java.io.Serializable;

/**
 * Represents a role a Dimension can possibly have in a data cube. One Dimension may therefore appear in different roles
 * within one and the same Cube
 */
public class DimensionRole extends CubeProperty implements Serializable {
    private int id;
    private Dimension dimension = null; //The dimension appearing in a certain role

    public DimensionRole(){
        super();
    }

    public DimensionRole(String name) {
        super(name);
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.getName() + ":" + this.getDimension().getName();
    }
}
