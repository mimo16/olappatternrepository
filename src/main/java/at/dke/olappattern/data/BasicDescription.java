package at.dke.olappattern.data;

import java.util.HashSet;
import java.util.Set;

/**
 * Abstract superclass aggregating the fields that are similar for both term and pattern descriptions
 */
public abstract class BasicDescription {
    private int id;
    //private String name = null;
    private String language = null;
    private Set<String> aliases = null;

    public BasicDescription() {
        this.aliases = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Set<String> getAliases() {
        return this.aliases;
    }

    public boolean addAlias(String alias) {
        return aliases.add(alias);
    }

    public void setAliases(Set<String> aliases) {
        this.aliases = aliases;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof BasicDescription)) return false;

        final BasicDescription o = (BasicDescription)obj;

        //if(this.name != null && !this.name.equals(o.getName())) return false;
        //if(this.name == null && o.getName() != null) return false;
        if(this.language != null && !this.language.equals(o.getLanguage())) return false;
        if(this.language == null && o.getLanguage() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
//        if(this.name != null) result += name.hashCode();
        if(this.language != null) result += language.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String str = "";

        str += "LANGUAGE = " + this.getLanguage() + ";\r\n";

        return str;
    }
}
