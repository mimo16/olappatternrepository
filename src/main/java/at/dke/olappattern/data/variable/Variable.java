package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

/**
 * Abstract super class for all kinds of variables that can be part of a pattern or term definition
 */
public abstract class Variable extends Target {
    private String name = null;
    private int position;
    private String variableRole = null;
    private String valueKind = null;
    private char optional;

    public Variable() {}

    public Variable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getVariableRole() {
        return variableRole;
    }

    public void setVariableRole(String variableRole) {
        this.variableRole = variableRole;
    }

    public String getValueKind() {
        return valueKind;
    }

    public void setValueKind(String valueKind) {
        this.valueKind = valueKind;
    }

    public char getOptional() {
        return optional;
    }

    public void setOptional(char optional) {
        this.optional = optional;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Variable)) return false;

        final Variable o = (Variable)obj;

        if(this.name != null && !this.name.equals(o.getName())) return false;
        if(this.name == null && o.getName() != null) return false;

        if(this.valueKind != null && !this.valueKind.equals(o.getName())) return false;
        if(this.valueKind == null && o.getValueKind() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.name != null) result += name.hashCode();
        return result;
    }

    /**
     * Creates a copy of this variable
     * @return The copy
     */
    public abstract Variable copy();

    public abstract String getDefinition();
}
