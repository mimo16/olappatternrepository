package at.dke.olappattern.data.variable;

/**
 * A class representing a rule, allowing to derive a variable from other parameters within a pattern
 */
public abstract class DerivationRule {
    private int id;
    private Variable variable = null;

    public DerivationRule(){ }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    /**
     * Copies this DerivationRule and returns an identical one
     * @return The copy
     */
    public abstract DerivationRule copy();
}
