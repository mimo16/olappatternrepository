package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

/**
 * A class representing the access to a specific entry in tuple value of a Variable
 * The columnIndex determines the entries the VariableAccess refers to
 */
public abstract class VariableAccess extends Target {
    private Integer columnIndex;

    public VariableAccess() {
    }

    public Integer getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(Integer columnIndex) {
        this.columnIndex = columnIndex;
    }

    public abstract Variable getVariable();

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof VariableAccess)) return false;

        final VariableAccess o = (VariableAccess)obj;

        if(!(this.columnIndex == o.getColumnIndex())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.columnIndex != null) result += columnIndex.hashCode();
        return result;
    }


}
