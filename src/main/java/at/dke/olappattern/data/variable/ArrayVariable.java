package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.value.ArrayVariableEntry;

import java.util.HashSet;
import java.util.Set;

/*
A variable of type array, i.e. potentially including multiple values that can be indexed by a number
 */
public class ArrayVariable extends Variable {
    private Set<ArrayVariableEntry> arrayVariableEntries = null;
    private int entryIndex = 1;

    public ArrayVariable(){
        super();
        this.setKind("ARRAY_VARIABLE");
        this.arrayVariableEntries = new HashSet<ArrayVariableEntry>();
    }

    /**
     * Adds an entry to the array
     * @param arrayVariableEntry the entry to be added
     * @return true if added, otherwise false
     */
    public boolean addArrayVariableEntry(ArrayVariableEntry arrayVariableEntry) {
        arrayVariableEntry.setIndex(this.entryIndex);
        this.entryIndex+=1;
        return arrayVariableEntries.add(arrayVariableEntry);
    }

    public Set<ArrayVariableEntry> getArrayVariableEntries() {
        return this.arrayVariableEntries;
    }

    public void setArrayVariableEntries(Set<ArrayVariableEntry> setVariableEntries) {
        this.arrayVariableEntries = setVariableEntries;
    }

    /**
     * Method to remove all entries
     */
    public void removeObjects(){
        this.arrayVariableEntries.clear();
    }

    @Override
    public String toString() {
        return this.getDefinition();
    }

    @Override
    public ArrayVariable copy(){
        ArrayVariable v = new ArrayVariable();
        v.setOptional(this.getOptional());
        v.setPosition(this.getPosition());
        v.setName(this.getName());
        v.setValueKind(this.getValueKind());
        v.setVariableRole(this.getVariableRole());

        for(ArrayVariableEntry ave : this.arrayVariableEntries){
            v.addArrayVariableEntry(ave.copy());
        }
        return v;
    }

    @Override
    public String getDefinition() {
        String str = "<" + this.getName() + ">";
        if(this.getValueKind().equals("TUPLE")) str += "()";
        str += "[]";
        return str;
    }
}
