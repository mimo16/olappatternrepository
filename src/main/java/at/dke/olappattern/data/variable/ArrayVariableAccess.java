package at.dke.olappattern.data.variable;

/**
 * A class representing the access to a specific value in an ArrayVariable by row index
 * In case of TupleValues additionally a column index is possible
 */
public class ArrayVariableAccess extends VariableAccess {
    private Integer rowIndex;
    private ArrayVariable variable = null;

    public ArrayVariableAccess() {
        this.setKind("ARRAY_VARIABLE_ACCESS");;
    }

    @Override
    public ArrayVariableAccess copy() {
        ArrayVariableAccess ava = new ArrayVariableAccess();
        ava.setVariable(this.variable.copy());
        ava.setColumnIndex(this.getColumnIndex());
        ava.setRowIndex(this.rowIndex);
        return ava;
    }

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    public ArrayVariable getVariable() {
        return variable;
    }

    public void setVariable(ArrayVariable variable) {
        this.variable = variable;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof ArrayVariableAccess)) return false;

        final ArrayVariableAccess o = (ArrayVariableAccess)obj;

        if(super.equals(obj)){
            if(!(rowIndex == o.getRowIndex())) return false;
            if(this.variable != null && !this.variable.equals(o.getVariable())) return false;
            if(this.variable == null && o.getVariable() != null) return false;
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.rowIndex != null) result += rowIndex.hashCode();
        if(this.variable != null) result += variable.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String str = "<" + this.variable.getName() + ">";
        if(this.getVariable() != null && this.getVariable().getValueKind().equals("TUPLE")) str += "()";
        if(this.getColumnIndex() != null) str += ".[" + this.getColumnIndex() + "]";
        if(this.getRowIndex() != null) str += "[" + rowIndex + "]";
        return str;
    }
}
