package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

/**
 * A variable that is not a collection
 * SingleVariables can only have one value
 */
public class SingleVariable extends Variable {
    private Target value = null;

    public SingleVariable(){
        super();
        this.setKind("SINGLE_VARIABLE");
    }

    public Target getValue() {
        return value;
    }

    public void setValue(Target value) {
        this.value = value;
    }

    @Override
    public String toString() {
        if(this.value == null) {
            return this.getDefinition();
        }
        return this.value.toString();
    }

    @Override
    public SingleVariable copy(){
        SingleVariable v = new SingleVariable();
        v.setOptional(this.getOptional());
        v.setPosition(this.getPosition());
        v.setName(this.getName());
        v.setValueKind(this.getValueKind());
        v.setVariableRole(this.getVariableRole());
        if(this.value != null) v.setValue(this.getValue().copy());
        return v;
    }

    @Override
    public String getDefinition() {
        String str = "<" + this.getName() + ">";
        if (this.getValueKind().equals("TUPLE")) str += "()";
        return str;
    }
}
