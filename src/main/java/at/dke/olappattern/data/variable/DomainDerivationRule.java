package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

public class DomainDerivationRule extends DerivationRule{
    private Target entity = null;
    private Target property = null;
    private Target domain = null;

    public Target getEntity() {
        return entity;
    }

    public void setEntity(Target entity) {
        this.entity = entity;
    }

    public Target getProperty() {
        return property;
    }

    public void setProperty(Target property) {
        this.property = property;
    }

    public Target getDomain() {
        return domain;
    }

    public void setDomain(Target domain) {
        this.domain = domain;
    }

    @Override
    public DomainDerivationRule copy(){
        DomainDerivationRule dr = new DomainDerivationRule();
        dr.setVariable(this.getVariable().copy());
        dr.setDomain(this.domain.copy());
        dr.setEntity(this.entity.copy());
        dr.setProperty(this.property.copy());
        return dr;
    }

    @Override
    public String toString() {
        if(this.getVariable() == this.domain){
            return " <= " + this.entity + "." + this.property;
        }
        String str = "";
        Variable v = null;

        if(this.entity instanceof Variable){
            v = (Variable)this.entity;
            if(v.getKind().equals("SINGLE_VARIABLE")){
                v = null;
            }
        }
        if(v == null && this.property instanceof Variable){
            v = (Variable)this.property;
            if(v.getKind().equals("SINGLE_VARIABLE")){
                v = null;
            }
        }
        if(v == null && this.entity instanceof VariableAccess){
            v = ((VariableAccess) this.entity).getVariable();
            if(v.getKind().equals("SINGLE_VARIABLE")){
                v = null;
            }
        }

        if(v == null && this.property instanceof VariableAccess){
            v = ((VariableAccess) this.property).getVariable();
            if(v.getKind().equals("SINGLE_VARIABLE")){
                v = null;
            }
        }

        if(v != null){
            str += "FOR <x>";
            if(v.getValueKind().equals("TUPLE")) str += "()";
            str += " IN " + v + " WITH\r\n";
            String temp = "\t\t\t" + this.domain + " <= " + this.entity + "." + this.property;
            temp = temp.replace(v.getName(), "x");
            temp = temp.replace("*", "");
            temp = temp.replace("[]", "");
            str += temp;
        }

        return str;
    }
}
