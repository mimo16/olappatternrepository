package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

public class MapVariableAccess extends VariableAccess {
    private Target rowIndex = null;
    private MapVariable variable = null;

    public MapVariableAccess() {
        this.setKind("MAP_VARIABLE_ACCESS");
    }

    public Target getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Target rowIndex) {
        this.rowIndex = rowIndex;
    }

    public MapVariable getVariable() {
        return variable;
    }

    public void setVariable(MapVariable variable) {
        this.variable = variable;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof ArrayVariableAccess)) return false;

        final ArrayVariableAccess o = (ArrayVariableAccess)obj;

        if(super.equals(obj)){
            if(this.rowIndex != null && !this.rowIndex.equals(o.getRowIndex())) return false;
            if(this.variable == null && o.getRowIndex() != null) return false;
            if(this.variable != null && !this.variable.equals(o.getVariable())) return false;
            if(this.variable == null && o.getVariable() != null) return false;
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.rowIndex != null) result += rowIndex.hashCode();
        if(this.variable != null) result += variable.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String str = "<" + this.variable.getName() + ">*"; //todo * is not printed
        if(this.getVariable().getValueKind() != null && this.getVariable().getValueKind().equals("TUPLE")) str += "()";
        if(this.getColumnIndex() != null) str += "[" + this.getColumnIndex() + "]";
        if(this.getRowIndex() != null) str += "[" + rowIndex + "]";
        return str;
    }

    @Override
    public MapVariableAccess copy(){
        MapVariableAccess mva = new MapVariableAccess();
        mva.setColumnIndex(this.getColumnIndex());
        if(this.rowIndex != null) mva.setRowIndex(this.rowIndex.copy());
        mva.setVariable(this.variable.copy());
        return mva;
    }
}
