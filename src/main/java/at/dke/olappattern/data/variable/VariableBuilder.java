package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

/**
 * Helper class to generate a Variable within the processing of a command in the at.dke.olap_pattern_repo.LanguageProcessor
 */
public class VariableBuilder extends Target {
    private String name = null;
    private int position;
    private String variableRole = null;
    private String valueKind = null;
    private char optional;
    public boolean isIndexVariable = false;
    public boolean isElement = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getValueKind() {
        return valueKind;
    }

    public void setValueKind(String valueKind) {
        this.valueKind = valueKind;
    }

    public void setOptional(char optional) {
        this.optional = optional;
    }

    public void setVariableRole(String variableRole) {
        this.variableRole = variableRole;
    }

    public VariableBuilder(){
        name = null;
        position = -1;
        variableRole = null;
        this.setKind("SINGLE_VARIABLE");
        valueKind = "SINGLE";
        optional = 'N';
    }

    @Override
    public Target copy() {
        return null;
    }

    public SingleVariable buildSingleVariable(){
        SingleVariable v = new SingleVariable();
        v.setName(name);
        v.setPosition(position);
        v.setVariableRole(variableRole);
        v.setKind(this.getKind());
        v.setValueKind(valueKind);
        v.setOptional(optional);
        return v;
    }

    public ArrayVariable buildArrayVariable(){
        ArrayVariable v = new ArrayVariable();
        v.setName(name);
        v.setPosition(position);
        v.setVariableRole(variableRole);
        v.setKind(this.getKind());
        v.setValueKind(valueKind);
        v.setOptional(optional);
        return v;
    }

    public MapVariable buildMapVariable(){
        MapVariable v = new MapVariable();
        v.setName(name);
        v.setPosition(position);
        v.setVariableRole(variableRole);
        v.setKind(this.getKind());
        v.setValueKind(valueKind);
        v.setOptional(optional);
        return v;
    }

    public Variable buildVariable(){
        if(this.getKind().equals("ARRAY_VARIABLE")){
            return this.buildArrayVariable();
        }
        else if(this.getKind().equals("MAP_VARIABLE")){
            return this.buildMapVariable();
        }
        return this.buildSingleVariable();
    }
}
