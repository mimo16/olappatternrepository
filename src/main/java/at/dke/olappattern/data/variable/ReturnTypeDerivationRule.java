package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

public class ReturnTypeDerivationRule extends DerivationRule{
    private Target businessTerm;

    public Target getBusinessTerm() {
        return businessTerm;
    }

    public void setBusinessTerm(Target businessTerm) {
        this.businessTerm = businessTerm;
    }

    @Override
    public ReturnTypeDerivationRule copy() {
        ReturnTypeDerivationRule dr = new ReturnTypeDerivationRule();
        dr.setVariable(this.getVariable().copy());
        dr.setBusinessTerm(this.businessTerm.copy());
        return dr;
    }

    @Override
    public String toString() {
        return " <= " + this.businessTerm + ".RETURNS";
    }
}
