package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.Target;

/**
 * A class representing the access to a specific value in an SingleVariable by column index
 * Hence, only meaningful for TupleValues
 */
public class SingleVariableAccess extends VariableAccess {
    private SingleVariable variable = null;

    public SingleVariableAccess() {
        this.setKind("SINGLE_VARIABLE_ACCESS");
    }

    @Override
    public Target copy() {
        return null;
    }

    public SingleVariable getVariable() {
        return variable;
    }

    public void setVariable(SingleVariable variable) {
        this.variable = variable;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof SingleVariableAccess)) return false;

        final SingleVariableAccess o = (SingleVariableAccess)obj;

        if(super.equals(obj)){
            if(this.variable != null && !this.variable.equals(o.getVariable())) return false;
            if(this.variable == null && o.getVariable() != null) return false;
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode()*29;
        if(this.variable != null) result += variable.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String str = "<" + this.variable.getName() + ">";
        if(this.getVariable().getValueKind().equals("TUPLE")) str += "()";
        str += "[" + this.getColumnIndex() + "]";
        return str;
    }
}
