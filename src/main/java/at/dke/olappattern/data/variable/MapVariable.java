package at.dke.olappattern.data.variable;

import at.dke.olappattern.data.value.MapVariableEntry;

import java.util.HashSet;
import java.util.Set;

/*
A variable of type map storing key-value pairs in form of MapVariableEntries
 */
public class MapVariable extends Variable {
    private Set<MapVariableEntry> mapVariableEntries = null;
    private int entryIndex = 1;

    public MapVariable() {
        super();
        this.setKind("MAP_VARIABLE");
        this.mapVariableEntries = new HashSet<MapVariableEntry>();
    }

    public MapVariable(String name){
        this();
        this.setName(name);
    }

    /**
     * Adds an entry to the map
     * @param mapVariableEntry the entry to be added
     * @return true if added, otherwise false
     */
    public boolean addMapVariableEntry(MapVariableEntry mapVariableEntry) {
        mapVariableEntry.setIndex(entryIndex++);
        return mapVariableEntries.add(mapVariableEntry);
    }

    public Set<MapVariableEntry> getMapVariableEntries() {
        return mapVariableEntries;
    }

    public void setMapVariableEntries(Set<MapVariableEntry> arrayVariableEntries) {
        this.mapVariableEntries = arrayVariableEntries;
    }

    /**
     * Removes all entries from the map
     */
    public void removeObjects(){
        this.mapVariableEntries.clear();
    }

    @Override
    public String toString() {
        return this.getDefinition();
    }

    @Override
    public MapVariable copy(){
        MapVariable v = new MapVariable();
        v.setOptional(this.getOptional());
        v.setPosition(this.getPosition());
        v.setName(this.getName());
        v.setValueKind(this.getValueKind());
        v.setVariableRole(this.getVariableRole());

        for(MapVariableEntry ave : this.mapVariableEntries){
            this.addMapVariableEntry(ave.copy());
        }
        return v;
    }

    @Override
    public String getDefinition() {
        String str = "<" + this.getName() + ">";
        if(this.getValueKind().equals("TUPLE")) str += "()";
        str += "*";
        return str;
    }
}
