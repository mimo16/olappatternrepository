package at.dke.olappattern.data.term;

import at.dke.olappattern.data.Context;
import at.dke.olappattern.data.Template;

import java.util.HashSet;
import java.util.Set;

/**
 * A business term as introduced by the OLAP Pattern approach
 */
public class Term extends Context {
    private int id;
    private String name = null;
    private Type type = null;
    private String returnType = null;
    private Set<TermDescription> descriptions = null;

    public Term(){
        super();
        this.descriptions = new HashSet<>();
    }

    public Term(Type type) {
        this();
        this.type = type;
    }

    public Term(String name){
        this();
        this.name = name;
    }

    public boolean addDescription(TermDescription description){
        return this.descriptions.add(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public Set<TermDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Set<TermDescription> descriptions) {
        this.descriptions = descriptions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method to generate a string representation for the description with the given name
     * @param @param repo the name of the repository the term is contained in
     * @param glossary the name of the glossary the term is contained in
     * @return The string corresponding to the description's create statement
     */
    public String descriptionStatements(String repo, String glossary){
        String str = "";
        for(TermDescription td : this.descriptions){
            str += "CREATE OR REPLACE TERM DESCRIPTION FOR " + repo + "/" + glossary + "/" + this.name + " WITH\r\n";
            str += "\tLANGUAGE = " + td.getLanguage() + ";\r\n";
            if(!td.getAliases().isEmpty()){
                str += "\tALIAS = ";
                for(String ta : td.getAliases()){
                    str += ta + ", ";
                }
                str = str.substring(0, str.length()-2);
                str += ";\r\n";
            }
            str += "\tDESCRIPTION = " + td.getDescription() + ";\r\n";
            str += "END TERM DESCRIPTION; \r\n \r\n";
        }
        return str;
    }

    /**
     * Method to generate a string representation for all the term's templates
     * @param repo the name of the repository the term is contained in
     * @param glossary the name of the glossary the term is contained in
     * @return The string corresponding to the template's create statement
     */
    public String templateStatements(String repo, String glossary){
        String str = "";
        for(Template pt : this.getTemplates()){
            str += "CREATE OR REPLACE PATTERN TEMPLATE FOR " + repo + "/" + glossary + "/" + this.name + " WITH\r\n";
            if(pt.getDataModel() != null) str += "\tDATA_MODEL = " + pt.getDataModel() + ";\r\n";
            if(pt.getVariant() != null) str += "\tVARIANT = " + pt.getVariant() + ";\r\n";
            if(pt.getLanguage() != null) str += "\tLANGUAGE = " + pt.getLanguage() + ";\r\n";
            if(pt.getDialect() != null) str += "\tDIALECT = " + pt.getDialect() + ";\r\n";
            if(pt.getExpression() != null) str += "\tEXPRESSION = " + pt.getExpression() + ";\r\n";

            str += "END PATTERN TEMPLATE;  \r\n \r\n";
        }

        return str;
    }

    /**
     * Method generating a string representation of the term
     * The string includes the term name and the lists of available descriptions and templates
     * @return the string representation
     */
    public String printPath(){
        String str = "Term " + this.getName() + ":\r\n";
        /*str += "------------------------------------------------------\r\n";
        if(!this.descriptions.isEmpty()){
            str += "Descriptions: \r\n";
            for(TermDescription d : this.descriptions){
                str += "\t" + d.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(!this.getTemplates().isEmpty()){
            str += "Templates: \r\n";
            for(Template t : this.getTemplates()){
                str += "\t" + t.getName() + "\r\n";
            }
            str += "------------------------------------------------------\r\n";
        }
        if(descriptions.isEmpty() && this.getTemplates().isEmpty()){
            str += "Empty\r\n";
            str += "------------------------------------------------------\r\n";
        }*/

        return str;
    }

    @Override
    public String toString() {
        String str = "CREATE OR REPLACE " + this.type + " " + this.name + "WITH\r\n";
        str += super.toString();
        str += "END " + this.type + ";";

        return str;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Term)) return false;

        final Term o = (Term)obj;

        if(!this.name.equals(o.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    public String generateCreateStatement(String repository, String glossary){
        String str = "CREATE OR REPLACE " + this.type + " " + repository + "/" + glossary + "/" + this.name + " WITH\r\n";
        str += super.toString();

        str += "END PATTERN;";
        return str;
    }
}
