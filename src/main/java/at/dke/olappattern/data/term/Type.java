package at.dke.olappattern.data.term;

/**
 * The Type of a Term or Pattern
 */
public class Type {
    private Integer id;
    private String name = null;

    public Type(){}

    public Type(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
