package at.dke.olappattern.data.term;

import at.dke.olappattern.data.BasicDescription;

/**
 * A TermDescription represents a set of information concerning a Term. Therefore, it contains meta-information
 * (a name, language and alias) and the description itself
 */
public class TermDescription extends BasicDescription {
    private int id;
    private String description = null;

    public TermDescription() {
        super();

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    /*@Override
    public String toString() {
        String str = "CREATE OR REPLACE DESCRIPTION " + this.getName() + "\r\n";
        if(this.getLanguage() != null){ str += "Language: " + this.getLanguage() + "\r\n"; }
        if(this.getAliases() != null){ str += "Alias: " + this.getAliases().toString() + "\r\n"; }
        if(this.description != null){ str += "Description: " + this.description + "\r\n"; }

        return str;
    }*/
}
