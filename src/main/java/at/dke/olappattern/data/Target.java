package at.dke.olappattern.data;

/**
 * A Target for which Constraints and Assertions are formulated
 */
public abstract class Target {
    private Integer id;
    private String kind;

    public Target() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * Creates a copy of this Target and returns an identical one
     * @return The copied Target
     */
    public abstract Target copy();
}
